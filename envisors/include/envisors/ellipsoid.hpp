/*
 * ellipsoid.hpp
 *
 *  Created on: Nov 20, 2016
 *      Author: veronica
 */

#ifndef ENVISORSSYSTEM_envisors_INCLUDE_envisors_ELLIPSOID_HPP_
#define ENVISORSSYSTEM_envisors_INCLUDE_envisors_ELLIPSOID_HPP_

#include <stdio.h>
#include <sstream>
#include <vector>

//pcl
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/point_cloud.h>
#include <pcl/console/parse.h>
#include <pcl/common/transforms.h>
#include <pcl/visualization/common/shapes.h>
#include <pcl/visualization/common/actor_map.h>

//ros
#include <ros/ros.h>
#include <visualization_msgs/Marker.h>

#include "envisors/common.hpp"

typedef struct
{
	double x;
	double y;
	double z;

}Point3;

typedef struct
{
	double minX;
	double minY;
	double minZ;
	double maxX;
	double maxY;
	double maxZ;
	double x_axis;
	double y_axis;
	double z_axis;
	Point3 center;
	std::vector<Point3> points;

}BoundingBox;

typedef struct
{
	double x_axis;
	double y_axis;
	double z_axis;
	Point3 center;
	std::vector<Point3> points;

}EllipsoidData;


class Ellipsoid
{
public:
	//variables
	BoundingBox b_box;
	EllipsoidData ellipsoid;
	 ros::NodeHandle n;
	 ros::Publisher bounding_box_pub;
	 visualization_msgs::Marker bounding_box_ros;
	 pcl::PointCloud<pcl::PointXYZRGB> pc_t_;


	//methods
	Ellipsoid();
	void set_pointCloud(
			pcl::PointCloud<pcl::PointXYZRGB>& pc_t);
	void set_bounding_box();
	void create_bounding_box();
	void publish_bounding_box();
	void remove_noise();
	void compute_b_box_coordinates();
	void cluster_pc();
	void set_ellipsoid();
	float is_inside(float x, float y, float z);

};



#endif /* ENVISORSSYSTEM_envisors_INCLUDE_envisors_ELLIPSOID_HPP_ */
