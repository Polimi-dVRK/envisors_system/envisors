/**
 * @author 		Veronica Penza
 * @version 	1.0
 * @date 		9 Aug 2015
 * @brief 		3drecosntruction
 */

#ifndef __3DRECOSNTRUCTION_HPP_
#define __3DRECOSNTRUCTION_HPP_

//include
#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include <iostream>
#include <sensor_msgs/image_encodings.h>

//OpenCV
#include <opencv2/opencv.hpp>

class StereoReconstruction{

public:
	StereoReconstruction();

	void setParam(cv::Mat& Q);
	void process(
			const cv::Mat& disparity,
			cv::Mat& pointCloud);

private:
	cv::Mat Q;
	void compute3DReconstruction(
			const cv::Mat& disparity,
			cv::Mat& pointCloud);

};

#endif //__3DRECOSNTRUCTION_HPP_
