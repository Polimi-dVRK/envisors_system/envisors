#ifndef PCL2ROS
#define PCL2ROS

//PCL CONVERSION TO ROS
#include <ros/ros.h>
#include "std_msgs/Float32.h"
#include <pcl/conversions.h>
#include <pcl/PCLHeader.h>
#include <std_msgs/Header.h>
#include <pcl/PCLImage.h>
#include <sensor_msgs/Image.h>
#include <pcl/PCLPointField.h>
#include <sensor_msgs/PointField.h>
#include <pcl/PCLPointCloud2.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl/io/pcd_io.h>
#include <pcl/people/height_map_2d.h>


namespace pcl2ros{

void toROSMsg(const pcl::PointCloud<pcl::PointXYZRGB> &pcl_cloud, sensor_msgs::PointCloud2 &cloud);

void moveFromPCL(pcl::PCLPointCloud2 &pcl_pc2, sensor_msgs::PointCloud2 &pc2);

void copyPCLPointCloud2MetaData(const pcl::PCLPointCloud2 &pcl_pc2, sensor_msgs::PointCloud2 &pc2);

void fromPCL_header(const pcl::PCLHeader &pcl_header, std_msgs::Header &header);

void fromPCL_field(const std::vector<pcl::PCLPointField> &pcl_pfs, std::vector<sensor_msgs::PointField> &pfs);

void fromPCL_in(const pcl::PCLPointField &pcl_pf, sensor_msgs::PointField &pf);

}//namespace



#endif
