/*
 * define.hpp
 *
 *  Created on: Nov 19, 2016
 *      Author: veronica
 */

#ifndef ENVISORSSYSTEM_ENVISORSLIB_INCLUDE_ENVISORSLIB_DEFINE_HPP_
#define ENVISORSSYSTEM_ENVISORSLIB_INCLUDE_ENVISORSLIB_DEFINE_HPP_

// define this symbol if you want to show the computational time
//#define COMPUTATIONAL_TIME

// define this symbol if you want to activate visual debug
#define VISUALIZATION

// define this symbol if you want to activate verbose debug
//#define VERBOSE

// define this symbol if you want to activate multithread support
#define MULTITHREAD

#define N_THREADS 8

#define MYPI 3.14159265


enum{MISUMI = 0,
	DAVINCI = 1,
	PROSILICA = 2,
	UEYE = 3};

enum{
	UR5 = 0,
	LWR4 = 1,
	PHANTOM = 2,
	DAVINCI_ARMS = 3,
	NO_ROBOT = 4};

enum{OPTITRACK = 0,
	VICRA = 1,
	VIRTUOSE = 2,
    DAVINCI_ECM = 3};

#define LABEL_NULL	1000

//#define EVALUATION_TRACKING

#endif /* ENVISORSSYSTEM_ENVISORSLIB_INCLUDE_ENVISORSLIB_DEFINE_HPP_ */
