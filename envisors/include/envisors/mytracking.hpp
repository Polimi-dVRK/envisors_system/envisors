/**
 * @author 		Veronica Penza
 * @version 	1.0
 * @date 		22 Gen 2016
 * @brief 		mytracking */

#define DEBUG

#ifndef _MYTRACKING_HPP_
#define _MYTRACKING_HPP_

//include
#include <stdio.h>
#include <stdlib.h>
#include <list>
#include <sstream>
#include <iostream>
#include <math.h>
#include <sensor_msgs/image_encodings.h>
#include <cfloat>


//OpenCV
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/features2d/features2d.hpp>

#include <hmatching/HLearningModule.hpp>
#include <hmatching/HMatchingModule.hpp>

#include "envisors/common.hpp"
#include "envisors/pnmio.h"
#include "envisors/klt.h"
#include "envisors/segmentation/seg_model.h"
#include "envisors/bayesian_network/bayesian_network.hpp"

#define VERBOSE

struct BRISKParam{
	int no_param;
	//	std::vector<float>& radiusList; // radii (in pixels) where the samples around a keypoint are taken
	//	std::vector<int>& numberList; // defines the number of sampling points on the sampling circle. == radii
	//	float dMax; // threshold for the short pairings used for descriptor formation
	//	float dMin; // threshold for the long pairings used for orientation determination
	//	std::vector<int> indexChange;// index remapping of the bits
};

struct FASTParam{
	int threshold; //threshold on difference between intensity of the central pixel and pixels of a circle around this pixel
	bool nonmaxSuppression; // true, non-maximum suppression is applied to detected corners
};

struct ORBParam{
	int nfeatures; // The maximum number of features to retain
	float scaleFactor; // Pyramid decimation ratio
	int nlevels; // The number of pyramid levels
	int edgeThreshold; //This is size of the border where the features are not detected
	int firstLevel;
	int WTA_K;
	int scoreType;
	int patchSize;
};

struct SHI_TOMASIOpenCVParam{
	int maxCorners; // Maximum number of corners to return
	double qualityLevel; // minimal accepted quality of image corners
	double minDistance; // Minimum possible Euclidean distance between the returned corners
	int blockSize; // Size of an average block for computing a derivative covariation matrix over each pixel neighborhood
	bool useHarrisDetector; // Parameter indicating whether to use a Harris detector
	double k; // Free parameter of the Harris detector.
};

struct STARParam{
	int maxSize; // maximum size of the features
	int responseThreshold; // threshold for the approximated laplacian, used to eliminate weak features
	int lineThresholdProjected; // another threshold for the laplacian to eliminate edges
	int lineThresholdBinarized; // yet another threshold for the feature size to eliminate edges
	int suppressNonmaxSize;
};

union DetectorParameters
{
	BRISKParam briskParam;
	FASTParam fastParam;
	ORBParam orbParam;
	SHI_TOMASIOpenCVParam shitomasiParam;
	STARParam starParam;
};

struct KLT_params{

	//detection
	int nFeature;

	//tracking
	int paramTracking_mindist ;
	int paramTracking_window_width;
	int paramTracking_window_height;
	bool paramTracking_smoothBeforeSelecting;
	bool paramTracking_lighting_insensitive;
	int  paramTracking_min_eigenvalue;
	bool paramTracking_affineConsistencyCheck;
	int paramTracking_affine_window_width;
	int paramTracking_affine_window_height;
	double paramTracking_affine_max_residue;
};

struct TrackingParameters
{
	// opencv tracking
	SHI_TOMASIOpenCVParam GFTT_params;
	cv::Size winsize_opt_flow;

	// KLT librarty tracking
	KLT_params klt_params;
	// bayesian network
	BNet_params bnet_params;
	double th_failure;
	//object segmentation
	double th_segmentation;

	//boolean
	bool opencv_tracking;


	float th_redetection;
};

struct ModelInfo
{
	std::vector<cv::Point2f> SA;
	int nTimesUsed;
	int nTimesNotUsed;
	double BAdistance;
	double weight;
};


class MyTracking{

public:

	//variables
	int n_features_detected;
	// Variables
	cv::Mat descriptor_prev;
	cv::Mat descriptor;
	bool isTrackingRefinement;
	bool KLT;
	bool KLT_opencv;
	segModel SegModel;
	TrackingParameters TrackingParam;
	int n_cluster;

	MyTracking(bool KLT, bool KLT_openCV);
	~MyTracking();


	/*IT: Iterative/Intensity Detection Method*/
	// Lucas-Kanade-Thomasi Detection and tracking
	void setKLTParamDetector(
			const std::string& featureDetectorName,
			std::vector<cv::Point2f>* points_detection,
			cv::Size img_size);

	void setKLTParamTracking(cv::Size& winSize);

	void detectKLTFeatures(
			const cv::Mat& img,
			const cv::Mat& mask,
			std::vector<cv::Point2f>& detcted_points);

	void trackingKLTFeatures(
			const cv::Mat img_prev,
			const cv::Mat img_curr,
			cv::Mat mask,
			std::vector<cv::Point2f>& points_prev,
			std::vector<cv::Point2f>& points,
			std::vector<uchar>& status);

	void redetectKLTFeatures(
			const cv::Mat& img,
			const cv::Mat& mask,
			std::vector<cv::Point2f>& points,
			std::vector<uchar>& status);

	/*IT: OpenCV Implementation*/
	void setParamOpenCVDetector(
			const std::string& featureDetectorName,
			DetectorParameters& param);

	void setParamOpenCVTracking(cv::Size& winSize);

	void detectOpenCVFeatures(
			const cv::Mat& img,
			const cv::Mat& mask,
			std::vector<cv::Point2f>& points_detected);

	void trackingOpenCVFeatures(
			const cv::Mat img_prev,
			const cv::Mat img_curr,
			cv::Mat mask,
			cv::TermCriteria criteria,
			std::vector<cv::Point2f>& points_prev,
			std::vector<cv::Point2f>& points,
			std::vector<uchar>& status);

	/*TbD: Tracking by Detection Methods  */

	void setTbDParamDetector(
			const std::string& featureDetectorName,
			DetectorParameters& param,
			std::vector<cv::Point2f>* points_detection,
			cv::Size img_size);

	void setTbDParamDetectorInSafetyArea(
			const std::string& featureDetectorName,
			DetectorParameters& param,
			std::vector<cv::Point2f>* points_detection,
			cv::Size img_size);

	void setTbDParamTracking(cv::Size& winSize);

	void detectTbDFeatures(
			std::vector<cv::Point2f>& feature_detected,
			const cv::Mat& img,
			const cv::Mat& mask);

	void descriptTbDFeatures(
			std::vector<cv::Point2f>& feature_detected,
			cv::Mat& descriptor,
			const cv::Mat& img);

	void trackingTbDFeatures(
			const cv::Mat img_prev,
			const cv::Mat img_curr,
			std::vector<cv::Point2f>& points_prev,
			std::vector<cv::Point2f>& points,
			cv::Mat desc_prev,
			cv::Mat desc_curr,
			cv::Mat mask,
			cv::TermCriteria criteria,
			std::vector<uchar>& status);

	/////////////////////////////////////////////////////////////

	void findRightFeatures(
			std::vector<cv::Point2f> points_left,
			std::vector<uchar>& status_left,
			std::vector<cv::Point2f> points_right,
			std::vector<uchar>& status_right,
			cv::Mat disparityMap,
			bool draw_features,
			cv::Mat imgL,
			cv::Mat imgR);

	void drawSafetyArea(
			std::vector<cv::Point2f> safety_area,
			cv::Mat img);

	void defineSafetyArea(
			std::vector<cv::Point2f> safety_area_left,
			std::vector<cv::Point2f> safety_area_right,
			cv::Mat imgL,
			cv::Mat imgR,
			cv::Mat disparity);

	cv::Mat getImg_features();

	int getNumberFeaturesDetected();

	void ForwardBackwardError(
			// Inputs
			const std::vector<cv::Point2f>& fw_pointsLeft_start,
			const std::vector<cv::Point2f>& bk_pointsLeft_end,
			//		const std::vector<cv::Point2f>& fw_pointsRight_start,
			//		const std::vector<cv::Point2f>& bk_pointsRight_end,
			std::vector<uchar>& statusLeft_fw,
			std::vector<uchar>& statusLeft_bw,
			//		std::vector<uchar>& statusRight_fw,
			//		std::vector<uchar>& statusRight_bw,
			int& threshold,
			// Outputs
			float& percentage_tracked_pointsLeft,
			float& average_distance_error_pointsLeft,
			int& n_valid_feat_tracked_pointsLeft,
			//		float& percentage_tracked_pointsRight,
			//		float& average_distance_error_pointsRight,
			//		int& n_valid_feat_tracked_pointsRight,
			std::vector<float>& distance_errors_pointsLeft,
			//		std::vector<float>& distance_errors_pointsRight,
			std::vector<uchar>& status_distanceLeft /*,
		std::vector<uchar>& status_distanceRight*/);

	void computeSafetyAreaError(
			std::vector<cv::Point2f>& safetyContour_start,
			std::vector<cv::Point2f>& safetyContour_end,
			std::vector<float>& safetyContour_distances,
			float& safetyContourn_meanError,
			float& safetyContourn_medianError);

	void saveFeaturesTrackingEvaluation(
			const std::string& fileName,
			const std::string& datasetName,
			const int& typedetector,
			int totalFramesNumber,
			const int& numberFeaturesDetected,
			float& detectorExecutionTime,
			const int& numberFeaturesTracked_pointsLeft,
			const float& average_distance_error_pointsLeft,
			const float& percentage_tracked_features_pointsLeft,
			std::vector<cv::Point2f>& pointStartLeft,
			std::vector<cv::Point2f>& pointEndLeft,
			const int& numberFeaturesTracked_pointsRight,
			const float& average_distance_error_pointsRight,
			const float& percentage_tracked_features_pointsRight,
			std::vector<cv::Point2f>& pointStartRight,
			std::vector<cv::Point2f>& pointEndRight);


	// to be fixed
	void computeQualityEvaluation();

	void getFeaturesDistributionOnImage(
			const cv::Size& imgSize,
			const std::vector<cv::Point2f>& features,
			float& mean,
			float& dev_std);

	void getFeaturesDistributionOnArea(
			std::vector<cv::Point2f>& safetyContourn,
			cv::Size imgSize,
			const std::vector<cv::Point2f>& features,
			float& mean,
			float& dev_std);

	void getAreaFeatures(
			std::vector<cv::Point2f>& features,
			double& area,
			cv::Point2f& center,
			float& radius/*,
		float& nFeaturesOnArea*/);

	void getPercentageFeaturesOnArea(
			int& nValidFeaturesTracked,
			double area,
			float& percentageFeaturesOnArea);

	void getPercentageFeaturesAreaOnImage(
			double& area_features,
			double& safety_area,
			float& percentage);

	void myDrawMatches(
			const cv::Mat img_prev,
			const cv::Mat img_curr,
			const std::vector<cv::Point2f> points_prev,
			const std::vector<cv::Point2f> points,
			cv::Mat& out,
			bool KLT,
			bool HMatching,
			cv::Mat keyFrame,
			std::vector<cv::Point2f> points_keyFrame,
			std::vector<cv::Point2f> points_matched);

	void getOpticalFlowDistribution(
			std::vector<cv::Point2f>& points_pre,
			std::vector<cv::Point2f>& points_act,
			std::vector<uchar>& status_act,
			double& mean,
			double& std);


	double calcAnglesMean(std::vector<double>& angles_deg);

	double calcAnglesStddev(std::vector<double>& angles_deg);

	bool checkHomography(
			cv::Size& imgSize,
			cv::Mat& homography,
			std::vector<cv::Point2f>& points_pre_trasformed,
			std::vector<cv::Point2f>& points_trasformed);

	bool checkLKTTrackingFailure(
			std::vector<cv::Point2f>& points_in_area_pre,
			std::vector<cv::Point2f>& points_in_area_act,
			bool& homograpyValidity,
			float& mean_OF_prev,
			float& std_OF_prev,
			float& mean_OF_act,
			float& std_OF_act);

	float detectBlurredFrame(
			cv::Mat& img);

	double detectFramesSimilarity(
			cv::Mat& i1, cv::Mat& i2);

	void redetectKLT(
			const cv::Mat& imgL_rect,
			const cv::Mat& imgL_gray,
			cv::Mat& imgL_hsv,
			cv::Mat& imgL_features,
			cv::Mat& img_seg_prob,
			cv::Mat& SA_mask,
			std::vector<cv::Point2f>& points_detected_L,
			std::vector<cv::Point2f>& pointsMatched_act,
			std::vector<cv::Point2f>& points_tracked_L,
			std::vector<cv::Point2f>& safetyArea_pointsLeft,
			std::vector<uchar>& points_inSafetyArea_L,
			std::vector<uchar>& status_L,
			bool init,
			bool KLTTracker,
			bool HMatcher,
			int safetyArea_nfeat);

	void redetectOpenCVFeatures_SURFMask(
			const cv::Mat& imgL_rect,
			const cv::Mat& imgL_gray,
			cv::Mat& imgL_features,
			cv::Mat& img_seg_prob,
			cv::Mat& specularity_mask,
			std::vector<cv::Point2f>& points_detected_L,
			std::vector<cv::Point2f>& pointsMatched_act,
			std::vector<cv::Point2f>& points_tracked_L,
			std::vector<cv::Point2f>& safetyArea_pointsLeft,
			bool KLTTracker);

	void computeSegmentationModel(
			const cv::Mat& imgL_rect,
			cv::Mat& imgL_features,
			cv::Mat& imgL_hsv,
			cv::Mat& img_seg_prob,
			std::vector<cv::Point2f>& points_tracked,
			std::vector<uchar>& status_points_tracked,
			std::vector<cv::Point2f>& safetyArea_pointsLeft,
			bool& KLTTracker,
			bool& HMatcher,
			bool& init);

	void find_points_in_safety_area(
			const std::vector<cv::Point2f>& points,
			const std::vector<cv::Point2f>& safety_area_contourn,
			std::vector<uchar>& tracking_status,
			std::vector<uchar>& inside_area);

	void find_points_high_seg_prob(
			const cv::Mat imgL_rect,
			const std::vector<cv::Point2f>& points,
			const std::vector<cv::Point2f>& safety_area_contourn,
			std::vector<uchar>& tracking_status,
			std::vector<uchar>& high_seg_prob);

	void find_points_in_safety_area_high_seg_prob(
			const cv::Mat imgL_rect,
			const cv::Mat imgL_hsv,
			const std::vector<cv::Point2f>& points,
			const std::vector<cv::Point2f>& safety_area_contourn,
			std::vector<uchar>& tracking_status,
			std::vector<uchar>& inside_area,
			int& n_feat_inside,
			double& th_segmentation);

	void find_SAContourn_partialOcclusion(
			cv::Mat imgL_rect,
			cv::Mat imgFeat,
			cv::Mat img_hsv,
			std::vector<cv::Point2f>& contourn_points,
			std::vector<std::vector<cv::Point> >& new_contour);

	void check_failure_parameters(
			cv::Size& img_size,
			cv::Mat& homography,
			std::vector<cv::Point2f>& safety_area_contourn_pre,
			std::vector<cv::Point2f>& safety_area_contourn_curr,
			bool&  hom_validity,
			int& n_feat_pre,
			int& n_feat_act,
			double& perc_feat_lost,
			cv::Mat& img_rgb,
			cv::Mat& hist_key,
			cv::Mat& hist,
			int hbin,
			int sbin,
			float th_seg,
			double& bh_distance);

	void computeSAModel(
			HLearningModule &learning,
			cv::Mat &img_grey,
			std::vector<cv::Point2f> &SA,
			cv::Mat &SA_mask,
			std::vector<cv::KeyPoint> &kp_cv,
			std::string &modelName,
			ObjectProperties &obj_prop);

	void computeModel(
			HLearningModule &learning,
			cv::Mat &img_rgb,
			cv::Mat &img_gray,
			std::vector<cv::Point2f> &SA,
			cv::Mat &SA_mask,
			std::vector<cv::KeyPoint> &kp_cv,
			std::string &modelName,
			ObjectProperties &obj_prop,
			cv::Mat &hist,
			int hbin, int sbin, float th_seg);

	bool updateModel(
			HLearningModule &learning,
			cv::Mat &img_rgb,
			cv::Mat &img_gray,
			std::vector<cv::Point2f> &SA,
			cv::Mat &SA_mask,
			std::vector<cv::KeyPoint> &kp_cv,
			std::string &modelName,
			double& bh_distance,
			std::deque<ObjectProperties> &objList,
			std::deque<ModelInfo> &modelInfo,
			bool &replaced);

	void create_safety_area_mask(
			cv::Mat& img,
			std::vector<cv::Point2f>& safety_contourn,
			cv::Mat& specularity_mask,
			cv::Mat& safety_area_mask);

	bool add_model(
			std::deque<ObjectProperties> &objList,
			std::deque<ModelInfo> &modelInfo,
			ObjectProperties &obj_prop,
			ModelInfo &info,
			int size);

	void replace_model(
			std::deque<ObjectProperties> &objList,
			std::deque<ModelInfo> &modelInfo,
			ObjectProperties &obj_prop,
			ModelInfo &info,
			int pos);

	bool get_model(
			std::deque<ObjectProperties> &objList,
			std::deque<ModelInfo> &modelInfo,
			ModelInfo &info,
			std::string &name);

	void clear_models(
			std::deque<ObjectProperties> &objList,
			std::deque<ModelInfo> &modelInfo);

	void print_model(
			std::deque<ObjectProperties> &objList,
			std::deque<ModelInfo> &modelInfo);

	void update_weight(ModelInfo &info);

	void measure_bhattacharyya_distance(
			cv::Mat& img_rgb,
			cv::Mat& hist_key,
			cv::Mat& hist,
			std::vector<cv::Point2f>& SA,
			int hbin,
			int sbin,
			float th_seg,
			double& bh_distance);

protected:

	/*IT: Iterative/Intensity Detection Method*/
	// Lucas-Kanade-Thomasi Detection and tracking

	KLT_TrackingContext paramTracking;
	KLT_FeatureList featureList;
	int nFeatures;
	unsigned char* imgKLT;
	unsigned char* specularityMapKLT;

	void fromKLTImgToMatImg(
			const unsigned char* imgKLT,
			cv::Mat& imgMat,
			const int rows, const int cols);
	void fromMatImgToKLTImg(
			const cv::Mat imgMat,
			unsigned char* imgKLT);

	void fromLKTFeaturesToCVFeatures(
			const KLT_FeatureList featuresLKT,
			std::vector<cv::Point2f>& featuresCV);
	void fromCVFeaturesToLKTFeatures(
			const std::vector<cv::Point2f> featuresCV,
			KLT_FeatureList& featuresLKT);


	////////////////////////////////////////////

	DetectorParameters DetectorsParam;
	std::vector<cv::KeyPoint> detected_points;
	cv::Ptr<cv::FeatureDetector> detector;
	std::string featureDetectorName;
	std::vector<cv::Point2f>* points;
	std::vector<cv::Point2f>* points_detection;
	cv::Mat img_features;
	cv::Size winSize;
	int numberFeaturesDetected;



};

#endif //_TRACKING_HPP_
