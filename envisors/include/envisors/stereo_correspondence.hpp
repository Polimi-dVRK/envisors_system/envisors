/**
 * @author 		Veronica Penza
 * @version 	1.0
 * @date 		9 Aug 2015
 * @brief 		stereo correspondence
 */

#ifndef __STEREOCORRESPONDENCE_HPP_
#define __STEREOCORRESPONDENCE_HPP_


#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <sstream>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

//OpenCV
#include <cv_bridge/cv_bridge.h>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/videostab/inpainting.hpp>

//PCL
//#include <pcl_ros/point_cloud.h>
//#include <pcl/point_types.h>

#include <pthread.h>

#include "envisors/census_image.hpp"
#include "envisors/hamming_distance.hpp"
#include "envisors/sad_image.hpp"
#include "envisors/common.hpp"
#include "envisors/define.hpp"
#include "envisors/super_pixels.hpp"


#define USE_IMAGE_TRANSPORT_SUBSCRIBER_FILTER 1

#define BLOCK_MATCHING_SAD_OPENCV 1
#define BLOCK_MATCHING_CENSUS_JESUS -1



//typedef pcl::PointCloud<pcl::PointXYZRGB> PointCloud;

typedef struct {
	int row_min;
	int row_max;
	int col_min;
	int col_max;
	int lenght;

	float probability_segmentation;
	bool background_object;

	int n_valid_values;
	float median;
	float average;
	float plane[3];
	float quadric[6];

}SuperPixelInfo;

typedef struct
{
	void *stereoCorr;
	cv::Mat *img_grey;
	cv::Mat *mask_SA;
	cv::Rect *SA_boundary;
}ThreadCensusData;

typedef struct
{
	void *stereoCorr;
	cv::Mat *mask;
}ThreadDisparityData;

typedef struct
{
	void *stereoCorr;
	cv::Mat *dispL2R;
	cv::Mat *mask;
	cv::Mat *mask_SA;
	int rowMin;
	int rowMax;
}ThreadDispL2RData;

typedef struct
{
	void *stereoCorr;
	cv::Mat *dispR2L;
	cv::Mat *mask_SA;
	int rowMin;
	int rowMax;
}ThreadDispR2LData;

class StereoCorrespondence{

public:

	cv::Mat mask_outliers;

	bool setParamCENSUS(
			bool isSubSampling,
			bool isSpecularity,
			double saturation,
			double value,
			bool isSubPixelRefinement,
			bool isModifiedCensus,
			bool isAggregationCost,
			int minDisparities,
			int numberOfDisparities,
			int spurie_Th,
			int n_census,
			int m_census,
			int threshold_LRC,
			int m_aggCost,
			int n_aggCost);

	bool setParamSAD(
			bool isSubSampling,
			bool isEqualized,
			bool isSpecularity,
			double saturation,
			double value,
			bool isSubPixelRefinement,
			int minDisparities,
			int numberOfDisparities,
			int spurie_Th,
			int threshold_LRC,
			int m_aggCost,
			int n_aggCost);

	bool setParamPostProcessing(
			bool ispostProcessing,
			bool isSpeckleRemoval,
			bool isFillHole,
			bool isAVERAGEFiltering,
			bool isMEDIANFiltering,
			bool isPLANE_FITFiltering,
			bool isQUAD_FITFiltering,
			bool isRANSAC_FITFiltering,
			bool isPoissonSmoothing,
			bool isBackgroundRemoval,
			bool isRemove_outliers,
			bool isReplace_all_points,
			bool isRemove_inclined_plane,
			bool isBilateralFilter,
			double newVal_speckle,
			int maxSize_speckle,
			double maxDiff_speckle,
			int diamPixelWind,
			double sigmaParam,
			int n_rand,
			int iteration,
			int min_num_point,
			double RANSAC_threshold);

	bool processCENSUS(
			cv::Mat& imgL_grey,
			cv::Mat& imgR_grey,
			/*const cv::Mat& label,*/
			cv::Mat& mask_SA_left,
			cv::Mat& mask_SA_right,
			cv::Mat& mask_SA_right_to_left,
			cv::Rect SA_boundary,
			/*cv::Mat& disparity,*/
			cv::Mat& mask_specularity);

	bool postProcess(
			cv::Mat& label,
			cv::Mat& disparity,
			segModel* SegModel,
			cv::Mat& img_hsv,
			std::vector<cv::Point2i>& contours,
			cv::Rect& SA_rect);

	bool processSAD(
			cv::Mat& imgL_rect,
			cv::Mat& imgR_rect,
			const cv::Mat& label,
			cv::Mat& disparity);

	//	void postProcessFillHoles(
	//		cv::Mat& disparity,
	//		const cv::Mat& label);

	cv::Mat getImgLGray();

	cv::Mat getImgRGray();

	void getImgLCENSUS(cv::Mat& censusImgL);

	void getImgRCENSUS(cv::Mat& censusImgR);

	cv::Mat getMask_InvalidValues();

	//	cv::Mat getMask_Specularity();

	static void removeSpecularity(
			const cv::Mat& imgRBG, cv::Mat& mask,
			double saturation, double value);


	cv::Mat mask_disparity;

private:

	//variables
	cv::Mat imgL_gray;
	cv::Mat imgR_gray;

	cv::Mat dispL2R_census;
	cv::Mat dispR2L_census;
	cv::Mat dispL2R_SAD;
	cv::Mat dispR2L_SAD;
	cv::Mat disp_LRC;
	cv::Mat disp;
	cv::Size imageSize;
	//	cv::Mat mask_specularity;
	std::vector<SuperPixelInfo> super_pixel_info;
	cv::Size subSize;
	//	cv::Mat imgR_rect_original;
	//	cv::Mat imgL_rect_original;

	//common census sad
	bool isSpecularity;
	bool isSubPixelRefinement;
	int minDisparities;
	int numberOfDisparities;
	int spurie_Th;
	int m_aggCost;
	int n_aggCost;
	double saturation;
	double value;
	int threshold_LRC;

	//census
	bool isModifiedCensus;
	bool isAggregationCost;
	int n_census;
	int m_census;

	//sad
	bool isEqualized;

	//post processing
	bool ispostProcessing;
	//	bool isSuperPixels;
	bool isSpeckleRemoval;
	bool isFillHole;
	bool isAVERAGEFiltering;
	bool isMEDIANFiltering;
	bool isPLANE_FITFiltering;
	bool isQUAD_FITFiltering;
	bool isRANSAC_FITFiltering;
	bool isPoissonSmoothing;
	bool isBackgroundRemoval;
	bool isRemove_outliers;
	bool isReplace_all_points;
	bool isRemove_inclined_plane;
	bool isBilateralFilter;
	bool isSubSampling;
	double newVal_speckle;
	int maxSize_speckle;
	double maxDiff_speckle;
	int diamPixelWind;
	double sigmaParam;
	int n_rand;
	int iteration;
	int min_num_point;
	double RANSAC_threshold;

	// Class Declaration
	CensusImage censusLeft;
	CensusImage censusRight;
	HammingDistance HammingImage;
	SADImage sadImage;

	// Init function
	void initVariables(int width, int height);

	// Stereo-correspondence PRE PROCESSING
	void equalizeImage(
			const cv::Mat& imgGray,
			cv::Mat& imgGray_equalized);



	// Stereo-correspondence ALGORITHMS
	//Census
	void computeDisparityL2R(
			cv::Mat& dispL2R,
			cv::Mat& mask);
	void computeDisparityR2L(cv::Mat& dispR2L);

	void computeDisparityL2RAggCost(
			cv::Mat& dispL2R,
			cv::Mat& mask,
			cv::Mat& mask_SA);

	void computeDisparityR2LAggCost(
			cv::Mat& dispR2L,
			cv::Mat& mask_SA);

	void computeSubPixelRefinementL2R(
			int row, int col,
			unsigned int humm_dist,
			short& dist_pos, float& dist_pos_sub);
	void computeSubPixelRefinementR2L(
			int row, int col,
			unsigned int humm_dist,
			short& dist_pos, float& dist_pos_sub);
	void computeSubPixelRefinementL2RAggCost(
			int row, int col,
			unsigned int humm_dist,
			short& dist_pos, float& dist_pos_sub);
	void computeSubPixelRefinementR2LAggCost(
			int row, int col,
			unsigned int humm_dist,
			short& dist_pos, float& dist_pos_sub);

	//SAD
	void SAD_disparityL2R_opt(
			cv::Mat& dispL2R,
			cv::Mat& mask);
	void SAD_disparityR2L_opt(
			cv::Mat& dispR2L);
	void SAD_computeSubPixelRefinementL2RAggCost(
			int row, int col,
			unsigned int sad_dist, short& dist_pos, float& dist_pos_sub);
	void SAD_computeSubPixelRefinementR2LAggCost(
			int row, int col,
			unsigned int sad_dist, short& dist_pos, float& dist_pos_sub);


	// Stereo-correspondence POST PROCESSING
	void checkLRConsistencyOcclusion(
			const cv::Mat& dispL2R, const cv::Mat& dispR2L,
			int threshold_LRC, cv::Mat& disparity, cv::Mat& mask, cv::Mat& mask_SA);

	bool postProcess_(cv::Mat& label,
			segModel* SegModel,
			cv::Mat& img_hsv,
			std::vector<cv::Point2i>& contours,
			cv::Rect& SA_rect);

	void removeSpeckle(cv::Mat& disparity, cv::Mat& mask);

	void fillDisparityHoles(
			cv::Mat& label,
			cv::Mat& disparity_,
			cv::Mat& mask,
			segModel* SegModel,
			cv::Mat& img_hsv);

	void getSuperPixelsInfo(
			const cv::Mat& label,
			std::vector<SuperPixelInfo> &super_pixel_info,
			int& max_superpixel_dim,
			segModel* SegModel,
			cv::Mat& img_hsv);

	void getMedianSuperPixel(float* buff_xydisp, int buff_length, float& median);
	void getAverageSuperPixel(float* buff_xydisp, int buff_length, float& average);
	void getPlaneSuperPixel(float* buff_xydisp, int buff_length, float *plane);
	void getQuadricSuperPixel(float* buff_xydisp, int buff_length, float *quadric);
	bool getRANSACPlaneSuperPixel(float* buff_xydisp, int buff_length, float* best_plane);

	void smooth_pointCloud(std::vector<cv::Point2i>& contours,
			cv::Mat& sp_label,
			cv::Mat& disparity,
			cv::Rect& SA_rect);

	void compute_poisson(
			cv::Mat &disparity,
			cv::Mat &contour_mask,
			cv::Rect& SA_rect);

	// Thread functions
	//static void *run_censusLeft(void *data);
	static void *run_censusRight(void *data);
	static void *run_computeDisparityR2LAggCost(void *data);
	static void *run_computeDisparityL2RAggCostInside(void *data);
	static void *run_computeDisparityR2LAggCostInside(void *data);
};

#endif //__STEREOCORRESPONDENCE_HPP_
