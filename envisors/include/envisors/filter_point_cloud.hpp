/*
 * filter_point_cloud.hpp
 *
 *  Created on: Dec 12, 2016
 *      Author: veronica
 */

#ifndef SRC_envisors_INCLUDE_envisors_FILTER_POINT_CLOUD_HPP_
#define SRC_envisors_INCLUDE_envisors_FILTER_POINT_CLOUD_HPP_

//ROS
#include "ros/ros.h"
#include "ros/rate.h"
#include <sensor_msgs/PointCloud2.h>

//pcl
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/point_cloud.h>
#include <pcl/console/parse.h>
#include <pcl/common/transforms.h>
#include <pcl/filters/radius_outlier_removal.h>
#include <pcl/conversions.h> //I believe you were using pcl/ros/conversion.h
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>

//OpenCV
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>

#include "envisors/stereo_correspondence.hpp"
#include "envisors/super_pixels.hpp"

class FilterPointCloud
{

public:

	FilterPointCloud();

	// variables
	pcl::PointCloud<pcl::PointXYZRGB>* pc_in; // = new pcl::PCLPointCloud2;
	pcl::PointCloud<pcl::PointXYZRGB> pc_out;
	bool pc_empty;

//	pcl::PointCloud<pcl::PointXYZRGB>::Ptr pc_in;
//	pcl::PointCloud<pcl::PointXYZRGB>::Ptr pc_out;

	//methods
	void setPointCloud(sensor_msgs::PointCloud2& pc);
	void filter();
	void get_filtered_pointCloud(sensor_msgs::PointCloud2& pc);


};



#endif /* SRC_envisors_INCLUDE_envisors_FILTER_POINT_CLOUD_HPP_ */
