/*
 * SAD.h
 *
 *  Created on: Apr 12, 2014
 *      Author: Veronica
 */

#ifndef _SADIMAGE_HPP_
#define _SADIMAGE_HPP_

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <sstream>
#include <iostream>
#include <fstream>
#include <string>
//OpenCV
#include <cv_bridge/cv_bridge.h>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>


class SADImage{

public:

	unsigned short* image;
	cv::Size imageSize;
	int deltaDisparity;
	int minDisparity;

	//Method

	SADImage();
	~SADImage();

	unsigned short* computeSAD(cv::Mat& imgLeft, cv::Mat& imgRight,
			int minDisparity, int maxDisparity);
	unsigned short getSADValue(int row, int col, int disparity);

};

#endif //_SADIMAGE_HPP_
