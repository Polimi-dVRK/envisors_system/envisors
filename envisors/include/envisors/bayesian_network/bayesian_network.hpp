/*
 * bayesian_netwrok.hpp
 *
 *  Created on: Mar 14, 2016
 *      Author: veronica
 */

#ifndef BAYESIAN_NETWROK_HPP_
#define BAYESIAN_NETWROK_HPP_



#include <iostream>
#include <stdlib.h>

#include <dlib/bayes_utils.h>
#include <dlib/graph_utils.h>
#include <dlib/graph.h>
#include <dlib/directed_graph.h>
#include <iostream>

#include <gsl/gsl_randist.h>

#include <ros/ros.h>

//#define DEBUG

struct BNet_params{
	double distribution_std_max;
	double nFeatSigma;				// nFeatures_max / 3
	double nFeatSigmaF;			// 4
	double percLostFeatSigma;		// 15
	double percLostFeatSigmaF;		// 30
	double validityHomographySigma;	// 0.01
	double validityHomographySigmaF;// 0.6
	double stdDistributionSigma;	// 15
	double stdDistributionSigmaF;	// 150
	double bhDistanceSigma;			// 0.1
	double bhDistanceSigmaF;
};

class BNetwork
{

private:

	// bayesian network declaration
	dlib::directed_graph<dlib::bayes_node>::kernel_1a_c bn;
	BNet_params params_;
	// define nodes
	enum nodes
	{
		A = 0, // # features in SA
		B = 1, // % lost features in SA
		C = 2, // homography validity
		D = 3, // std distribution angle optical flow
		E = 4, // Bhacharryyan distance
		F = 5  // Tracking Failure
	};

	// define joint tree
	typedef dlib::set<unsigned long>::compare_1b_c set_type;
	typedef dlib::graph<set_type, set_type>::kernel_1a_c join_tree_type;
	join_tree_type join_tree;

	//variable
	int nFeatures_max_;

public:

	//methods

	void init_network(
			BNet_params &params,
			int nFeatures_max);

	double compute_joint_probability(
			int nFeat,
			double percLostFeat,
			bool validityHomography,
			double stdDistribution,
			double bhDistance);
};



#endif /* BAYESIAN_NETWROK_HPP_ */
