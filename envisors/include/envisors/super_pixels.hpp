/**
 * @author 		Veronica Penza
 * @version 	1.0
 * @date 		8 Aug 2015
 * @brief 		SLIC Super Pixels Algorithm
 *
 * Written by: Pascal Mettes.
 * This file contains the class elements of the class Slic. This class is an
 * implementation of the SLIC Superpixel algorithm by Achanta et al. [PAMI'12,
 * vol. 34, num. 11, pp. 2274-2282].
 * This implementation is created for the specific purpose of creating
 * over-segmentations in an OpenCV-based environment.
 */


#ifndef SLICSuperpixels_HPP_
#define SLICSuperpixels_HPP_

#include <stdio.h>
#include <sstream>
#include <iostream>
#include <math.h>
#include <limits>

#include <opencv2/opencv.hpp>

struct ColorRep;

class SLICSuperpixel
{

private:
    cv::Mat clusters;
    cv::Mat distances;
    std::vector<ColorRep> centers;
    std::vector<int> centerCounts;

    cv::Mat image;
    int K;
    int S;
    int m;
    int maxIterations;

    inline bool withinRange( int x, int y );
    double calcDistance( ColorRep& c, cv::Vec3b& p, int x, int y );
    cv::Point2i findLocalMinimum( cv::Mat& image, cv::Point2i center );

public:

    SLICSuperpixel();

    SLICSuperpixel(
    		cv::Mat& src,
			int no_of_superpixels, int m = 10, int max_iterations = 10 );

    void init(
    		cv::Mat& src,
    		int no_of_superpixels, int m = 10, int max_iterations = 10);

    void clear();
    void generateSuperPixels();

    int getS(); //Grid interval
    int getM();

    cv::Mat recolor();
    cv::Mat getClustersIndex();
    std::vector<ColorRep> getCenters();
    std::vector<cv::Point2i> getClusterCenters();
    std::vector<cv::Point2i> getContours();
    cv::Mat getImage();
};


/**
 * 5 dimension color representation, i.e
 * color represented in LAB color space and X Y coords
 * It's a struct, well struct behaves like class
 */
struct ColorRep{
//    float l = 0;
//    float a = 0;
//    float b = 0;
//    float x = 0;
//    float y = 0;
	float l;
	float a;
	float b;
	float x;
	float y;

    ColorRep() {
    	this->l = 0;
        this->a = 0;
        this->b = 0;
        this->x = 0;
        this->y = 0;
    }

    ColorRep( cv::Vec3b& color, cv::Point2i coord ) {
        init( color, coord.x, coord.y );
    }

    ColorRep( cv::Vec3b& color, int x, int y ) {
        init( color, x, y );
    }

    void init( cv::Vec3b& color, int x, int y ) {
        this->l = color[0];
        this->a = color[1];
        this->b = color[2];
        this->x = x;
        this->y = y;
    }

    void add( cv::Vec3b& color, int x, int y ) {
        this->l += color[0];
        this->a += color[1];
        this->b += color[2];
        this->x += x;
        this->y += y;
    }

    void divColor( float divisor ) {
        this->l /= divisor;
        this->a /= divisor;
        this->b /= divisor;
    }

    void div( float divisor ) {
        this->l /= divisor;
        this->a /= divisor;
        this->b /= divisor;
        this->x /= divisor;
        this->y /= divisor;
    }

    double colorDist( const ColorRep& other ) {
        return (this->l - other.l) * (this->l - other.l)
        + (this->a - other.a) * (this->a - other.a)
        + (this->b - other.b) * (this->b - other.b);
    }


    double coordDist( const ColorRep& other ) {
        return (this->x - other.x) * (this->x - other.x)
        + (this->y - other.y) * (this->y - other.y);
    }

    std::string toString() {
        std::stringstream ss;
        ss << l << " " << a << " " << b << " " << x << " " << y;
        return ss.str();
    }
};

#endif /* SLICSuperpixels_HPP_ */
