/**
 * @author 		Veronica Penza
 * @version 	1.0
 * @date 		8 Aug 2015
 * @brief common functions
 */

#ifndef COMMON_HPP_
#define COMMON_HPP_

//include
#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include <iostream>
#include <sensor_msgs/image_encodings.h>
#include <kdl/frames.hpp>

//OpenCV
#include <opencv2/opencv.hpp>

//ROS
#include "ros/ros.h"
#include "ros/rate.h"
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/CameraInfo.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/PointCloud2.h>
#include <geometry_msgs/Pose.h>

#include "envisors/segmentation/seg_model.h"

// Define terminal colors
#define COLOR_UPDATE "\e[1;32m"
#define COLOR_REPLACE "\e[1;31m"
#define COLOR_RESET "\e[0m]"

template<class T>
class FrameIDManager{

private:
	bool is_new;
	T frameID;
public:
	FrameIDManager()
{
		this->is_new = false;
}

	bool isNew()
	{
		return is_new;
	}

	void clear()
	{
		this->is_new = false;
	}

	void setID(T frameID)
	{
		this->is_new = (frameID != this->frameID);
		this->frameID = frameID;
	}

	T getID()
	{
		return this->frameID;
	}
};

void fromMattoImage(
		const cv::Mat& img_in,
		sensor_msgs::Image* img_out,
		const std::string& encoding);

void fromImagetoMat(
		const sensor_msgs::Image& img_in,
		cv::Mat* img_out,
		const std::string& encoding);

bool isOdd(int integer);

bool isEven(int integer);

void fromRGBtoGray(
		const cv::Mat& imgRGB,
		cv::Mat& imgGray);

void fromMattoPointCloud2(
		const cv::Mat& imgL_rect,
		const cv::Mat& pointCloud_Mat,
		sensor_msgs::PointCloud2& pointCloud_msgs);

void fromPosetoMat(const geometry_msgs::Pose& pose_input,
		cv::Mat& R, cv::Mat& t);

void fromMattoPose(const cv::Mat& R, const cv::Mat& t,
		geometry_msgs::Pose& pose_output);

void readParamsCamera(
		const std::string fileName,
		cv::Mat& cameraMatrix,
		cv::Mat& distCoeff);

void readExtrisicParamsCamera(
		const std::string fileName,
		cv::Mat& R,
		cv::Mat& T);

void readStereoParamsCamera(
		const std::string fileName,
		cv::Mat& cameraMatrixL,
		cv::Mat& distCoeffL,
		cv::Mat& cameraMatrixR,
		cv::Mat& distCoeffR,
		cv::Mat& R, cv::Mat& T,cv::Mat& E,cv::Mat& F);

void readRectificationMatrix(
		const std::string fileName,
		cv::Mat& R);

void readTranslation(
		const std::string fileName,
		cv::Mat& T);

void saveParamsCamera(
		const std::string fileName,
		cv::Mat& cameraMatrix,
		cv::Mat& distCoeff);

void saveExtrisicParamsCamera(
		const std::string fileName,
		cv::Mat& R,
		cv::Mat& T);

void saveStereoParamsCamera(
		const std::string fileName,
		cv::Mat& cameraMatrixL,
		cv::Mat& distCoeffL,
		cv::Mat& cameraMatrixR,
		cv::Mat& distCoeffR,
		cv::Mat& R, cv::Mat& T,cv::Mat& E,cv::Mat& F);

void readParamQ(
		const std::string fileName,
		cv::Mat& Q);

bool getColorMapValue(
		float value,
		cv::Vec3b& color);

void create_color_bar(
		cv::Mat& img,
		double th,
		double act_value);

void images_equalization(
		const cv::Mat& img_1,
		const cv::Mat& img_2,
		cv::Mat& img_1_eq);

void image_equalization(
		const cv::Mat& img_1,
		cv::Mat& img_1_eq);

void compute_histogram(
		const cv::Mat& img,
		cv::Mat& hist);

void compute_histogram_acc(
		const cv::Mat& hist,
		cv::Mat& hist_acc);

void hsv_image_histogram(
		const cv::Mat& hsv_img,
		cv::Mat& hist_asv,
		std::vector<cv::Point2f> &safety_area,
		cv::Mat& SA_mask,
		int n_hbin,
		int n_sbin,
		float th_seg,
		segModel* SegModel);

void compute_BhattaCharryya_distance(
		cv::Mat& hsv_hist_1,
		cv::Mat& hsv_hist_2,
		double &bh_distance);

void calculate_mean_SD(
		std::vector<float>& data,
		float& mean,
		float& standardDeviation);

void draw_gauge(
		cv::Mat& img,
		cv::Point top_left_corner,
		cv::Mat& gauge_img,
		cv::Mat& gauge_mask,
		float alpha);

void fromMattoKDL(
		cv::Mat& R,
		cv::Mat& t,
		KDL::Frame& T);

void fromKDLtoMat(
		KDL::Frame& T,
		cv::Mat& R,
		cv::Mat& t);


#endif //COMMON_HPP_
