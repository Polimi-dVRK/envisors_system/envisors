#ifndef SEG_MODEL
#define SEG_MODEL

#include <iostream>
#include <numeric>
#include <vector>
#include <opencv2/core/core.hpp>
#include <opencv2/features2d/features2d.hpp>

#include "envisors/segmentation/possible_center.h"
#include "envisors/segmentation/mutils.h"
//#include "envisors/common.hpp"

class segModel
{
public:
	segModel(int num_hbin=12, int num_sbin=12, int num_vbin=8);
	~segModel();

	int m_num_hbin;
	int m_num_sbin;
	int m_num_vbin;
	int m_num_bin;

	float m_h_step;
	float m_s_step;
	float m_v_step;

	float m_prob_c1_c0;
	float m_prob_c1_c1;
	float m_prob_c0_c1;
	float m_prob_c0_c0;
	float m_alpha;

	bool m_init_flag;

	std::vector<float> m_prob_c0_y;
	std::vector<float> m_prob_c1_y;
	std::vector<float> m_prob_y_c0;
	std::vector<float> m_prob_y_c1;

	int m_kp_patch_size;

	int getBinIdx(const cv::Vec3b& pixel) const;

	void initProb(const cv::Mat &img_hsv, const cv::Rect &search_roi, const cv::Rect &ob_roi);

	void updateProb(
			const cv::Mat& img_hsv,
			const cv::Rect& center,
			const std::vector<cv::Point>* hull,
			int roi_width, int roi_height,
			bool init,
			int n_convex_hull);

	float getKpProb(const cv::Point &kp_pt, const cv::Mat &img_hsv) const;
	float getsingleKpProb(const cv::Point &kp_pt, const cv::Mat &img_hsv) const;

	void showProbMap(
			const cv::Mat &img_hsv,
			const cv::Rect &roi,
			cv::Mat& img,
			double& th,
			const std::string& win_name = "prob_map") const;
};

#endif // SEG_MODEL

