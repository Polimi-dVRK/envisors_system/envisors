#ifndef POSSIBLECENTER_H
#define POSSIBLECENTER_H


class ghtCenter
{
public:
    ghtCenter(float x, float y, float score=0, float scale=1, float angle=0);
    ~ghtCenter();

    void setScale(float scale);
    void setScore(float score);
    void setRotation(float angle);

    float m_x;
    float m_y;
    float m_score;
    float m_scale;
    float m_rotation;

};

#endif // POSSIBLECENTER_H
