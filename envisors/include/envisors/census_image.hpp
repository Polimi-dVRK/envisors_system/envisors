#ifndef _CENSUSIMAGE_HPP_
#define _CENSUSIMAGE_HPP_

#include <stdlib.h>
#include <string.h>

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <sstream>
#include <iostream>
#include <fstream>
#include <string>
#include <sensor_msgs/image_encodings.h>

//OpenCV
#include <cv_bridge/cv_bridge.h>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/videostab/inpainting.hpp>

#define CENSUS_TRANSFORM 			1
#define CENSUS_TRANSFORM_MODIFIED	2

class CensusImage
{
private:
	unsigned char *image;
	int rows;
	int cols;
	int bitsPerPixel;
	int bytesPerPixel;

public:
	CensusImage();
	~CensusImage();

	void create(int rows, int cols, int winW, int winH, int criteria);

	int getBitsPerPixel();
	int getBytesPerPixel();
	int rowsN();
	int colsN();

	unsigned char *getPixel(int row, int col);

	void setPixel(int row, int col, unsigned char *pixel);

	bool setCensus(
			const cv::Mat &inputImage,
			int winW, int winH,
			cv::Mat& mask_SA,
			int criteria,
			bool right, cv::Rect boundary, int disp_min, int disp_max);

	int computeMax(const int x, const int y);
	int computeMin(const int x, const int y);
	bool convertToMat(cv::Mat &outputImage);
};

#endif // _CENSUSIMAGE_HPP_
