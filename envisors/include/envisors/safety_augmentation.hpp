/*
 * safety_augmentation.hpp
 *
 *  Created on: Jan 7, 2017
 *      Author: veronica
 */

#ifndef envisors_INCLUDE_envisors_SAFETY_AUGMENTATION_HPP_
#define envisors_INCLUDE_envisors_SAFETY_AUGMENTATION_HPP_


#include <stdio.h>
#include <sstream>
#include <vector>

//pcl
//#include <pcl/io/pcd_io.h>
//#include <pcl/io/ply_io.h>
//#include <pcl/point_cloud.h>
//#include <pcl/console/parse.h>
//#include <pcl/common/transforms.h>
#include <pcl/visualization/common/shapes.h>
#include <pcl/visualization/common/actor_map.h>

//eigen
#include <Eigen/Geometry>

//ros
#include <ros/ros.h>
#include <visualization_msgs/Marker.h>

#include "envisors/common.hpp"

typedef struct
{
	double x;
	double y;
	double z;

}Point3;

typedef struct
{
	double minX;
	double minY;
	double minZ;
	double maxX;
	double maxY;
	double maxZ;
	double x_axis;
	double y_axis;
	double z_axis;
	double average_x;
	double average_y;
	double average_z;
	Point3 center;
	std::vector<Point3> points;

}BoundingBox;

typedef struct
{
	double x_axis;
	double y_axis;
	double z_axis;
	Point3 center;
	std::vector<Point3> points;

}EllipsoidData;


class SafetyAugmentation
{
public:
	//variables
	BoundingBox b_box;
	EllipsoidData ellipsoid;
	 ros::NodeHandle n;
	 ros::Publisher bounding_box_pub;
	 visualization_msgs::Marker bounding_box_ros;
	 cv::Mat pointCloud_t;
//	 pcl::PointCloud<pcl::PointXYZRGB> pc_t_;


	//methods
	 SafetyAugmentation();
	void set_pointCloud(
			cv::Mat& pc_t);
	void set_bounding_box(float *coord);
	void create_bounding_box();
	void publish_bounding_box();
	void remove_noise();
	void remove_noise_2();
	void compute_b_box_coordinates();
	void cluster_pc();
	void set_ellipsoid();
	float is_inside(float x, float y, float z);

};


#endif /* envisors_INCLUDE_envisors_SAFETY_AUGMENTATION_HPP_ */
