/**
 * @author 		Veronica Penza
 * @version 	1.0
 * @date 		14 November 2015
 * @brief 		height map
 * Class height map computation
 */

#ifndef HEIGHMAP_HPP_
#define HEIGHMAP_HPP_


//include
#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include <iostream>
#include <sensor_msgs/image_encodings.h>

//OpenCV
#include <opencv2/opencv.hpp>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/core/core.hpp>

//PCL
#include <pcl/common/common.h>


class HeighMap
{

public:
	/* methods */
	void setPlane(const double* plane_coeff);
	void setBinSize(double bin_size);
	void compute(pcl::PointCloud<pcl::PointXYZRGB>& pc);

	double getDistance(const cv::Point3f& pt);
	bool getInside(const cv::Point3f& pt);
	void getHeightMap(cv::Mat& height_map);
	void getSize(cv::Point2f& size);
	void getZmax(double& z_max);
	void getZmin(double& z_min);


private:

	/*variables*/
	cv::Mat height_map_;
	double plane_coeffs_[4];
	double bin_size_;
	double minX_;
	double maxX_;
	double minY_;
	double maxY_;
	double minZ_;
	double maxZ_;

	cv::Point2f origin_;
	cv::Point2f size_;

};


#endif //HEIGHMAP_HPP_
