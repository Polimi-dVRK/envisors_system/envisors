#ifndef ROS2PCL
#define ROS2PCL

//PCL CONVERSION TO ROS
#include <ros/ros.h>
#include "std_msgs/Float32.h"
#include <pcl/conversions.h>
#include <pcl/PCLHeader.h>
#include <std_msgs/Header.h>
#include <pcl/PCLImage.h>
#include <sensor_msgs/Image.h>
#include <pcl/PCLPointField.h>
#include <sensor_msgs/PointField.h>
#include <pcl/PCLPointCloud2.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl/io/pcd_io.h>
#include <pcl/people/height_map_2d.h>


namespace ros2pcl{

void fromROSMsg(const sensor_msgs::PointCloud2 &cloud, pcl::PointCloud<pcl::PointXYZRGB> &pcl_cloud);

void toPCL(const sensor_msgs::PointCloud2 &pc2, pcl::PCLPointCloud2 &pcl_pc2);

void copyPointCloud2MetaData(const sensor_msgs::PointCloud2 &pc2, pcl::PCLPointCloud2 &pcl_pc2);

void toPCL(const std_msgs::Header &header, pcl::PCLHeader &pcl_header);

void toPCL(const std::vector<sensor_msgs::PointField> &pfs, std::vector<pcl::PCLPointField> &pcl_pfs);

void toPCL(const sensor_msgs::PointField &pf, pcl::PCLPointField &pcl_pf);

}//namespace



#endif
