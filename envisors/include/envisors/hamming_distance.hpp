/*
 * LUT_hamming.hpp
 *
 *  Created on: March 9, 2014
 *      Author: Veronica
 */
#ifndef _HAMMINGDISTANCE_HPP_
#define _HAMMINGDISTANCE_HPP_


#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <sstream>
#include <iostream>
#include <fstream>
#include <string>
//OpenCV
#include <cv_bridge/cv_bridge.h>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/videostab/inpainting.hpp>

#include "envisors/census_image.hpp"
#include "envisors/define.hpp"

typedef struct
{
	void* HammingD;
	CensusImage* imgLeft;
	CensusImage* imgRight;
	cv::Mat* mask_SA;
	int minDisparity;
	int maxDisparity;
	int minDisparityTotal;
}ThreadHammingData;

class HammingDistance{

public:

	static unsigned char LUTHammingValues[256];
	unsigned short* HammingDistanceImage;
	cv::Size imageSize;
	int deltaDisparity;
	int minDisparity;

	//Method

	HammingDistance();
	~HammingDistance();
	void initilizeLUTHamming();
	unsigned char getHammingDistancefromLUT(unsigned char hamm);

	unsigned short* computeHammingDistanceImage(
			CensusImage& imgLeft,
			CensusImage& imgRight,
			cv::Mat& mask_SA,
			int minDisparity,
			int maxDisparity);
	static unsigned char getHammingDistance(unsigned char x, unsigned char y);
	unsigned short getHammingDistanceImageValue(int row, int col, int disparity);

	// Thread functions
	static void *run_Hamming(void *data);

};

#endif //_HAMMINGDISTANCE_HPP_
