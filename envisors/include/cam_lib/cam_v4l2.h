/**
 * @file     cam_v4l2.h
 * @author   Jesús Ortiz
 * @version  1.0
 * @date     02-Dec-2013
 * @brief    v4l2 camera class
 */

#ifndef CAM_V4L2_H_
#define CAM_V4L2_H_

/*
 * INCLUDE
 */

#include <unistd.h>
#include "cam_lib/cam_interface.h"
#include "cam_lib/videocap.h"

/*
 * CLASS: Cam_v4l2
 */

/** Prosilica camera class */
class Cam_v4l2 : public Cam_interface
{
private:
  Videocap videocap;

public:
  /** Constructor */
  Cam_v4l2(int width, int height);

  /** Destructor */
  ~Cam_v4l2();

  /** Init camera */
  bool initCamera(const std::string &config);

  /** Finish camera */
  bool finishCamera();

  /** Grabs an image from the camera
   *  This function should pull a new image from the camera driver
   *  and save it in the internal image variable
   * @return        True if the image was acquired,
   *             false otherwise
   */
  bool grabImage();

  /** Set / Get Controls */
  bool controls(const char *name, int *value, bool set);

private:
  int width, height;

  static unsigned char clip(int v);

  static void yuv444torgb888(
    unsigned char y,
    unsigned char u,
    unsigned char v,
    unsigned char *rgb);

  static void yuv422torgb24(
    unsigned char *yuv422,
    unsigned char *rgb);

  static void convertyuv422torgb24(
    unsigned char *bufferYUV,
    int lenBufferYUV,
    unsigned char *bufferRGB,
    int lenBufferRGB);
};

#endif  /* CAM_V4L2_H_ */
