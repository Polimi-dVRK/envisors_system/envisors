#ifndef CAMAUX_H_
#define CAMAUX_H_

#include <time.h>
#include <string>
#include <iostream>
#include <iomanip>
#include <pthread.h>

/** Gets the delta time from the initial time */
struct timespec camAux_getTime();

/** Gets the seconds */
double camAux_sec(struct timespec time);

/** Gets the miliseconds */
unsigned long camAux_msec(struct timespec time);

/** Gets the microseconds */
unsigned long camAux_usec(struct timespec time);

/** Print message for profiling */
void camAux_print(const std::string &msg);

/** Print message in CSV format for profiling */
void camAux_printCSV(unsigned int id);

#endif	/* CAMAUX_H_ */
