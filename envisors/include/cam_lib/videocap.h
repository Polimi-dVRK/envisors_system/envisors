#ifndef VIDEOCAP_H_
#define VIDEOCAP_H_

#include <iostream>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <malloc.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <asm/types.h>
#include <stdio.h>
#include <string.h>

#include <linux/videodev2.h>

#include <pthread.h>

#define VIDEOCAP_STATE_STOP  0
#define VIDEOCAP_STATE_RUN  1

#define BRIGHTNESS  0x00
#define CONTRAST    0x01
#define SATURATION  0x02
#define HUE         0x03
#define GAMMA       0x04
#define EXPOSURE    0x05
#define GAIN        0x06

struct buffer
{
  void *start;
  size_t length;
};

class Videocap
{
private:
  int fdcam;
  struct buffer *buffers;
  unsigned int n_buffers;

  int size;
  char *frame;

  // Pthreads
  pthread_mutex_t mutex;
  pthread_t thread;
  int state;

public:
  Videocap();
  ~Videocap();

  int init(const char *dev, int width, int height, int format);
  char *getFrame();
  int getSize();
  int finish();

  int Controls(int id, int *value, bool set);

private:
  char *captureFrame();
  static void *thread_capture(void *arg);
  int xioctl(int fd, int request, void *arg);
};

#endif /*VIDEOCAP_H_*/
