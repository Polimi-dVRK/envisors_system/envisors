
#include "envisors/sad_image.hpp"


SADImage::SADImage(){

	this->image = NULL;
	this->imageSize.height = 0;
	this->imageSize.width = 0;
}

SADImage::~SADImage(){
	if(this->image != NULL){
		delete [] this->image;
	}

}


unsigned short* SADImage::computeSAD(cv::Mat& imgLeft, cv::Mat& imgRight,
		int minDisparity, int maxDisparity){

	unsigned short SAD_dist;

	// Check if the image exists
	if (this->image != NULL)
	{
		// The image exists, but it has different dimensions
		if (this->imageSize.height != imgLeft.rows || this->imageSize.width != imgLeft.cols
				|| (maxDisparity - minDisparity) != this->deltaDisparity)
		{
			delete [] this->image;
			this->image = new unsigned short[imgLeft.rows * imgLeft.cols * (maxDisparity - minDisparity)];
			this->imageSize.height = imgLeft.rows;
			this->imageSize.width = imgLeft.cols;
			this->deltaDisparity = maxDisparity - minDisparity;
			this->minDisparity = minDisparity;
		}
	}
	// The image doesn't exist yet
	else
	{
		this->image = new unsigned short[imgLeft.rows * imgLeft.cols * (maxDisparity - minDisparity)];
		this->imageSize.height = imgLeft.rows;
		this->imageSize.width = imgLeft.cols;
		this->deltaDisparity = maxDisparity - minDisparity;
		this->minDisparity = minDisparity;
	}


	for(int j = minDisparity; j < maxDisparity; j++){

		for(int row = 0; row < this->imageSize.height; row++){
			for(int col = j; col < this->imageSize.width; col++){
				SAD_dist = (ushort)abs(imgLeft.at<uchar>(row, col) - imgRight.at<uchar>(row, col -j));
				image[col + row * imageSize.width + (j - minDisparity) * imageSize.width * imageSize.height] = SAD_dist;
			}
		}
	}

	return image;
}

unsigned short SADImage::getSADValue(int row, int col, int disparity){

	return this->image[col + row * imageSize.width + (disparity - minDisparity) *
					   imageSize.width * imageSize.height];
}


