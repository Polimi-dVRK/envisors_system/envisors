/*
 * safety_augmentation.cpp
 *
 *  Created on: Jan 7, 2017
 *      Author: veronica
 */

/*
 * ellipse.cpp
 *
 *  Created on: Nov 20, 2016
 *      Author: veronica
 */

#include "envisors/safety_augmentation.hpp"

SafetyAugmentation::SafetyAugmentation()
{
	this->bounding_box_pub = this->n.advertise<visualization_msgs::Marker>("/SA/bounding_box", 1);
	this->ellipsoid.center.x = 0.0;
	this->ellipsoid.center.y = 0.0;
	this->ellipsoid.center.z = 0.0;
	this->ellipsoid.x_axis = 0.0;
	this->ellipsoid.y_axis = 0.0;
	this->ellipsoid.z_axis = 0.0;
	this->b_box.center.x = 0.0;
	this->b_box.center.y = 0.0;
	this->b_box.center.z = 0.0;
	this->b_box.x_axis = 0.0;
	this->b_box.y_axis = 0.0;
	this->b_box.z_axis = 0.0;
	this->b_box.x_axis = 0.0;
}

void SafetyAugmentation::set_pointCloud(
		cv::Mat& pc_t)
{
	if (pc_t.empty())
	{
		std::cout << "[ERROR] Teh point cloud is invalid!" << std::endl;
		return;
	}

	this->pointCloud_t = pc_t;
}

void SafetyAugmentation::set_bounding_box(float *coord)
{

	if (this->pointCloud_t.empty())
	{
		std::cout << "[ERROR] The point cloud is invalid!" << std::endl;
		return;
	}

//	this->remove_noise_2();

	this->b_box.minX = 1000;
	this->b_box.minY = 1000;
	this->b_box.minZ = 1000;
	this->b_box.maxX = -1000;
	this->b_box.maxY = -1000;
	this->b_box.maxZ = -1000;

	int count = 0;
	this->b_box.average_x = 0.0;
	this->b_box.average_y = 0.0;
	this->b_box.average_z = 0.0;

	for(int i = 0; i < this->pointCloud_t.rows; i++)
	{
		for(int j = 0; j < this->pointCloud_t.cols; j++)
		{
			cv::Point3f p_temp;
			p_temp = this->pointCloud_t.at<cv::Point3f>(i,j);

			if(p_temp.x == -1 || p_temp.y == -1 || p_temp.z == -1)
			{
				continue;
			}
			/*find max X*/
			if(p_temp.x > this->b_box.maxX)
			{
				this->b_box.maxX = p_temp.x;
			}
			/*find max Y*/
			if(p_temp.y > this->b_box.maxY)
			{
				this->b_box.maxY = p_temp.y;
			}
			/*find max Z*/
			if(p_temp.z > this->b_box.maxZ)
			{
				this->b_box.maxZ = p_temp.z;
			}
			/*find max X*/
			if(p_temp.x < this->b_box.minX)
			{
				this->b_box.minX = p_temp.x;
			}
			/*find max Y*/
			if(p_temp.y < this->b_box.minY)
			{
				this->b_box.minY = p_temp.y;
			}
			/*find max Z*/
			if(p_temp.z < this->b_box.minZ)
			{
				this->b_box.minZ = p_temp.z;
			}

			this->b_box.average_x += p_temp.x;
			this->b_box.average_y += p_temp.y;
			this->b_box.average_z += p_temp.z;
			count++;
		}


	}

	this->b_box.average_x /= count;
	this->b_box.average_y /= count;
	this->b_box.average_z /= count;

	this->b_box.x_axis = this->b_box.maxX - this->b_box.minX;
	this->b_box.y_axis = this->b_box.maxY - this->b_box.minY;
	this->b_box.z_axis = this->b_box.maxZ - this->b_box.minZ;

	this->b_box.center.x = this->b_box.minX + this->b_box.x_axis/2.0;
	this->b_box.center.y = this->b_box.minY + this->b_box.y_axis/2.0;
	this->b_box.center.z = this->b_box.minZ + this->b_box.z_axis/2.0;

	/*
	std::cout << "xmin " << this->b_box.minX   << " xmax " << this->b_box.maxX  << std::endl;
	std::cout << "ymin " << this->b_box.minY   << " ymax " << this->b_box.maxY  << std::endl;
	std::cout << "zmin " << this->b_box.minZ   << " zmax " << this->b_box.maxZ  << std::endl;

	std::cout << "x_axis " << this->b_box.x_axis << std::endl;
	std::cout << "y_axis " << this->b_box.y_axis << std::endl;
	std::cout << "z_axis " << this->b_box.z_axis << std::endl;

	std::cout << "xc " << this->b_box.center.x  << std::endl;
	std::cout << "yc " << this->b_box.center.y  << std::endl;
	std::cout << "zc " << this->b_box.center.z  << std::endl;
*/
	coord[0] = this->b_box.minX;
	coord[1] = this->b_box.maxX;
	coord[2] = this->b_box.minY;
	coord[3] = this->b_box.maxY;
	coord[4] = this->b_box.minZ;
	coord[5] = this->b_box.maxZ;



	this->remove_noise();
}


void SafetyAugmentation::create_bounding_box()
{

	Eigen::Vector3f  	translation(
			this->b_box.center.x,
			this->b_box.center.y,
			this->b_box.center.z);
	Eigen::Quaternionf 	rotation;
	rotation.Identity();

	vtkSmartPointer< vtkDataSet > rectangle = pcl::visualization::createCube(translation,
			rotation,
			this->b_box.x_axis, // width
			this->b_box.y_axis, //height,
			this->b_box.z_axis //depth
	);
}

void SafetyAugmentation::publish_bounding_box()
{
	this->bounding_box_ros.header.frame_id = "PSM1remote_center";
	this->bounding_box_ros.header.stamp = ros::Time::now();
	this->bounding_box_ros.ns = "basic_shapes";
	this->bounding_box_ros.id = 0;
	this->bounding_box_ros.type = visualization_msgs::Marker::CUBE;
	this->bounding_box_ros.action = visualization_msgs::Marker::ADD;

	this->bounding_box_ros.pose.position.x = this->b_box.center.x;
	this->bounding_box_ros.pose.position.y = this->b_box.center.y;
	this->bounding_box_ros.pose.position.z = this->b_box.center.z;
	this->bounding_box_ros.pose.orientation.x = 0.0;
	this->bounding_box_ros.pose.orientation.y = 0.0;
	this->bounding_box_ros.pose.orientation.z = 0.0;
	this->bounding_box_ros.pose.orientation.w = 1.0;

	// Set the scale of the marker -- 1x1x1 here means 1m on a side
	this->bounding_box_ros.scale.x = this->b_box.x_axis;
	this->bounding_box_ros.scale.y = this->b_box.y_axis;
	this->bounding_box_ros.scale.z = this->b_box.z_axis;

	// Set the color -- be sure to set alpha to something non-zero!
	this->bounding_box_ros.color.r = 0.0f;
	this->bounding_box_ros.color.g = 1.0f;
	this->bounding_box_ros.color.b = 0.0f;
	this->bounding_box_ros.color.a = 0.5;

	this->bounding_box_ros.lifetime = ros::Duration();

	this->bounding_box_pub.publish(this->bounding_box_ros);

}

void SafetyAugmentation::remove_noise()
{
	// compute the distance of each point to the centroid
	std::vector<float> diff_to_centroid;
	float std = 0;
	float mean = 0;
	double diff = 0;
	diff_to_centroid.clear();

	if (this->pointCloud_t.empty())
		return;

	for(int i = 0; i < this->pointCloud_t.rows; i++)
	{
		for(int j = 0; j < this->pointCloud_t.cols; j++)
		{
			cv::Point3f p_temp;
			p_temp = this->pointCloud_t.at<cv::Point3f>(i,j);

			if(p_temp.x == -1 || p_temp.y == -1 || p_temp.z == -1)
			{
				continue;
			}

			diff = sqrt((p_temp.x - this->b_box.average_x)*(p_temp.x - this->b_box.average_x) +
					(p_temp.y - this->b_box.average_y)*(p_temp.y - this->b_box.average_y) +
					(p_temp.z - this->b_box.average_z)*(p_temp.z - this->b_box.average_z));


			diff_to_centroid.push_back(diff);
		}
	}

	calculate_mean_SD(diff_to_centroid, mean, std);
//	std::cout << "pc mean : " <<  mean << std::endl;
//	std::cout << "pc standard deviation: " <<  std << std::endl;

	//recompute the polygon coordinates


	this->b_box.minX = 10000;
	this->b_box.minY = 10000;
	this->b_box.minZ = 10000;
	this->b_box.maxX = -10000;
	this->b_box.maxY = -10000;
	this->b_box.maxZ = -10000;


	for(int i = 0; i < this->pointCloud_t.rows; i++)
	{
		for(int j = 0; j < this->pointCloud_t.cols; j++)
		{
			cv::Point3f p_temp;
			p_temp = this->pointCloud_t.at<cv::Point3f>(i,j);

			if(p_temp.x == -1 || p_temp.y == -1 || p_temp.z == -1)
			{
				continue;
			}

			diff = sqrt((p_temp.x - this->b_box.average_x)*(p_temp.x - this->b_box.average_x) +
					(p_temp.y - this->b_box.average_y)*(p_temp.y - this->b_box.average_y) +
					(p_temp.z - this->b_box.average_z)*(p_temp.z - this->b_box.average_z));

			if(diff > mean + 1.5*std )
				continue;

			/*find max X*/
			if(p_temp.x > this->b_box.maxX)
			{
				this->b_box.maxX = p_temp.x;
			}
			/*find max Y*/
			if(p_temp.y > this->b_box.maxY)
			{
				this->b_box.maxY = p_temp.y;
			}
			/*find max Z*/
			if(p_temp.z > this->b_box.maxZ)
			{
				this->b_box.maxZ = p_temp.z;
			}
			/*find max X*/
			if(p_temp.x < this->b_box.minX)
			{
				this->b_box.minX = p_temp.x;
			}
			/*find max Y*/
			if(p_temp.y < this->b_box.minY)
			{
				this->b_box.minY = p_temp.y;
			}
			/*find max Z*/
			if(p_temp.z < this->b_box.minZ)
			{
				this->b_box.minZ = p_temp.z;
			}
		}
	}

	this->b_box.center.x = this->b_box.minX + this->b_box.x_axis/2.0;
	this->b_box.center.y = this->b_box.minY + this->b_box.y_axis/2.0;
	this->b_box.center.z = this->b_box.minZ + this->b_box.z_axis/2.0;

	this->b_box.x_axis = this->b_box.maxX - this->b_box.minX;
	this->b_box.y_axis = this->b_box.maxY - this->b_box.minY;
	this->b_box.z_axis = this->b_box.maxZ - this->b_box.minZ;


/*
	std::cout << "xmin " << this->b_box.minX   << " xmax " << this->b_box.maxX  << std::endl;
	std::cout << "ymin " << this->b_box.minY   << " ymax " << this->b_box.maxY  << std::endl;
	std::cout << "zmin " << this->b_box.minZ   << " zmax " << this->b_box.maxZ  << std::endl;

	std::cout << "x_axis " << this->b_box.x_axis << std::endl;
	std::cout << "y_axis " << this->b_box.y_axis << std::endl;
	std::cout << "z_axis " << this->b_box.z_axis << std::endl;

	std::cout << "xc " << this->b_box.center.x  << std::endl;
	std::cout << "yc " << this->b_box.center.y  << std::endl;
	std::cout << "zc " << this->b_box.center.z  << std::endl;
	*/
}

void SafetyAugmentation::remove_noise_2()
{
	std::vector<float> neigh_diff(8);
	float neigh_diff_min = 100000;
	float neigh_diff_max = 0.0;
	float neigh_diff_average = 0.0;
	float th_avg = 10;
	float th_minmax = 10;
	float th_max = 100;
	int c = 0;

	for(int i = 0; i < this->pointCloud_t.rows; i++)
	{
		for(int j = 0; j < this->pointCloud_t.cols; j++)
		{
			cv::Point3f p_temp;
			p_temp = this->pointCloud_t.at<cv::Point3f>(i,j);

			if(p_temp.x == -1 || p_temp.y == -1 || p_temp.z == -1)
			{
				continue;
			}

			c = 0;

			for(int m = -1; m <= 1; m++)
			{
				for(int n = -1; n <= 1; n++)
				{
					if(m == 0 && n == 0)
						continue;

					if( i == 0 && m == -1)
						continue;

					if( j == 0 && n == -1)
						continue;

					if( i == this->pointCloud_t.rows-1 && m == 1)
						continue;

					if( j == this->pointCloud_t.cols-1 && n == 1)
						continue;

					//					neigh_diff[i] = fabs(this->pointCloud_t.at<cv::Point3f>(i,j)[2] - this->pointCloud_t.at<cv::Point3f>(i+m,j+n)[2]);
					neigh_diff[c] =
							sqrt((this->pointCloud_t.at<cv::Point3f>(i,j).x - this->pointCloud_t.at<cv::Point3f>(i+m,j+n).x) *
							(this->pointCloud_t.at<cv::Point3f>(i,j).x - this->pointCloud_t.at<cv::Point3f>(i+m,j+n).x)      +
							(this->pointCloud_t.at<cv::Point3f>(i,j).y - this->pointCloud_t.at<cv::Point3f>(i+m,j+n).y)      *
							(this->pointCloud_t.at<cv::Point3f>(i,j).y - this->pointCloud_t.at<cv::Point3f>(i+m,j+n).y)      +
							(this->pointCloud_t.at<cv::Point3f>(i,j).z - this->pointCloud_t.at<cv::Point3f>(i+m,j+n).z)      *
							(this->pointCloud_t.at<cv::Point3f>(i,j).z - this->pointCloud_t.at<cv::Point3f>(i+m,j+n).z));

					c++;

				}
			}

			for(int l = 0; l <  neigh_diff.size(); l++)
			{
				if(neigh_diff[l] < neigh_diff_min)
					neigh_diff_min = neigh_diff[l];

				if(neigh_diff[l] > neigh_diff_max)
					neigh_diff_max = neigh_diff[l];

				neigh_diff_average += neigh_diff[l];
			}

			neigh_diff_average /= neigh_diff.size();

//			if((neigh_diff_max - neigh_diff_min < th_minmax) )
//			{
//				if(neigh_diff_average > th_avg)
//				{
//					this->pointCloud_t.at<cv::Point3f>(i,j) = cv::Point3f(-1.0, -1.0,-1.0);
//				}
//
//
//			}
			if(neigh_diff_max > th_max)
			{
				this->pointCloud_t.at<cv::Point3f>(i,j) = cv::Point3f(-1.0, -1.0,-1.0);
			}

		}
	}

}

void  SafetyAugmentation::set_ellipsoid()
{
	if(this->b_box.center.x == 0.0 && this->b_box.center.y == 0.0 &&	this->b_box.center.z == 0.0 &&
			this->b_box.x_axis == 0.0 && this->b_box.y_axis == 0.0 && this->b_box.z_axis == 0.0)
	{
		float bb_coord[6];
		this->set_bounding_box(bb_coord);
	}

	this->ellipsoid.center = this->b_box.center;
	this->ellipsoid.x_axis = this->b_box.x_axis;
	this->ellipsoid.y_axis = this->b_box.y_axis;
	this->ellipsoid.z_axis = this->b_box.z_axis;

}

float SafetyAugmentation::is_inside(float x, float y, float z)
{
	float distance_x, distance_y ,distance_z;

	distance_x = fabs(x - this->b_box.center.x) - this->b_box.x_axis/2;
	distance_y = fabs(y - this->b_box.center.y) - this->b_box.y_axis/2;
	distance_z = fabs(z - this->b_box.center.z) - this->b_box.z_axis/2;

	if(this->b_box.center.x == 0.0 && this->b_box.center.y == 0.0 &&	this->b_box.center.z == 0.0 &&
			this->b_box.x_axis == 0.0 && this->b_box.y_axis == 0.0 && this->b_box.z_axis == 0.0)
	{
		std::cout << "[ERROR] Please see the ellipsoid parameters" << std::endl;
		return -1.0;
	}
	else
	{
		if(distance_x < 0.0 && distance_y < 0.0 && distance_z < 0.0)
		{
			return 0.0;
		}

		if (distance_x < 0.0f)
			distance_x = 0.0f;
		if (distance_y < 0.0f)
			distance_y = 0.0f;
		if (distance_z < 0.0f)
			distance_z = 0.0f;

		float distance = sqrtf(
				distance_x * distance_x +
				distance_y * distance_y +
				distance_z * distance_z);
		return distance;


		//		if (x > this->b_box.minX && x < this->b_box.maxX &&
		//				y > this->b_box.minY && y < this->b_box.maxY &&
		//				z > this->b_box.minZ && z < this->b_box.maxZ)
		//			return true;
		//		else
		//			return false;
		//
		//		if(((((x - this->ellipsoid.center.x) * (x - this->ellipsoid.center.x))/(this->ellipsoid.x_axis*this->ellipsoid.x_axis)) +
		//				(((y - this->ellipsoid.center.y) * (y - this->ellipsoid.center.y))/(this->ellipsoid.y_axis*this->ellipsoid.y_axis)) +
		//				(((z - this->ellipsoid.center.z) * (z - this->ellipsoid.center.z))/(this->ellipsoid.z_axis*this->ellipsoid.z_axis)))  < 1)
		//			return true;
		//		else
		//			return false;
	}

}



