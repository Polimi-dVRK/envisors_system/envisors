#include "envisors/pcl2ros.hpp"

namespace pcl2ros{

void toROSMsg(const pcl::PointCloud<pcl::PointXYZRGB> &pcl_cloud, sensor_msgs::PointCloud2 &cloud){

	pcl::PCLPointCloud2 pcl_pc2;
	pcl::toPCLPointCloud2(pcl_cloud, pcl_pc2);
	moveFromPCL(pcl_pc2, cloud);
}

void moveFromPCL(pcl::PCLPointCloud2 &pcl_pc2, sensor_msgs::PointCloud2 &pc2)
{
	copyPCLPointCloud2MetaData(pcl_pc2, pc2);
	pc2.data.swap(pcl_pc2.data);
}

void copyPCLPointCloud2MetaData(const pcl::PCLPointCloud2 &pcl_pc2, sensor_msgs::PointCloud2 &pc2)
{
	fromPCL_header(pcl_pc2.header, pc2.header);
	pc2.height = pcl_pc2.height;
	pc2.width = pcl_pc2.width;
	fromPCL_field(pcl_pc2.fields, pc2.fields);
	pc2.is_bigendian = pcl_pc2.is_bigendian;
	pc2.point_step = pcl_pc2.point_step;
	pc2.row_step = pcl_pc2.row_step;
	pc2.is_dense = pcl_pc2.is_dense;
}

void fromPCL_header(const pcl::PCLHeader &pcl_header, std_msgs::Header &header)
{
	header.stamp.fromNSec(pcl_header.stamp * 1e3);  // Convert from us to ns
	header.seq = pcl_header.seq;
	header.frame_id = pcl_header.frame_id;
}

void fromPCL_field(const std::vector<pcl::PCLPointField> &pcl_pfs, std::vector<sensor_msgs::PointField> &pfs)
{
	pfs.resize(pcl_pfs.size());
	std::vector<pcl::PCLPointField>::const_iterator it = pcl_pfs.begin();
	int i = 0;
	for(; it != pcl_pfs.end(); ++it, ++i) {
		fromPCL_in(*(it), pfs[i]);
	}
}

void fromPCL_in(const pcl::PCLPointField &pcl_pf, sensor_msgs::PointField &pf)
{
	pf.name = pcl_pf.name;
	pf.offset = pcl_pf.offset;
	pf.datatype = pcl_pf.datatype;
	pf.count = pcl_pf.count;
}

}// namespace

