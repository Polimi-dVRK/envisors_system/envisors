#include "envisors/ros2pcl.hpp"

namespace ros2pcl{

void fromROSMsg(const sensor_msgs::PointCloud2 &cloud, pcl::PointCloud<pcl::PointXYZRGB> &pcl_cloud)
{
	pcl::PCLPointCloud2 pcl_pc2;
	toPCL(cloud, pcl_pc2);
	pcl::fromPCLPointCloud2(pcl_pc2, pcl_cloud);
}

void toPCL(const sensor_msgs::PointCloud2 &pc2, pcl::PCLPointCloud2 &pcl_pc2)
{
	copyPointCloud2MetaData(pc2, pcl_pc2);
	pcl_pc2.data = pc2.data;
}

void copyPointCloud2MetaData(const sensor_msgs::PointCloud2 &pc2, pcl::PCLPointCloud2 &pcl_pc2)
{
	toPCL(pc2.header, pcl_pc2.header);
	pcl_pc2.height = pc2.height;
	pcl_pc2.width = pc2.width;
	toPCL(pc2.fields, pcl_pc2.fields);
	pcl_pc2.is_bigendian = pc2.is_bigendian;
	pcl_pc2.point_step = pc2.point_step;
	pcl_pc2.row_step = pc2.row_step;
	pcl_pc2.is_dense = pc2.is_dense;
}

void toPCL(const std_msgs::Header &header, pcl::PCLHeader &pcl_header)
{
	pcl_header.stamp = header.stamp.toNSec() / 1e3;  // Convert from ns to us
	pcl_header.seq = header.seq;
	pcl_header.frame_id = header.frame_id;
}

void toPCL(const std::vector<sensor_msgs::PointField> &pfs, std::vector<pcl::PCLPointField> &pcl_pfs)
{
	pcl_pfs.resize(pfs.size());
	std::vector<sensor_msgs::PointField>::const_iterator it = pfs.begin();
	int i = 0;
	for(; it != pfs.end(); ++it, ++i) {
		toPCL(*(it), pcl_pfs[i]);
	}
}

void toPCL(const sensor_msgs::PointField &pf, pcl::PCLPointField &pcl_pf)
{
	pcl_pf.name = pf.name;
	pcl_pf.offset = pf.offset;
	pcl_pf.datatype = pf.datatype;
	pcl_pf.count = pf.count;
}

} // namespace

