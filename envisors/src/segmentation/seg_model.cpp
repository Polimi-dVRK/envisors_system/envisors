#include "envisors/segmentation/seg_model.h"


segModel::segModel(int num_hbin, int num_sbin, int num_vbin)
{

	m_prob_c1_c0 = 0.4;
	m_prob_c1_c1 = 0.6;
	m_prob_c0_c1 = 0.1;
	m_prob_c0_c0 = 0.9;
	m_alpha = 0.1;
	//	m_prob_c1_c0 = 0.2;
	//	m_prob_c1_c1 = 0.8;
	//	m_prob_c0_c1 = 0.1;
	//	m_prob_c0_c0 = 0.9;
	//	m_alpha = 0.1;


	m_num_hbin = num_hbin;
	m_num_sbin = num_sbin;
	m_num_vbin = num_vbin;

	m_num_bin = num_hbin * num_sbin + num_vbin;
	m_prob_c0_y = std::vector<float>(m_num_bin, 0.5f);
	m_prob_c1_y = std::vector<float>(m_num_bin, 0.5f);
	m_prob_y_c0 = std::vector<float>(m_num_bin, 0.0);
	m_prob_y_c1 = std::vector<float>(m_num_bin, 0.0);

	m_h_step = 180.0 / m_num_hbin;
	m_s_step = 256.0 / m_num_sbin;
	m_v_step = 256.0 / m_num_vbin;

	m_init_flag = false;
	m_kp_patch_size = 5;
}

segModel::~segModel()
{

}

int segModel::getBinIdx(const cv::Vec3b &pixel) const
{
	float h = pixel[0];
	float s = pixel[1];
	float v = pixel[2];

	int h_idx = int(h/m_h_step);
	int s_idx = int(s/m_s_step);
	int v_idx = int(v/m_v_step);

	int bin_idx;
	if(s < 15 || v < 30)
		bin_idx = m_num_hbin * m_num_sbin + v_idx;
	else
		bin_idx = h_idx * m_num_sbin + s_idx;

	return bin_idx;
}


void segModel::initProb(const cv::Mat& img_hsv, const cv::Rect& search_roi, const cv::Rect& ob_roi)
{

	std::vector<int> pixels_c0(m_num_bin, 0);
	std::vector<int> pixels_c1(m_num_bin, 0);

	for(int row = search_roi.tl().y; row <= search_roi.br().y; ++row)
		for(int col = search_roi.tl().x; col <= search_roi.br().x; ++col)
		{
			cv::Vec3b pixel = img_hsv.at<cv::Vec3b>(row, col);
			int bin_idx = getBinIdx(pixel);
			if(ob_roi.contains(cv::Point(col, row)))
				pixels_c1[bin_idx] += 1;
			else
				pixels_c0[bin_idx] += 1;
		}

	int num_pixel_c0 = std::accumulate(pixels_c0.begin(), pixels_c0.end(), 0);
	int num_pixel_c1 = std::accumulate(pixels_c1.begin(), pixels_c1.end(), 0);
	for(int idx=0; idx<m_num_bin; ++idx)
	{
		m_prob_y_c0[idx] = 1.0 * pixels_c0[idx] / num_pixel_c0;
		m_prob_y_c1[idx] = 1.0 * pixels_c1[idx] / num_pixel_c1;

		int num_pixel_bin = pixels_c0[idx] + pixels_c1[idx];
		if (num_pixel_bin == 0)
		{
			m_prob_c0_y[idx] = 0.5;
			m_prob_c1_y[idx] = 0.5;
		}
		else
		{
			m_prob_c0_y[idx] = 1.0 * pixels_c0[idx] / num_pixel_bin;
			m_prob_c1_y[idx] = 1.0 * pixels_c1[idx] / num_pixel_bin;
		}
	}
	m_init_flag = true;
}

void segModel::updateProb(
		const cv::Mat& img_hsv,
		const cv::Rect& center,
		const std::vector<cv::Point>* hull,
		int roi_width, int roi_height,
		bool init,
		int n_convex_hull)
{
	float width = roi_width;
	float height = roi_height;

	int img_height = img_hsv.rows;
	int img_width = img_hsv.cols;
	bool inside_hull = false;

	m_init_flag = init;

	cv::Rect b_outer_roi = getRoi(cv::Point(center.x, center.y), 2*width, 2*height, img_width, img_height);
	cv::Rect b_inner_roi = getRoi(cv::Point(center.x, center.y), 1.6*width, 1.6*height, img_width, img_height);

	std::vector<int> pixels_c0(m_num_bin, 0);
	std::vector<int> pixels_c1(m_num_bin, 0);

	cv::Mat hull_mask;
	hull_mask.create(img_height, img_width, CV_8UC1);
	hull_mask.setTo(0);

	std::vector<std::vector<cv::Point> > hull_contour(n_convex_hull);

	for (int i = 0; i < n_convex_hull; i++)
	{
		hull_contour[i].resize(hull[i].size());

		for (int j = 0; j < hull[i].size(); j++)
		{
			hull_contour[i][j] = hull[i][j];
		}
	}

	cv::drawContours(hull_mask, hull_contour,-1, cv::Scalar(255), CV_FILLED);

	for(int row=b_outer_roi.tl().y; row<=b_outer_roi.br().y; ++row)
	{
		for(int col=b_outer_roi.tl().x; col<=b_outer_roi.br().x; ++col)
		{
			cv::Vec3b pixel = img_hsv.at<cv::Vec3b>(row, col);
			int bin_idx = getBinIdx(pixel);
			inside_hull = false;
			/*
			double inside_hulls;
			for(int i = 0; i < n_convex_hull; i++)
			{
				inside_hulls = cv::pointPolygonTest(hull[i], cv::Point2f(col,row), false);

				if(inside_hulls >= 0)
				{
					inside_hull = true;
				}
			}*/

			inside_hull = (hull_mask.at<uchar>(row, col) == 255);

			if(inside_hull)
			{
				pixels_c1[bin_idx] += 1;
			}
			else if(b_inner_roi.contains(cv::Point(col, row)))
			{
				pixels_c0[bin_idx] += 1;
			}
		}
	}

	int num_pixel_c0 = std::accumulate(pixels_c0.begin(), pixels_c0.end(), 0);
	int num_pixel_c1 = std::accumulate(pixels_c1.begin(), pixels_c1.end(), 0);
	if(num_pixel_c0 == 0 || num_pixel_c1 == 0)
		return;

	std::vector<float> prob_c0_y(m_num_bin, 0.0);
	std::vector<float> prob_c1_y(m_num_bin, 0.0);

	if(!m_init_flag)
	{
		// first time initialization
		for(int idx=0; idx<m_num_bin; ++idx)
		{
			m_prob_y_c0[idx] = 1.0 * pixels_c0[idx] / num_pixel_c0;
			m_prob_y_c1[idx] = 1.0 * pixels_c1[idx] / num_pixel_c1;

			prob_c0_y[idx] = m_prob_y_c0[idx] * (m_prob_c0_c1 * m_prob_c1_y[idx] + m_prob_c0_c0 * m_prob_c0_y[idx]);
			prob_c1_y[idx] = m_prob_y_c1[idx] * (m_prob_c1_c1 * m_prob_c1_y[idx] + m_prob_c1_c0 * m_prob_c0_y[idx]);
			float sum_bin = prob_c0_y[idx] + prob_c1_y[idx];
			if (sum_bin != 0)
			{
				m_prob_c0_y[idx] = 1.0 * prob_c0_y[idx] / sum_bin;
				m_prob_c1_y[idx] = 1.0 * prob_c1_y[idx] / sum_bin;
			}
		}
		m_init_flag = true;
	}
	else
	{
		for(int idx=0; idx<m_num_bin; ++idx)
		{
			m_prob_y_c0[idx] = m_alpha * pixels_c0[idx] / num_pixel_c0 + (1-m_alpha) * m_prob_y_c0[idx];
			m_prob_y_c1[idx] = m_alpha * pixels_c1[idx] / num_pixel_c1 + (1-m_alpha) * m_prob_y_c1[idx];

			prob_c0_y[idx] = m_prob_y_c0[idx] * (m_prob_c0_c1 * m_prob_c1_y[idx] + m_prob_c0_c0 * m_prob_c0_y[idx]);
			prob_c1_y[idx] = m_prob_y_c1[idx] * (m_prob_c1_c1 * m_prob_c1_y[idx] + m_prob_c1_c0 * m_prob_c0_y[idx]);

			float sum_bin = prob_c0_y[idx] + prob_c1_y[idx];
			if(sum_bin != 0)
			{
				m_prob_c0_y[idx] = 1.0 * prob_c0_y[idx] / sum_bin;
				m_prob_c1_y[idx] = 1.0 * prob_c1_y[idx] / sum_bin;
			}
		}
	}

	std::cout << "updating segmentation model" << std::endl;
}

float segModel::getKpProb(const cv::Point& kp_pt, const cv::Mat& img_hsv) const
{
	if (!m_init_flag)
		return 0.5;

	int img_height = img_hsv.rows;
	int img_width = img_hsv.cols;

	cv::Rect kp_roi = getRoi(kp_pt, m_kp_patch_size, m_kp_patch_size, img_width, img_height);
	float kp_prob_c1 = 0.0f;
	int num_pixel = 0;
	for( int row=kp_roi.tl().y; row<=kp_roi.br().y; ++row )
		for( int col=kp_roi.tl().x; col<=kp_roi.br().x; ++col )
		{
			cv::Vec3b pixel = img_hsv.at<cv::Vec3b>(row, col);
			int bin_idx = getBinIdx(pixel);
			kp_prob_c1 += m_prob_c1_y[bin_idx];
			num_pixel++;
		}
	kp_prob_c1 /= num_pixel;
	return kp_prob_c1;
}

float segModel::getsingleKpProb(const cv::Point &kp_pt, const cv::Mat &img_hsv)const
{
	if (!m_init_flag)
		return 0.5;

	float kp_prob_c1 = 0.0f;

	cv::Vec3b pixel = img_hsv.at<cv::Vec3b>(kp_pt.y, kp_pt.x);
	int bin_idx = getBinIdx(pixel);
	kp_prob_c1 = m_prob_c1_y[bin_idx];

	return kp_prob_c1;
}

void segModel::showProbMap(
		const cv::Mat& img_hsv,
		const cv::Rect& roi,
		cv::Mat& img,
		double& th,
		const std::string &win_name) const
{

	if (!m_init_flag)
		return;
	int img_height = img_hsv.rows;
	int img_width = img_hsv.cols;

	cv::Mat prob_map = cv::Mat::zeros(img_hsv.size(), CV_32F);
	for(int row=0; row<img_height; ++row){
		for(int col=0; col<img_width; ++col){
			if(roi.contains(cv::Point(col, row)))
			{
				cv::Vec3b pixel = img_hsv.at<cv::Vec3b>(row, col);
				int bin_idx = getBinIdx(pixel);
				prob_map.at<float>(row, col) = m_prob_c1_y[bin_idx];
			}
		}
	}
	cv::Mat prob_col_map = colorMap(prob_map);
	// draw color bar
	cv::Mat rect( img.rows ,100, CV_32F);
	double min_val,max_val;

	for(int i = 0; i < rect.rows; i++)
	{
		for(int j = 0; j < rect.cols; j++)
		{
			rect.at<float>(i,j) = i;
		}
	}
	//double min_val, max_val;
	cv::minMaxIdx(rect, &min_val, &max_val);
	float ratio = 255.0 / max_val;
	cv::Mat norm_img = cv::Mat(rect.size(), CV_32F);
	norm_img = ratio * rect;
	norm_img.convertTo(norm_img, CV_8UC1);

	cv::Mat col_img;
	cv::applyColorMap(norm_img, col_img, cv::COLORMAP_JET);

	//std::cout << "fail: " << act_value << std::endl;

	int row_inv;
	for(int row=0; row <  rect.rows; ++row)
	{
		row_inv = rect.rows - row - 1;

		for(int col=0; col < rect.cols; ++col)
		{
			if(row < 5)
			{
				prob_col_map.at<uchar>(row_inv,col+60) = 0;
			}
			else if(row > rect.rows -5)
			{
				prob_col_map.at<uchar>(row_inv,col+60) = 0;
			}
			else if (row >= (int)(th * rect.rows) - 4 &&
					row <= (int)(th * rect.rows) + 4)
			{
				prob_col_map.at<uchar>(row_inv,col+60) = 0;
			}
			else
			{
				prob_col_map.at<uchar>(row_inv,col+60) = col_img.at<uchar>(row,col);
			}
		}
	}

	prob_col_map.copyTo(img);
}



