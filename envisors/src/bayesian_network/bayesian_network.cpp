/*
 * bayesian_network.cpp
 *
 *  Created on: Mar 14, 2016
 *      Author: veronica
 */

#include "envisors/bayesian_network/bayesian_network.hpp"


/*
The function compute the posterior probability of a failure in KLT Tracker,
returns 0 for not failure and 1 for failure
 */

void BNetwork::init_network(
		BNet_params &params,
		int nFeatures_max)			// 0.6
{

	params.nFeatSigma = params.nFeatSigma * nFeatures_max;
	params_ = params;
	nFeatures_max_ = nFeatures_max;

	params.nFeatSigma = 200;
	nFeatures_max_ = 100;
	// configure the network
	this->bn.set_number_of_nodes(6);
	this->bn.add_edge(F, A);
	this->bn.add_edge(F, B);
	this->bn.add_edge(F, C);
	this->bn.add_edge(F, D);
	this->bn.add_edge(F, E);

	// set the number of state of each nodes
	dlib::bayes_node_utils::set_node_num_values(this->bn, A, nFeatures_max_);
	dlib::bayes_node_utils::set_node_num_values(this->bn, B, 100); // percentage
	dlib::bayes_node_utils::set_node_num_values(this->bn, C, 2);
	dlib::bayes_node_utils::set_node_num_values(this->bn, D, params.distribution_std_max);
	dlib::bayes_node_utils::set_node_num_values(this->bn, E, 100); // Percentage
	dlib::bayes_node_utils::set_node_num_values(this->bn, F, 2);

	dlib::assignment parent_state;
	// Now we will enter all the conditional probability information for each node.
	// Each node's conditional probability is dependent on the state of its parents.
	// To specify this state we need to use the assignment object.  This assignment
	// object allows us to specify the state of each nodes parents.

	/*
	 * IF F = 0
	 */

	double p_F;
	double p_F_sum = 0.0;

	parent_state.clear();

	dlib::bayes_node_utils::set_node_probability(this->bn, F, 0, parent_state, 0.5);
	dlib::bayes_node_utils::set_node_probability(this->bn, F, 1, parent_state, 0.5);

	parent_state.add(this->F, 0);

	/*
	 * A
	 */

	// compute all value
	for(int a = 0; a < nFeatures_max_; a++)
	{
		p_F = gsl_ran_gaussian_pdf (a, params.nFeatSigma);
		p_F_sum += p_F;

	}
	// assign pdf values
	//	std::cout << "a : ";
	for(int a = 0; a < nFeatures_max_; a++)
	{
		p_F = gsl_ran_gaussian_pdf (a, params.nFeatSigma);
		dlib::bayes_node_utils::set_node_probability(
				this->bn, this->A, nFeatures_max_ - a -1, parent_state, p_F / p_F_sum);
		//		std::cout << nFeatures_max_ - a -1 << ", ";


	}
	//	std::cout << std::endl;

	/*
	 * B
	 */

	// compute all value
	p_F_sum = 0.0;
	for(int b = 0; b < 100; b++)
	{
		//		double b_temp = b / 100;
		p_F = gsl_ran_gaussian_pdf (b, params.percLostFeatSigma);
		p_F_sum += p_F;
	}

	// assign pdf values
	//	std::cout << "b : ";
	for(int b = 0; b < 100; b++)
	{
		//		double b_temp = b / 100;
		p_F = gsl_ran_gaussian_pdf (b, params.percLostFeatSigma);
		dlib::bayes_node_utils::set_node_probability(
				this->bn, this->B, b, parent_state, p_F / p_F_sum);

		//		std::cout << b << ", ";
	}
	//	std::cout << std::endl;


	/*
	 * C
	 */
	// homography = 0 -- invalid
	// homography = 1 -- valid

	dlib::bayes_node_utils::set_node_probability(
			this->bn, this->C, 0, parent_state, params.validityHomographySigma);
	dlib::bayes_node_utils::set_node_probability(
			this->bn, this->C, 1, parent_state, 1.0 - params.validityHomographySigma);

	/*
	 * D 15
	 */
	// compute all value
	p_F_sum = 0.0;
	for(int d = 0; d < params.distribution_std_max; d++)
	{
		//		double d_temp = d / distribution_std_max_;
		p_F = gsl_ran_gaussian_pdf (d, params.stdDistributionSigma);
		p_F_sum += p_F;
	}
	// assign pdf values
	//	std::cout << "d : ";
	for(int d = 0; d < params.distribution_std_max; d++)
	{
		//		double d_temp = d / distribution_std_max_;
		p_F = gsl_ran_gaussian_pdf (d, params.stdDistributionSigma);
		dlib::bayes_node_utils::set_node_probability(
				this->bn, this->D, d, parent_state, p_F/p_F_sum);

		//		std::cout << d << ", ";
	}
	//	std::cout << std::endl;


	/*
	 * E
	 */
	// compute all value
	p_F_sum = 0.0;
	for(int b = 0; b < 100; b++)
	{
		p_F = gsl_ran_gaussian_pdf (b, params.bhDistanceSigma);
		p_F_sum += p_F;
	}

	// assign pdf values
	for(int b = 0; b < 100; b++)
	{
		p_F = gsl_ran_gaussian_pdf (b, params.bhDistanceSigma);
		dlib::bayes_node_utils::set_node_probability(
				this->bn, this->E, b, parent_state, p_F / p_F_sum);
	}


	/*
	 * IF F = 1
	 */

	// tracking failure
	parent_state[F] = 1;
	p_F = 0.0;
	p_F_sum = 0.0;

	/*
	 * A 4
	 */
	// compute all value
	for(int a = 0; a < nFeatures_max_; a++)
	{
		p_F = gsl_ran_exponential_pdf (a, params.nFeatSigmaF);
		p_F_sum += p_F;

	}
	// assign pdf values
	//	std::cout << "a : ";
	for(int a = 0; a < nFeatures_max_; a++)
	{
		p_F = gsl_ran_exponential_pdf (a, params.nFeatSigmaF);
		dlib::bayes_node_utils::set_node_probability(
				this->bn, this->A, a , parent_state, p_F / p_F_sum);

		//		std::cout << a << ", ";
	}
	//	std::cout << std::endl;

	/*
	 * B 18
	 */
	// compute all value
	p_F_sum = 0.0;
	for(int b = 0; b < 100; b++)
	{
		//		double b_temp = b / 100;
		p_F = gsl_ran_gaussian_pdf (b, params.percLostFeatSigmaF);
		p_F_sum += p_F;
	}

	// assign pdf values
	//	std::cout << "b : ";
	for(int b = 0; b < 100; b++)
	{
		//		double b_temp = b / 100;
		p_F = gsl_ran_gaussian_pdf (b, params.percLostFeatSigmaF);
		dlib::bayes_node_utils::set_node_probability(
				this->bn, this->B, 99- b, parent_state, p_F / p_F_sum);

		//		std::cout << 99 - b << ", ";
	}
	//	std::cout << std::endl;

	/*
	 * C
	 */
	// homography = 0 -- invalid
	// homography = 1 -- valid

	dlib::bayes_node_utils::set_node_probability(
			this->bn, this->C, 0, parent_state, params.validityHomographySigmaF);
	dlib::bayes_node_utils::set_node_probability(
			this->bn, this->C, 1, parent_state, 1.0 - params.validityHomographySigmaF);

	/*
	 * D 90
	 */
	// compute all value
	p_F_sum = 0.0;
	for(int d = 0; d < params.distribution_std_max; d++)
	{
		//		double d_temp = d / distribution_std_max_;
		p_F = gsl_ran_gaussian_pdf (d, params.stdDistributionSigmaF);
		p_F_sum += p_F;

	}

	// assign pdf values
	//	std::cout << "d : ";
	for(int d = 0; d < params.distribution_std_max; d++)
	{
		//		double d_temp = d / distribution_std_max_;
		p_F = gsl_ran_gaussian_pdf (d, params.stdDistributionSigmaF);
		dlib::bayes_node_utils::set_node_probability(
				this->bn, this->D, params.distribution_std_max - d -1, parent_state, p_F/p_F_sum);
		//		std::cout << p_F/p_F_sum << std::endl;
		//		std::cout << distribution_std_max_ - d -1 << ", ";

	}
	//	std::cout << std::endl;

	/*
	 * E
	 */
	// compute all value
	p_F_sum = 0.0;
	for(int b = 0; b < 100; b++)
	{
		p_F = gsl_ran_gaussian_pdf (b, params.bhDistanceSigmaF);
		p_F_sum += p_F;
	}

	// assign pdf values
	for(int b = 0; b < 100; b++)
	{
		p_F = gsl_ran_gaussian_pdf (b, params.bhDistanceSigmaF);
		dlib::bayes_node_utils::set_node_probability(
				this->bn, this->E, 99- b, parent_state, p_F / p_F_sum);
	}


	create_moral_graph(this->bn, this->join_tree);
	create_join_tree(this->join_tree, this->join_tree);
}

double BNetwork::compute_joint_probability(
		int nFeat,
		double percLostFeat,
		bool validityHomography,
		double stdDistribution,
		double bhDistance)
{
	ros::Time timeBegin;
	ros::Time timeEnd;
	ros::Duration deltaTime;

	// nfeat
	if(nFeat < 0)
		nFeat = 0;
	if(nFeat > nFeatures_max_-1)
		nFeat = nFeatures_max_ -1;


	int temp_perc = (int)(percLostFeat*100.0);
	int temp_std = (int)stdDistribution;
	int temp_bhDistance = (int)(bhDistance*100.0);

	// percLostFeat
	if(temp_perc < 0)
		temp_perc = 0;
	if(temp_perc > 99)
		temp_perc = 99;
	// stdDistribution
	if(temp_std < 0)
		temp_std = 0;
	if(temp_std > params_.distribution_std_max -1)
		temp_std = params_.distribution_std_max -1;

	timeBegin = ros::Time::now();
	dlib::bayes_node_utils::set_node_value(this->bn, this->A, nFeat);
	dlib::bayes_node_utils::set_node_as_evidence(this->bn, this->A);
	dlib::bayes_node_utils::set_node_value(this->bn, this->B, temp_perc);
	dlib::bayes_node_utils::set_node_as_evidence(this->bn, this->B);
	dlib::bayes_node_utils::set_node_value(this->bn, this->C, validityHomography);
	dlib::bayes_node_utils::set_node_as_evidence(this->bn, this->C);
	dlib::bayes_node_utils::set_node_value(this->bn, this->D, temp_std);
	dlib::bayes_node_utils::set_node_as_evidence(this->bn, this->D);
	dlib::bayes_node_utils::set_node_value(this->bn, this->E, temp_bhDistance);
	dlib::bayes_node_utils::set_node_as_evidence(this->bn, this->E);
	timeEnd = ros::Time::now();
	deltaTime = timeEnd - timeBegin;
	std::cout << "[TIME] bayes def:" << deltaTime.toSec() << std::endl;

	timeBegin = ros::Time::now();
	dlib::bayesian_network_join_tree solution_with_evidence(this->bn, this->join_tree);
	timeEnd = ros::Time::now();
	deltaTime = timeEnd - timeBegin;
	std::cout << "[TIME] bayes1 prob:" << deltaTime.toSec() << std::endl;
	timeBegin = ros::Time::now();
	double prob = solution_with_evidence.probability(F)(1);
	timeEnd = ros::Time::now();
	deltaTime = timeEnd - timeBegin;
	std::cout << "[TIME] bayes2 prob:" << deltaTime.toSec() << std::endl;

#ifdef DEBUG
	std::cout << "-----------"  << std::endl;
	std::cout << "N features              : " << nFeat << std::endl;
	std::cout << "Percentage lost features: " << percLostFeat << std::endl;
	std::cout << "Homography valid        : " << validityHomography << std::endl;
	std::cout << "STD distribution        : " << stdDistribution << std::endl;
	std::cout << "                           " << stdDistribution << std::endl;
	std::cout << "Failure PROBABILITY: " << prob << std::endl;
	std::cout << "-----------"  << std::endl;
#endif

	return prob;
}


//int main(int argc, char **argv)
//{
//	BNetwork BNet;
//
//	BNet.init_params(100, 360.0);
//	BNet.init_network();
//
//	BNet.compute_joint_probability(3, 0.7, false, 10);
//	BNet.compute_joint_probability(30, 0.2, true, 10);
//	BNet.compute_joint_probability(50, 0.5, true, 100);
//	BNet.compute_joint_probability(10, 0.2, true, 200);
//}


