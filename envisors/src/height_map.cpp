

#include "envisors/height_map.hpp"


/* Set ground plane*/
void HeighMap::setPlane(const double* plane_coeff)
{
	//TODO
//	plane_coeffs_[0] = plane_coeff;
//	plane_coeffs_[1] = plane_coeff;
//	plane_coeffs_[2] = plane_coeff;
//	plane_coeffs_[3] = plane_coeff;
}

/**/
void HeighMap::setBinSize(double bin_size)
{
	bin_size_ = bin_size;
}

/**/
void HeighMap::compute(pcl::PointCloud<pcl::PointXYZRGB>& pc)
{
	if (pc.empty())
		return;

	cv::Mat count;
	pcl::PointCloud<pcl::PointXYZRGB>::iterator pc_it;

	pc_it = pc.begin();
	this->minX_ = pc_it->x;
	this->minY_ = pc_it->y;
	this->minZ_ = pc_it->z;
	this->maxX_ = pc_it->x;
	this->maxY_ = pc_it->y;
	this->maxZ_ = pc_it->z;


	for(pc_it = pc.begin(); pc_it != pc.end(); pc_it++)
	{
		/*find max X*/
		if(pc_it->x > this->maxX_)
		{
			this->maxX_ = pc_it->x;
		}
		/*find max Y*/
		if(pc_it->y > this->maxY_)
		{
			this->maxY_ = pc_it->y;
		}
		/*find max Z*/
		if(pc_it->z > this->maxZ_)
		{
			this->maxZ_ = pc_it->z;
		}

		/*find max X*/
		if(pc_it->x < this->minX_)
		{
			this->minX_ = pc_it->x;
		}
		/*find max Y*/
		if(pc_it->y < this->minY_)
		{
			this->minY_ = pc_it->y;
		}
		/*find max Z*/
		if(pc_it->z < this->minZ_)
		{
			this->minZ_ = pc_it->z;
		}
	}

	std::cout << "x min " << this->minX_ << " y min " << this->minY_ << " z_min " << this->minZ_ << std::endl;
	std::cout << "x max " << this->maxX_ << " y max " << this->maxY_ << " z_max " << this->maxZ_ << std::endl;

	/*
	this->minX_ = -0.13;
	this->maxX_ = 0.11;
	this->minY_ = 0.0;
	this->maxY_ = 0.26;
	this->minZ_ = -0.11;
	this->maxZ_ = 0.18;*/

	/*set origin height map*/
	this->origin_.x = this->minX_;
	this->origin_.y = this->minY_;

	/*set height map size*/
	this->size_.x = this->maxX_ - this->minX_;
	this->size_.y = this->maxY_ - this->minY_;
	int rows = (this->size_.x / this->bin_size_) +1;
	int cols = (this->size_.y / this->bin_size_) +1;

	//this->height_map_.create(rows, cols , CV_64F);
	this->height_map_.create(rows, cols, CV_32FC3);
	count.create(rows, cols , CV_8UC1);

	this->height_map_.setTo(0);
	count.setTo(0);

	int bin_x;
	int bin_y;

	for(pc_it = pc.begin(); pc_it != pc.end(); pc_it++)
	{
		bin_x = (pc_it->x - this->origin_.x) / this->bin_size_;
		bin_y = (pc_it->y - this->origin_.y) / this->bin_size_;

		if (bin_x < 0 || bin_y < 0 || bin_x >= rows || bin_y >= cols)
			continue;

		this->height_map_.at<cv::Point3f>(bin_x, bin_y).z += pc_it->z;
		count.at<unsigned char>(bin_x, bin_y) ++;
	}

	for(int i = 0; i < this->height_map_.rows; i++)
	{
		for(int j = 0; j < this->height_map_.cols; j++)
		{
			this->height_map_.at<cv::Point3f>(i, j).x =
					(float)i * this->bin_size_ + this->origin_.x;
			this->height_map_.at<cv::Point3f>(i, j).y =
					(float)j * this->bin_size_ + this->origin_.y;

			if(count.at<unsigned char>(i, j) != 0)
			{
				this->height_map_.at<cv::Point3f>(i, j).z =
						this->height_map_.at<cv::Point3f>(i, j).z/count.at<unsigned char>(i, j);
			}
			else
			{
				this->height_map_.at<cv::Point3f>(i, j).z = this->minZ_;
				//this->height_map_.at<cv::Point3f>(i, j).z = -1;
			}
		}
	}

}

/**/
double HeighMap::getDistance(const cv::Point3f& pt)
{
	double distance;

	/*check limits*/
	if(pt.x < this->minX_ || pt.y < this->minY_ ||
			pt.x > this->maxX_ || pt.y > this->maxY_)
	{
		return 0.0;
	}

	int bin_x;
	int bin_y;

	/* find bin coordinates*/
	bin_x = (pt.x - this->origin_.x) / this->bin_size_;
	bin_y = (pt.y - this->origin_.y) / this->bin_size_;

	/*compute distance*/
	distance = pt.z - this->height_map_.at<double>(bin_x, bin_y);

	return distance;

}

/**/
bool HeighMap::getInside(const cv::Point3f& pt)
{
	double distance;

	/*get distance*/
	distance = getDistance(pt);

	if(distance < 0.0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

/**/
void HeighMap::getHeightMap(cv::Mat& height_map)
{
	height_map = height_map_;
}

/**/
void HeighMap::getSize(cv::Point2f& size)
{
	size = size_;
}

void HeighMap::getZmax(double& z_max)
{
	z_max = this->maxZ_;
}

void HeighMap::getZmin(double& z_min)
{
	z_min = this->minZ_;
}
