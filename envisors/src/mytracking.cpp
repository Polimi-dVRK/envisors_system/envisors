/**
 * @author 		Veronica Penza
 * @version 	1.0
 * @date 		22 Gen 2016
 * @brief 		mytracking */

#include "envisors/mytracking.hpp"

MyTracking::MyTracking(bool KLT, bool KLT_openCV)
{
	//TbD
	this->points_detection = NULL;
	this->points = NULL;

	// OPENCV
	//GFTT
	TrackingParam.GFTT_params.maxCorners = 200;
	TrackingParam.GFTT_params.qualityLevel = 0.01;
	TrackingParam.GFTT_params.minDistance = 1;
	TrackingParam.GFTT_params.blockSize = 21;
	TrackingParam.GFTT_params.useHarrisDetector = false;
	TrackingParam.GFTT_params.k = 0.04;

	// optical flow
	TrackingParam.winsize_opt_flow = cv::Size(21,21);

	//KLT Library

	TrackingParam.klt_params.paramTracking_mindist = 6;
	TrackingParam.klt_params.paramTracking_window_width = 11;
	TrackingParam.klt_params.paramTracking_window_height = 11;
	TrackingParam.klt_params.paramTracking_smoothBeforeSelecting = TRUE;
	TrackingParam.klt_params.paramTracking_lighting_insensitive = TRUE;
	TrackingParam.klt_params.paramTracking_min_eigenvalue = 1;
	/* for affine mapping */
	TrackingParam.klt_params.paramTracking_affineConsistencyCheck = 1;
	TrackingParam.klt_params.paramTracking_affine_window_width = 9;
	TrackingParam.klt_params.paramTracking_affine_window_height = 9;
	TrackingParam.klt_params.paramTracking_affine_max_residue = 15.0;

	//BNet
	//liver-movement
	//	TrackingParam.bnet_params.nFeatures_max = 103;

	//liver-4
	//	TrackingParam.bnet_params.nFeatures_max = 1036;
	//prostatectomy 720*576
	//	TrackingParam.bnet_params.nFeatures_max = 414;


	TrackingParam.th_failure = 0.4;
	//object segmentation
	TrackingParam.th_segmentation = 0.7; // 0.7

	//boolean
	TrackingParam.opencv_tracking = false;

	//	TrackingParam.th_redetection

	this->n_cluster = 8; //8

	std::cout <<"___________________" << std::endl;

	TrackingParam.bnet_params.distribution_std_max = 360.0;
	TrackingParam.bnet_params.nFeatSigma = 0.3;				// nFeatures_max / 3
	TrackingParam.bnet_params.nFeatSigmaF = 6;			// 4
	TrackingParam.bnet_params.percLostFeatSigma = 18;		// 15
	TrackingParam.bnet_params.percLostFeatSigmaF = 53;		// 30
	TrackingParam.bnet_params.validityHomographySigma = 0.1;	// 0.01
	TrackingParam.bnet_params.validityHomographySigmaF = 0.47;// 0.6
	TrackingParam.bnet_params.stdDistributionSigma = 43;	// 15
	TrackingParam.bnet_params.stdDistributionSigmaF = 165;	// 150
	TrackingParam.bnet_params.bhDistanceSigma = 25;			// 0.1
	TrackingParam.bnet_params.bhDistanceSigmaF = 50;
	std::cout <<"___________________" << std::endl;

	//Initialization
	if(!TrackingParam.opencv_tracking)
	{
		// IT
		this->points_detection = NULL;
		this->points = NULL;
		this->imgKLT = NULL;
		this->specularityMapKLT = NULL;
	}
	else if(TrackingParam.opencv_tracking)
	{
		//		this->points_detection = NULL;
		//		this->points = NULL;
	}

}

MyTracking::~MyTracking()
{
	if(KLT)
	{
		delete[] imgKLT;
		delete[] specularityMapKLT;
	}
}

/*IT: Iterative/Intensity Detection Method*/
// Lucas-Kanade-Thomasi Detection and tracking

void MyTracking::setKLTParamDetector(
		const std::string& featureDetectorName,
		std::vector<cv::Point2f>* points_detection,
		cv::Size img_size)
{

	int cols = img_size.width; //360
	int rows = img_size.height; //288
	//	nFeatures = 10000;
	//	nFeatures = (img_size.width * img_size.height)/500;
	nFeatures = 400;
	//	nFeatures = (img_size.width * img_size.height)/500.0;

	this->points_detection = points_detection;
	this->featureDetectorName = featureDetectorName;

	this->featureList = KLTCreateFeatureList(nFeatures);
	//	   imgKLT = new unsigned char[imgMat.cols * imgMat.rows * sizeof(char)]();
	if (imgKLT == NULL)  {
		std::cout << "Image memory not allocated" <<std::endl;
		imgKLT = new unsigned char[cols * rows * sizeof(char)]();
	}

	this->paramTracking = KLTCreateTrackingContext();
	//	this->paramTracking->mindist = 20;
	//	this->paramTracking->window_width  = 9;
	//	this->paramTracking->window_height = 9;


}

void MyTracking::setKLTParamTracking(cv::Size& winSize){

	/* Set values to default values */
	this->paramTracking->mindist = TrackingParam.klt_params.paramTracking_mindist;
	this->paramTracking->window_width = TrackingParam.klt_params.paramTracking_window_width;
	this->paramTracking->window_height = TrackingParam.klt_params.paramTracking_window_height;
	//  this->paramTracking->sequentialMode = FALSE;
	this->paramTracking->smoothBeforeSelecting = TrackingParam.klt_params.paramTracking_smoothBeforeSelecting;
	this->paramTracking->lighting_insensitive = TrackingParam.klt_params.paramTracking_min_eigenvalue;
	this->paramTracking->min_eigenvalue = 1;
	//  this->paramTracking->min_determinant = 0.01f;
	//  this->paramTracking->max_iterations = 10;
	//  this->paramTracking->min_displacement = 0.1f;
	//  this->paramTracking->max_residue = 5;
	//  this->paramTracking->grad_sigma = 1.0f;
	//  this->paramTracking->smooth_sigma_fact = 0.1f;
	//  this->paramTracking->pyramid_sigma_fact = 0.9f;
	//  this->paramTracking->step_factor = 1.0f;
	//  this->paramTracking->nSkippedPixels = 0;
	//  this->paramTracking->pyramid_last = NULL;
	//  this->paramTracking->pyramid_last_gradx = NULL;
	//  this->paramTracking->pyramid_last_grady = NULL;
	/* for affine mapping */
	this->paramTracking->affineConsistencyCheck = TrackingParam.klt_params.paramTracking_affineConsistencyCheck;
	this->paramTracking->affine_window_width = TrackingParam.klt_params.paramTracking_affine_window_width;
	this->paramTracking->affine_window_height = TrackingParam.klt_params.paramTracking_affine_window_height;
	//  this->paramTracking->affine_max_iterations = 10;
	this->paramTracking->affine_max_residue = TrackingParam.klt_params.paramTracking_affine_max_residue;
	//  this->paramTracking->affine_min_displacement = 1.5f;
	//  this->paramTracking->affine_max_displacement_differ = affine_max_displacement_differ;

	////  KLTChangeTCPyramid(this->paramTracking, 15);
	////  KLTUpdateTCBorder(this->paramTracking);
}

void MyTracking::detectKLTFeatures(
		const cv::Mat& img,
		const cv::Mat& mask,
		std::vector<cv::Point2f>& detcted_points)
{

	if (this->specularityMapKLT == NULL)  {
		this->specularityMapKLT = new unsigned char[img.cols * img.rows * sizeof(char)]();
	}


	//convert image to  unsigned char * img
	fromMatImgToKLTImg(img, imgKLT);
	fromMatImgToKLTImg(mask, specularityMapKLT);
	//	this->points_detection->clear();

	//select features
	KLTSelectGoodFeatures(this->paramTracking, imgKLT, img.cols, img.rows,
			this->featureList, specularityMapKLT);

	// from KLTfeatures to std::vector<cv::Point2f>
	fromLKTFeaturesToCVFeatures(featureList, *this->points_detection );

	detcted_points = *this->points_detection;

}

void MyTracking::redetectKLTFeatures(
		const cv::Mat& img,
		const cv::Mat& mask,
		std::vector<cv::Point2f>& points,
		std::vector<uchar>& status)
{
	fromMatImgToKLTImg(img, imgKLT);
	fromMatImgToKLTImg(mask, specularityMapKLT);

	KLTReplaceLostFeatures(this->paramTracking, imgKLT,
			img.cols, img.rows, specularityMapKLT,
			this->featureList);

	fromLKTFeaturesToCVFeatures(featureList, points);

	status.resize(points.size(), 0);

	for(int i = 0; i < this->featureList->nFeatures; i++)
	{
		if (this->featureList->feature[i]->val >= 0 &&
				this->featureList->feature[i]->x > 0 &&
				this->featureList->feature[i]->y > 0)
		{
			status[i] = 1;
		}
		else
		{
			status[i] = 0;
		}
	}
}



void MyTracking::trackingKLTFeatures(
		const cv::Mat img_prev,
		const cv::Mat img_curr,
		cv::Mat mask,
		std::vector<cv::Point2f>& points_prev,
		std::vector<cv::Point2f>& points,
		std::vector<uchar>& status)
{
	if(img_prev.empty())
		img_curr.copyTo(img_prev);
	//	std::cout << "LKT" << std::endl;
	unsigned char imgKLT_prev[img_prev.cols * img_prev.rows * sizeof(char)];
	unsigned char imgKLT_curr[img_prev.cols * img_prev.rows * sizeof(char)];

	//convert image to  unsigned char * img
	fromMatImgToKLTImg(img_prev, imgKLT_prev);
	fromMatImgToKLTImg(img_curr, imgKLT_curr);

	//std::cout << "N features: " << this->featureList->nFeatures << std::endl;
	KLTTrackFeatures(this->paramTracking, imgKLT_prev, imgKLT_curr,
			img_prev.cols, img_prev.rows, this->featureList);


	fromLKTFeaturesToCVFeatures(featureList, points);

	//	std::cout << "points" << points.size() << std::endl;

	status.resize(points.size(), 0);

	for(int i = 0; i < this->featureList->nFeatures; i++)
	{
		if (this->featureList->feature[i]->val >= 0 &&
				this->featureList->feature[i]->x > 0 &&
				this->featureList->feature[i]->y > 0)
		{
			status[i] = 1;
		}
		else
		{
			status[i] = 0;
		}
	}

	//	for(int i = 0; i < this->featureList->nFeatures; i++)
	//	{
	//		if (!(this->featureList->feature[i]->val >= 0 &&
	//				this->featureList->feature[i]->x > 0 &&
	//				this->featureList->feature[i]->y > 0))
	//		{
	//			status[i] = 0;
	//		}
	//	}

}


void MyTracking::fromKLTImgToMatImg(
		const unsigned char* imgKLT,
		cv::Mat& imgMat,
		const int rows, const int cols)
{

	if (imgKLT == NULL)  {
		std::cout << "Image memory not allocated" <<std::endl;
	}

	for(int row = 0; row < rows; row++){
		for(int col = 0; col < cols; col++){
			//			std::cout << "row" << row << "col" << col << std::endl;
			imgMat.at<uchar>(row, col) = imgKLT[row * cols+col];
		}
	}
}

void MyTracking::fromMatImgToKLTImg(
		const cv::Mat imgMat,
		unsigned char* imgKLT)
{

	for(int row = 0; row < imgMat.rows; row++){
		for(int col = 0; col < imgMat.cols; col++){
			imgKLT[row * imgMat.cols + col] = imgMat.at<uchar>(row, col);
		}
	}

}

void MyTracking::fromLKTFeaturesToCVFeatures(
		const KLT_FeatureList featuresLKT,
		std::vector<cv::Point2f>& featuresCV)
{
	cv::Point2f temp;
	featuresCV.clear();

	for(int i = 0; i < featuresLKT->nFeatures; i++)
	{
		temp.x = featuresLKT->feature[i]->x;
		temp.y = featuresLKT->feature[i]->y;
		featuresCV.push_back(temp);
	}
	//	std::cout << "size " << featuresCV.size() << std::endl;

}

/*IT: OpenCV Implementation  */
// Lucas-Kanade-Thomasi Detection and tracking

void MyTracking::setParamOpenCVDetector(
		const std::string& featureDetectorName,
		DetectorParameters& param)
{

	if(featureDetectorName.empty())
	{
		std::cout << "[ERROR] No detector name found." << std::endl;
	}
	else{
		// set parameters
		if (!featureDetectorName.compare("BRISK"))
		{
			this->detector = cv::BRISK::create();
		}

		else if(!featureDetectorName.compare("FAST"))
		{
			this->detector = cv::FastFeatureDetector::create();
		}
		// ORB detector
		else if(!featureDetectorName.compare("ORB"))
		{
			this->detector = cv::ORB::create(
					/*nfeatures*/ 1000,
					/*scaleFactor*/ 1.2f,
					/*nlevels*/ 4,
					/*edgeThreshold*/ 31,
					/*firstLevel*/ 0,
					/*WTA_K*/ 4,
					/*scoreType*/ cv::ORB::HARRIS_SCORE,
					/*patchSize*/ 31,
					/*fastThreshold*/ 1);
		}
		// Good Features to track detector
		else if(!featureDetectorName.compare("GFTT"))
		{
			this->detector = cv::GFTTDetector::create(
					this->TrackingParam.GFTT_params.maxCorners,
					this->TrackingParam.GFTT_params.qualityLevel,
					this->TrackingParam.GFTT_params.minDistance,
					this->TrackingParam.GFTT_params.blockSize ,
					this->TrackingParam.GFTT_params.useHarrisDetector,
					this->TrackingParam.GFTT_params.k);

		}
		else if(!featureDetectorName.compare("STAR"))
		{
			//this->detector = cv::StarDetector::create();
		}

		this->DetectorsParam = param;
	}
}

void MyTracking::setParamOpenCVTracking(cv::Size& winSize){
	this->winSize = winSize;
}

void MyTracking::detectOpenCVFeatures(
		const cv::Mat& img,
		const cv::Mat& mask,
		std::vector<cv::Point2f>& points_detected)
{

	if(this->detector != NULL)
	{
		this->detected_points.clear();
		// detect features
		this->detector->detect(img, this->detected_points, mask);
		this->numberFeaturesDetected = this->detected_points.size();

		cv::KeyPoint::convert(this->detected_points, points_detected);
#ifdef DEBUG
		std::cout << "Number of features detected: " << this->numberFeaturesDetected << std::endl;
#endif

	}
	else
	{
#ifdef DEBUG
		std::cout << "[ERROR] Detector " << this->featureDetectorName.c_str()<< std::endl;
#endif


	}
}

void MyTracking::trackingOpenCVFeatures(
		const cv::Mat img_prev,
		const cv::Mat img_curr,
		cv::Mat mask,
		cv::TermCriteria criteria,
		std::vector<cv::Point2f>& points_prev,
		std::vector<cv::Point2f>& points,
		std::vector<uchar>& status)
{
	/* tracking Lucas-Kanade*/

	std::vector<float> error_;

	if(img_prev.empty())
		img_curr.copyTo(img_prev);

	cv::TermCriteria criteria1(CV_TERMCRIT_ITER | CV_TERMCRIT_EPS, 20, 0.3);

	calcOpticalFlowPyrLK(
			img_prev, img_curr,
			points_prev, points,
			status, error_,
			winSize, //size of the search window at each pyramid level.
			4, // pyramidal level
			criteria1, /*specifying the termination criteria of the iterative search algorithm
		    after the specified maximum number of iterations criteria.maxCount or when the search window
			moves by less than criteria.epsilon.*/
			cv::OPTFLOW_LK_GET_MIN_EIGENVALS, 1e-3);

	//	calcOpticalFlowPyrLK(
	//				img_prev, img_curr,
	//				points_prev, points,
	//				status, error_,
	//				winSize, //size of the search window at each pyramid level.
	//				4, // pyramidal level
	//				criteria1, /*specifying the termination criteria of the iterative search algorithm
	//			    after the specified maximum number of iterations criteria.maxCount or when the search window
	//				moves by less than criteria.epsilon.*/
	//				1e-2);
}


/*TbD: Tracking by Detection Methods  */

void MyTracking::setTbDParamDetector(
		const std::string& featureDetectorName,
		DetectorParameters& param,
		std::vector<cv::Point2f>* points_detection,
		cv::Size img_size
/*std::vector<cv::Point2f> points_detection*/)
{

	this->points_detection = points_detection;

	//cv::Ptr<cv::FeatureDetector> detector_temp = cv::FeatureDetector::create(featureDetectorName);
	//detector = detector_temp;
	this->featureDetectorName = featureDetectorName;

	// set parameters
	if (!this->featureDetectorName.compare("BRISK"))
	{
		this->detector = cv::BRISK::create();
	}

	else if(!this->featureDetectorName.compare("FAST"))
	{
		this->detector = cv::FastFeatureDetector::create();
	}
	// ORB detector
	else if(!this->featureDetectorName.compare("ORB"))
	{
		this->detector = cv::ORB::create(
				/*nfeatures*/ 1000,
				/*scaleFactor*/ 1.2f,
				/*nlevels*/ 8,
				/*edgeThreshold*/ 31,
				/*firstLevel*/ 0,
				/*WTA_K*/ 4,
				/*scoreType*/ cv::ORB::HARRIS_SCORE,
				/*patchSize*/ 31,
				/*fastThreshold*/ 8);
	}
	// Good Features to track detector
	else if(!this->featureDetectorName.compare("GFTT"))
	{
		this->detector = cv::GFTTDetector::create(
				/*max corner*/ 1000,
				/*quality level*/ 1e-2,
				/*min distance*/ 10,
				/*block size*/ 3,
				/*use harris detector*/ false,
				/*k*/ 0.04
		);
	}
	else if(!this->featureDetectorName.compare("STAR"))
	{
		//this->detector = cv::StarDetector::create();
	}

	this->DetectorsParam = param;
}

void MyTracking::setTbDParamDetectorInSafetyArea(
		const std::string& featureDetectorName,
		DetectorParameters& param,
		std::vector<cv::Point2f>* points_detection,
		cv::Size img_size)
{

	this->points_detection = points_detection;

	this->featureDetectorName = featureDetectorName;

	// set parameters
	if (!this->featureDetectorName.compare("BRISK"))
	{
		this->detector = cv::BRISK::create();
	}

	else if(!this->featureDetectorName.compare("FAST"))
	{
		this->detector = cv::FastFeatureDetector::create();
	}
	// ORB detector
	else if(!this->featureDetectorName.compare("ORB"))
	{
		this->detector = cv::ORB::create(
				/*nfeatures*/ 150,
				/*scaleFactor*/ 1.2f,
				/*nlevels*/ 8,
				/*edgeThreshold*/ 31,
				/*firstLevel*/ 0,
				/*WTA_K*/ 4,
				/*scoreType*/ cv::ORB::HARRIS_SCORE,
				/*patchSize*/ 31,
				/*fastThreshold*/ 12);
	}
	// Good Features to track detector
	else if(!this->featureDetectorName.compare("GFTT"))
	{
		this->detector = cv::GFTTDetector::create(
				/*max corner*/ 1000,
				/*quality level*/ 1e-2,
				/*min distance*/ 10,
				/*block size*/ 3,
				/*use harris detector*/ false,
				/*k*/ 0.04
		);
	}
	else if(!this->featureDetectorName.compare("STAR"))
	{
		//this->detector = cv::StarDetector::create();
	}

	this->DetectorsParam = param;
}

void MyTracking::setTbDParamTracking(cv::Size& winSize){
	this->winSize = winSize;
}

void MyTracking::detectTbDFeatures(
		std::vector<cv::Point2f>& feature_detected,
		const cv::Mat& img,
		const cv::Mat& mask)
{

	std::vector<cv::KeyPoint> points_detected;

	if(detector != NULL)
	{
		// detect features
		detector->detect(img, points_detected, mask);

		cv::drawKeypoints(
				img,
				points_detected,
				this->img_features,
				cv::Scalar(255,0, 0));

		cv::KeyPoint::convert(points_detected, feature_detected);

	}
	else
	{
#ifdef DEBUG
		std::cout << "[ERROR] Detector " << this->featureDetectorName.c_str()<< std::endl;
#endif


	}
}

void MyTracking::descriptTbDFeatures(
		std::vector<cv::Point2f>& feature_detected,
		cv::Mat& descriptor,
		const cv::Mat& img)
{

	std::vector<cv::KeyPoint> points_detected;

	cv::KeyPoint::convert(feature_detected, points_detected);

	this->detector->compute(
			img,
			points_detected, descriptor);

	cv::KeyPoint::convert(points_detected, feature_detected);

}

void MyTracking::trackingTbDFeatures(
		const cv::Mat img_prev,
		const cv::Mat img_curr,
		std::vector<cv::Point2f>& points_prev,
		std::vector<cv::Point2f>& points,
		cv::Mat desc_prev,
		cv::Mat desc_curr,
		cv::Mat mask,
		cv::TermCriteria criteria,
		std::vector<uchar>& status)
{
	std::vector<cv::DMatch> matches;
	std::vector<cv::Point2f> points_tmp_prev;
	std::vector<cv::Point2f> points_tmp;
	std::vector<char> status_matches;
	std::vector<char> status_matches_prev;
	bool BFMatcher = true;
	bool FlannMatcher = false;
	cv::BFMatcher matcherBF(cv::NORM_HAMMING2, true);
	cv::FlannBasedMatcher matcherFLANN;

	if(BFMatcher)
	{
		// detect features and compute descriptor for current frame
		matcherBF.match(desc_prev, desc_curr, matches);
	}
	else if(FlannMatcher)
	{
		//		index_params= dict(algorithm = FLANN_INDEX_LSH,
		//				table_number = 6, // 12
		//				key_size = 12,     // 20
		//				multi_probe_level = 1); //2;
		// detect features and compute descriptor for current frame
		matcherFLANN.match(desc_prev, desc_curr, matches);
	}

	// Mantain only good matches
	double max_dist = 0;
	double min_dist = 100;
	//-- Quick calculation of max and min distances between keypoints
	for( int i = 0; i < matches.size(); i++ )
	{
		double dist = matches[i].distance;
		if( dist < min_dist ) min_dist = dist;
		if( dist > max_dist ) max_dist = dist;
	}
	//
	//	std::cout << "Max dist : "<< max_dist << std::endl;
	//	std::cout << "Min dist : " << min_dist << std::endl;

	//-- Draw only "good" matches (i.e. whose distance is less than 3*min_dist )
	std::vector<cv::DMatch> good_matches;

	for( int i = 0; i < matches.size(); i++ ) {
		if( matches[i].distance < 3.5 * min_dist)
			good_matches.push_back( matches[i]);
	}



	points_prev.resize(good_matches.size());
	points.resize(good_matches.size());
	points_tmp_prev.resize(good_matches.size());
	points_tmp.resize(good_matches.size());
	status.resize(good_matches.size(), 0);


	for(int i = 0; i < good_matches.size(); i++)
	{
		points_tmp_prev[i].x = points_prev[good_matches[i].queryIdx].x;
		points_tmp_prev[i].y = points_prev[good_matches[i].queryIdx].y;
		points_tmp[i].x = points[good_matches[i].trainIdx].x;
		points_tmp[i].y = points[good_matches[i].trainIdx].y;
		status[i] = 1;
	}


	points_prev = points_tmp_prev;
	points = points_tmp;

	if(this->isTrackingRefinement)
	{

	}

}


///////////////////////////////////////////////////////////////////////////////////
void MyTracking::findRightFeatures(
		std::vector<cv::Point2f> points_left,
		std::vector<uchar>& status_left,
		std::vector<cv::Point2f> points_right,
		std::vector<uchar>& status_right,
		cv::Mat disparityMap,
		bool draw_features,
		cv::Mat imgL,
		cv::Mat imgR)
{

	cv::Point2f tmp;
	tmp.x = 0;
	tmp.y = 0;

	// finding tracked features in right images
	points_right.resize(points_left.size(), tmp);
	status_right.resize(points_left.size(), 0);

	for( int i = 0; i < points_left.size(); i++ ){
		int row, col;
		row = points_left[i].y;
		col = points_left[i].x;

		if(disparityMap.at<float>(row, col) != 0 && status_left[i] == 1){
			points_right[i].x = points_left[i].x - disparityMap.at<float>(row, col);
			points_right[i].y = points_left[i].y ;
			status_right[i] = 1;
		}
		else{
			points_right[i]= tmp;
			status_right[i] = 0;
		}

		if(draw_features)
		{
			circle( imgL, points_left[i], 3, cv::Scalar(0,255,0), -1, 8);
			circle( imgR, points_right[i], 3, cv::Scalar(0,255,0), -1, 8);
		}
	}
}


void MyTracking::drawSafetyArea(
		std::vector<cv::Point2f> safety_area,
		cv::Mat img)
{
	for( int j = 0; j < safety_area.size(); j++ )
	{
		cv::line( img, safety_area[j],
				safety_area[(j+1)%safety_area.size()],
				cv::Scalar( 255 ), 3, 8 );
	}
}

void MyTracking::defineSafetyArea(
		std::vector<cv::Point2f> safety_area_left,
		std::vector<cv::Point2f> safety_area_right,
		cv::Mat imgL,
		cv::Mat imgR,
		cv::Mat disparity)
{


	std::cout << "You want to define a safety area!" << std::endl;


	// draw a segments between mouse tracked points
	this->drawSafetyArea(
			safety_area_left,
			imgL);
}


cv::Mat MyTracking::getImg_features(){
	return this->img_features;
}

int MyTracking::getNumberFeaturesDetected(){
	return this->numberFeaturesDetected;
}

/* Error Evaluation form:
 * [1] Kalal, Zdenek, Krystian Mikolajczyk, and Jiri Matas.
 * "Forward-backward error: Automatic detection of tracking failures."
 * Pattern Recognition (ICPR), 2010 20th International Conference on. IEEE, 2010.
 * even-odd evaluatio:
 * [2] Selka, F., et al. "Context-specific selection of algorithms for recursive
 * feature tracking in endoscopic image using a new methodology."
 * Computerized Medical Imaging and Graphics 40 (2015): 49-61.
 */
void MyTracking::ForwardBackwardError(
		// Inputs
		const std::vector<cv::Point2f>& fw_pointsLeft_start,
		const std::vector<cv::Point2f>& bk_pointsLeft_end,
		//		const std::vector<cv::Point2f>& fw_pointsRight_start,
		//		const std::vector<cv::Point2f>& bk_pointsRight_end,
		std::vector<uchar>& statusLeft_fw,
		std::vector<uchar>& statusLeft_bw,
		//		std::vector<uchar>& statusRight_fw,
		//		std::vector<uchar>& statusRight_bw,
		int& threshold,
		// Outputs
		float& percentage_tracked_pointsLeft,
		float& average_distance_error_pointsLeft,
		int& n_valid_feat_tracked_pointsLeft,
		//		float& percentage_tracked_pointsRight,
		//		float& average_distance_error_pointsRight,
		//		int& n_valid_feat_tracked_pointsRight,
		std::vector<float>& distance_errors_pointsLeft,
		//		std::vector<float>& distance_errors_pointsRight,
		std::vector<uchar>& status_distanceLeft /*,
		std::vector<uchar>& status_distanceRight*/)
{
	//initialization

	distance_errors_pointsLeft.resize(fw_pointsLeft_start.size());
	//	distance_errors_pointsRight.resize(fw_pointsRight_start.size());
	status_distanceLeft.resize(fw_pointsLeft_start.size());
	//	status_distanceRight.resize(fw_pointsRight_start.size());

	n_valid_feat_tracked_pointsLeft = 0;
	//	n_valid_feat_tracked_pointsRight = 0;
	average_distance_error_pointsLeft = 0;
	//	average_distance_error_pointsRight = 0;

	//compute Euclidean distances between forward-backward trajectories for
	// Left points

	for (int i = 0; i < fw_pointsLeft_start.size(); i++ )
	{
		if(statusLeft_fw[i] == 1 && statusLeft_bw[i] == 1){

			distance_errors_pointsLeft[i] =
					sqrtf(powf((fw_pointsLeft_start[i].x - bk_pointsLeft_end[i].x),2)
							+ powf((fw_pointsLeft_start[i].y - bk_pointsLeft_end[i].y),2));
			status_distanceLeft[i] = 1;

		}
		else{
			status_distanceLeft[i] = 0;
		}
	}

	//LEFT POINTS
	//Compute errors for evaluation
	for(int i = 0; i< distance_errors_pointsLeft.size(); i++ ){

		if(distance_errors_pointsLeft[i] < threshold && status_distanceLeft[i] == 1){
			n_valid_feat_tracked_pointsLeft++;
			average_distance_error_pointsLeft += distance_errors_pointsLeft[i];
		}

	}
	// Average of distances -- Precision
	average_distance_error_pointsLeft = average_distance_error_pointsLeft / (float)n_valid_feat_tracked_pointsLeft;
	// Percentage of features below the threshold
	percentage_tracked_pointsLeft = (float)n_valid_feat_tracked_pointsLeft / (float)fw_pointsLeft_start.size() * 100.0;
//	std::cout << "distance error:" << average_distance_error_pointsLeft << std::endl;

}

void MyTracking::computeSafetyAreaError(
		std::vector<cv::Point2f>& safetyContour_start,
		std::vector<cv::Point2f>& safetyContour_end,
		std::vector<float>& safetyContour_distances,
		float& safetyContourn_meanError,
		float& safetyContourn_medianError)
{
	// check if the contour start and end are of same dimension
	if(safetyContour_start.size() != safetyContour_start.size())
	{
		std::cout << "[ERROR] The contourn start and end have to be of "
				"the same size" << std::endl;
		return;
	}

	safetyContour_distances.resize(safetyContour_start.size());

	//compute euclidean distances between points
	for(int i = 0; i < safetyContour_start.size(); i++)
	{
		safetyContour_distances[i] =
				sqrtf(powf((safetyContour_start[i].x - safetyContour_end[i].x),2)
						+ powf((safetyContour_start[i].y - safetyContour_end[i].y),2));

		safetyContourn_meanError += safetyContour_distances[i];
	}

	// compute the median of the distances
	safetyContourn_meanError = safetyContourn_meanError / safetyContour_start.size();

	//compute the median of the distances
	int dim;
	dim = safetyContour_distances.size();
	std::sort(&safetyContour_distances[0], &safetyContour_distances[dim-1]);
	safetyContourn_medianError = dim % 2 ? safetyContour_distances[dim / 2] :
			(safetyContour_distances[dim / 2 - 1] + safetyContour_distances[dim / 2]) / 2;

	//debug
//	std::cout << "safetyContourn_meanError" << safetyContourn_meanError << std::endl;
//	std::cout << "safetyContourn_medianError" << safetyContourn_medianError << std::endl;
}

void MyTracking::saveFeaturesTrackingEvaluation(
		const std::string& fileName,
		const std::string& datasetName,
		const int& typedetector,
		int totalFramesNumber,
		const int& numberFeaturesDetected,
		float& detectorExecutionTime,
		const int& numberFeaturesTracked_pointsLeft,
		const float& average_distance_error_pointsLeft,
		const float& percentage_tracked_features_pointsLeft,
		std::vector<cv::Point2f>& pointStartLeft,
		std::vector<cv::Point2f>& pointEndLeft,
		const int& numberFeaturesTracked_pointsRight,
		const float& average_distance_error_pointsRight,
		const float& percentage_tracked_features_pointsRight,
		std::vector<cv::Point2f>& pointStartRight,
		std::vector<cv::Point2f>& pointEndRight){

	cv::FileStorage file;
	file.open(fileName, cv::FileStorage::WRITE);

	file << "Dataset Name                   " << datasetName;
	file << "Detector Type                  " << typedetector;
	file << "Total number of frames         " << totalFramesNumber;
	file << "Num Features Detected          " << numberFeaturesDetected;
	file << "Detector Execution time        " << detectorExecutionTime;
	file << "Valid Features Tracked         " << numberFeaturesTracked_pointsLeft;
	file << "Average Distance Error Left    " << average_distance_error_pointsLeft;
	file << "Percentage Trck Features Left  " << percentage_tracked_features_pointsLeft;
	file << "Average Distance Error Right   " << average_distance_error_pointsRight;
	file << "Percentage Trck Features Right " << percentage_tracked_features_pointsRight;
	file << "Forward Start Point Left       " << pointStartLeft;
	file << "Backward End Point Left        " << pointEndLeft;
	file << "Forward Start Point Right      " << pointStartRight;
	file << "Backward End Point Right       " << pointEndRight;

	file.release();

}


void MyTracking::getFeaturesDistributionOnImage(
		const cv::Size& imgSize,
		const std::vector<cv::Point2f>& features,
		float& mean,
		float& dev_std)
{
	cv::Mat binary_img;
	cv::Mat distanceMap;
	cv::Mat distanceMap8;
	cv::Mat mean_;
	cv::Mat dev_std_;

	cv::Mat histogram;
	int hbins = 100;
	int histSize[] = {hbins};
	float diag;

	diag = sqrt(imgSize.height*imgSize.height + imgSize.width*imgSize.width);
	// distance map varies from 0 (black-gray-white) to 256
	float hranges[] = { 0, diag };
	const float* ranges[] = { hranges};
	int channels[] = {0};

	binary_img.create(imgSize.height, imgSize.width, CV_8UC1);
	binary_img.setTo(0);
	distanceMap.create(imgSize.height, imgSize.width, CV_32FC1);
	distanceMap8.create(imgSize.height, imgSize.width, CV_8UC1);
	distanceMap.setTo(0);

	//compute binary image with features = 1 and background = 0
	for(int i = 0; i < features.size(); i++ )
	{
		binary_img.at<char>(features[i].y, features[i].x) = 255;
	}
	// compute distance map
	binary_img = 255 - binary_img;
	cv::distanceTransform(binary_img, distanceMap, CV_DIST_L2, 3);
	distanceMap.convertTo(distanceMap8, CV_8UC1);
	cv::normalize(distanceMap, distanceMap8, 0.0, 255.0, cv::NORM_MINMAX);

	cv::imshow("Binary", binary_img);

	cv::imshow("DistanceMap", distanceMap8);


	// compute the histogram of the distance map
	//	cv::calcHist( &distanceMap, 1, channels, cv::Mat(), // do not use mask
	//             histogram, 1, histSize, ranges,
	//             true, //the histogram is uniform
	//             false );

	//	std::cout << distanceMap << std::endl;
	//	std::cout << histogram << std::endl;

	cv::meanStdDev(distanceMap, mean_, dev_std_, cv::Mat());

	mean = mean_.at<double>(0,0);
	dev_std = dev_std_.at<double>(0,0);

	//	mean = mean/diag;
	//	dev_std = dev_std/diag;

}

void MyTracking::getFeaturesDistributionOnArea(
		std::vector<cv::Point2f>& safetyContourn,
		cv::Size imgSize,
		const std::vector<cv::Point2f>& features,
		float& mean,
		float& dev_std)
{
	cv::Mat binary_img;
	cv::Mat distanceMap;
	cv::Mat areaMap;
	cv::Mat distanceMap8;
	cv::Mat mean_;
	cv::Mat dev_std_;
	std::vector<float> distanceMapArea;
	distanceMapArea.clear();

	binary_img.create(imgSize.height, imgSize.width, CV_8UC1);
	binary_img.setTo(0);
	areaMap.create(imgSize.height, imgSize.width, CV_8UC1);
	areaMap.setTo(0);
	distanceMap.create(imgSize.height, imgSize.width, CV_32FC1);
	distanceMap8.create(imgSize.height, imgSize.width, CV_8UC1);
	distanceMap.setTo(0);

	//compute binary image with features = 1 and background = 0
	for(int i = 0; i < features.size(); i++ )
	{
		binary_img.at<char>(features[i].y, features[i].x) = 255;
	}
	// compute distance map
	binary_img = 255 - binary_img;
	cv::distanceTransform(binary_img, distanceMap, CV_DIST_L2, 3);
	distanceMap.convertTo(distanceMap8, CV_8UC1);
	cv::normalize(distanceMap, distanceMap8, 0.0, 255.0, cv::NORM_MINMAX);

	//
	cv::imshow("DistanceMap", distanceMap8);
	cv::Point contour[1][safetyContourn.size()];

	for(int i = 0; i < safetyContourn.size(); i++)
	{
		contour[0][i].x = safetyContourn[i].x;
		contour[0][i].y = safetyContourn[i].y;
	}

	const cv::Point * ppt[1] = {contour[0]};
	auto sa_size = static_cast<int>(safetyContourn.size());
	cv::fillPoly(areaMap, ppt, &sa_size, 1, cv::Scalar(255, 255, 255));

	cv::imshow("Binary", areaMap);

	for(int i = 0; i < areaMap.rows; i++)
	{
		for(int j = 0; j < areaMap.cols; j++)
		{
			if(areaMap.at<uchar>(i, j) == 255){
				distanceMapArea.push_back(distanceMap.at<float>(i,j));
			}
		}
	}

	//cv::meanStdDev(distanceMapArea, mean_, dev_std_, cv::Mat());
	cv::meanStdDev(distanceMap, mean_, dev_std_, areaMap);

	mean = mean_.at<double>(0,0);
	dev_std = dev_std_.at<double>(0,0);

}

void MyTracking::getAreaFeatures(
		std::vector<cv::Point2f>& features,
		double& area,
		cv::Point2f& center,
		float& radius/*,
		float& nFeaturesOnArea*/)
{
	//	 find number of feature on cluster
	cv::minEnclosingCircle(features, center, radius);

	area = M_PI * pow(radius,2);

	//	nFeaturesOnArea = features.size()/area;

	//TODO
	//	std::vector<cv::Point> contours0;
	//	std::vector<cv::Vec4i> hierarchy;
	//
	//	cv::findContours(binary_img, contours0, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);
	//	drawContours(binary_img, contours0, -1, cv::Scalar(255,0,0));
	//
	//	cv::imshow("Binary", binary_img);
	//	area = contourArea(contours0);
}

/*
 * Compute the percentage of features with respect to the area (image/safety area)
 */
void MyTracking::getPercentageFeaturesOnArea(
		int& nValidFeaturesTracked,
		double area,
		float& percentageFeaturesOnArea)
{
	percentageFeaturesOnArea = ((float)nValidFeaturesTracked * 100)/area;
//	std::cout << "nValidFeaturesTracked" << nValidFeaturesTracked << std::endl;
//	std::cout << " safety area" << area << std::endl;
//	std::cout << "percentageFeaturesOnArea" << percentageFeaturesOnArea << std::endl;
}

/*
 * Compute the percentage of area of the  features with respect to the area
 * (image/safety area)
 */
void MyTracking::getPercentageFeaturesAreaOnImage(
		double& area_features,
		double& safety_area,
		float& percentage)
{
	percentage = (area_features * 100) / safety_area;

#ifdef DEBUB
	std::cout << "getPercentageFeaturesAreaOnImage" << percentage << std::endl;
#endif

}


/*
 * Draw matches manually
 */
void MyTracking::myDrawMatches(
		const cv::Mat img_prev,
		const cv::Mat img_curr,
		const std::vector<cv::Point2f> points_prev,
		const std::vector<cv::Point2f> points,
		cv::Mat& out,
		bool KLT,
		bool HMatching,
		cv::Mat keyFrame,
		std::vector<cv::Point2f> points_keyFrame,
		std::vector<cv::Point2f> points_matched)
{
	// convert Point2f to KeyPoint
	std::vector<cv::KeyPoint> k;
	std::vector<cv::KeyPoint> k_prev;


	// create output image
	out.create(img_curr.rows, img_curr.cols*2, CV_8UC3);
	cv::Mat roi_left = out(cv::Rect(0, 0, img_curr.cols, img_curr.rows));
	cv::Mat roi_right = out(cv::Rect(img_curr.cols, 0, img_curr.cols, img_curr.rows));
	cv::Mat img1(img_curr.rows, img_curr.cols, CV_8UC3);
	cv::Mat img2(img_curr.rows, img_curr.cols, CV_8UC3);




	cv::Point2f temp;
	// create random color variable

	if(KLT)
	{
		if(!img_prev.empty())
		{
			img_prev.copyTo(img1);
			img_curr.copyTo(img2);
			// draw features
			cv::KeyPoint::convert(points_prev, k_prev);
			cv::KeyPoint::convert(points, k);
			cv::drawKeypoints(img_prev, k_prev, img1, 0.5 );
			cv::drawKeypoints(img_curr, k, img2, 0.5 );
			img1.copyTo(roi_left);
			img2.copyTo(roi_right);

			// draw matches
			if(points.size() > 0){
				//draw line
				for(int i = 0; i < points.size(); i++){
					if(points[i].x != 0 && points[i].y != 0)
					{
						cv::Scalar clr(std::rand() % 255,rand() % 255,rand() % 255);
						temp.x = points[i].x + img_curr.cols;
						temp.y = points[i].y;
						cv::line(out, points_prev[i] , temp, clr, 2);
					}
				}
			}
		}

	}
	else
	{
		if(!img_prev.empty())
		{
			keyFrame.copyTo(img1);
			img_curr.copyTo(img2);
			// draw features
			cv::KeyPoint::convert(points_keyFrame, k_prev);
			cv::KeyPoint::convert(points_matched, k);
			cv::drawKeypoints(keyFrame, k_prev, img1, 0.5 );
			cv::drawKeypoints(img_curr, k, img2, 0.5 );

			img1.copyTo(roi_left);
			img2.copyTo(roi_right);


			if(points.size() > 0){
				//draw line
				for(int i = 0; i < points_matched.size(); i++){
					if(points_matched[i].x != 0 && points_matched[i].y != 0)
					{
						cv::Scalar clr(std::rand() % 255,rand() % 255,rand() % 255);
						temp.x = points_matched[i].x + img_curr.cols;
						temp.y = points_matched[i].y;
						cv::line(out, points_keyFrame[i] , temp, clr, 2);
						//					std::cout << points_keyFrame << std::endl;
					}
				}
			}
		}
	}
}


void MyTracking::getOpticalFlowDistribution(
		std::vector<cv::Point2f>& points_pre,
		std::vector<cv::Point2f>& points_act,
		std::vector<uchar>& status_act,
		double& mean,
		double& std)
{
	//variables
	cv::Point2f p,q;
	cv::Point2f angle_p;
	double angle;
	double module;
	double moduleTh = 5;	// Module threshold
	std::vector<double> angles;

	cv::Mat angle_cv;
	cv::Mat mean_, dev_std_;

	angles.clear();

	// compute mean and std_dev of the actual frame

#ifdef DEBUB
	std::cout << "mean " << mean << std::endl;
#endif

	for (int i = 0; i < points_act.size(); i++)
	{
		if(status_act[i] == 1)
		{
			p.x = (int) points_pre[i].x;
			p.y = (int) points_pre[i].y;
			q.x = (int) points_act[i].x;
			q.y = (int) points_act[i].y;

			// Check module
			module = sqrt((p.x - q.x) * (p.x - q.x) + (p.y - q.y) * (p.y - q.y));

			// If the module is too small, then the angle is not representative
			if (module < moduleTh)
			{
				angle = 0.0;
			}
			else
			{
				// compute optical flow angles direction
				// gives angle between [-pi, pi]
				angle = atan2( (double) (q.y - p.y), (double) (q.x - p.x));

				// gives angle between [0, 2pi] and in degrees
				if(angle >= 0)
				{
					angles.push_back(angle *180.0/M_PI);
				}
				else if(angle < 0)
				{
					//M_PI*180/M_PI - angle*180/M_PI + M_PI*180/M_PI
					angles.push_back(360.0 - angle*180.0/M_PI);
				}
			}
		}

		if (angles.empty())
		{
			mean = 0.0;
			std = 0.0;
		}
		else
		{
			// compute mean
			mean = this->calcAnglesMean(angles);
			// compute std_dev
			std = this->calcAnglesStddev(angles);
			if(std != std )
			{
				std = 360;
			}
		}
	}
#ifdef DEBUB
	std::cout << "angles mean: " << mean << std::endl;
	std::cout << "angles std_dev: " << std << std::endl;
#endif

}

/*
 * Mean of circular distribution
 * as in:https://en.wikipedia.org/wiki/Mean_of_circular_quantities
 * http://stackoverflow.com/questions/13928404/calculating-standard-deviation-of-angles?rq=1
 * input: angle in degrees
 * output: mean in degrees
 * */
double MyTracking::calcAnglesMean(std::vector<double>& angles_deg){
	double sin_ = 0;
	double cos_ = 0;
	for(int i = 0; i < angles_deg.size(); i++){
		sin_ += sin(angles_deg[i] * (M_PI/180.0));
		cos_ += cos(angles_deg[i] * (M_PI/180.0));
	}

	sin_ /= angles_deg.size();
	cos_ /= angles_deg.size();

	double result = atan2(sin_,cos_)*(180/M_PI);

	if(cos_ > 0 && sin_ < 0) result += 360;
	else if(cos_ < 0) result += 180;

	return result;
}

/*
 * standard deviation of circular distribution
 * as in:https://en.wikipedia.org/wiki/Mean_of_circular_quantities
 * http://stackoverflow.com/questions/13928404/calculating-standard-deviation-of-angles?rq=1
 * input: angle in degrees
 * output: std_dev in degrees
 * */
double MyTracking::calcAnglesStddev(std::vector<double>& angles_deg){
	double sin_ = 0;
	double cos_ = 0;
	for(int i = 0; i < angles_deg.size(); i++){
		sin_ += sin(angles_deg[i] * (M_PI/180.0));
		cos_ += cos(angles_deg[i] * (M_PI/180.0));
	}
	sin_ /= angles_deg.size();
	cos_ /= angles_deg.size();

	double log_ = -log(sin_*sin_+ cos_*cos_);
	if(log_ <= 0)
	{
		return 0.0;
	}

	double stddev = sqrt(log_)* (180.0/M_PI);

	return stddev;
}

bool MyTracking::checkHomography(
		cv::Size& imgSize,
		cv::Mat& homography,
		std::vector<cv::Point2f>& points_pre_trasformed,
		std::vector<cv::Point2f>& points_trasformed)
{
	//check if the vector is empty
	if ( points_trasformed.empty() )
	{
#ifdef DEBUG
		std::cout << "[LKT FAILURE]: Empty Points Transformed!" << std::endl;
#endif
		return false;
	}
	//	else if(homography.at<double>(2, 0) < 0 || homography.at<double>(2, 0) > imgSize.height ||
	//			homography.at<double>(2, 1) < 0 || homography.at<double>(2, 1) > imgSize.width)
	//	{
	//		std::cout << "size: " << imgSize.height << " x " << imgSize.width << std::endl;
	//		std::cout << "[LKT FAILURE]: Homography Translation out of bound!" << std::endl;
	//		return false;
	//	}
	else if (homography.at<double>(2, 2) == 0)
	{
#ifdef DEBUG
		std::cout << "[LKT FAILURE]: Hyomgraphy Wrong!" << std::endl;
#endif
		return false;
	}
	else{

		// Check validity on points transformed with homography
		for ( unsigned i = 0; i < points_trasformed.size(); i++ )
		{
			// compute Z
			double x = points_pre_trasformed[i].x;
			double y = points_pre_trasformed[i].y;
			double Z = ( homography.at<double>(2, 0) * x
					+ homography.at<double>(2, 1) * y
					+ homography.at<double>(2, 2));

			// check if x and y are valid (inside the window size)
			/*
			if(points_trasformed[i].y < 0 || points_trasformed[i].y > imgSize.height ||
					points_trasformed[i].x < 0 || points_trasformed[i].x > imgSize.width)
			{
#ifdef DEBUG
				std::cout << "[LKT FAILURE]: Points Translation out of bound!" << std::endl;
#endif

				return false;
			}
			else */
			if ( Z <= 0 )
			{
#ifdef DEBUG
				std::cout << "[LKT FAILURE]: Z negative" << std::endl;
#endif

				return false;
			}
		}
	}

	return true;
}


bool MyTracking::checkLKTTrackingFailure(
		std::vector<cv::Point2f>& points_in_area_pre,
		std::vector<cv::Point2f>& points_in_area_act,
		bool& homograpyValidity,
		float& mean_OF_prev,
		float& std_OF_prev,
		float& mean_OF_act,
		float& std_OF_act)
{

	// number of valid tracked features
	int num_feat = points_in_area_act.size();

	// number of valid previous tracked features
	int num_feat_prev = points_in_area_pre.size();


	// compute % of lost feat form prev frame
	float perc_lost_feat;
	float perc_th = 0.6;
	perc_lost_feat = (num_feat_prev - num_feat)/num_feat_prev;

	// compute % of optical flow direction change
	float prec_mean_OF;
	float prec_std_OF;

	prec_mean_OF = (mean_OF_prev - mean_OF_act) / mean_OF_prev;
	prec_std_OF = (std_OF_prev - std_OF_act) / std_OF_prev;



	// Find a possible LKT failures in DIFFERENT cases:

	// 1) minimum number of points to find homography
	if(num_feat < 8)
	{
#ifdef DEBUG
		std::cout << "[LKT FAILURE]: Points Tracked less than 4" << std::endl;
#endif

		return true;
	}
	// 2) percentage of tracked features decreases of 60% with respect to prev frame
	else if(perc_lost_feat > perc_th)
	{
		std::cout << "[LKT FAILURE]: percentage of tracked features decreases of "
				<< perc_th*100 << "% with respect to prev frame" << std::endl;
		return true;
	}
	// 3) invalid homography
	else if (!homograpyValidity)
	{
		std::cout << "[LKT FAILURE]: Invalid Homography found!" << std::endl;
		return true;
	}
	// 4) Optical Flow distribution has changed suddently
	else if(prec_mean_OF > 0.6 && prec_std_OF > 0.6)
	{
		std::cout << "[LKT FAILURE]: Optical Flow distribution has changed suddently" << std::endl;
		return true;
	}
	/* TODO images are very different*/
	//	else if(-1)
	//	{
	//		std::cout << "[LKT FAILURE]: Images are different" << std::endl;
	//		return true;
	//	}
	else
	{
		return false;
	}

}

float MyTracking::detectBlurredFrame(
		cv::Mat& img)
{
	//variables
	float blur_th;
	float blur_val;
	cv::Mat hsl_image, F;
	std::vector<cv::Mat> hls_channels(3);
	double m, M;
	int n_kern_x = 9, n_kern_y = 1;
	cv::Mat Bver, Bhor;
	cv::Mat h_kernel;
	cv::Mat v_kernel;
	cv::Mat Vver, Vhor;
	cv::Rect r;
	cv::Mat D_Fver_no_b, D_Fhor_no_b, Vver_no_b, Vhor_no_b;

	// split image in HLS channel
	cv::cvtColor(img, hsl_image, cv::COLOR_RGB2HLS);     //Convert from RGB to HLS

	// take only the luminance channel
	split(hsl_image, hls_channels);
	F = hls_channels[1]; //luminance

	// Normalization between 0 and 255
	minMaxLoc(F, &m, &M);
	F.convertTo(F, CV_32F, 255./(M-m), -m * 255.0/(M - m));

	//CRETE
	h_kernel =  1.0/n_kern_x * cv::Mat::ones(cv::Size(n_kern_x, n_kern_y), CV_32F);   //h_kernel = 1/9*[1 1 1 1 1 1 1 1 1 ]-->smussa verticalmente

	transpose(h_kernel, v_kernel);
	//cout << h_kernel << endl;

	//non flippo perchè il kernel è simm--> corr = conv
	filter2D(F, Bver, -1, h_kernel);  //h_kernel = 1/9*[1 1 1 1 1 1 1 1 1 ]-->smussa verticalmente
	filter2D(F, Bhor, -1, v_kernel);

	//finite differences
	cv::Mat D_Fver = cv::Mat::zeros(F.size(), CV_32F);
	cv::Mat D_Fhor = cv::Mat::zeros(F.size(), CV_32F);
	cv::Mat D_Bver = cv::Mat::zeros(F.size(), CV_32F);
	cv::Mat D_Bhor = cv::Mat::zeros(F.size(), CV_32F);

	// find absolute differences on row and line of sharp and blurred images
	for (int i = 1; i < img.rows-2; i++){
		for (int j = 1; j < img.cols-2; j++){

			D_Fver.at<float>(i,j) = abs(F.at<float>(i,j) - F.at<float>(i-1,j));
			D_Fhor.at<float>(i,j) = abs(F.at<float>(i,j) - F.at<float>(i,j-1));
			D_Bver.at<float>(i,j) = abs(Bver.at<float>(i,j) - Bver.at<float>(i-1,j));
			D_Bhor.at<float>(i,j) = abs(Bhor.at<float>(i,j) - Bhor.at<float>(i,j-1));
		}
	}


	// take only the absolute difference that are decreased (> 0)
	Vver = cv::max(0, D_Fver - D_Bver);
	Vhor = cv::max(0, D_Fhor - D_Bhor);

	//remove borders
	r.y = 1;
	r.x = 1;
	r.width = F.cols-2;
	r.height = F.rows -2;

	D_Fver_no_b = D_Fver(r);
	D_Fhor_no_b = D_Fhor(r);
	Vver_no_b = Vver(r);
	Vhor_no_b = Vhor(r);

	// sum the differences
	double s_Fver = cv::sum( D_Fver_no_b )[0];
	double s_Fhor = cv::sum( D_Fhor_no_b )[0];
	double s_Vver = cv::sum( Vver_no_b )[0];
	double s_Vhor = cv::sum( Vhor_no_b )[0];


	// compute the blurrign value and normalize it
	float b_Fver = float(s_Fver-s_Vver)/s_Fver;  //0-1
	float b_Fhor = float(s_Fhor-s_Vhor)/s_Fhor; //0-1

	// find the higher component in x and y
	float blur_F = cv::max(b_Fver, b_Fhor);
	//	std::cout << "end: " << blur_F << " [with: 0->blurred and 1->sharp]" << std::endl;

	// compute image blurred

	return blur_F;

}


double MyTracking::detectFramesSimilarity(
		cv::Mat& i1, cv::Mat& i2)
{

	const double C1 = 6.5025, C2 = 58.5225;
	/***************************** INITS **********************************/
	int d     = CV_32F;

	cv::Mat I1, I2;
	i1.convertTo(I1, d);           // cannot calculate on one byte large values
	i2.convertTo(I2, d);

	cv::Mat I2_2   = I2.mul(I2);        // I2^2
	cv::Mat I1_2   = I1.mul(I1);        // I1^2
	cv::Mat I1_I2  = I1.mul(I2);        // I1 * I2

	/***********************PRELIMINARY COMPUTING ******************************/

	cv::Mat mu1, mu2;   //
	cv::GaussianBlur(I1, mu1, cv::Size(11, 11), 1.5);
	cv::GaussianBlur(I2, mu2, cv::Size(11, 11), 1.5);

	cv::Mat mu1_2   =   mu1.mul(mu1);
	cv::Mat mu2_2   =   mu2.mul(mu2);
	cv::Mat mu1_mu2 =   mu1.mul(mu2);

	cv::Mat sigma1_2, sigma2_2, sigma12;

	cv::GaussianBlur(I1_2, sigma1_2, cv::Size(11, 11), 1.5);
	sigma1_2 -= mu1_2;

	cv::GaussianBlur(I2_2, sigma2_2, cv::Size(11, 11), 1.5);
	sigma2_2 -= mu2_2;

	cv::GaussianBlur(I1_I2, sigma12, cv::Size(11, 11), 1.5);
	sigma12 -= mu1_mu2;

	///////////////////////////////// FORMULA ////////////////////////////////
	cv::Mat t1, t2, t3;

	t1 = 2 * mu1_mu2 + C1;
	t2 = 2 * sigma12 + C2;
	t3 = t1.mul(t2);              // t3 = ((2*mu1_mu2 + C1).*(2*sigma12 + C2))

	t1 = mu1_2 + mu2_2 + C1;
	t2 = sigma1_2 + sigma2_2 + C2;
	t1 = t1.mul(t2);               // t1 =((mu1_2 + mu2_2 + C1).*(sigma1_2 + sigma2_2 + C2))

	cv::Mat ssim_map;
	cv::divide(t3, t1, ssim_map);      // ssim_map =  t3./t1;

	cv::Scalar mssim = mean( ssim_map ); // mssim = average of ssim map

	//	cv::Scalar similarity = cv::mean(mssim);

	return mssim.val[0];

}

void MyTracking::computeSegmentationModel(
		const cv::Mat& imgL_rect,
		cv::Mat& imgL_features,
		cv::Mat& imgL_hsv,
		cv::Mat& img_seg_prob,
		std::vector<cv::Point2f>& points_tracked,
		std::vector<uchar>& status_points_tracked,
		std::vector<cv::Point2f>& safetyArea_pointsLeft,
		bool& KLTTracker,
		bool& HMatcher,
		bool& init)
{

	cv::Rect templateROI;
	int k_cluster = this->n_cluster;
	std::vector<cv::Point> hull[k_cluster];

	// compute features clustering
	cv::Mat k_labels;
	cv::Mat k_centers;
	std::vector<cv::Point2f> p_to_be_clustered;
	std::vector<cv::Point> p_clusters[k_cluster];

	ros::Time timeBegin, timeEnd;
	ros::Duration deltaTime;

	timeBegin = ros::Time::now();
	if(points_tracked.size() <= k_cluster)
	{
		k_cluster = 1;
	}

	if(init)
	{
		k_cluster = 1;
	}


	if(init){
		// find points tracked only inside the safety aerea
		for(int i = 0; i < points_tracked.size(); i++ )
		{
			p_to_be_clustered.push_back(points_tracked[i]);
		}
	}
	else if(KLTTracker)
	{
		// find points tracked only inside the safety aerea
		for(int i = 0; i < points_tracked.size(); i++ )
		{
			if(status_points_tracked[i] == 1 )
			{
				p_to_be_clustered.push_back(points_tracked[i]);
			}
		}
	}
	else if(HMatcher)
	{
		// find points tracked only inside the safety aerea
		for(int i = 0; i < points_tracked.size(); i++ )
		{
			p_to_be_clustered.push_back(points_tracked[i]);
		}
	}

	if(p_to_be_clustered.size() <= k_cluster)
		return;

	// compute ROI around the safet area
	templateROI = boundingRect(safetyArea_pointsLeft);

	kmeans(p_to_be_clustered, k_cluster, k_labels,
			cv::TermCriteria( cv::TermCriteria::EPS+cv::TermCriteria::COUNT, 10, 0.1),
			3, cv::KMEANS_PP_CENTERS, k_centers);


	for(int n = 0; n < k_centers.rows; n++)
	{
		for(int i = 0; i < k_labels.rows; i++ )
		{
			if(k_labels.at<uchar>(i,0) == n)
			{
				cv::Point temp;
				temp.x = p_to_be_clustered[i].x;
				temp.y = p_to_be_clustered[i].y;
				p_clusters[n].push_back(temp);
			}
		}
	}

	std::vector<uchar> cluster_found;
	cluster_found.resize(k_centers.rows);
	for(int i = 0; i < k_centers.rows; i++)
	{
		if(p_clusters[i].size() > 4)
		{
			cluster_found[i] = 1;
		}
		else
		{
			cluster_found[i] = 0;
		}
	}


	int n_convex_hull = 0;
	// compute the convex hull on matched features
	for(int i = 0; i < cluster_found.size(); i++ )
	{
		if(cluster_found[i] == 1)
		{

			cv::convexHull(p_clusters[i], hull[n_convex_hull]);

			// draw the convex hull
			/*
			for( int j = 0; j < hull[n_convex_hull].size(); j++ )
			{
				cv::line( imgL_features, hull[n_convex_hull][j],
						hull[n_convex_hull][(j+1)%hull[n_convex_hull].size()],
						cv::Scalar( 15, 230, 23 ), 1, 8 );
			}*/

			n_convex_hull++;
		}

	}
	timeEnd = ros::Time::now();
	deltaTime = timeEnd - timeBegin;
#ifdef COMPUTATIONAL_TIME
	std::cout << "[TIME] init segmentation :" << deltaTime.toSec() << std::endl;
#endif
	timeBegin = ros::Time::now();
	// compute segmentation probability
	if(init)
	{

		this->SegModel.updateProb(
				imgL_hsv,
				templateROI,
				hull,
				templateROI.width,
				templateROI.height,
				false,
				n_convex_hull);
	}
	else
	{

		this->SegModel.updateProb(
				imgL_hsv,
				templateROI,
				hull,
				templateROI.width,
				templateROI.height,
				true,
				n_convex_hull);
	}

	timeEnd = ros::Time::now();
	deltaTime = timeEnd - timeBegin;

#ifdef COMPUTATIONAL_TIME
	std::cout << "[TIME] segmentation core :" << deltaTime.toSec() << std::endl;
#endif

	timeBegin = ros::Time::now();
	this->SegModel.showProbMap(
			imgL_hsv,
			templateROI,
			img_seg_prob,
			TrackingParam.th_segmentation);
	timeEnd = ros::Time::now();
	deltaTime = timeEnd - timeBegin;
#ifdef COMPUTATIONAL_TIME
	std::cout << "[TIME] show segmentation :" << deltaTime.toSec() << std::endl;
#endif
}


void MyTracking::redetectKLT(
		const cv::Mat& imgL_rect,
		const cv::Mat& imgL_gray,
		cv::Mat& imgL_hsv,
		cv::Mat& imgL_features,
		cv::Mat& img_seg_prob,
		cv::Mat& SA_mask,
		std::vector<cv::Point2f>& points_detected_L,
		std::vector<cv::Point2f>& pointsMatched_act,
		std::vector<cv::Point2f>& points_tracked_L,
		std::vector<cv::Point2f>& safetyArea_pointsLeft,
		std::vector<uchar>& points_inSafetyArea_L,
		std::vector<uchar>& status_L,
		bool init,
		bool KLTTracker,
		bool HMatcher,
		int safetyArea_nfeat)
{

	safetyArea_nfeat = 0;
	points_inSafetyArea_L.clear();


	this->computeSegmentationModel(
			imgL_rect,
			imgL_features,
			imgL_hsv,
			img_seg_prob,
			pointsMatched_act,
			status_L,
			safetyArea_pointsLeft,
			KLTTracker,
			HMatcher,
			init);

	//re -detect features
	points_tracked_L.clear();

	this->redetectKLTFeatures(
			imgL_gray,
			SA_mask,
			points_detected_L,
			status_L);



	points_tracked_L.resize(points_detected_L.size());
	points_tracked_L = points_detected_L;
	points_inSafetyArea_L.resize(points_detected_L.size(), 0);


	//save detected features with probability > 04 inside the SA
	for(int i = 0; i < points_tracked_L.size(); i++)
	{
		cv::Point p_temp;
		p_temp.x = points_tracked_L[i].x;
		p_temp.y = points_tracked_L[i].y;

		//		std::cout << "prob seg model: " << this->SegModel.getKpProb(p_temp, imgL_hsv) << std::endl;

		if(this->SegModel.getKpProb(p_temp, imgL_hsv) > TrackingParam.th_segmentation &&
				status_L[i] == 1)
		{
			points_inSafetyArea_L[i] = 1;
			safetyArea_nfeat++;
		}
		else
		{
			points_inSafetyArea_L[i] = 0;
		}
	}

}


void MyTracking::redetectOpenCVFeatures_SURFMask(
		const cv::Mat& imgL_rect,
		const cv::Mat& imgL_gray,
		cv::Mat& imgL_features,
		cv::Mat& img_seg_prob,
		cv::Mat& specularity_mask,
		std::vector<cv::Point2f>& points_detected_L,
		std::vector<cv::Point2f>& pointsMatched_act,
		std::vector<cv::Point2f>& points_tracked_L,
		std::vector<cv::Point2f>& safetyArea_pointsLeft,
		bool KLTTracker)
{
	points_detected_L.clear();
	cv::Mat features_mask;
	features_mask.create(imgL_gray.rows, imgL_gray.cols, CV_8UC1);
	features_mask.setTo(255);

	for(int i = 0; i < pointsMatched_act.size(); i++)
	{
		cv::Point2f p;
		p.x = pointsMatched_act[i].x;
		p.y = pointsMatched_act[i].y;
		if(cv::pointPolygonTest(safetyArea_pointsLeft, p, false) >= 0)
		{
			circle(features_mask, p, 20, cv::Scalar(0), -1);
		}
	}

	//re -detect features
	if(TrackingParam.opencv_tracking){
		this->detectOpenCVFeatures(
				imgL_gray,
				255 - features_mask,
				points_detected_L);
	}
	else
	{

		////	this->redetectITFeatures(
		//		imgL_gray,
		//		specularity_mask,
		//		points_detected_L,
		//		status_L);
	}

	points_tracked_L = points_detected_L;
	points_detected_L.clear();
}


void MyTracking::find_points_in_safety_area_high_seg_prob(
		const cv::Mat imgL_rect,
		const cv::Mat imgL_hsv,
		const std::vector<cv::Point2f>& points,
		const std::vector<cv::Point2f>& safety_area_contourn,
		std::vector<uchar>& tracking_status,
		std::vector<uchar>& inside_area,
		int& n_feat_inside,
		double& th_segmentation)
{
	n_feat_inside = 0;
	inside_area.clear();
	inside_area.resize(tracking_status.size());


	for(int i = 0; i < points.size(); i++)
	{
		if(this->SegModel.getKpProb(points[i], imgL_hsv) > th_segmentation
				&& tracking_status[i] == 1)
		{
			inside_area[i] = 1;
			n_feat_inside++;

		}
		else
		{
			inside_area[i] = 0;
		}
	}
}

void MyTracking::find_SAContourn_partialOcclusion(
		cv::Mat imgL_rect,
		cv::Mat imgFeat,
		cv::Mat img_hsv,
		std::vector<cv::Point2f>& contourn_points,
		std::vector<std::vector<cv::Point> >& new_contour)
{
//#ifndef EVALUATION_TRACKING
//	return;
//#endif

	float th = 0.1;

	cv::Mat new_contour_mask;
	cv::Mat contours;
	std::vector<std::vector<cv::Point> > new_contour_aux;
	double area_contour;
	//	cv::namedWindow("partial occlusion mask", CV_WINDOW_NORMAL);

	cv::cvtColor(imgL_rect, img_hsv, cv::COLOR_BGR2HSV);

	new_contour_mask.create(imgL_rect.rows, imgL_rect.cols, CV_8UC1);
	new_contour_mask.setTo(0);

	contours.create(imgL_rect.rows, imgL_rect.cols, CV_8UC3);
	contours.setTo(0);

	for(int i = 0; i < imgL_rect.rows; i++)
	{
		for(int j = 0; j < imgL_rect.cols; j++)
		{
			cv::Point p_temp;
			p_temp.x = j;
			p_temp.y = i;
			if(cv::pointPolygonTest(contourn_points, p_temp, false) >= 0 &&
					this->SegModel.getKpProb(p_temp, img_hsv) > th)
			{
				new_contour_mask.at<uchar>(i,j) = 255;
			}
		}
	}

	//	cv::imshow("partial occlusion mask",new_contour_mask);
	//	cv::waitKey();
	new_contour_aux.clear();
	cv::findContours(new_contour_mask, new_contour_aux, CV_RETR_EXTERNAL,CV_CHAIN_APPROX_NONE);

	for( size_t k = 0; k < new_contour_aux.size(); k++ )
	{
		area_contour = contourArea(new_contour_aux[k]);
		if (area_contour > 100)
			new_contour.push_back(new_contour_aux[k]);
	}

	for( size_t k = 0; k < new_contour.size(); k++ )
		cv::approxPolyDP(cv::Mat(new_contour[k]), new_contour[k], 3, true);


	//cv::drawContours(imgFeat, new_contour, -1, cv::Scalar(255,0,0));

}


void MyTracking::check_failure_parameters(
		cv::Size& img_size,
		cv::Mat& homography,
		std::vector<cv::Point2f>& safety_area_contourn_pre,
		std::vector<cv::Point2f>& safety_area_contourn_curr,
		bool&  hom_validity,
		int& n_feat_pre,
		int& n_feat_act,
		double& perc_feat_lost,
		cv::Mat& img_rgb,
		cv::Mat& hist_key,
		cv::Mat& hist,
		int hbin,
		int sbin,
		float th_seg,
		double& bh_distance)
{

	hom_validity = this->checkHomography(
			img_size,
			homography,
			safety_area_contourn_pre,
			safety_area_contourn_curr);


	if(n_feat_pre != 0)
	{
		perc_feat_lost  = ((float)n_feat_pre - (float)n_feat_act) / (float)n_feat_pre ;
		if(perc_feat_lost < 0)
		{
			perc_feat_lost = abs(perc_feat_lost);
		}
	}
	else
	{
		perc_feat_lost = 0.0;
	}

	// compute bhatacharryya distance
	this->measure_bhattacharyya_distance(
			img_rgb,
			hist_key,
			hist,
			safety_area_contourn_curr,
			hbin,
			sbin,
			th_seg,
			bh_distance);

}

void MyTracking::computeSAModel(
		HLearningModule &learning,
		cv::Mat &img_grey,
		std::vector<cv::Point2f> &SA,
		cv::Mat &SA_mask,
		std::vector<cv::KeyPoint> &kp_cv,
		std::string &modelName,
		ObjectProperties &obj_prop)
{

	kp_cv.clear();
	obj_prop.deleteAll();

	// declare mask of the safety area on img gray prev
	ImageMaskCV template_mask(SA_mask, 0, 244);

	// compute learning of the template

	learning.computeTemplateProperties(
			modelName,
			img_grey,
			template_mask,
			kp_cv,
			obj_prop);
}

bool MyTracking::updateModel(
		HLearningModule &learning,
		cv::Mat &img_rgb,
		cv::Mat &img_gray,
		std::vector<cv::Point2f> &SA,
		cv::Mat &SA_mask,
		std::vector<cv::KeyPoint> &kp_cv,
		std::string &modelName,
		double& bh_distance,
		std::deque<ObjectProperties> &objList,
		std::deque<ModelInfo> &modelInfo,
		bool &replaced)
{
	ModelInfo info;
	ObjectProperties obj_prop;
	replaced = false;

	// First check if the image is blurred
	// TODO: Test also with the SA
	float img_blur = this->detectBlurredFrame(img_rgb);
	if (img_blur > 0.8)
		return false;

	// Computer model
	this->computeSAModel(
			learning,
			img_gray,
			SA,
			SA_mask,
			kp_cv,
			modelName,
			obj_prop);

	//	double bh_distance;
	//
	//	// compare this model with the keymodel
	//	compute_BhattaCharryya_distance(
	//			hist,
	//			hist_key,
	//			bh_distance);

	if (bh_distance > 0.1 && bh_distance < 0.5)
	{
		info.nTimesUsed = 0;
		info.nTimesNotUsed = 0;
		info.BAdistance = bh_distance;
		info.SA = SA;

		this->update_weight(info);


		if (!this->add_model(objList, modelInfo, obj_prop, info, 10))
		{
			// Find weakest element
			std::deque<ModelInfo>::iterator it;
			double weight = 1.0;
			int weakestElement = -1;
			int counter = 0;
			bool firstElement = true;


			for (it = modelInfo.begin(); it != modelInfo.end(); it++)
			{
				if (!firstElement && (*it).weight < weight)
				{
					weight = it->weight;
					weakestElement = counter;
				}

				if (firstElement)
					firstElement = false;

				counter++;
			}

			if (weakestElement != -1)
			{
				if (modelInfo[weakestElement].nTimesNotUsed > 5)
				{
					this->replace_model(
							objList, modelInfo, obj_prop, info, weakestElement);
					std::cout << "Replacing the model"<< std::endl;
					replaced = true;
					return true;
				}
			}
		}
		else
		{
			return true;
		}
	}

	return false;
}

void MyTracking::create_safety_area_mask(
		cv::Mat& img,
		std::vector<cv::Point2f>& safety_contourn,
		cv::Mat& specularity_mask,
		cv::Mat& safety_area_mask)
{
	cv::Point2f pt;
	safety_area_mask.create(img.rows, img.cols, CV_8UC1);
	safety_area_mask.setTo(0);
	double res;

	std::vector<std::vector<cv::Point> > contour(1);
	contour[0].resize(safety_contourn.size());

	for (int i = 0; i < safety_contourn.size(); i++)
	{
		contour[0][i].x = safety_contourn[i].x;
		contour[0][i].y = safety_contourn[i].y;
	}

	cv::drawContours(safety_area_mask, contour,-1, cv::Scalar(255), CV_FILLED);

	cv::bitwise_and(safety_area_mask, 255- specularity_mask, safety_area_mask);


//	cv::namedWindow("provaaa", CV_NORMAL);
//	cv::imshow("provaaa", safety_area_mask);
//	cv::waitKey();
//	for(int i = 0; i < img.rows; i++)
//	{
//		for(int j = 0; j < img.cols; j++)
//		{
//			pt.x = j;
//			pt.y = i;
//
//			res = cv::pointPolygonTest(safety_contourn, pt, false);
//
//			if(res == 1 ||
//					res == 0)
//			{
//				if(specularity_mask.at<uchar>(i,j) == 255)
//				{
//					safety_area_mask.at<uchar>(i,j) = 0;
//				}
//				else
//				{
//					safety_area_mask.at<uchar>(i,j) = 255;
//				}
//
//			}
//
//		}
//	}
}

bool MyTracking::add_model(
		std::deque<ObjectProperties> &objList,
		std::deque<ModelInfo> &modelInfo,
		ObjectProperties &obj_prop,
		ModelInfo &info,
		int size)
{
	if (objList.size() >= size)
	{
		std::cout << "Model list full" << std::endl;
		return false;
	}

	objList.push_back(obj_prop);
	modelInfo.push_back(info);

	std::cout << COLOR_UPDATE << "adding new model" << COLOR_RESET << std::endl;
	print_model(objList, modelInfo);
	return true;
}

void MyTracking::clear_models(
			std::deque<ObjectProperties> &objList,
			std::deque<ModelInfo> &modelInfo)
{
	objList.clear();
	modelInfo.clear();
}

void MyTracking::replace_model(
		std::deque<ObjectProperties> &objList,
		std::deque<ModelInfo> &modelInfo,
		ObjectProperties &obj_prop,
		ModelInfo &info,
		int pos)
{
	print_model(objList, modelInfo);

	if (pos < 0 || pos >= objList.size())
	{
		std::cout << "Model ID out of boundaries" << std::endl;
		return;
	}

	std::cout << COLOR_REPLACE << "Replacing model [" << pos << "]" << COLOR_RESET << std::endl;
	objList[pos] = obj_prop;
	modelInfo[pos] = info;

	print_model(objList, modelInfo);
}

bool MyTracking::get_model(
		std::deque<ObjectProperties> &objList,
		std::deque<ModelInfo> &modelInfo,
		ModelInfo &info,
		std::string &name)
{
	std::deque<ObjectProperties>::iterator it;
	int counter = 0;

	for (it = objList.begin(); it != objList.end(); it++)
	{
		if (it->getName().compare(name) == 0)
		{
			modelInfo[counter].nTimesUsed++;
			this->update_weight(modelInfo[counter]);
			info = modelInfo[counter];
		}
		else
		{
			modelInfo[counter].nTimesNotUsed++;
			this->update_weight(modelInfo[counter]);
		}
		counter++;
	}

	return false;
}

void MyTracking::print_model(
		std::deque<ObjectProperties> &objList,
		std::deque<ModelInfo> &modelInfo)
{
	std::deque<ObjectProperties>::iterator it;
	std::vector<ImagePropertiesCV*> prop;

	for (it = objList.begin(); it != objList.end(); it++)
	{
		std::cout << "Object:" << std::endl;
		std::cout << "  name: " << (*it).getName() << std::endl;
		std::cout << "  images: " << std::endl;
		prop = (*it).getImageProperties();
		for (unsigned int i = 0; i < prop.size(); i++)
		{
			std::cout << "    - " << prop[i]->getCenter().x() << std::endl;
			if (prop[i]->getKeyPoints() == NULL)
				std::cout << "    - no keypoints" << std::endl;
		}
	}
}

void MyTracking::update_weight(ModelInfo &info)
{
	//	info.weight =
	//			info.BAdistance * 0.6 +
	//			(1.0 - 1.0 / (info.nTimesUsed + 1.0)) * 0.4;

	info.weight =
			info.BAdistance * 0.5 +
			((double)info.nTimesUsed / (double)(info.nTimesUsed + info.nTimesNotUsed)) * 0.5;
}

void MyTracking::measure_bhattacharyya_distance(
		cv::Mat& img_rgb,
		cv::Mat& hist_key,
		cv::Mat& hist,
		std::vector<cv::Point2f>& SA,
		int hbin,
		int sbin,
		float th_seg,
		double& bh_distance)
{

	// compare this model with the keymodel
	compute_BhattaCharryya_distance(
			hist,
			hist_key,
			bh_distance);
}
