#include "envisors/3dreconstruction.hpp"

StereoReconstruction::StereoReconstruction(){

}

void StereoReconstruction::setParam(cv::Mat& Q){
	this->Q = Q;
}
void StereoReconstruction::process(
		const cv::Mat& disparity,
		cv::Mat& pointCloud){

	this->compute3DReconstruction(disparity,pointCloud);
	//Float Depth Map
	cv::Point3f p;
	p.x = -1.0;
	p.y = -1.0;
	p.z = -1.0;



	for(int i = 0; i < pointCloud.rows; i++){
		for(int j = 0; j < pointCloud.cols; j++){

			cv::Point3f p_aux;

			p_aux = pointCloud.at<cv::Point3f>(i, j);
//			std::cout << p_aux.z  << " , ";
			if(p_aux.z >= 1000.0 || disparity.at<float>(i, j) == 0.0){
				pointCloud.at<cv::Point3f>(i, j) = p;
			}

		}
	}
//	std::cout << std::endl;
}



// OpenCV function for 3D points reconstruction
void StereoReconstruction::compute3DReconstruction(
		const cv::Mat& disparity,
		cv::Mat& pointCloud)
{

	cv::Mat error_mask;
	cv::Mat disparity_withMask;

//	pointCloud.create(disparity.rows, disparity.cols ,CV_32FC3);
//	pointCloud.setTo(0);
//	disparity.copyTo(disparity_withMask);

	reprojectImageTo3D(disparity, pointCloud, this->Q, true);
//	reprojectImageTo3D(disparity_withMask, pointCloud, this->Q, true);



}



