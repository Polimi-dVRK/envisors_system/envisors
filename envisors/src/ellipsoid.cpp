/*
 * ellipse.cpp
 *
 *  Created on: Nov 20, 2016
 *      Author: veronica
 */

#include<envisors/ellipsoid.hpp>

Ellipsoid::Ellipsoid()
{
	this->bounding_box_pub = this->n.advertise<visualization_msgs::Marker>("/SA/bounding_box", 1);
	this->ellipsoid.center.x = 0.0;
	this->ellipsoid.center.y = 0.0;
	this->ellipsoid.center.z = 0.0;
	this->ellipsoid.x_axis = 0.0;
	this->ellipsoid.y_axis = 0.0;
	this->ellipsoid.z_axis = 0.0;
	this->b_box.center.x = 0.0;
	this->b_box.center.y = 0.0;
	this->b_box.center.z = 0.0;
	this->b_box.x_axis = 0.0;
	this->b_box.y_axis = 0.0;
	this->b_box.z_axis = 0.0;
	this->b_box.x_axis = 0.0;
}

void Ellipsoid::set_pointCloud(
		pcl::PointCloud<pcl::PointXYZRGB>& pc_t)
{
	if (pc_t.empty())
	{
		std::cout << "[ERROR] Teh point cloud is invalid!" << std::endl;
		return;
	}

	pc_t_ = pc_t;
}

void Ellipsoid::set_bounding_box()
{

	if (pc_t_.empty())
	{
		std::cout << "[ERROR] Teh point cloud is invalid!" << std::endl;
		return;
	}

	pcl::PointCloud<pcl::PointXYZRGB>::iterator pc_t_it;

	pc_t_it = pc_t_.begin();
	this->b_box.minX = 1000;
	this->b_box.minY = 1000;
	this->b_box.minZ = 1000;
	this->b_box.maxX = -1000;
	this->b_box.maxY = -1000;
	this->b_box.maxZ = -1000;


	for(pc_t_it = pc_t_.begin(); pc_t_it != pc_t_.end(); pc_t_it++)
	{
		if(pc_t_it->x == -1 || pc_t_it->y == -1 || pc_t_it->z == -1)
		{
			continue;
		}
		/*find max X*/
		if(pc_t_it->x > this->b_box.maxX)
		{
			this->b_box.maxX = pc_t_it->x;
		}
		/*find max Y*/
		if(pc_t_it->y > this->b_box.maxY)
		{
			this->b_box.maxY = pc_t_it->y;
		}
		/*find max Z*/
		if(pc_t_it->z > this->b_box.maxZ)
		{
			this->b_box.maxZ = pc_t_it->z;
		}
		/*find max X*/
		if(pc_t_it->x < this->b_box.minX)
		{
			this->b_box.minX = pc_t_it->x;
		}
		/*find max Y*/
		if(pc_t_it->y < this->b_box.minY)
		{
			this->b_box.minY = pc_t_it->y;
		}
		/*find max Z*/
		if(pc_t_it->z < this->b_box.minZ)
		{
			this->b_box.minZ = pc_t_it->z;
		}

	}

	this->b_box.x_axis = this->b_box.maxX - this->b_box.minX;
	this->b_box.y_axis = this->b_box.maxY - this->b_box.minY;
	this->b_box.z_axis = this->b_box.maxZ - this->b_box.minZ;

	this->b_box.center.x = this->b_box.minX + this->b_box.x_axis/2.0;
	this->b_box.center.y = this->b_box.minY + this->b_box.y_axis/2.0;
	this->b_box.center.z = this->b_box.minZ + this->b_box.z_axis/2.0;

	std::cout << "xmin " << this->b_box.minX   << " xmax " << this->b_box.maxX  << std::endl;
	std::cout << "ymin " << this->b_box.minY   << " ymax " << this->b_box.maxY  << std::endl;
	std::cout << "zmin " << this->b_box.minZ   << " zmax " << this->b_box.maxZ  << std::endl;

	std::cout << "x_axis " << this->b_box.x_axis << std::endl;
	std::cout << "y_axis " << this->b_box.y_axis << std::endl;
	std::cout << "z_axis " << this->b_box.z_axis << std::endl;

	std::cout << "xc " << this->b_box.center.x  << std::endl;
	std::cout << "yc " << this->b_box.center.y  << std::endl;
	std::cout << "zc " << this->b_box.center.z  << std::endl;

	//	this->remove_noise();
}


void Ellipsoid::create_bounding_box()
{

	Eigen::Vector3f  	translation(
			this->b_box.center.x,
			this->b_box.center.y,
			this->b_box.center.z);
	Eigen::Quaternionf 	rotation;
	rotation.Identity();

	vtkSmartPointer< vtkDataSet > rectangle = pcl::visualization::createCube(translation,
			rotation,
			this->b_box.x_axis, // width
			this->b_box.y_axis, //height,
			this->b_box.z_axis //depth
	);
}

void Ellipsoid::publish_bounding_box()
{
	this->bounding_box_ros.header.frame_id = "/world";
	this->bounding_box_ros.header.stamp = ros::Time::now();
	this->bounding_box_ros.ns = "basic_shapes";
	this->bounding_box_ros.id = 0;
	this->bounding_box_ros.type = visualization_msgs::Marker::CUBE;
	this->bounding_box_ros.action = visualization_msgs::Marker::ADD;

	this->bounding_box_ros.pose.position.x = this->b_box.center.x;
	this->bounding_box_ros.pose.position.y = this->b_box.center.y;
	this->bounding_box_ros.pose.position.z = this->b_box.center.z;
	this->bounding_box_ros.pose.orientation.x = 0.0;
	this->bounding_box_ros.pose.orientation.y = 0.0;
	this->bounding_box_ros.pose.orientation.z = 0.0;
	this->bounding_box_ros.pose.orientation.w = 1.0;

	// Set the scale of the marker -- 1x1x1 here means 1m on a side
	this->bounding_box_ros.scale.x = this->b_box.x_axis;
	this->bounding_box_ros.scale.y = this->b_box.y_axis;
	this->bounding_box_ros.scale.z = this->b_box.z_axis;

	// Set the color -- be sure to set alpha to something non-zero!
	this->bounding_box_ros.color.r = 0.0f;
	this->bounding_box_ros.color.g = 1.0f;
	this->bounding_box_ros.color.b = 0.0f;
	this->bounding_box_ros.color.a = 0.5;

	this->bounding_box_ros.lifetime = ros::Duration();

	this->bounding_box_pub.publish(this->bounding_box_ros);

}

void Ellipsoid::remove_noise()
{
	// compute the distance of each point to the centroid
	std::vector<float> diff_to_centroid;
	float std = 0;
	float mean = 0;
	double diff = 0;
	diff_to_centroid.clear();

	if (pc_t_.empty())
		return;

	pcl::PointCloud<pcl::PointXYZRGB>::iterator pc_it;

	pc_it = pc_t_.begin();

	for(pc_it = pc_t_.begin(); pc_it != pc_t_.end(); pc_it++)
	{

		if(pc_it->x == -1 || pc_it->y == -1 || pc_it->z == -1)
			continue;

		diff = sqrt((pc_it->x - this->b_box.center.x)*(pc_it->x - this->b_box.center.x) +
				(pc_it->y - this->b_box.center.y)*(pc_it->y - this->b_box.center.y) +
				(pc_it->z - this->b_box.center.z)*(pc_it->z - this->b_box.center.z));


		diff_to_centroid.push_back(diff);

	}

	calculate_mean_SD(diff_to_centroid, mean, std);
	std::cout << "pc mean : " <<  mean << std::endl;
	std::cout << "pc standard deviation: " <<  std << std::endl;

	//recompute the polygon coordinates

	pc_it = pc_t_.begin();
	this->b_box.minX = 1000;
	this->b_box.minY = 1000;
	this->b_box.minZ = 1000;
	this->b_box.maxX = -1000;
	this->b_box.maxY = -1000;
	this->b_box.maxZ = -1000;


	for(pc_it = pc_t_.begin(); pc_it != pc_t_.end(); pc_it++)
	{
		if(pc_it->x == -1 || pc_it->y == -1 || pc_it->z == -1)
			continue;

		diff = sqrt((pc_it->x - this->b_box.center.x)*(pc_it->x - this->b_box.center.x) +
				(pc_it->y - this->b_box.center.y)*(pc_it->y - this->b_box.center.y) +
				(pc_it->z - this->b_box.center.z)*(pc_it->z - this->b_box.center.z));

		if(diff < mean-std || diff > mean+std )
			continue;

		/*find max X*/
		if(pc_it->x > this->b_box.maxX)
		{
			this->b_box.maxX = pc_it->x;
		}
		/*find max Y*/
		if(pc_it->y > this->b_box.maxY)
		{
			this->b_box.maxY = pc_it->y;
		}
		/*find max Z*/
		if(pc_it->z > this->b_box.maxZ)
		{
			this->b_box.maxZ = pc_it->z;
		}
		/*find max X*/
		if(pc_it->x < this->b_box.minX)
		{
			this->b_box.minX = pc_it->x;
		}
		/*find max Y*/
		if(pc_it->y < this->b_box.minY)
		{
			this->b_box.minY = pc_it->y;
		}
		/*find max Z*/
		if(pc_it->z < this->b_box.minZ)
		{
			this->b_box.minZ = pc_it->z;
		}
	}

	this->b_box.center.x = this->b_box.minX + this->b_box.x_axis/2.0;
	this->b_box.center.y = this->b_box.minY + this->b_box.y_axis/2.0;
	this->b_box.center.z = this->b_box.minZ + this->b_box.z_axis/2.0;

	this->b_box.x_axis = this->b_box.maxX - this->b_box.minX;
	this->b_box.y_axis = this->b_box.maxY - this->b_box.minY;
	this->b_box.z_axis = this->b_box.maxZ - this->b_box.minZ;



	std::cout << "xmin " << this->b_box.minX   << " xmax " << this->b_box.maxX  << std::endl;
	std::cout << "ymin " << this->b_box.minY   << " ymax " << this->b_box.maxY  << std::endl;
	std::cout << "zmin " << this->b_box.minZ   << " zmax " << this->b_box.maxZ  << std::endl;

	std::cout << "x_axis " << this->b_box.x_axis << std::endl;
	std::cout << "y_axis " << this->b_box.y_axis << std::endl;
	std::cout << "z_axis " << this->b_box.z_axis << std::endl;

	std::cout << "xc " << this->b_box.center.x  << std::endl;
	std::cout << "yc " << this->b_box.center.y  << std::endl;
	std::cout << "zc " << this->b_box.center.z  << std::endl;
}

void  Ellipsoid::set_ellipsoid()
{
	if(this->b_box.center.x == 0.0 && this->b_box.center.y == 0.0 &&	this->b_box.center.z == 0.0 &&
			this->b_box.x_axis == 0.0 && this->b_box.y_axis == 0.0 && this->b_box.z_axis == 0.0)
	{
		this->set_bounding_box();
	}

	this->ellipsoid.center = this->b_box.center;
	this->ellipsoid.x_axis = this->b_box.x_axis;
	this->ellipsoid.y_axis = this->b_box.y_axis;
	this->ellipsoid.z_axis = this->b_box.z_axis;

}

float Ellipsoid::is_inside(float x, float y, float z)
{
	float distance_x, distance_y ,distance_z;

	distance_x = fabsf(x - this->b_box.center.x) - this->b_box.x_axis/2;
	distance_y = fabsf(y - this->b_box.center.y) - this->b_box.y_axis/2;
	distance_z = fabsf(z - this->b_box.center.z) - this->b_box.z_axis/2;


	if(this->ellipsoid.center.x == 0.0 && this->ellipsoid.center.y == 0.0 &&	this->ellipsoid.center.z == 0.0 &&
			this->ellipsoid.x_axis == 0.0 && this->ellipsoid.y_axis == 0.0 && this->ellipsoid.z_axis == 0.0)
	{
		std::cout << "[ERROR] Please see the ellipsoid parameters" << std::endl;
		return -1;
	}
	else
	{
		if(distance_x < 0 && distance_y < 0 && distance_z < 0)
		{
			return 0.0;
		}

		if (distance_x < 0.0f)
			distance_x = 0.0f;
		if (distance_y < 0.0f)
			distance_y = 0.0f;
		if (distance_z < 0.0f)
			distance_z = 0.0f;

		float distance = sqrtf(
				distance_x * distance_x +
				distance_y * distance_y +
				distance_z * distance_z);
		return distance;


		//		if (x > this->b_box.minX && x < this->b_box.maxX &&
		//				y > this->b_box.minY && y < this->b_box.maxY &&
		//				z > this->b_box.minZ && z < this->b_box.maxZ)
		//			return true;
		//		else
		//			return false;
		//
		//		if(((((x - this->ellipsoid.center.x) * (x - this->ellipsoid.center.x))/(this->ellipsoid.x_axis*this->ellipsoid.x_axis)) +
		//				(((y - this->ellipsoid.center.y) * (y - this->ellipsoid.center.y))/(this->ellipsoid.y_axis*this->ellipsoid.y_axis)) +
		//				(((z - this->ellipsoid.center.z) * (z - this->ellipsoid.center.z))/(this->ellipsoid.z_axis*this->ellipsoid.z_axis)))  < 1)
		//			return true;
		//		else
		//			return false;
	}

}
