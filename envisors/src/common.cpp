
#include "envisors/common.hpp"

/*
 * Convert Images from cv::Mat to sensor_msgs::Images
 */

void fromMattoImage(
		const cv::Mat& img_in,
		sensor_msgs::Image* img_out,
		const std::string& encoding){


	cv_bridge::CvImage pics;
	pics.encoding = sensor_msgs::image_encodings::RGB8 ;
	pics.image = img_in;
	pics.toImageMsg(*img_out);
	img_out->header.stamp = ros::Time::now();
	img_out->header.frame_id = "depth_image";
	img_out->encoding = encoding ;
	img_out->width = img_in.cols;
	img_out->height = img_in.rows;

}

/*
 * Convert Images from sensor_msgs::Images to cv:Mat
 */

void fromImagetoMat(
		const sensor_msgs::Image& img_in,
		cv::Mat* img_out,
		const std::string& encoding){

	cv_bridge::CvImagePtr image_bridge;

	try
	{
		image_bridge = cv_bridge::toCvCopy(img_in, encoding);
	}
	catch (cv_bridge::Exception& e)
	{
		ROS_ERROR("cv_bridge exception: %s", e.what());
		return;
	}

	*img_out = image_bridge->image.clone();
}

/*
 * The function return true if the number is odd, otherwise return false
 * */
bool isOdd(int integer){

	return (integer % 2) != 0;
}

bool isEven(int integer){

	return (integer % 2) == 0;
}
/*
 * The function converts rgb images to gray images
 * */
void fromRGBtoGray(
		const cv::Mat& imgRGB,
		cv::Mat& imgGray){

	cv::cvtColor(imgRGB, imgGray, CV_BGR2GRAY);
}

void fromMattoPointCloud2(
		const cv::Mat& imgL_rect,
		const cv::Mat& pointCloud_Mat,
		sensor_msgs::PointCloud2& pointCloud_msgs){

	pointCloud_msgs.header.frame_id = "PSM1remote_center";
	pointCloud_msgs.header.seq = 0;
	pointCloud_msgs.header.stamp = ros::Time::now();
	pointCloud_msgs.fields.resize(4);
	pointCloud_msgs.fields[0].name = "x";
	pointCloud_msgs.fields[1].name = "y";
	pointCloud_msgs.fields[2].name = "z";
	pointCloud_msgs.fields[3].name = "rgb";
	pointCloud_msgs.fields[0].datatype = sensor_msgs::PointField::FLOAT32;
	pointCloud_msgs.fields[1].datatype = sensor_msgs::PointField::FLOAT32;
	pointCloud_msgs.fields[2].datatype = sensor_msgs::PointField::FLOAT32;
	pointCloud_msgs.fields[3].datatype = sensor_msgs::PointField::FLOAT32;
	pointCloud_msgs.fields[0].count = 1;
	pointCloud_msgs.fields[1].count = 1;
	pointCloud_msgs.fields[2].count = 1;
	pointCloud_msgs.fields[3].count = 1;
	pointCloud_msgs.fields[0].offset = 0;
	pointCloud_msgs.fields[1].offset = 4;
	pointCloud_msgs.fields[2].offset = 8;
	pointCloud_msgs.fields[3].offset = 16;
	pointCloud_msgs.width = pointCloud_Mat.cols * pointCloud_Mat.rows;
	pointCloud_msgs.height = 1;
	pointCloud_msgs.is_bigendian = true;
	pointCloud_msgs.is_dense = false;
	pointCloud_msgs.point_step = 32;
	pointCloud_msgs.row_step = pointCloud_msgs.point_step * pointCloud_msgs.width;
	pointCloud_msgs.data.resize(pointCloud_msgs.row_step * pointCloud_msgs.height);

	unsigned char *pcData = (unsigned char*)&pointCloud_msgs.data[0];
	float *pcPos;
	unsigned char color;
	int counter = 0;

	for (unsigned int i = 0; i < pointCloud_Mat.rows; i++)
	{
		for (unsigned int j = 0; j < pointCloud_Mat.cols; j++)
		{
			// Get pixel offset
			pcPos = (float*)&pcData[counter * pointCloud_msgs.point_step];

			// Write 3D coordinates
			cv::Point3f cvPoint = pointCloud_Mat.at<cv::Point3f>(i, j);
			pcPos[0] = cvPoint.x;
			pcPos[1] = cvPoint.y;
			pcPos[2] = cvPoint.z;

			// Write color
			uint8_t b = imgL_rect.at<cv::Vec3b>(i,j)[0];
			uint8_t g = imgL_rect.at<cv::Vec3b>(i,j)[1];
			uint8_t r = imgL_rect.at<cv::Vec3b>(i,j)[2];
			uint32_t rgb = ((uint32_t)r << 16 | (uint32_t)g << 8 | (uint32_t)b);
			pcPos[4] = *(float*)&rgb;

			counter++;
		}
	}
}


void fromPosetoMat(const geometry_msgs::Pose& pose_input,
		cv::Mat& R, cv::Mat& t)
{
	t.create(3,1, CV_32F);
	t.at<float>(0,0) = pose_input.position.x;
	t.at<float>(1,0) = pose_input.position.y;
	t.at<float>(2,0) = pose_input.position.z;

	R.create(4,1, CV_32F);
	R.at<float>(0,0) = pose_input.orientation.x;
	R.at<float>(1,0) = pose_input.orientation.y;
	R.at<float>(2,0) = pose_input.orientation.z;
	R.at<float>(3,0) = pose_input.orientation.w;
}

void fromMattoPose(const cv::Mat& R, const cv::Mat& t,
		geometry_msgs::Pose& pose_output)
{

	pose_output.position.x = t.at<float>(0,0);
	pose_output.position.y = t.at<float>(1,0);
	pose_output.position.z = t.at<float>(2,0);

	pose_output.orientation.x = R.at<float>(0,0);
	pose_output.orientation.y = R.at<float>(1,0);
	pose_output.orientation.z = R.at<float>(2,0);
	pose_output.orientation.w = R.at<float>(3,0);
}

void readParamsCamera(
		const std::string fileName,
		cv::Mat& cameraMatrix,
		cv::Mat& distCoeff)
{
	//reading intrinsic parameters
	cv::FileStorage fs;
	fs.open(fileName, cv::FileStorage::READ);

	if(!fs.isOpened())
	{
		std::cout << "[ERROR] Failed to open :" << fileName << std::endl;
		return;
	}

	fs["M"] >> cameraMatrix;
	fs["D"] >> distCoeff;
	fs.release();

}

void readExtrisicParamsCamera(
		const std::string fileName,
		cv::Mat& R,
		cv::Mat& T)
{
	//reading intrinsic parameters
	cv::FileStorage fs;
	fs.open(fileName, cv::FileStorage::READ);

	if(!fs.isOpened())
	{
		std::cout << "[ERROR] Failed to open :" << fileName << std::endl;
		return;
	}
	else
	{
		//		std::cout << "Reading :" << fileName << std::endl;
	}

	fs["R"] >> R;
	fs["T"] >> T;
	fs.release();

}

void readRectificationMatrix(
		const std::string fileName,
		cv::Mat& R)
{
	cv::FileStorage fs;
	fs.open(fileName, cv::FileStorage::READ);

	if(!fs.isOpened())
	{
		std::cout << "[ERROR] Failed to open :" << fileName << std::endl;
		return;
	}

	fs["R"] >> R;
	fs.release();
}

void readTranslation(
		const std::string fileName,
		cv::Mat& T)
{
	//reading intrinsic parameters
	cv::FileStorage fs;
	fs.open(fileName, cv::FileStorage::READ);

	if(!fs.isOpened())
	{
		std::cout << "[ERROR] Failed to open :" << fileName << std::endl;
		return;
	}
	else
	{
		std::cout << "Reading :" << fileName << std::endl;
	}

	fs["T"] >> T;
	fs.release();
}

void readStereoParamsCamera(
		const std::string fileName,
		cv::Mat& cameraMatrixL,
		cv::Mat& distCoeffL,
		cv::Mat& cameraMatrixR,
		cv::Mat& distCoeffR,
		cv::Mat& R, cv::Mat& T,cv::Mat& E,cv::Mat& F)

{
	//reading all parameters
	cv::FileStorage fs;
	fs.open(fileName, cv::FileStorage::READ);

	if(!fs.isOpened())
	{
		std::cout << "[ERROR] Failed to open :" << fileName << std::endl;
		return;
	}

	fs["M_l"] >> cameraMatrixL;
	fs["D_l"] >> distCoeffL;
	fs["M_r"] >> cameraMatrixR;
	fs["D_r"] >> distCoeffR;
	fs["R"] >> R;
	fs["T"] >> T;
	fs["E"] >> E;
	fs["F"] >> F;
	fs.release();
}

void saveParamsCamera(
		const std::string fileName,
		cv::Mat& cameraMatrix,
		cv::Mat& distCoeff)
{

	//saving intrinsic parameters
	cv::FileStorage fs(fileName, cv::FileStorage::WRITE);

	fs << "M" << cameraMatrix;
	fs << "D" << distCoeff;
	fs.release();
}


void saveExtrisicParamsCamera(
		const std::string fileName,
		cv::Mat& R,
		cv::Mat& T)
{
	//saving all parameters
	cv::FileStorage fs(fileName, cv::FileStorage::WRITE);
	if (!fs.isOpened()) {
		ROS_ERROR_STREAM("Unable to open file " << fileName);
	}

	fs << "R" << R;
	fs << "T" << T;
	fs.release();
}


void saveStereoParamsCamera(
		const std::string fileName,
		cv::Mat& cameraMatrixL,
		cv::Mat& distCoeffL,
		cv::Mat& cameraMatrixR,
		cv::Mat& distCoeffR,
		cv::Mat& R, cv::Mat& T,cv::Mat& E,cv::Mat& F)
{

	//saving all parameters
	cv::FileStorage fs(fileName, cv::FileStorage::WRITE);

	fs << "M_l" << cameraMatrixL;
	fs << "D_l" << distCoeffL;
	fs << "M_r" << cameraMatrixR;
	fs << "D_r" << distCoeffR;
	fs << "R" << R;
	fs << "T" << T;
	fs << "E" << E;
	fs << "F" << F;
	fs.release();
}

void readParamQ(
		const std::string fileName,
		cv::Mat& Q)
{
	//reading all parameters
	cv::FileStorage fs;
	fs.open(fileName, cv::FileStorage::READ);

	if(!fs.isOpened())
	{
		std::cout << "Failed to open :" << fileName << std::endl;
		return;
	}

	fs["Q"] >> Q;
	fs.release();

}


bool getColorMapValue(
		float value,
		cv::Vec3b& color)
{
	cv::Mat prob_value;
	cv::Mat color_prob_value;
	prob_value.create(100, 1, CV_32FC1);
	prob_value.setTo(0);

	//check value range
	if(value < 0 || value > 1 || value != value)
	{
		std::cout << "Value out of range [0-1]! " << std::endl;
		return false;
	}
	else{

		for(int i = 0; i < prob_value.rows; i++)
		{
			int j = 0;
			prob_value.at<float>(i,j) = (float)i/100.0;
		}

		applyColorMap(prob_value,
				color_prob_value,
				cv::COLORMAP_JET);

		color = color_prob_value.at<cv::Vec3b>(int(value*100),0);


		return true;
	}
}


void create_color_bar(
		cv::Mat& img,
		double th,
		double act_value)
{

	if(img.rows != 0){
		// draw color bar
		cv::Mat rect( img.rows ,100, CV_32F);
		double min_val,max_val;

		for(int i = 0; i < rect.rows; i++)
		{
			for(int j = 0; j < rect.cols; j++)
			{
				rect.at<float>(i,j) = i;
			}
		}

		//double min_val, max_val;
		cv::minMaxIdx(rect, &min_val, &max_val);
		float ratio = 255.0 / max_val;
		cv::Mat norm_img = cv::Mat(rect.size(), CV_32F);
		norm_img = ratio * rect;
		norm_img.convertTo(norm_img, CV_8UC1);

		cv::Mat col_img;
		cv::applyColorMap(norm_img, col_img, cv::COLORMAP_JET);

		//std::cout << "fail: " << act_value << std::endl;

		int row_inv;

		for(int row=0; row <  rect.rows; ++row)
		{
			row_inv = rect.rows - row - 1;

			for(int col=0; col < rect.cols; ++col)
			{
				if(row < 5)
				{
					img.at<uchar>(row_inv,col+60) = 0;
				}
				else if(row > rect.rows -5)
				{
					img.at<uchar>(row_inv,col+60) = 0;
				}
				else if (row >= (int)(th * rect.rows) - 4 &&
						row <= (int)(th * rect.rows) + 4)
				{
					img.at<uchar>(row_inv,col+60) = 0;
				}
				else if (row == (int)(act_value * rect.rows) - 7 ||
						row == (int)(act_value * rect.rows) - 6 ||
						row == (int)(act_value * rect.rows) + 6 ||
						row == (int)(act_value * rect.rows) + 7)
				{
					img.at<uchar>(row_inv,col+60) = 0;
				}
				else
				{
					img.at<uchar>(row_inv,col+60) = col_img.at<uchar>(row,col);
				}

			}
		}

	}

}

void compute_histogram(
		const cv::Mat& img,
		cv::Mat& hist)
{
	hist.create(256, 1, CV_32FC1);
	hist.setTo(0);

	for(int i = 0; i < img.rows; i++)
	{
		for(int j = 0; j < img.cols; j++)
		{
			hist.at<float>(img.at<uchar>(i,j),1)++;

		}
	}
}

void compute_histogram_acc(
		const cv::Mat& hist,
		cv::Mat& hist_acc)
{
	hist_acc.create(256, 1, CV_32FC1);
	hist_acc.setTo(0);

	hist_acc.at<float>(0,1) = hist.at<float>(0,1);

	for(int i = 1; i < hist.rows; i++)
	{
		hist_acc.at<float>(i,1) = hist_acc.at<float>(i-1,1) + hist.at<float>(i,1);
	}
}

void image_equalization(
		const cv::Mat& img_1,
		cv::Mat& img_1_eq)
{
	//	cv::SparseMat hist_1;
	//	cv::SparseMat hist_2;

	cv::Mat histMap(256, 1, CV_8UC1);
	cv::Mat hist_1(256, 1, CV_32FC1);
	cv::Mat hist_acc_1(256, 1, CV_32FC1);

	float img_dim = img_1.rows * img_1.cols;

	img_1_eq.create(img_1.rows, img_1.cols, CV_8UC1);

	// compute histogram image_1
	compute_histogram(img_1, hist_1);

	// accumulate histogram image_1
	compute_histogram_acc(hist_1, hist_acc_1);

	for(int i = 0; i < img_1.rows; i++)
	{
		for(int j = 0; j < img_1.cols; j++)
		{
			img_1_eq.at<uchar>(i,j) = hist_acc_1.at<float>(img_1.at<uchar>(i,j),1)*(255/img_dim);

		}
	}
}


void images_equalization(
		const cv::Mat& img_1, // to be transformed
		const cv::Mat& img_2, // reference
		cv::Mat& img_1_eq)
{

	//	cv::SparseMat hist_1;
	//	cv::SparseMat hist_2;
	cv::Mat histMap(255, 1, CV_8UC1);
	cv::Mat hist_1(255, 1, CV_32FC1);
	cv::Mat hist_2(255, 1, CV_32FC1);
	cv::Mat hist_acc_1(255, 1, CV_32FC1);
	cv::Mat hist_acc_2(255, 1, CV_32FC1);

	img_1_eq.create(img_1.rows, img_1.cols, CV_8UC1);

	//	int nchannel[1] = {0};
	//	int histSize[1] = {256};
	//	float range[] = {0, 255};
	//	const float* ranges[] = {range};
	//	cv::calcHist(&img_1, 1, nchannel,
	//			cv::Mat(), hist_1, 1, histSize, ranges, true, true);
	//
	//	cv::calcHist(&img_1, 1, nchannel,
	//			cv::Mat(), hist_2, 1, histSize, ranges, true, true);

	// compute histogram image_1
	compute_histogram(img_1, hist_1);
	// compute histogram image_2
	compute_histogram(img_2, hist_2);

	// accumulate histogram image_1
	compute_histogram_acc(hist_1, hist_acc_1);
	// accumulate histogram image_2
	compute_histogram_acc(hist_2, hist_acc_2);

	float hist_prev;
	for(int i = 0; i < 256; i++)
	{
		hist_prev = hist_2.at<float>(0,1);
		for(int j = 1; j < 256; j++)
		{
			if(hist_2.at<float>(j,1) > hist_1.at<float>(i,1))
			{
				float temp_dist1 = 0;
				float temp_dist2 = 0;
				temp_dist1 = hist_1.at<float>(i,1) - hist_prev;
				temp_dist2 =  hist_2.at<float>(j,1) - hist_1.at<float>(i,1);
				if(temp_dist1 < temp_dist2)
				{
					histMap.at<uchar>(i,1) = j-1;
				}
				else
				{
					histMap.at<uchar>(i,1) = j;
				}

			}
			hist_prev = hist_2.at<float>(j,1);
		}

	}

	for(int i = 0; i < img_1.rows; i++)
	{
		for(int j = 0; j < img_1.cols; j++)
		{
			img_1_eq.at<uchar>(i,j) = histMap.at<uchar>(img_1.at<uchar>(i,j),1);

		}
	}
}

// this function compute normalized hsv histogram as a bidimensional combination
// of s and h channel
void hsv_image_histogram(
		const cv::Mat& hsv_img,
		cv::Mat& hist_asv,
		std::vector<cv::Point2f> &safety_area,
		cv::Mat& SA_mask,
		int n_hbin,
		int n_sbin,
		float th_seg,
		segModel* SegModel)
{
	int num_bin;
	float h_step;
	float s_step;
	float h;
	float s;
	int h_idx;
	int s_idx;
	int bin_idx;

	num_bin = n_hbin * n_sbin;

	h_step = 180.0 / n_hbin;
	s_step = 256.0 / n_sbin;

	//std::cout << __LINE__ << std::endl;
	hist_asv.create(n_hbin*n_sbin, 1, CV_32FC1);
	//std::cout << __LINE__ << std::endl;
	hist_asv.setTo(0);

	// compute rectangular area around the SA
	//std::cout << __LINE__ << std::endl;
	cv::Rect rect_SA = boundingRect(safety_area);
	bool totallyOut = false;

	if (rect_SA.x < 0)
		rect_SA.x = 0;
	if (rect_SA.x >= hsv_img.cols)
		totallyOut = true;
	if (rect_SA.x + rect_SA.width >= hsv_img.cols)
		rect_SA.width = hsv_img.cols - rect_SA.x -1;

	if (rect_SA.y < 0)
		rect_SA.y = 0;
	if (rect_SA.y >= hsv_img.rows)
		totallyOut = true;
	if (rect_SA.y + rect_SA.height >= hsv_img.rows)
		rect_SA.height = hsv_img.rows - rect_SA.y -1;



	if(!totallyOut){
		int count_nhist = 0;
		//std::cout << __LINE__ << std::endl;

		for(int row = rect_SA.y; row < rect_SA.y + rect_SA.height; row++)
		{
			for(int col = rect_SA.x; col < rect_SA.x + rect_SA.width; col++)
			{
				cv::Point p_temp;
				p_temp.x = col;
				p_temp.y = row;

				//if (cv::pointPolygonTest(safety_area, p_temp, false) < 0)
				//	continue;
				if (SA_mask.at<uchar>(row, col) == 0)
					continue;

				// The point is inside the safety area

				//std::cout << __LINE__ << std::endl;
				if (SegModel != NULL && SegModel->getKpProb(p_temp, hsv_img) <= th_seg)
					continue;

				// The point belongs to the segmentation model

				//std::cout << __LINE__ << std::endl;
				cv::Vec3b pixel = hsv_img.at<cv::Vec3b>(row, col);
				// get bin idx
				h = pixel[0];
				s = pixel[1];

				if(h > 180.0)
					h = 180.0;

				if(s > 256.0)
					s = 256.0;

				h_idx = int(h/h_step);
				s_idx = int(s/s_step);

				if (h_idx >= n_hbin || s_idx >= n_sbin)
				{
					//				std::cout << "HSV: " << h << ", " << s << std::endl;
					continue;
				}

				bin_idx = h_idx + s_idx * n_hbin;

				//std::cout << __LINE__ << std::endl;
				hist_asv.at<float>(bin_idx, 0)++;
				count_nhist++;
				//std::cout << __LINE__ << std::endl;
			}
		}

		for (int i = 0; i < hist_asv.rows; i++)
		{
			if (count_nhist != 0){
				hist_asv.at<float>(i, 0) = hist_asv.at<float>(i, 0) / count_nhist;
			}
		}
	}
}


void compute_BhattaCharryya_distance(
		cv::Mat& hsv_hist_1,
		cv::Mat& hsv_hist_2,
		double &bh_distance)
{

	double ro_h1_h2 = 0.0;

	// check if hist_1 and hist_2 have the same size
	if(hsv_hist_1.rows == hsv_hist_2.rows)
	{
		for(int i = 0; i < hsv_hist_1.rows; i++)
		{
			ro_h1_h2 = ro_h1_h2 + sqrt(hsv_hist_1.at<float>(i, 0)*hsv_hist_2.at<float>(i, 0));
		}

		if (ro_h1_h2 >= 1.0)
			bh_distance = 0.0;
		else
			bh_distance = sqrt(1.0 - ro_h1_h2);

		std::cout << "BhattaCharryya distance: " << bh_distance << std::endl;

	}
	else
	{
		bh_distance = 1.0;
		std::cout << "hist_1 and hist_2 have to have the same size" << std::endl;
	}
}


void calculate_mean_SD(
		std::vector<float>& data,
		float& mean,
		float& standardDeviation)
{
	float sum = 0.0;
	mean = 0.0;
	standardDeviation = 0.0;

	int i;

	for(i = 0; i < data.size(); ++i)
	{
		sum += data[i];
	}

	mean = sum/data.size();

	for(i = 0; i < data.size(); ++i)
		standardDeviation += pow(data[i] - mean, 2);

	standardDeviation =  sqrt(standardDeviation / data.size());

	std::cout << "pc mean : " <<  mean << std::endl;
	std::cout << "pc standard deviation: " <<  standardDeviation << std::endl;
}

void draw_gauge(
		cv::Mat& img,
		cv::Point top_left_corner,
		cv::Mat& gauge_img,
		cv::Mat& gauge_mask,
		float alpha)
{
	cv::Rect rect;
	float r = gauge_img.rows;
	float d = r * 0.06;

	rect.x = top_left_corner.x;
	rect.y = top_left_corner.y;
	rect.height = gauge_img.rows;
	rect.width = gauge_img.cols;

	//draw gauge on img
	for(int i = rect.y; i < rect.y + rect.height; i++)
	{
		for(int j = rect.x; j < rect.x + rect.width; j++)
		{

			if(gauge_mask.at<uchar>(i - rect.y, j - rect.x) == 255)
			{
				img.at<cv::Vec3b>(i , j) = gauge_img.at<cv::Vec3b>(i - rect.y, j - rect.x);

			}
		}
	}


	// compute the cursor
	std::vector<std::vector<cv::Point> > p;// p0, p1, p2, p3, p4;
	p.resize(1);
	p[0].resize(5);

	cv::Point p0;

	p0.x = rect.x + rect.width/2;
	p0.y = rect.y + rect.height;

	p[0][0].x = p0.x - r * cos(alpha);
	p[0][0].y = p0.y - r * sin(alpha);

	p[0][1].x = p0.x - d * sin(alpha);
	p[0][1].y = p0.y + d * cos(alpha);

	p[0][2].x = p0.x + d * cos(alpha);
	p[0][2].y = p0.y + d * sin(alpha);

	p[0][3].x = p0.x + d * sin(alpha);
	p[0][3].y = p0.y - d * cos(alpha);

	p[0][4].x = p0.x - r * cos(alpha);
	p[0][4].y = p0.y - r * sin(alpha);


	cv::fillPoly(img, p, cv::Scalar(176,176,176));
	cv::drawContours(img, p, -1,cv::Scalar(0,0,0));

}

void fromMattoKDL(
		cv::Mat& R,
		cv::Mat& t,
		KDL::Frame& T)
{
	T.Identity();
	T.M = KDL::Rotation(
			R.at<double>(0,0), R.at<double>(0,1), R.at<double>(0,2),
			R.at<double>(1,0), R.at<double>(1,1), R.at<double>(1,2),
			R.at<double>(2,0), R.at<double>(2,1), R.at<double>(2,2));
	T.p[0] = t.at<double>(0,0);
	T.p[1] = t.at<double>(1,0);
	T.p[2] = t.at<double>(2,0);
}

void fromKDLtoMat(
		KDL::Frame& T,
		cv::Mat& R,
		cv::Mat& t)
{
//	R.at<double>(0,0) = T.M[0];
//	R.at<double>(0,1) = T.M[3];
//	R.at<double>(0,2) = T.M[6];
//	R.at<double>(1,0) = T.M[1];
//	R.at<double>(1,1) = T.M[4];
//	R.at<double>(1,2) = T.M[7];
//	R.at<double>(2,0) = T.M[2];
//	R.at<double>(2,1) = T.M[5];
//	R.at<double>(2,2) = T.M[8];

	t.at<double>(0,0) = T.p[0];
	t.at<double>(1,0) = T.p[1];
	t.at<double>(2,0) = T.p[2];

}
