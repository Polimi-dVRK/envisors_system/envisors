/*
 * stereo_correspondence.cpp
 *
 *  Created on: Sept 5, 2014
 *      Author: Veronica
 */

#include "envisors/stereo_correspondence.hpp"


bool StereoCorrespondence::setParamCENSUS(
		bool isSubSampling,
		bool isSpecularity,
		double saturation,
		double value,
		bool isSubPixelRefinement,
		bool isModifiedCensus,
		bool isAggregationCost,
		int minDisparities,
		int numberOfDisparities,
		int spurie_Th,
		int n_census,
		int m_census,
		int threshold_LRC,
		int m_aggCost,
		int n_aggCost)
{
	this->isSubSampling = isSubSampling;
	this->isSpecularity = isSpecularity;
	this->saturation = saturation;
	this->value = value;
	this->isSubPixelRefinement = isSubPixelRefinement;
	this->isModifiedCensus = isModifiedCensus;
	this->isAggregationCost = isAggregationCost;

	this->minDisparities = minDisparities;
	this->numberOfDisparities = numberOfDisparities;

	this->spurie_Th = spurie_Th;
	this->n_census = n_census;
	this->m_census = m_census;
	this->threshold_LRC = threshold_LRC;
	this->m_aggCost = m_aggCost;
	this->n_aggCost = n_aggCost;

	return true;
}

bool StereoCorrespondence::setParamSAD(
		bool isSubSampling,
		bool isEqualized,
		bool isSpecularity,
		double saturation,
		double value,
		bool isSubPixelRefinement,
		int minDisparities,
		int numberOfDisparities,
		int spurie_Th,
		int threshold_LRC,
		int m_aggCost,
		int n_aggCost)
{
	this->isSubSampling = isSubSampling;
	this->isEqualized = isEqualized;
	this->isSpecularity = isSpecularity;
	this->saturation = saturation;
	this->value = value;
	this->isSubPixelRefinement = isSubPixelRefinement;
	this->minDisparities = minDisparities;
	this->numberOfDisparities = numberOfDisparities;
	this->spurie_Th = spurie_Th;
	this->threshold_LRC = threshold_LRC;
	this->m_aggCost = m_aggCost;
	this->n_aggCost = n_aggCost;

	return true;
}

bool StereoCorrespondence::setParamPostProcessing(
		bool ispostProcessing,
		bool isSpeckleRemoval,
		bool isFillHole,
		bool isAVERAGEFiltering,
		bool isMEDIANFiltering,
		bool isPLANE_FITFiltering,
		bool isQUAD_FITFiltering,
		bool isRANSAC_FITFiltering,
		bool isPoissonSmoothing,
		bool isBackgroundRemoval,
		bool isRemove_outliers,
		bool isReplace_all_points,
		bool isRemove_inclined_plane,
		bool isBilateralFilter,
		double newVal_speckle,
		int maxSize_speckle,
		double maxDiff_speckle,
		int diamPixelWind,
		double sigmaParam,
		int n_rand,
		int iteration,
		int min_num_point,
		double RANSAC_threshold)
{
	this->ispostProcessing = ispostProcessing;
	this->isSpeckleRemoval = isSpeckleRemoval;
	this->isFillHole = isFillHole;
	this->isAVERAGEFiltering = isAVERAGEFiltering;
	this->isMEDIANFiltering = isMEDIANFiltering;
	this->isPLANE_FITFiltering = isPLANE_FITFiltering;
	this->isQUAD_FITFiltering = isQUAD_FITFiltering;
	this->isRANSAC_FITFiltering = isRANSAC_FITFiltering;
	this->isPoissonSmoothing = isPoissonSmoothing;
	this->isBackgroundRemoval = isBackgroundRemoval;
	this->isRemove_outliers = isRemove_outliers;
	this->isReplace_all_points = isReplace_all_points;
	this->isRemove_inclined_plane = isRemove_inclined_plane;
	this->isBilateralFilter = isBilateralFilter;
	this->newVal_speckle = newVal_speckle;
	this->maxSize_speckle = maxSize_speckle;
	this->maxDiff_speckle = maxDiff_speckle;
	this->diamPixelWind = diamPixelWind;
	this->sigmaParam = sigmaParam;
	this->n_rand = n_rand;
	this->iteration = iteration;
	this->min_num_point = min_num_point;
	this->RANSAC_threshold = RANSAC_threshold;

	return true;
}

bool StereoCorrespondence::processCENSUS(
		cv::Mat& imgL_grey,
		cv::Mat& imgR_grey,
		/*const cv::Mat& label,*/
		cv::Mat& mask_SA_left,
		cv::Mat& mask_SA_right,
		cv::Mat& mask_SA_right_to_left,
		cv::Rect SA_boundary,
		/*cv::Mat& disparity,*/
		cv::Mat& mask_specularity)
{
	mask_specularity.copyTo(this->mask_disparity);
	mask_specularity.setTo(0);
	
    mask_disparity.setTo(0);
	initVariables(imgL_grey.cols, imgL_grey.rows);


	// PRE-PROCESSING*********************************************************
#ifdef MULTITHREAD
	ThreadCensusData dataRight = {(void*)this, &imgR_grey, &mask_SA_right, &SA_boundary};
	pthread_t threadCensusRight;
	pthread_create(&threadCensusRight, NULL, &StereoCorrespondence::run_censusRight, (void*)&dataRight);

	censusLeft.setCensus(
			imgL_grey, n_census, m_census, mask_SA_left, CENSUS_TRANSFORM_MODIFIED, false, SA_boundary, 0, 0);

	//pthread_join(threadCensusLeft, NULL);
	pthread_join(threadCensusRight, NULL);
#else
	censusLeft.setCensus(
			imgL_grey, n_census, m_census, mask_SA_left, CENSUS_TRANSFORM_MODIFIED, false, SA_boundary, 0, 0);
	censusRight.setCensus(
			imgR_grey, n_census, m_census, mask_SA_right, CENSUS_TRANSFORM_MODIFIED, true, SA_boundary,minDisparities, numberOfDisparities);
#endif
	
    // Hamming distance is computed not between two identical pixels but between all the pixels less than 'n'
    // pixels away on the same row where 'n' is related to the maximum disparity.
	HammingImage.computeHammingDistanceImage(
			censusLeft, censusRight, mask_SA_left, minDisparities, numberOfDisparities);


	// Compute Disparity Left to Right and Right to Left
#ifdef MULTITHREAD
	ThreadDisparityData dispData = {(void*)this, &mask_SA_right_to_left};
	
    pthread_t threadDisparity;
	pthread_create(&threadDisparity, NULL, &StereoCorrespondence::run_computeDisparityR2LAggCost, (void*)&dispData);
	
    computeDisparityL2RAggCost(this->dispL2R_census, this->mask_disparity, mask_SA_left);
	pthread_join(threadDisparity, NULL);
	
#else
	timeBegin = ros::Time::now();
	computeDisparityL2RAggCost(this->dispL2R_census, this->mask_disparity, mask_SA_left);
	timeEnd = ros::Time::now();
	time = timeEnd - timeBegin;
#ifdef COMPUTATIONAL_TIME
	std::cout << "[TIME] disparity L2R calculation: " << time << std::endl;
#endif

	timeBegin = ros::Time::now();
	computeDisparityR2LAggCost(this->dispR2L_census, mask_SA_right_to_left);
	timeEnd = ros::Time::now();
	time = timeEnd - timeBegin;
#ifdef COMPUTATIONAL_TIME
	std::cout << "[TIME] disparity R2L calculation: " << time << std::endl;
#endif
#endif

	// Check for occlusion
	checkLRConsistencyOcclusion(
			dispL2R_census, dispR2L_census,
			threshold_LRC, disp, mask_disparity, mask_SA_left);
	
#ifdef COMPUTATIONAL_TIME
	std::cout << "[TIME] consistency check: " << time << std::endl;
#endif

	//	// Postprocessing
	//	if (ispostProcessing)
	//	{
	//		std::cout << "post processing" << std::endl;
	//		if (!this->postProcess(label))
	//			return false;
	//	}
	//
	//
	//	this->disp.copyTo(disparity);


	return true;
}

bool StereoCorrespondence::postProcess(
		cv::Mat& label,
		cv::Mat& disparity,
		segModel* SegModel,
		cv::Mat& img_hsv,
		std::vector<cv::Point2i>& contours,
		cv::Rect& SA_rect)
{
	// Postprocessing
	if (ispostProcessing)
	{
		if (!this->postProcess_(label, SegModel, img_hsv, contours, SA_rect))
			return false;

	}

	this->disp.copyTo(disparity);
	return true;
}


bool StereoCorrespondence::processSAD(
		cv::Mat& imgL_rect,
		cv::Mat& imgR_rect,
		const cv::Mat& label,
		cv::Mat& disparity)
{

	this->initVariables(imgL_rect.cols, imgL_rect.rows);


	// PRE-PROCESSING*********************************************************
	// Remove Specular reflection
	if(this->isSpecularity){
		removeSpecularity(imgL_rect, this->mask_disparity,
				this->saturation, this->value);
		//		removeSpecularity(imgR_rect, maskR_specularity);
	}

	// Convertion to gray images
	fromRGBtoGray(imgL_rect, this->imgL_gray);
	fromRGBtoGray(imgR_rect, this->imgR_gray);

	// Gray Images Equalization
	if (this->isEqualized){
		equalizeImage(this->imgL_gray, this->imgL_gray);
		equalizeImage(this->imgR_gray, this->imgR_gray);
	}

	sadImage.computeSAD(this->imgL_gray, this->imgR_gray,
			this->minDisparities, this->numberOfDisparities);
	SAD_disparityL2R_opt(this->dispL2R_SAD, this->mask_disparity);
	SAD_disparityR2L_opt(this->dispR2L_SAD);

	cv::Mat mask_SA;
	mask_SA.create(this->imgL_gray.rows, this->imgL_gray.cols, CV_8UC1);
	mask_SA.setTo(0);
	//Consistency Check
	checkLRConsistencyOcclusion(
			this->dispL2R_SAD, this->dispR2L_SAD,
			this->threshold_LRC, this->disp, this->mask_disparity, mask_SA);

	// Postprocessing
	//	if (ispostProcessing)
	//		if (!this->postProcess(label))
	//			return false;


	this->disp.copyTo(disparity);


	return true;
}


cv::Mat StereoCorrespondence::getImgLGray()
{
	return this->imgL_gray;
}


cv::Mat StereoCorrespondence::getImgRGray()
{
	return this->imgR_gray;
}

void StereoCorrespondence::getImgLCENSUS(cv::Mat& censusImgL){
	this->censusLeft.convertToMat(censusImgL);
}

void StereoCorrespondence::getImgRCENSUS(cv::Mat& censusImgR){
	this->censusRight.convertToMat(censusImgR);
}

cv::Mat StereoCorrespondence::getMask_InvalidValues(){
	return this->mask_disparity;
}

//cv::Mat StereoCorrespondence::getMask_Specularity(){
//	return this->mask_specularity;
//}


//private:

void StereoCorrespondence::initVariables(int width, int height)
{
	this->imageSize.width = width;
	this->imageSize.height = height;
	this->imgL_gray.create(height, width, CV_8UC1);
	this->imgL_gray.setTo(0);
	this->imgR_gray.create(height, width, CV_8UC1);
	this->imgR_gray.setTo(0);
	this->mask_disparity.create(height, width, CV_8UC1);
	this->mask_disparity.setTo(0);
	this->dispL2R_census.create(height, width, CV_32FC1);
	this->dispL2R_census.setTo(0);
	this->dispR2L_census.create(height, width, CV_32FC1);
	this->dispR2L_census.setTo(0);
	this->dispL2R_SAD.create(height, width, CV_32FC1);
	this->dispL2R_SAD.setTo(0);
	this->dispR2L_SAD.create(height, width, CV_32FC1);
	this->dispR2L_SAD.setTo(0);
	this->disp_LRC.create(height, width, CV_32FC1);
	this->disp_LRC.setTo(0);
	this->disp.create(height, width, CV_32FC1);
	this->disp.setTo(0);

	this->super_pixel_info.clear();
}

void StereoCorrespondence::equalizeImage(
		const cv::Mat& imgGray,
		cv::Mat& imgGray_equalized)
{
	// Equalization of both the image
	equalizeHist( imgGray, imgGray_equalized );
}

/*
 * The function remove specularity --
 * [Barbalata, Corina, and Leonardo S. Mattos. "Laryngeal Tumor Classification in Endoscopic Video."]
 * */
void StereoCorrespondence::removeSpecularity(
		const cv::Mat& imgRBG, cv::Mat& mask,
		double saturation, double value)
{

	// This method has been described
	cv::Mat hsv_image, s_treshold, v_treshold, reconstruct;
	cv::String filename;
	double Vmax, Vmin, Smax, Smin;
	std::vector<cv::Mat> hsv_channels(3), rgb_channels(3);
	cv::Size_<int> se_size = cv::Size(3,3);
	int no_iteration = 2;

	//Convert from RGB to HSV
	cv::cvtColor(imgRBG, hsv_image, CV_RGB2HSV);
	// Split the HSV image into each individual channel
	cv::split(hsv_image,hsv_channels);
	// Find maximum pixel in S channel
	minMaxLoc(hsv_channels[1], &Smin, &Smax);
	// Find maximum pixel in V channel
	minMaxLoc(hsv_channels[2], &Vmin, &Vmax);

	int th_saturation = round(saturation * (Smax));
	int th_value = round(value * (Vmax));

	threshold(hsv_channels[1], s_treshold, th_saturation,255,cv::THRESH_BINARY_INV); // replace the pixels that have the saturation component
	// smaller than a treshold with the value 255

	threshold(hsv_channels[2], v_treshold, th_value,255,cv::THRESH_BINARY); // replace the pixels that have the value value component higher
	// than a treshold with the value 255

	cv::Mat mask_int( imgRBG.rows, imgRBG.cols, CV_8UC1);
	cv::Mat green_ch( imgRBG.rows, imgRBG.cols, CV_8UC1);

	bitwise_and(s_treshold, v_treshold, mask_int);  // combine the binary images obtained previously using AND operation

	mask_int.copyTo(reconstruct);
	cv::Mat drawing = cv::Mat::zeros(reconstruct.size(),CV_8UC1);
	cv::Mat drawing_small = cv::Mat::zeros(reconstruct.size(),CV_8UC1);
	std::vector<std::vector<cv::Point> > contours;
	findContours(reconstruct, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

	for (int i = 0; i < contours.size(); i++)
	{
		double areac = contourArea(contours[i]);
		//cout<<areac<<"\n";
		if ((areac>50)) // && (areac<1000))
		{
			drawContours(drawing, contours, i, CV_RGB(255,255,255), CV_FILLED);
		}
	}

	cv::Mat struc_elem = getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(3,3));
	dilate(drawing,drawing,struc_elem,cv::Point(-1,-1),2);

	for (int i = 0; i < contours.size(); i++)
	{
		double areac = contourArea(contours[i]);
		//cout<<areac<<"\n";
		if ((areac<=50))
		{
			drawContours(drawing_small, contours, i, CV_RGB(255,255,255), CV_FILLED);
		}
	}

	struc_elem = getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(5,5));
	dilate(drawing_small, drawing_small, struc_elem, cv::Point(-1,-1),3); //3
	bitwise_or(drawing, drawing_small, drawing_small);

	//   inpaint(imageRBG, (drawing_small == 255), reconstruct, 1, INPAINT_NS); // using the previous mask replace the information in the specular reflexion
	// by means of interpolation
	drawing_small.copyTo(mask);
}

/*
 * Check consistency between left and right images
 * */
void StereoCorrespondence::checkLRConsistencyOcclusion(
		const cv::Mat& dispL2R, const cv::Mat& dispR2L,
		int threshold_LRC, cv::Mat& disparity, cv::Mat& mask, cv::Mat& mask_SA){

	int LRCCheck_rows, LRCCheck_cols;

	//	Find the size (columns and rows) of the L2R Disparity map and assign the rows to variable nrLRCCheck, and columns to variable ncLRCCheck
	LRCCheck_rows = dispL2R.rows;
	LRCCheck_cols = dispL2R.cols;

	//	Create an image of size nrLRCCheck and ncLRCCheck, fill it with zeros and assign it to variable dispMapLRC
	this->disp_LRC.create(LRCCheck_rows,LRCCheck_cols,CV_32FC1);
	this->disp_LRC.setTo(0);
	//  Find out how many rows and columns are to the left/right/up/down of the central pixel based on the window size
	//	int win=(n_census_prop-1)/2;
	int xr;
	int xlp;
	int xl;
	int n = 0;
	int m = 3;
	for(int i = m; i < LRCCheck_rows-m; i++){
		for(int j = m; j< LRCCheck_cols-m; j++){

			if(mask_SA.at<uchar>(i,j) > 0)
			{
				xl= j;
				xr = xl - dispL2R.at<float>(i, xl);
				if (xr >= LRCCheck_cols || xr < 0){
					disp_LRC.at<float>(i, j) = 0; // occluded pixel
				}
				else{
					xlp = xr + dispR2L.at<float>(i, xr);
					if (abs(xl-xlp) < threshold_LRC){
						disp_LRC.at<float>(i,j) = dispL2R.at<float>(i,j);  // non-occluded pixel
					}
					else{
						disp_LRC.at<float>(i,j) = 0; // occluded pixel
						//					if(this->isSuperPixels_prop){
						mask.at<uchar>(i, j) = 255; // creating the mask of occluded pixels into the label
						//					}
					}
				}
			}// endif
		}
	}

	this->disp_LRC.copyTo(disparity);
}

bool StereoCorrespondence::postProcess_(cv::Mat& label,
		segModel* SegModel,
		cv::Mat& img_hsv,
		std::vector<cv::Point2i>& contours,
		cv::Rect& SA_rect)
{

	//	cv::namedWindow("mask pre", CV_WINDOW_AUTOSIZE);
	//	cv::imshow("mask pre", this->mask_disparity);

	if (this->isSpeckleRemoval){
		removeSpeckle(this->disp, this->mask_disparity);
	}

	// rescale the disparity map to the original dimension of the image
	if(this->isSubSampling){

		cv::Mat disparity_rescaled;
		cv::Mat mask_disparity_rescaled;
		float scale_factor;
  
		disparity_rescaled.create(label.rows, label.cols, CV_32FC1);
		disparity_rescaled.setTo(0);

		mask_disparity_rescaled.create(label.rows, label.cols, CV_8UC1);
		mask_disparity_rescaled.setTo(0);

		scale_factor = (float)disparity_rescaled.cols / (float)this->disp.cols;
  
		// iwth is3dReconEvaluation = True: CRAAAAAAASH!
		// disparity_rescaled has size 0 so the resize causes an OpenCV assert to trigger because you can't resize an
		// image to empty
        assert(!disparity_rescaled.empty() && !mask_disparity_rescaled.empty());
		resize(this->disp, disparity_rescaled, disparity_rescaled.size(), 0, 0, cv::INTER_NEAREST );
		resize(this->mask_disparity, mask_disparity_rescaled, mask_disparity_rescaled.size(), 0, 0, cv::INTER_NEAREST );

		this->disp.create(disparity_rescaled.rows, disparity_rescaled.cols, CV_32FC1);
		disparity_rescaled.copyTo(this->disp);

		this->disp *= scale_factor;

		this->mask_disparity.create(mask_disparity_rescaled.rows, mask_disparity_rescaled.cols, CV_8UC1);
		mask_disparity_rescaled.copyTo(this->mask_disparity);
	}

	for(int i = 0; i < this->mask_disparity.rows; i++)
	{
		for(int j = 0; j < this->mask_disparity.cols; j++)
		{
			if(this->disp.at<float>(i,j) <= 0.0){
				this->mask_disparity.at<uchar>(i,j) = 255 ;

			}
		}
	}

	//	cv::namedWindow("mask post", CV_WINDOW_AUTOSIZE);
	//	cv::imshow("mask post", this->mask_disparity);

	//check segmentation map and invalid superpixel with a probability value < th


	if (this->isFillHole){
		fillDisparityHoles(label, this->disp, this->mask_disparity, SegModel,img_hsv);
        
        if (this->isPoissonSmoothing){
            this->smooth_pointCloud(contours, label, this->disp, SA_rect);
        }
	}


	if (this->isBilateralFilter)
	{
		cv::Mat temp(this->disp.rows, this->disp.cols, CV_32FC1);
		bilateralFilter(
				this->disp,
				temp,
				this->diamPixelWind, this->sigmaParam,
				this->sigmaParam);
		temp.copyTo(this->disp);
	}

	return true;
}


void StereoCorrespondence::removeSpeckle(cv::Mat& disparity, cv::Mat& mask){

	cv::Mat disp_16S;
	cv::Mat disp_temp;
	cv::Mat mask_temp;

	disp_16S.create(disparity.rows, disparity.cols,CV_16SC1);
	disp_temp.create(disparity.rows, disparity.cols,CV_16SC1);
	mask_temp.create(disparity.rows, disparity.cols,CV_8UC1);

	mask_temp.setTo(0);
	disp_16S.setTo(0);
	disp_temp.setTo(0);

	disparity.convertTo(disp_16S, CV_16SC1);
	disp_16S.copyTo(disp_temp);

	filterSpeckles(disp_16S, this->newVal_speckle, this->maxSize_speckle,
			this->maxDiff_speckle);

	compare(disp_16S, disp_temp, mask_temp, cv::CMP_NE);

	bitwise_or(mask_temp, mask, mask);

	for(int row = 0; row < disparity.rows; row++){
		for(int col = 0; col < disparity.cols; col++){
			if(mask_temp.at<uchar>(row, col) == 255){
				disparity.at<float>(row, col) = 0.0;
			}
		}
	}
}

/*
 * The function apply filter on disparity considering the label of super_pixel and filling the mask of invalid pixel
 *  with the median value of the labels
 * */
void StereoCorrespondence::fillDisparityHoles(
		cv::Mat& label,
		cv::Mat& disparity_,
		cv::Mat& mask,
		segModel* SegModel,
		cv::Mat& img_hsv)
{

	int max_superpixel_dim;
	this->getSuperPixelsInfo(label, this->super_pixel_info, max_superpixel_dim, SegModel, img_hsv);
	int count;
	bool *plane_ransac = new bool[this->super_pixel_info.size()];
	float *buff_xydisp = new float[max_superpixel_dim*3];
	int index;

	if (max_superpixel_dim <= 0)
		return;
	if (this->super_pixel_info.empty())
		return;

	// Create outliers mask
	this->mask_outliers.create(disparity_.rows, disparity_.cols, CV_8UC1);

	if(isReplace_all_points)
	{
		this->mask_outliers.setTo(255);
	}
	else
	{
		this->mask_outliers.setTo(0);
	}


	// Store in buff_xydisp only the position and values of disparity with the same label number
	for(int i = 0; i < this->super_pixel_info.size(); i++){
		index = 0;
		count = 0;
		for(int h_ = super_pixel_info[i].row_min; h_ <= super_pixel_info[i].row_max; h_++){
			for(int k_ = super_pixel_info[i].col_min; k_ <= super_pixel_info[i].col_max; k_++){
				if (label.at<ushort>(h_,k_) == i){
					buff_xydisp[index++] = (float)h_ ;
					buff_xydisp[index++] = (float)k_ ;
					buff_xydisp[index++] = disparity_.at<float>(h_,k_) ;

					// count the number of valid disparity values, i.e. != 0
					if(disparity_.at<float>(h_,k_) != 0){
						count++;
					}
				}
			}
		}

		super_pixel_info[i].n_valid_values = count;
		plane_ransac[i] = false;


		// Compute the average, median, plane and quadratic parameter that fit the disparity values in the super pixel

		if(this->isAVERAGEFiltering){
			this->getAverageSuperPixel(buff_xydisp, super_pixel_info[i].lenght, super_pixel_info[i].average);
		}

		else if(this->isMEDIANFiltering){
			this->getMedianSuperPixel(buff_xydisp, super_pixel_info[i].lenght, super_pixel_info[i].median);
		}

		else if(this->isPLANE_FITFiltering){
			this->getPlaneSuperPixel(buff_xydisp, super_pixel_info[i].lenght, super_pixel_info[i].plane);
		}
		else if(this->isRANSAC_FITFiltering){

			if(super_pixel_info[i].n_valid_values < n_rand ||
					!this->getRANSACPlaneSuperPixel(buff_xydisp, super_pixel_info[i].lenght, super_pixel_info[i].plane))
			{
				this->getMedianSuperPixel(buff_xydisp, super_pixel_info[i].lenght, super_pixel_info[i].median);
				plane_ransac[i] = false;
			}
			else{
				plane_ransac[i] = true;
			}
		}
		else if(this->isQUAD_FITFiltering){
			this->getQuadricSuperPixel(buff_xydisp, super_pixel_info[i].lenght, super_pixel_info[i].quadric);
		}
	}


	if(this->isRemove_outliers || this->isReplace_all_points){
		bitwise_or(this->mask_outliers, mask, mask);
	}


	for(int row = 0; row < mask.rows; row++){
		for(int col = 0; col < mask.cols; col++){

			int i = label.at<ushort>(row,col);

			if(super_pixel_info[i].background_object)
			{
				disparity_.at<float>(row, col) = 0.0;
			}
			else if (mask.at<uchar>(row,col) == 255){

				if(this->isAVERAGEFiltering){
					disparity_.at<float>(row, col) = super_pixel_info[i].average;
				}

				else if(this->isMEDIANFiltering){
					disparity_.at<float>(row, col) = super_pixel_info[i].median;
				}

				else if(this->isPLANE_FITFiltering){
					disparity_.at<float>(row, col) =
							super_pixel_info[i].plane[0] * row +
							super_pixel_info[i].plane[1] * col +
							super_pixel_info[i].plane[2];
				}

				else if(this->isRANSAC_FITFiltering){
					if(!plane_ransac[i]){
						disparity_.at<float>(row, col) = super_pixel_info[i].median;
						//						cout << super_pixel_info[i].median << endl;
					}
					else{
						//						 Write back the points fitted to the plane
						disparity_.at<float>(row, col) =
								super_pixel_info[i].plane[0] * row +
								super_pixel_info[i].plane[1] * col +
								super_pixel_info[i].plane[2];
						//						cout << "RANSAC" << endl;
					}

				}
				else if(this->isQUAD_FITFiltering){
					disparity_.at<float>(row, col) =
							super_pixel_info[i].quadric[0] * row +
							super_pixel_info[i].quadric[1] * col +
							super_pixel_info[i].quadric[2] * row * row +
							super_pixel_info[i].quadric[3] * col * col +
							super_pixel_info[i].quadric[4] * row * col +
							super_pixel_info[i].quadric[5];
				}
			}
		}
	}

	//	std::cout << "n plane found" << plane_found << std::endl;

	delete [] buff_xydisp;
	delete [] plane_ransac;
}

/*
 * The function retun information of super_pixels :  umbero of super pixels, super pixels boundaries in row and col
 * */
void StereoCorrespondence::getSuperPixelsInfo(
		const cv::Mat& label,
		std::vector<SuperPixelInfo> &super_pixel_info,
		int& max_superpixel_dim,
		segModel* SegModel,
		cv::Mat& img_hsv)
{

	int labelMax = 0;

	for(int h_ = 0; h_ < label.rows; h_++){
		for(int k_ = 0; k_ < label.cols; k_++){
			if (label.at<ushort>(h_,k_) == LABEL_NULL)
				continue;
			if (label.at<ushort>(h_,k_) > labelMax){
				labelMax = label.at<ushort>(h_,k_);
			}
		}
	}

	super_pixel_info.resize(labelMax + 1);
	int nLabels = labelMax + 1;
	int labelIndex = 0;
	max_superpixel_dim = 0;

	for (int i = 0; i< nLabels; i++){
		super_pixel_info[i].row_min = label.rows;
		super_pixel_info[i].row_max = 0;
		super_pixel_info[i].col_min = label.cols;
		super_pixel_info[i].col_max = 0;
		super_pixel_info[i].lenght = 0;
		super_pixel_info[i].probability_segmentation = 0.0;
	}

	for(int h_ = 0; h_ < label.rows; h_++){
		for(int k_ = 0; k_ < label.cols; k_++){
			labelIndex = label.at<ushort>(h_,k_);

			if (labelIndex == LABEL_NULL)
				continue;

			super_pixel_info[labelIndex].lenght++;
			super_pixel_info[labelIndex].probability_segmentation += SegModel->getsingleKpProb(cv::Point(k_,h_), img_hsv);

			if (h_ < super_pixel_info[labelIndex].row_min )
				super_pixel_info[labelIndex].row_min = h_;
			if (k_ < super_pixel_info[labelIndex].col_min )
				super_pixel_info[labelIndex].col_min = k_;
			if (h_ > super_pixel_info[labelIndex].row_max )
				super_pixel_info[labelIndex].row_max = h_;
			if (k_ > super_pixel_info[labelIndex].col_max )
				super_pixel_info[labelIndex].col_max = k_;

			if (super_pixel_info[labelIndex].lenght > max_superpixel_dim )
				max_superpixel_dim = super_pixel_info[labelIndex].lenght;
		}
	}

	for (int i = 0; i< nLabels; i++)
	{
		super_pixel_info[i].probability_segmentation =
				super_pixel_info[i].probability_segmentation/(float)super_pixel_info[i].lenght;
		if(super_pixel_info[i].probability_segmentation < 0.5 && this->isBackgroundRemoval)
			super_pixel_info[i].background_object = true;
		else
			super_pixel_info[i].background_object = false;
	}

}

/*
 * Get the median values of disparity of a super pixel
 */
void StereoCorrespondence::getMedianSuperPixel(float* buff_xydisp, int buff_length, float& median){

	float* disp_temp = new float[buff_length];
	int index = 0;

	for (int i = 0; i < buff_length; i++ ){
		if(buff_xydisp[i*3 +2] != 0)
			disp_temp[index++] = buff_xydisp[i*3 + 2];
	}

	if (index == 0) {
		median = 0.0f;
		return;
	}
	else if (index == 1) {
		median = disp_temp[0];
		return;
	}

	std::sort(&disp_temp[0], &disp_temp[index-1]);
	median = index % 2 ? disp_temp[index / 2] : (disp_temp[index / 2 - 1] + disp_temp[index / 2]) / 2;

	delete [] disp_temp;

}

/*
 * Get the average of the disparity's value of a super pixel
 */
void StereoCorrespondence::getAverageSuperPixel(float* buff_xydisp, int buff_length, float& average){

	// Media
	int counter = 0;

	for (int i = 0; i < buff_length; i++){
		if(buff_xydisp[i*3 +2] != 0 /*&& buff_xydisp[i*3 + 2] != 255*/){
			average += buff_xydisp[i*3 + 2];
			counter++;
		}
	}

	if (counter == 0)
		return;

	average /= (float)counter;

}

/*
 * Get the plane parameters of the plane passing through the disparity's values of a super pixel
 */
void StereoCorrespondence::getPlaneSuperPixel(float* buff_xydisp, int buff_length, float *plane){
	// Allocate points buffer
	float *px = new float[buff_length];
	float *py = new float[buff_length];
	float *pz = new float[buff_length];

	float px_mean;
	float py_mean;
	float pz_mean;

	float zmin = 10000.0f;
	float zmax = 0.0f;

	float A[9];
	float B[3];
	float Ainv[9];

	int nValidPoints = 0;

	A[0] = 0.0f;
	A[1] = 0.0f;
	A[2] = 0.0f;
	A[3] = 0.0f;
	A[4] = 0.0f;
	A[5] = 0.0f;
	A[6] = 0.0f;
	A[7] = 0.0f;
	A[8] = 0.0f;

	B[0] = 0.0f;
	B[1] = 0.0f;
	B[2] = 0.0f;


	for (int i = 0; i < buff_length; i++) {

		px_mean += buff_xydisp[i * 3];
		py_mean += buff_xydisp[i * 3 + 1];
		pz_mean += buff_xydisp[i * 3 + 2];

	}

	px_mean = px_mean/(buff_length);
	py_mean = py_mean/(buff_length);
	pz_mean = pz_mean/(buff_length);

	// Copy points to float format
	for (int i = 0; i < buff_length; i++) {

		px[i] = buff_xydisp[i * 3]; // - px_mean;
		py[i] = buff_xydisp[i * 3 + 1]; //  - py_mean;
		pz[i] = buff_xydisp[i * 3 + 2]; //  - pz_mean;

		if (buff_xydisp[i * 3 + 2] == 0)
			continue;

		if (zmin > pz[i])
			zmin = pz[i];
		if (zmax < pz[i])
			zmax = pz[i];

		nValidPoints++;
	}



	// Check if there is any valid point
	if (nValidPoints == 0) {
		plane[0] = 0.0f;
		plane[1] = 0.0f;
		plane[2] = 0.0f;
		return;
	}

	// Generate matrices
	for (int i = 0; i < buff_length; i++) {
		if (buff_xydisp[i * 3 + 2] == 0)
			continue;

		A[0] += px[i] * px[i];
		A[1] += px[i] * py[i];
		A[2] += px[i];
		A[3] += px[i] * py[i];
		A[4] += py[i] * py[i];
		A[5] += py[i];
		A[6] += px[i];
		A[7] += py[i];
		A[8] += 1.0f;

		B[0] += px[i] * pz[i];
		B[1] += py[i] * pz[i];
		B[2] += pz[i];
	}

	// Invert matrix A
	cv::Mat A_aux(3, 3, CV_32FC1, A);
	//cout << "A: " << A_aux << endl;
	cv::Mat Ainv_aux;
	invert(A_aux, Ainv_aux);
	//cout << "A_inv: " << Ainv_aux << endl;
	Ainv[0] = Ainv_aux.at<float>(0, 0);
	Ainv[1] = Ainv_aux.at<float>(1, 0);
	Ainv[2] = Ainv_aux.at<float>(2, 0);
	Ainv[3] = Ainv_aux.at<float>(0, 1);
	Ainv[4] = Ainv_aux.at<float>(1, 1);
	Ainv[5] = Ainv_aux.at<float>(2, 1);
	Ainv[6] = Ainv_aux.at<float>(0, 2);
	Ainv[7] = Ainv_aux.at<float>(1, 2);
	Ainv[8] = Ainv_aux.at<float>(2, 2);

	// Multiply A invert by B
	plane[0] = Ainv[0] * B[0] + Ainv[1] * B[1] + Ainv[2] * B[2];
	plane[1] = Ainv[3] * B[0] + Ainv[4] * B[1] + Ainv[5] * B[2];
	plane[2] = Ainv[6] * B[0] + Ainv[7] * B[1] + Ainv[8] * B[2];

	// Release memory
	delete [] px;
	delete [] py;
	delete [] pz;
}

/*
 * Get the parameters of the quadratic function passing through the disparity's values of a super pixel
 */
void StereoCorrespondence::getQuadricSuperPixel(float* buff_xydisp, int buff_length, float *quadric){
	// Allocate points buffer
	float *px = new float[buff_length];
	float *py = new float[buff_length];
	float *pz = new float[buff_length];

	float zmin = 10000.0f;
	float zmax = 0.0f;

	float A[36];
	float B[6];
	float Ainv[36];
	float Acomp[15];

	int nValidPoints = 0;

	memset(A, 0, 36 * sizeof(float));
	memset(B, 0, 6 * sizeof(float));
	memset(Acomp, 0, 15 * sizeof(float));

	// Copy points to float format
	for (int i = 0; i < buff_length; i++) {

		px[i] = buff_xydisp[i * 3];
		py[i] = buff_xydisp[i * 3 + 1];
		pz[i] = buff_xydisp[i * 3 + 2];

		if (buff_xydisp[i * 3 + 2] == 0)
			continue;

		if (zmin > pz[i])
			zmin = pz[i];
		if (zmax < pz[i])
			zmax = pz[i];

		nValidPoints++;
	}

	// Check if there is any valid point
	if (nValidPoints == 0) {
		quadric[0] = 0.0f;
		quadric[1] = 0.0f;
		quadric[2] = 0.0f;
		quadric[3] = 0.0f;
		quadric[4] = 0.0f;
		quadric[5] = 0.0f;
		return;
	}

	// Generate matrices
	for (int i = 0; i < buff_length; i++) {
		if (buff_xydisp[i * 3 + 2] == 0)
			continue;

		Acomp[0] += 1.0f;

		Acomp[1] += px[i];
		Acomp[2] += py[i];

		Acomp[3] += px[i] * px[i];
		Acomp[4] += px[i] * py[i];
		Acomp[5] += py[i] * py[i];

		Acomp[6] += px[i] * px[i] * px[i];
		Acomp[7] += px[i] * px[i] * py[i];
		Acomp[8] += px[i] * py[i] * py[i];
		Acomp[9] += py[i] * py[i] * py[i];

		Acomp[10] += px[i] * px[i] * px[i] * px[i];
		Acomp[11] += px[i] * px[i] * px[i] * py[i];
		Acomp[12] += px[i] * px[i] * py[i] * py[i];
		Acomp[13] += px[i] * py[i] * py[i] * py[i];
		Acomp[14] += py[i] * py[i] * py[i] * py[i];

		B[0] += pz[i] * px[i];
		B[1] += pz[i] * py[i];
		B[2] += pz[i] * px[i] * px[i];
		B[3] += pz[i] * py[i] * py[i];
		B[4] += pz[i] * px[i] * py[i];
		B[5] += pz[i];
	}

	int lut[36] = {
			3,  4,  6,  8,  7,  1,
			4,  5,  7,  9,  8,  2,
			6,  7, 10, 12, 11,  3,
			8,  9, 12, 14, 13,  5,
			7,  8, 11, 13, 12,  4,
			1,  2,  3,  5,  4,  0};

	for (int i = 0; i < 36; i++)
		A[i] = Acomp[lut[i]];

	// Invert matrix A
	cv::Mat A_aux(6, 6, CV_32FC1, A);
	//cout << "A: " << A_aux << endl;
	cv::Mat Ainv_aux;
	invert(A_aux, Ainv_aux);
	//cout << "A_inv: " << Ainv_aux << endl;
	memcpy(Ainv, (void*)Ainv_aux.data, 36 * sizeof(float));

	// Multiply A invert by B
	for (int i = 0; i < 6; i++) {
		quadric[i] =
				Ainv[i * 6    ] * B[0] +
				Ainv[i * 6 + 1] * B[1] +
				Ainv[i * 6 + 2] * B[2] +
				Ainv[i * 6 + 3] * B[3] +
				Ainv[i * 6 + 4] * B[4] +
				Ainv[i * 6 + 5] * B[5];
	}

	// Release memory
	delete [] px;
	delete [] py;
	delete [] pz;
}

bool StereoCorrespondence::getRANSACPlaneSuperPixel(
		float* buff_xydisp, int buff_length, float* best_plane) {

	//input

	int iter;
	int d_consensus; // max number of inlier to consider the model good
	int n_rand = this->n_rand;
	int random_buff[n_rand];
	bool find_plane;

	//output
	float plane_param[3];
	float best_plane_param[3];

	std::vector<float> consensus_set;
	float error_act_acc;
	float error_act;
	float error_best;

	float inlier[n_rand * 3];
	float distance; // between the point and the model
	float threshold; // mm?
	int count;
	unsigned int inlier_flag[buff_length];
	unsigned int disp_non_zero[buff_length];

	memset(inlier_flag,0,buff_length * sizeof(unsigned int));
	memset(disp_non_zero,0,buff_length * sizeof(unsigned int));
	find_plane = false;

	//initialization

	iter = this->iteration;
	d_consensus = this->min_num_point; // minimum number of point
	threshold = (float)this->RANSAC_threshold; // mm?
	error_best = 10000.0f;
	count = 0;
	error_act_acc = 10000.0f;
	int count_non_zero;

	count_non_zero = 0;

	//save only the valid disparity values
	for(int i = 0; i< buff_length; i++){
		if(buff_xydisp[i * 3 + 2] != 0){
			disp_non_zero[count_non_zero] = i;
			count_non_zero++;
		}
	}

	if(count_non_zero < d_consensus)
		return false;

	for(int k = 0; k < iter; k++){
		// take the inlier randomly
		for(int i = 0; i< n_rand; i++){
			random_buff[i] = disp_non_zero[rand() % (count_non_zero)];
		}

		for(int j = 0; j < n_rand; j++){
			inlier_flag[random_buff[j]] = 1;
			inlier[j * 3] = buff_xydisp[random_buff[j] * 3];
			inlier[j * 3 + 1] = buff_xydisp[random_buff[j] * 3 + 1];
			inlier[j * 3 + 2] = buff_xydisp[random_buff[j] * 3 + 2];
		}

		//compute the model that fits the outliers
		getPlaneSuperPixel(inlier, n_rand, plane_param);


		// insert the inlier in the consensus set
		for(int i = 0; i < n_rand; i++){
			consensus_set.push_back(inlier[i * 3]);
			consensus_set.push_back(inlier[i * 3 + 1]);
			consensus_set.push_back(inlier[i * 3 + 2]);
		}

		// check if the other points fit the model e consider them as possible inlier
		for(int i = 0; i< buff_length; i++){
			if(inlier_flag[i] == 0){

				// error as the distance between the plane and the point
				distance = abs((plane_param[0] * buff_xydisp[i * 3] + plane_param[1] * buff_xydisp[i * 3 + 1] + plane_param[2]) - buff_xydisp[i * 3 + 2]);

				if (distance < threshold){
					inlier_flag[i] = 1;
					consensus_set.push_back(buff_xydisp[i * 3]);
					consensus_set.push_back(buff_xydisp[i * 3 + 1]);
					consensus_set.push_back(buff_xydisp[i * 3 + 2]);
					count++;
				}
			}
		}

		if((count + n_rand) > d_consensus){
			this->getPlaneSuperPixel(&consensus_set[0], count + n_rand, plane_param);
			count = 0;
			for (int j = 0; j < buff_length; j++) {
				if(inlier_flag[j] == 1){
					error_act_acc += pow((plane_param[0] * buff_xydisp[j * 3] + plane_param[1] * buff_xydisp[j * 3 + 1] + plane_param[2]) - buff_xydisp[j * 3 + 2], 2);
					count++;
				}
			}

			error_act = error_act_acc/count;

			if(error_act < error_best){
				error_best = error_act;

				// save best plane params
				best_plane_param[0] = plane_param[0];
				best_plane_param[1] = plane_param[1];
				best_plane_param[2] = plane_param[2];

				find_plane = true;
			}

		}
	}

	// find plane inclination

	if(this->isRemove_inclined_plane){

		double k;
		double n_z;
		double angle;

		// module computation - c = 1
		k = sqrt(best_plane_param[0] * best_plane_param[0] +
				best_plane_param[1] * best_plane_param[1] +
				1) ;
		// z normalization
		n_z = 1 / k;

		// computaton of the angle between the z component of the versor orthonormal
		//to the plane and the xy plane of the camera
		angle =  acos (n_z);


		if (angle < MYPI/18)
		{
			find_plane = true;
		}
		else
		{
			find_plane = false;
		}
	}

	//    std::cout << "angle" << angle * 180 / PI << std::endl;

	if(!find_plane)
		return false;

	// add in the mask the outliers to be replaced by fillholes function

	for(int i = 0; i< buff_length; i++){
		if(inlier_flag[i] == 0){
			mask_outliers.at<uchar>
			(buff_xydisp[i * 3], buff_xydisp[i * 3 + 1]) = 255;
		}
	}


	best_plane[0] = best_plane_param[0];
	best_plane[1] = best_plane_param[1];
	best_plane[2] = best_plane_param[2];
	return true;
}


void StereoCorrespondence::smooth_pointCloud(std::vector<cv::Point2i>& contours,
		cv::Mat& sp_label,
		cv::Mat& disparity,
		cv::Rect& SA_rect)
{
	cv::Mat contour_mask;
	contour_mask.create(sp_label.rows, sp_label.cols, CV_8UC1);
	contour_mask.setTo(0);
	float contour_mean = 0.0;
	int count_mean = 0;
	float th = 70.0;
	cv::Mat disp_temp;
	float dispCenter;
	float dispNeigh;

	disparity.copyTo(disp_temp);


	// All the contour of the SA rectangle is border
	for (int i = SA_rect.x; i < SA_rect.x + SA_rect.width; i++)
	{
		if (disparity.at<float>(SA_rect.y, i) != 0.0)
			contour_mask.at<uchar>(SA_rect.y, i) = 255;

		if (disparity.at<float>(SA_rect.y + SA_rect.height - 1, i) != 0.0)
			contour_mask.at<uchar>(SA_rect.y + SA_rect.height - 1, i) = 255;
	}

	for (int j = SA_rect.y; j < SA_rect.y + SA_rect.height; j++)
	{
		if (disparity.at<float>(j, SA_rect.x) != 0.0)
			contour_mask.at<uchar>(j, SA_rect.x) = 255;

		if (disparity.at<float>(j, SA_rect.x + SA_rect.width - 1) != 0.0)
			contour_mask.at<uchar>(j, SA_rect.x + SA_rect.width - 1) = 255;
	}

	for(int i = 0; i < contours.size(); i++)
	{
		contour_mask.at<uchar>(contours[i].y,contours[i].x) = 255;

		if(contours[i].x == 0 || contours[i].x == contour_mask.cols -1 ||
				contours[i].y == 0 || contours[i].y == contour_mask.rows -1)
			continue;

		dispCenter = disparity.at<float>(contours[i].y,contours[i].x);

		if (dispCenter != 0.0)
		{
			contour_mean = dispCenter;
			count_mean = 1;
		}
		else
		{
			contour_mean = 0.0;
			count_mean = 0;
		}

		for (int k = -1; k <= 1; k++)
		{
			for (int l = -1; l <= 1; l++)
			{
				if (k == 0 && l == 0)
					continue;

				dispNeigh = disparity.at<float>(contours[i].y - k,contours[i].x - l);

				if(dispCenter != dispNeigh &&
						dispNeigh != 0.0 &&
						abs(dispCenter - dispNeigh) < th)
				{
					contour_mean += dispNeigh;
					count_mean++;
				}
				else
				{
//					contour_mask.at<uchar>(contours[i].y -k ,contours[i].x -l) = 255;
				}
			}
		}

		if (count_mean == 0)
		{
			contour_mask.at<uchar>(contours[i].y, contours[i].x) = 0;
		}
		else
		{
			contour_mean = contour_mean / count_mean;
			disp_temp.at<float>(contours[i].y,contours[i].x) = contour_mean;
		}
	}

//	cv::namedWindow("disp before", CV_NORMAL);
//	cv::imshow("disp before", disparity / 32.0);

	disp_temp.copyTo(disparity);

	this->compute_poisson(disparity, contour_mask, SA_rect);

//	cv::namedWindow("contour mask", CV_NORMAL);
//	cv::imshow("contour mask", contour_mask);

}

void StereoCorrespondence::compute_poisson(
		cv::Mat &disparity,
		cv::Mat &contour_mask,
		cv::Rect& SA_rect)
{
	int nIterations = 1000;
	int rows = disparity.rows;
	int cols = disparity.cols;
	int rowMin = SA_rect.y;
	int rowMax = SA_rect.y + SA_rect.height;
	int colMin = SA_rect.x;
	int colMax = SA_rect.x + SA_rect.width;

	if (rowMin < 0)
		rowMin = 0;
	if (rowMax > rows - 1)
		rowMax = rows - 1;
	if (colMin < 0)
		colMin = 0;
	if (colMax > cols - 1)
		colMax = cols - 1;

	// Calculate mean and give that value to the empty points as initial condition
	float mean = 0.0;
	int meanPoints = 0;

	for (int row = rowMin; row < rowMax; row++)
	{
		for (int col = colMin; col < colMax; col++)
		{
			if (disparity.at<float>(row, col) != 0.0)
			{
				mean += disparity.at<float>(row, col);
				meanPoints++;
			}
		}
	}

	if (meanPoints > 0)
		mean /= meanPoints;

	for (int row = rowMin; row < rowMax; row++)
	{
		for (int col = colMin; col < colMax; col++)
		{
			if (disparity.at<float>(row, col) == 0.0)
				disparity.at<float>(row, col) = mean;
		}
	}

	// Come on, do it baby
	for (int i = 0; i < nIterations; i++)
	{
		// Blacks
		for (int row = rowMin + 1; row < rowMax - 1; row++)
		{
			for (int col = colMin + row % 2 + 1; col < colMax - 1; col += 2)
			{
				if (contour_mask.at<unsigned char>(row, col) == 0)
				{
					disparity.at<float>(row, col) = 0.25f *
							(disparity.at<float>(row + 1, col) +
									disparity.at<float>(row - 1, col) +
									disparity.at<float>(row, col + 1) +
									disparity.at<float>(row, col - 1));
				}
			}
		}

		// Black borders
		for (int row = rowMin; row < rowMax; row++)
		{
			if ((row + colMin) % 2 == 0)
			{
				if (row == rowMin)
				{
					disparity.at<float>(row, colMin) = 0.5f *
							(disparity.at<float>(row + 1, colMin) +
									disparity.at<float>(row, colMin + 1));
				}
				else if (row == rowMax - 1)
				{
					disparity.at<float>(row, colMin) = 0.5f *
							(disparity.at<float>(row - 1, colMin) +
									disparity.at<float>(row, colMin + 1));
				}
				else
				{
					disparity.at<float>(row, colMin) = 0.3333f *
							(disparity.at<float>(row + 1, colMin) +
									disparity.at<float>(row - 1, colMin) +
									disparity.at<float>(row, colMin + 1));
				}
			}

			if ((row + (colMax - 1)) % 2 == 0)
			{
				if (row == rowMin)
				{
					disparity.at<float>(row, colMax - 1) = 0.5f *
							(disparity.at<float>(row + 1, colMax - 1) +
									disparity.at<float>(row, colMax - 2));
				}
				else if (row == rowMax - 1)
				{
					disparity.at<float>(row, colMax - 1) = 0.5f *
							(disparity.at<float>(row - 1, colMax - 1) +
									disparity.at<float>(row, colMax - 2));
				}
				else
				{
					disparity.at<float>(row, colMax - 1) = 0.3333f *
							(disparity.at<float>(row + 1, colMax - 1) +
									disparity.at<float>(row - 1, colMax - 1) +
									disparity.at<float>(row, colMax - 2));
				}
			}
		}

		for (int col = colMin + 1; col < colMax - 1; col++)
		{
			if ((col + rowMin) % 2 == 0)
			{
				disparity.at<float>(rowMin, col) = 0.3333f *
						(disparity.at<float>(rowMin + 1, col) +
								disparity.at<float>(rowMin, col + 1) +
								disparity.at<float>(rowMin, col - 1));
			}

			if ((col + (rowMax - 1)) % 2 == 0)
			{
				disparity.at<float>(rowMax - 1, col) = 0.3333f *
						(disparity.at<float>(rowMax - 2, col) +
								disparity.at<float>(rowMax - 1, col + 1) +
								disparity.at<float>(rowMax - 1, col - 1));
			}
		}

		// Reds
		for (int row = rowMin + 1; row < rowMax - 1; row++)
		{
			for (int col = colMin + 2 - row % 2; col < colMax - 1; col += 2)
			{
				if (contour_mask.at<unsigned char>(row, col) == 0)
				{
					disparity.at<float>(row, col) = 0.25f *
							(disparity.at<float>(row + 1, col) +
									disparity.at<float>(row - 1, col) +
									disparity.at<float>(row, col + 1) +
									disparity.at<float>(row, col - 1));
				}
			}
		}

		// Red borders
		for (int row = rowMin; row < rowMax; row++)
		{
			if ((row + colMin) % 2 == 1)
			{
				if (row == rowMin)
				{
					disparity.at<float>(row, colMin) = 0.5f *
							(disparity.at<float>(row + 1, colMin) +
									disparity.at<float>(row, colMin + 1));
				}
				else if (row == rowMax - 1)
				{
					disparity.at<float>(row, colMin) = 0.5f *
							(disparity.at<float>(row - 1, colMin) +
									disparity.at<float>(row, colMin + 1));
				}
				else
				{
					disparity.at<float>(row, colMin) = 0.3333f *
							(disparity.at<float>(row + 1, colMin) +
									disparity.at<float>(row - 1, colMin) +
									disparity.at<float>(row, colMin + 1));
				}
			}

			if ((row + (colMax - 1)) % 2 == 1)
			{
				if (row == rowMin)
				{
					disparity.at<float>(row, colMax - 1) = 0.5f *
							(disparity.at<float>(row + 1, colMax - 1) +
									disparity.at<float>(row, colMax - 2));
				}
				else if (row == rowMax - 1)
				{
					disparity.at<float>(row, colMax - 1) = 0.5f *
							(disparity.at<float>(row - 1, colMax - 1) +
									disparity.at<float>(row, colMax - 2));
				}
				else
				{
					disparity.at<float>(row, colMax - 1) = 0.3333f *
							(disparity.at<float>(row + 1, colMax - 1) +
									disparity.at<float>(row - 1, colMax - 1) +
									disparity.at<float>(row, colMax - 2));
				}
			}
		}

		for (int col = colMin + 1; col < colMax - 1; col++)
		{
			if ((col + rowMin) % 2 == 1)
			{
				disparity.at<float>(rowMin, col) = 0.3333f *
						(disparity.at<float>(rowMin + 1, col) +
								disparity.at<float>(rowMin, col + 1) +
								disparity.at<float>(rowMin, col - 1));
			}

			if ((col + (rowMax - 1)) % 2 == 1)
			{
				disparity.at<float>(rowMax - 1, col) = 0.3333f *
						(disparity.at<float>(rowMax - 2, col) +
								disparity.at<float>(rowMax - 1, col + 1) +
								disparity.at<float>(rowMax - 1, col - 1));
			}
		}
	}
}

void StereoCorrespondence::SAD_disparityL2R_opt(
		cv::Mat& dispL2R,
		cv::Mat& mask){

	int col, row, j, m_caL, n_caL;
	unsigned int sad_dist;
	unsigned int sad_min[2];
	short sad_pos;
	float sad_pos_sub;
	col = 0;
	row = 0;
	j = 0;
	bool jumpPixel = true;
	m_caL = this->m_aggCost;
	n_caL = this->n_aggCost;

	int dispMax;
	unsigned char *valueLeft;
	unsigned char *valueRight;

	dispL2R.create(mask.rows, mask.cols,CV_32FC1);
	dispL2R.setTo(0);

	for (row = m_caL/2; row < mask.rows - m_caL/2; row++){

		for(col = n_caL/2; col < mask.cols - n_caL/2; col++){

			//			if(col - n_caL/2 - this->numberOfDisparities_prop < 0)
			//				continue;

			sad_min[0] = UINT_MAX;
			sad_min[1] = UINT_MAX;
			sad_pos = 0;
			sad_pos_sub = 0;

			if(col - n_caL/2 - this->numberOfDisparities < 0) {
				dispMax = col - n_caL/2;
			}
			else {
				dispMax = this->numberOfDisparities - 1;
			}

			for(j = this->minDisparities + 1; j < dispMax; j++){
				sad_dist = 0;
				for(int k = - m_caL/2; k <= m_caL/2; k++ ){
					for(int h = - n_caL/2; h <= n_caL/2; h++){
						jumpPixel = !jumpPixel;
						if (jumpPixel)
							continue;
						sad_dist += (unsigned int)sadImage.getSADValue(row + k , col + h, j);
					}
				}

				if(sad_dist < sad_min[0]){
					sad_min[1] = sad_min[0];
					sad_min[0] = sad_dist;
					sad_pos = (short)j;
				}
				else if (sad_dist < sad_min[1]){
					sad_min[1] = sad_dist;
				}
			}

			if (abs(sad_min[0] - sad_min[1]) < this->spurie_Th){
				sad_pos = 0;
				mask.at<uchar>(row, col) = 255;
			}
			else if (sad_pos == this->numberOfDisparities - 1){
				sad_pos = 0;

			}
			if(this->isSubPixelRefinement){
				// sub pixel refinement
				SAD_computeSubPixelRefinementL2RAggCost(row, col, sad_dist, sad_pos, sad_pos_sub);
				dispL2R.at<float>(row, col) = sad_pos_sub;
			}
			else{
				dispL2R.at<float>(row, col) = (float)sad_pos;
			}

		}
	}
}


void StereoCorrespondence::SAD_disparityR2L_opt(cv::Mat& dispR2L){

	int col, row, j, m_acR, n_acR;
	unsigned int sad_dist;
	unsigned int sad_dist_min[2];
	short sad_dist_pos;
	float sad_dist_pos_sub;

	col = 0;
	row = 0;
	j = 0;
	bool jumpPixel = true;
	m_acR = this->m_aggCost;
	n_acR = this->n_aggCost;
	int dispMax;

	dispR2L.create(this->imageSize.height,this->imageSize.width,CV_32FC1);
	dispR2L.setTo(0);

	for (row = m_acR/2; row < this->imageSize.height - m_acR/2; row++){

		for(col = n_acR/2; col < this->imageSize.width - n_acR/2; col++){

			//			if(col + n_acR/2 + this->numberOfDisparities_prop >= this->imageSize.width)
			//				continue;

			sad_dist_min[0] = UINT_MAX;
			sad_dist_min[1] = UINT_MAX;
			sad_dist_pos = 0;
			sad_dist_pos_sub = 0;

			if(col + n_acR/2 + this->numberOfDisparities >= this->imageSize.width) {
				dispMax = this->imageSize.width - col - n_acR/2 - 1;
			}
			else {
				dispMax = this->numberOfDisparities - 1;
			}

			for(j = this->minDisparities + 1; j < dispMax ; j++){
				sad_dist = 0;

				for(int k = - m_acR/2; k <= m_acR/2; k++ ){
					for(int h =- n_acR/2; h <= n_acR/2; h++){
						jumpPixel = !jumpPixel;
						if (jumpPixel)
							continue;
						sad_dist += (unsigned int)sadImage.getSADValue(row + k , col + h + j, j);
					}
				}

				if(sad_dist < sad_dist_min[0]){
					sad_dist_min[1] = sad_dist_min[0];
					sad_dist_min[0] = sad_dist;
					sad_dist_pos = (short)j;
				}
				else if (sad_dist < sad_dist_min[1]){
					sad_dist_min[1] = sad_dist;
				}
			}

			if (abs(sad_dist_min[0] - sad_dist_min[1]) < this->spurie_Th)
				sad_dist_pos = 0;
			else if (sad_dist_pos == this->numberOfDisparities - 1){
				sad_dist_pos = 0;
			}

			if(this->isSubPixelRefinement){
				// sub pixel refinement
				SAD_computeSubPixelRefinementR2LAggCost(row, col, sad_dist, sad_dist_pos, sad_dist_pos_sub);
				dispR2L.at<float>(row, col) = sad_dist_pos_sub;
			}
			else{
				dispR2L.at<float>(row, col) = (float)sad_dist_pos;
			}
		}
	}
}


/*
 * The function apply Hamming Distance to Left (reference) and Right Census images with Aggregation Cost
 * */
void StereoCorrespondence::computeDisparityL2RAggCost(
		cv::Mat& dispL2R,
		cv::Mat& mask,
		cv::Mat& mask_SA)
{
	dispL2R.create(mask.rows, mask.cols,CV_32FC1);
	dispL2R.setTo(0);

#ifdef MULTITHREAD
	int nThread = N_THREADS;

	ThreadDispL2RData *data = new ThreadDispL2RData[nThread];
	pthread_t *threadDispL2R = new pthread_t[nThread];

	int rowStep = (this->imageSize.height - this->m_aggCost) / nThread;
	int rowMin = this->m_aggCost/2;
	int rowMax = rowMin + rowStep;

	for (int i = 0; i < nThread; i++)
	{
		if (i == nThread - 1)
			rowMax = this->imageSize.height - this->m_aggCost/2;

		data[i].stereoCorr = this;
		data[i].dispL2R = &dispL2R;
		data[i].mask_SA = &mask_SA;
		data[i].mask = &mask;
		data[i].rowMin = rowMin;
		data[i].rowMax = rowMax;

		rowMin = rowMax;
		rowMax += rowStep;

		pthread_create(
				&threadDispL2R[i],
				NULL,
				&StereoCorrespondence::run_computeDisparityL2RAggCostInside,
				(void*)&data[i]);
	}

	for (int i = 0; i < nThread; i++)
		pthread_join(threadDispL2R[i], NULL);

	delete [] data;
	delete [] threadDispL2R;

#else
	ThreadDispL2RData data = {(void*)this, &dispL2R, &mask, &mask_SA, this->m_aggCost/2, this->imageSize.height - this->m_aggCost/2};
	StereoCorrespondence::run_computeDisparityL2RAggCostInside(&data);
#endif
}


/*
 * The function apply Hamming Distance to Left (reference) and Right Census images with aggregation cost
 * */
void StereoCorrespondence::computeDisparityR2LAggCost(
		cv::Mat& dispR2L,
		cv::Mat& mask_SA)
{
	dispR2L.create(this->imageSize.height,this->imageSize.width,CV_32FC1);
	dispR2L.setTo(0);

#ifdef MULTITHREAD
	int nThread = N_THREADS;

	ThreadDispR2LData *data = new ThreadDispR2LData[nThread];
	pthread_t *threadDispR2L = new pthread_t[nThread];

	int rowStep = (this->imageSize.height - this->m_aggCost) / nThread;
	int rowMin = this->m_aggCost/2;
	int rowMax = rowMin + rowStep;

	for (int i = 0; i < nThread; i++)
	{
		if (i == nThread - 1)
			rowMax = this->imageSize.height - this->m_aggCost/2;

		data[i].stereoCorr = this;
		data[i].dispR2L = &dispR2L;
		data[i].mask_SA = &mask_SA;
		data[i].rowMin = rowMin;
		data[i].rowMax = rowMax;

		rowMin = rowMax;
		rowMax += rowStep;

		pthread_create(
				&threadDispR2L[i],
				NULL,
				&StereoCorrespondence::run_computeDisparityR2LAggCostInside,
				(void*)&data[i]);
	}

	for (int i = 0; i < nThread; i++)
		pthread_join(threadDispR2L[i], NULL);

	delete [] data;
	delete [] threadDispR2L;

#else
	ThreadDispR2LData data = {(void*)this, &dispR2L, &mask_SA, this->m_aggCost/2, this->imageSize.height - this->m_aggCost/2};
	StereoCorrespondence::run_computeDisparityR2LAggCostInside(&data);
#endif
}


/*
 * The function apply Hamming Distance to Left (reference) and Right Census images
 * */
void StereoCorrespondence::computeDisparityL2R(
		cv::Mat& dispL2R,
		cv::Mat& mask){

	int col, row, j;
	unsigned int humm_dist;
	unsigned int humm_dist_min[2];
	short humm_dist_pos;
	float humm_dist_pos_sub;
	unsigned char *valueLeft;
	unsigned char *valueRight;
	col = 0;
	row = 0;
	j = 0;

	dispL2R.create(this->imageSize.height,this->imageSize.width,CV_32FC1);
	dispL2R.setTo(0);


	for (row = 0; row < this->imageSize.height; row++){

		for(col = this->numberOfDisparities; col < this->imageSize.width; col++){

			humm_dist_min[0] = UINT_MAX;
			humm_dist_min[1] = UINT_MAX;
			humm_dist_pos = 0;
			humm_dist_pos_sub = 0;

			for(j = 1; j < this->numberOfDisparities - 1; j++){

				valueLeft = censusLeft.getPixel(row, col );
				valueRight = censusRight.getPixel(row, col - j);

				humm_dist = 0;

				for (int i = 0; i < censusLeft.getBytesPerPixel(); i++){
					//					cout << "left " << (int)valueLeft[i] << endl;
					//					cout << "right " << (int)valueRight[i] << endl;
					humm_dist += (unsigned int)HammingDistance::getHammingDistance(valueLeft[i], valueRight[i]);
				}

				if(humm_dist < humm_dist_min[0]){//cout << __LINE__ << endl;
					humm_dist_min[1] = humm_dist_min[0];
					humm_dist_min[0] = humm_dist;
					humm_dist_pos = (short)j;
				}
				else if (humm_dist < humm_dist_min[1]){
					humm_dist_min[1] = humm_dist;
				}
			}

			if (abs(humm_dist_min[0] - humm_dist_min[1]) < this->spurie_Th){
				humm_dist_pos = 0;
				mask.at<uchar>(row, col) = 255;
			}
			else if (humm_dist_pos == this->numberOfDisparities - 1){
				humm_dist_pos = 0;

			}

			if(this->isSubPixelRefinement){
				// sub pixel refinement
				computeSubPixelRefinementL2R(row, col, humm_dist, humm_dist_pos, humm_dist_pos_sub);
				dispL2R.at<float>(row, col) = humm_dist_pos_sub;
			}
			else{
				dispL2R.at<float>(row, col) = (float)humm_dist_pos;
			}
		}
	}
}

/*
 * The function apply Hamming Distance to Left (reference) and Right Census images
 * */
void StereoCorrespondence::computeDisparityR2L(cv::Mat& dispR2L){

	int col, row, j;
	unsigned int humm_dist;
	unsigned int humm_dist_min[2];
	short humm_dist_pos;
	float humm_dist_pos_sub;
	unsigned char *valueLeft;
	unsigned char *valueRight;
	col = 0;
	row = 0;
	j = 0;

	dispR2L.create(this->imageSize.height,this->imageSize.width,CV_32FC1);
	dispR2L.setTo(0);

	for (row = 0; row < this->imageSize.height; row++){

		for(col = 0; col < this->imageSize.width - this->numberOfDisparities; col++){

			humm_dist_min[0] = UINT_MAX;
			humm_dist_min[1] = UINT_MAX;
			humm_dist_pos = 0;
			humm_dist_pos_sub = 0;

			for(j = 1; j < this->numberOfDisparities-1 ; j++){

				valueRight = censusRight.getPixel(row, col);
				valueLeft = censusLeft.getPixel(row, col +j);
				humm_dist = 0;
				for (int i = 0; i < censusLeft.getBytesPerPixel(); i++){
					humm_dist += (unsigned int)HammingDistance::getHammingDistance(valueRight[i], valueLeft[i]);
				}

				if(humm_dist < humm_dist_min[0]){
					humm_dist_min[1] = humm_dist_min[0];
					humm_dist_min[0] = humm_dist;
					humm_dist_pos = (short)j;
				}
				else if (humm_dist < humm_dist_min[1]){
					humm_dist_min[1] = humm_dist;
				}
			}

			if (abs(humm_dist_min[0] - humm_dist_min[1]) < this->spurie_Th)
				humm_dist_pos = 0;
			else if (humm_dist_pos == this->numberOfDisparities - 1){
				humm_dist_pos = 0;
			}

			if(this->isSubPixelRefinement){
				// sub pixel refinement
				computeSubPixelRefinementR2L(row, col, humm_dist, humm_dist_pos, humm_dist_pos_sub);
				dispR2L.at<float>(row, col) = humm_dist_pos_sub;
			}
			else{
				dispR2L.at<float>(row, col) = (float)humm_dist_pos;
			}
		}
	}
}

void StereoCorrespondence::SAD_computeSubPixelRefinementL2RAggCost(
		int row, int col, unsigned int sad_dist, short& dist_pos, float& dist_pos_sub){
	unsigned int sad_dist_old;
	unsigned int sad_dist_fut;
	int m_h, n_h;

	if (dist_pos == 0) {
		dist_pos_sub = 0.0f;
		return;
	}

	m_h = this->m_aggCost;
	n_h = this->n_aggCost;
	bool jumpPixel = true;
	dist_pos_sub = 0;
	sad_dist_old = 0;
	sad_dist_fut = 0;

	// sub-pixel refinement and spurious remover
	for(int k = - m_h/2; k <= m_h/2; k++ ){
		for(int h = - n_h/2; h <= n_h/2; h++){
			jumpPixel = !jumpPixel;
			if (jumpPixel)
				continue;
			sad_dist_old += (unsigned int)sadImage.getSADValue(row + k , col + h , ((int)dist_pos - 1));
			sad_dist_fut += (unsigned int)sadImage.getSADValue(row + k , col + h , ((int)dist_pos + 1));
		}
	}
	dist_pos_sub = (float)dist_pos + (((float)sad_dist_fut - (float)sad_dist_old)/(2.0 * (2.0*(float)sad_dist - (float)sad_dist_old - (float)sad_dist_fut)));

}
void StereoCorrespondence::SAD_computeSubPixelRefinementR2LAggCost(
		int row, int col, unsigned int sad_dist, short& dist_pos, float& dist_pos_sub){
	unsigned int sad_dist_old;
	unsigned int sad_dist_fut;
	int m_h, n_h;

	if (dist_pos == 0) {
		dist_pos_sub = 0.0f;
		return;
	}

	m_h = this->m_aggCost;
	n_h = this->n_aggCost;
	bool jumpPixel = true;
	dist_pos_sub = 0;
	sad_dist_old = 0;
	sad_dist_fut = 0;

	// sub-pixel refinement and spurious remover
	for(int k = - m_h/2; k <= m_h/2; k++ ){
		for(int h = - n_h/2; h <= n_h/2; h++){
			jumpPixel = !jumpPixel;
			if (jumpPixel)
				continue;
			sad_dist_old += (unsigned int)sadImage.getSADValue(row + k , col + h + ((int)dist_pos - 1), ((int)dist_pos - 1));
			sad_dist_fut += (unsigned int)sadImage.getSADValue(row + k , col + h + ((int)dist_pos + 1), ((int)dist_pos + 1));
		}
	}
	dist_pos_sub = (float)dist_pos + (((float)sad_dist_fut - (float)sad_dist_old)/(2.0 * (2.0*(float)sad_dist - (float)sad_dist_old - (float)sad_dist_fut)));
}

// The function apply sub pixel refinement with parabola fitting Left to Right
void StereoCorrespondence::computeSubPixelRefinementL2RAggCost(
		int row, int col, unsigned int humm_dist, short& dist_pos, float& dist_pos_sub){

	if (dist_pos == 0) {
		dist_pos_sub = 0.0f;
		return;
	}

	unsigned int humm_dist_old;
	unsigned int humm_dist_fut;
	int m_h, n_h;
	m_h = this->m_aggCost;
	n_h = this->n_aggCost;
	bool jumpPixel = true;
	dist_pos_sub = 0;
	humm_dist_old = 0;
	humm_dist_fut = 0;
	// sub-pixel refinement and spurious remover
	for(int k = - m_h/2; k <= m_h/2; k++ ){
		for(int h = - n_h/2; h <= n_h/2; h++){
			jumpPixel = !jumpPixel;
			if (jumpPixel)
				continue;
			humm_dist_old += (unsigned int)HammingImage.getHammingDistanceImageValue(row + k , col + h , ((int)dist_pos - 1));
			humm_dist_fut += (unsigned int)HammingImage.getHammingDistanceImageValue(row + k , col + h , ((int)dist_pos + 1));
		}
	}
	dist_pos_sub = (float)dist_pos + (((float)humm_dist_fut - (float)humm_dist_old)/(2.0 * (2.0*(float)humm_dist - (float)humm_dist_old - (float)humm_dist_fut)));

}

// The function apply sub pixel refinement with parabola fitting Right to left
void StereoCorrespondence::computeSubPixelRefinementR2LAggCost(
		int row, int col, unsigned int humm_dist, short& dist_pos, float& dist_pos_sub){

	if (dist_pos == 0) {
		dist_pos_sub = 0.0f;
		return;
	}

	unsigned int humm_dist_old;
	unsigned int humm_dist_fut;
	int m_h, n_h;
	m_h = this->m_aggCost;
	n_h = this->n_aggCost;
	bool jumpPixel = true;
	dist_pos_sub = 0;
	humm_dist_old = 0;
	humm_dist_fut = 0;

	// sub-pixel refinement and spurious remover
	for(int k = - m_h/2; k <= m_h/2; k++ ){
		for(int h = - n_h/2; h <= n_h/2; h++){
			jumpPixel = !jumpPixel;
			if (jumpPixel)
				continue;
			humm_dist_old += (unsigned int)HammingImage.getHammingDistanceImageValue(row + k , col + h + ((int)dist_pos - 1), ((int)dist_pos - 1));
			humm_dist_fut += (unsigned int)HammingImage.getHammingDistanceImageValue(row + k , col + h + ((int)dist_pos + 1), ((int)dist_pos + 1));
		}
	}
	dist_pos_sub = (float)dist_pos + (((float)humm_dist_fut - (float)humm_dist_old)/(2.0 * (2.0*(float)humm_dist - (float)humm_dist_old - (float)humm_dist_fut)));

}

// The function apply sub pixel refinement with parabola fitting Left to Right
void StereoCorrespondence::computeSubPixelRefinementL2R(
		int row, int col, unsigned int humm_dist, short& dist_pos, float& dist_pos_sub){
	unsigned int humm_dist_old;
	unsigned int humm_dist_fut;
	unsigned char *valueLeft;
	unsigned char *valueRight_old;
	unsigned char *valueRight_fut;
	dist_pos_sub = 0;
	humm_dist_old = 0;
	humm_dist_fut = 0;


	// sub-pixel refinement and spurious remover

	valueLeft = censusLeft.getPixel(row , col );
	valueRight_old = censusRight.getPixel(row , col  - ((int)dist_pos - 1));
	valueRight_fut = censusRight.getPixel(row , col  - ((int)dist_pos + 1));

	if (valueLeft == NULL || valueRight_old == NULL || valueRight_fut == NULL ){
		dist_pos_sub = 0.0f;
	}
	else {

		for (int i = 0; i < censusLeft.getBytesPerPixel(); i++){
			humm_dist_old += (unsigned int)HammingDistance::getHammingDistance(valueLeft[i], valueRight_old[i]);
			humm_dist_fut += (unsigned int)HammingDistance::getHammingDistance(valueLeft[i], valueRight_fut[i]);
		}

		dist_pos_sub = (float)dist_pos + (((float)humm_dist_fut - (float)humm_dist_old)/(2.0 * (2.0*(float)humm_dist - (float)humm_dist_old - (float)humm_dist_fut)));
	}
}

// The function apply sub pixel refinement with parabola fitting Right to left
void StereoCorrespondence::computeSubPixelRefinementR2L(
		int row, int col, unsigned int humm_dist, short& dist_pos, float& dist_pos_sub){
	unsigned int humm_dist_old;
	unsigned int humm_dist_fut;

	unsigned char *valueRight;
	unsigned char *valueLeft_old;
	unsigned char *valueLeft_fut;

	dist_pos_sub = 0;
	humm_dist_old = 0;
	humm_dist_fut = 0;
	// sub-pixel refinement and spurious remover

	valueRight = censusRight.getPixel(row , col );
	valueLeft_old = censusLeft.getPixel(row , col  - ((int)dist_pos - 1));
	valueLeft_fut = censusLeft.getPixel(row , col  - ((int)dist_pos + 1));

	if (valueRight == NULL || valueLeft_old == NULL || valueLeft_fut == NULL ){
		dist_pos_sub = 0.0f;
	}
	else {
		for (int i = 0; i < censusLeft.getBytesPerPixel(); i++){
			humm_dist_old += (unsigned int)HammingDistance::getHammingDistance(valueRight[i], valueLeft_old[i]);
			humm_dist_fut += (unsigned int)HammingDistance::getHammingDistance(valueRight[i], valueLeft_fut[i]);
		}

		dist_pos_sub = (float)dist_pos + (((float)humm_dist_fut - (float)humm_dist_old)/(2.0 * (2.0*(float)humm_dist - (float)humm_dist_old - (float)humm_dist_fut)));
	}
}

// Thread functions
/*
void *StereoCorrespondence::run_censusLeft(void *data)
{
	ThreadCensusData *dataLeft = (ThreadCensusData*)data;
	StereoCorrespondence *stereoCorr = (StereoCorrespondence*)dataLeft->stereoCorr;

	stereoCorr->censusLeft.setCensus(
 *dataLeft->img_grey,
			stereoCorr->n_census,
			stereoCorr->m_census,
 *dataLeft->mask_SA,
			CENSUS_TRANSFORM_MODIFIED,
			false,
 *dataLeft->SA_boundary,
			0,
			0);
	return NULL;
}*/

void *StereoCorrespondence::run_censusRight(void *data)
{
	ThreadCensusData *dataRight = (ThreadCensusData*)data;
	StereoCorrespondence *stereoCorr = (StereoCorrespondence*)dataRight->stereoCorr;

	stereoCorr->censusRight.setCensus(
			*dataRight->img_grey,
			stereoCorr->n_census,
			stereoCorr->m_census,
			*dataRight->mask_SA,
			CENSUS_TRANSFORM_MODIFIED, true,
			*dataRight->SA_boundary,
			stereoCorr->minDisparities,
			stereoCorr->numberOfDisparities);
	return NULL;
}


void *StereoCorrespondence::run_computeDisparityR2LAggCost(void *data)
{
	ThreadDisparityData *disparityData = (ThreadDisparityData*)data;
	StereoCorrespondence *stereoCorr = (StereoCorrespondence*)disparityData->stereoCorr;

	stereoCorr->computeDisparityR2LAggCost(
			stereoCorr->dispR2L_census, *disparityData->mask);
	return NULL;
}

void *StereoCorrespondence::run_computeDisparityL2RAggCostInside(void *data)
{
	ThreadDispL2RData *dispL2RData = (ThreadDispL2RData*)data;
	StereoCorrespondence *stereoCorr = (StereoCorrespondence*)dispL2RData->stereoCorr;
 
	bool jumpPixel = true;

    // FIXME: What are m_cal, n_cal - Size of window to consider for disparity ?
	int m_caL = stereoCorr->m_aggCost;
	int n_caL = stereoCorr->n_aggCost;

	unsigned char *valueLeft;
	unsigned char *valueRight;

	int dispMax;

	for (int row = dispL2RData->rowMin; row < dispL2RData->rowMax; row++) {
		for(int col = n_caL/2; col < dispL2RData->mask->cols - n_caL/2; col++) {

			if(dispL2RData->mask_SA->at<uchar>(row, col) > 0)
			{
                unsigned int humm_dist_min[2] = {UINT_MAX, UINT_MAX};
                
                short humm_dist_pos = 0;

				if(col - n_caL/2 - stereoCorr->numberOfDisparities < 0) {
					dispMax = col - n_caL/2;
				}
				else {
					dispMax = stereoCorr->numberOfDisparities - 1;
				}

                unsigned int humm_dist = 0;
				for(int j = stereoCorr->minDisparities + 1; j < dispMax; j++){
				    humm_dist = 0;
                    
                    // FIXME: iterate over what ?
					for(int k = - m_caL/2; k <= m_caL/2; k++ ){
						for(int h = - n_caL/2; h <= n_caL/2; h++){
							jumpPixel = !jumpPixel;
							if (jumpPixel)
                                continue;
							
                            humm_dist += (unsigned int)stereoCorr->HammingImage.getHammingDistanceImageValue(row + k , col + h, j);
						}
					}
                    // Get the minimum of hamming distance over the window
                    // FIXME: Why keep the previous value around
					if(humm_dist < humm_dist_min[0]){
						humm_dist_min[1] = humm_dist_min[0]; // Keep the previous value around
						humm_dist_min[0] = humm_dist;
						humm_dist_pos = (short) j;
					}
					else if (humm_dist < humm_dist_min[1]){
						humm_dist_min[1] = humm_dist;
					}
				}

                // What is Spurie Th ?
				if (abs(humm_dist_min[0] - humm_dist_min[1]) < stereoCorr->spurie_Th){
					humm_dist_pos = 0;
					dispL2RData->mask->at<uchar>(row, col) = 255;
				}
				else if (humm_dist_pos == stereoCorr->numberOfDisparities - 1){
					humm_dist_pos = 0;
				}
			
				if(stereoCorr->isSubPixelRefinement){
                    float humm_dist_pos_sub = 0;
                    stereoCorr->computeSubPixelRefinementL2RAggCost(row, col, humm_dist, humm_dist_pos, humm_dist_pos_sub);
					dispL2RData->dispL2R->at<float>(row, col) = humm_dist_pos_sub;
				}
				else{
					dispL2RData->dispL2R->at<float>(row, col) = (float)humm_dist_pos;
				}
			}
		}
	}

	return NULL;
}

void *StereoCorrespondence::run_computeDisparityR2LAggCostInside(void *data)
{
	ThreadDispR2LData *dispR2LData = (ThreadDispR2LData*)data;
	StereoCorrespondence *stereoCorr = (StereoCorrespondence*)dispR2LData->stereoCorr;

	int col, row, j, m_acR, n_acR;
	unsigned int humm_dist;
	unsigned int humm_dist_min[2];
	short humm_dist_pos;
	float humm_dist_pos_sub;
	unsigned char *valueLeft;
	unsigned char *valueRight;

	col = 0;
	row = 0;
	j = 0;
	bool jumpPixel = true;
	m_acR = stereoCorr->m_aggCost;
	n_acR = stereoCorr->n_aggCost;
	int dispMax;

	for (row = dispR2LData->rowMin; row < dispR2LData->rowMax; row++){

		for(col = n_acR/2; col < stereoCorr->imageSize.width - n_acR/2; col++){

			if(dispR2LData->mask_SA->at<uchar>(row, col) > 0)
			{
				//			if(col + n_acR/2 + this->numberOfDisparities_prop >= this->imageSize.width)
				//				continue;
				if(col + n_acR/2 + stereoCorr->numberOfDisparities >= stereoCorr->imageSize.width) {
					dispMax = stereoCorr->imageSize.width - col - n_acR/2 - 1;
				}
				else {
					dispMax = stereoCorr->numberOfDisparities - 1;
				}

				humm_dist_min[0] = UINT_MAX;
				humm_dist_min[1] = UINT_MAX;
				humm_dist_pos = 0;
				humm_dist_pos_sub = 0;

				for(j = stereoCorr->minDisparities + 1; j < dispMax ; j++){
					humm_dist = 0;

					for(int k = - m_acR/2; k <= m_acR/2; k++ ){
						for(int h =- n_acR/2; h <= n_acR/2; h++){
							jumpPixel = !jumpPixel;
							if (jumpPixel)
								continue;
							humm_dist += (unsigned int)stereoCorr->HammingImage.getHammingDistanceImageValue(row + k , col + h + j, j);
						}
					}

					if(humm_dist < humm_dist_min[0]){
						humm_dist_min[1] = humm_dist_min[0];
						humm_dist_min[0] = humm_dist;
						humm_dist_pos = (short)j;
					}
					else if (humm_dist < humm_dist_min[1]){
						humm_dist_min[1] = humm_dist;
					}
				}

				if (abs(humm_dist_min[0] - humm_dist_min[1]) < stereoCorr->spurie_Th)
					humm_dist_pos = 0;
				else if (humm_dist_pos == stereoCorr->numberOfDisparities - 1){
					humm_dist_pos = 0;
				}

				if(stereoCorr->isSubPixelRefinement){
					// sub pixel refinement
					stereoCorr->computeSubPixelRefinementR2LAggCost(row, col, humm_dist, humm_dist_pos, humm_dist_pos_sub);
					dispR2LData->dispR2L->at<float>(row, col) = humm_dist_pos_sub;
				}
				else{
					dispR2LData->dispR2L->at<float>(row, col) = (float)humm_dist_pos;
				}
			}
		}
	}

	return NULL;
}
