#include "envisors/census_image.hpp"


CensusImage::CensusImage()
{
	this->image = NULL;
	this->rows = 0;
	this->cols = 0;
	this->bitsPerPixel = 0;
	this->bytesPerPixel = 0;
}

CensusImage::~CensusImage()
{
	if (this->image != NULL)
		delete [] this->image;
}

int CensusImage::rowsN(){
	return this->rows;
}

int CensusImage::colsN(){
	return this->cols;
}

/*
 *  Create the Census Image
 */
void CensusImage::create(int rows, int cols, int winW, int winH, int criteria)
{
	int bitPerPixelTmp;
	int bytesPerPixelTmp;
	int size;

	// Calculate bit and byte size
	if(criteria == CENSUS_TRANSFORM){
		bitPerPixelTmp = (winW * winH - 1);
	}
	else if(criteria == CENSUS_TRANSFORM_MODIFIED){
		bitPerPixelTmp = (winW * winH - 1) * 2;
	}

	bytesPerPixelTmp = ceil(bitPerPixelTmp / 8);

	// Check if the image exists
	if (this->image != NULL)
	{
		// The image exists, but it has different dimensions
		if (this->rows != rows || this->cols != cols ||
				bytesPerPixelTmp != this->bytesPerPixel)
		{
			delete [] this->image;
			this->bitsPerPixel = bitPerPixelTmp;
			this->bytesPerPixel = bytesPerPixelTmp;
			size = rows * cols * this->bytesPerPixel;
			this->image = new unsigned char[size];
			this->rows = rows;
			this->cols = cols;
		}
		// The image exists and has the same dimensions
		else
		{
			this->bitsPerPixel = bitPerPixelTmp;
			return;
		}
	}
	// The image doesn't exist yet
	else
	{
		this->bitsPerPixel = bitPerPixelTmp;
		this->bytesPerPixel = bytesPerPixelTmp;
		size = rows * cols * this->bytesPerPixel;
		this->image = new unsigned char[size];
		this->rows = rows;
		this->cols = cols;
	}
}

/*
 *  Get the number of bits per pixel
 */
int CensusImage::getBitsPerPixel()
{
	return this->bitsPerPixel;
}

/*
 *  Get the number of bytes per pixel
 */
int CensusImage::getBytesPerPixel()
{
	return this->bytesPerPixel;
}

/*
 *  Get a byte value in the census image
 */
unsigned char *CensusImage::getPixel(int row, int col)
{
	if (row < 0 || col < 0 || row >= this->rows || col >= this->cols)
		return NULL;

	int offset = (row * this->cols + col) * this->bytesPerPixel;
	return &this->image[offset];

}

/*
 *  Set a byte value in the census image
 */
void CensusImage::setPixel(int row, int col, unsigned char *pixel)
{
	if (row < 0 || col < 0 || row >= this->rows || col >= this->cols)
		return;

	int offset = (row * this->cols + col) * this->bytesPerPixel;

	memcpy(&this->image[offset], pixel, this->bytesPerPixel);
}

/*
 *  Convert the cv:.Mat Image in the Census Transformed Image. If criteria is set to CENSUS_TRANSFORM is apply the census transform.
 *  If criteria is set to CENSUS_TRASFORM_MODIFIED it applies the modified census transform.
 *  [Ma, Li, et al. "A Modified Census Transform Based on the Neighborhood Information for Stereo Matching Algorithm."
 *   Image and Graphics (ICIG), 2013 Seventh International Conference on. IEEE, 2013.]
 */
bool CensusImage::setCensus(
		const cv::Mat &inputImage,
		int winW, int winH,
		cv::Mat& mask_SA,
		int criteria,
		bool right, cv::Rect boundary, int disp_min, int disp_max)
{
	// Variables
	int mean, max, min, countMean;
	unsigned char censusByte = 0;
	unsigned int CensusBit = 0;
	int countBit = 0;
	int rows, cols;
	int k;

	if(inputImage.rows <=0 || inputImage.cols <=0)
		return false;

	rows = inputImage.rows;
	cols = inputImage.cols;

	// Create a new Census image with the dimensions of the input image
	this->create(rows, cols, winW, winH, criteria);

	//Resize the temporary bytes per Pixel
	unsigned char censusBytePerPixel[bytesPerPixel];


	// Do the Census transform for each pixel of the image, except the borders
	for (int x = winH / 2; x < rows - winH / 2; x++)
	{
		for(int y = winW / 2; y < cols - winW / 2; y++)
		{
			censusByte = 0;
			countMean = 0;
			mean = 0;
			min = 0;
			max = 0;
			CensusBit = 0;
			k = 0;

			// compute Census image only in a pre-defined area
			if(mask_SA.at<uchar>(x,y) > 0)
			{
				// Calculate the mean of the surrounding pixels
				for (int i = x - winH / 2; i <= x + winH / 2; i++)
				{
					for (int j = y - winW / 2; j <= y + winW / 2; j++)
					{
						mean += (int)inputImage.at<uchar>(i, j);
						countMean ++;
					}
				}

				mean = mean / countMean;
				countBit = 0;
				// Compute the Census Transform
				for (int i = x - winH / 2; i <= x + winH / 2; i++)
				{
					for (int j = y - winW / 2; j <= y + winW / 2; j++)
					{
						//skip the center pixel
						if( i != x || j != y ){

							if(criteria == CENSUS_TRANSFORM_MODIFIED){

								if (countBit ==  4){
									countBit = 0;
									censusByte = 0;
								}

								censusByte <<= 2;

								//find min and max value of the current pixel with respect to the mean value
								min = computeMin((int)inputImage.at<uchar>(x,y), mean);
								max = computeMax((int)inputImage.at<uchar>(x,y), mean);

								//compare pixel values in the neighborhood
								if( inputImage.at<uchar>(i,j) <= min)
									CensusBit = 0b00;
								else if (inputImage.at<uchar>(i,j) < inputImage.at<uchar>(x,y)){
									CensusBit = 0b01;
								}
								else if(inputImage.at<uchar>(x,y) < inputImage.at<uchar>(i,j)){
									CensusBit = 0b10;
								}
								else if(inputImage.at<uchar>(x,y) >= max){
									CensusBit = 0b11;
								}

								censusByte = censusByte | CensusBit;
								countBit ++;

							}
							else if(criteria == CENSUS_TRANSFORM){

								if (countBit ==  8){
									countBit = 0;
									censusByte = 0;
								}
								censusByte <<= 1;

								if( inputImage.at<uchar>(i,j) < inputImage.at<uchar>(x,y) )//compare pixel values in the neighborhood
									CensusBit = 1;
								else
									CensusBit = 0;

								censusByte = censusByte | CensusBit;
								countBit ++;

							}
							else
								return false;
							if(criteria == CENSUS_TRANSFORM_MODIFIED){
								if (countBit ==  4)
									censusBytePerPixel[k++] = censusByte;
							}
							else if(criteria == CENSUS_TRANSFORM){
								if (countBit ==  8)
									censusBytePerPixel[k++] = censusByte;
							}

						}

					}
				}
				if(criteria == CENSUS_TRANSFORM_MODIFIED){
					if (countBit !=  4)
						censusBytePerPixel[k++] = censusByte;
				}
				else if(criteria == CENSUS_TRANSFORM){
					if (countBit !=  8)
						censusBytePerPixel[k++] = censusByte;
				}
				this->setPixel(x, y, censusBytePerPixel);
			} // end if
//			else
//			{
//				this->setPixel(x, y, 0);
//			}
		}
	}

	//delete [] censusBytePerPixel;
	return true;
}

/*
 *  Compute the max value between x and y
 */
int CensusImage::computeMax(const int x, const int y){
	return ( x > y ) ? x : y;
}

/*
 *  Compute the min value between x and y
 */
int CensusImage::computeMin(const int x, const int y){
	return ( x < y ) ? x : y;
}

/*
 *  Convert the image stored in an unsigned char into a cv::Mat
 */
bool CensusImage::convertToMat(cv::Mat &outputImage){

	if(this->rows <=0 || this->cols <=0)
		return false;

	if(this->bytesPerPixel <=0)
		return false;

	if(this->bytesPerPixel == 1){
		outputImage.create(this->rows, this->cols, CV_8UC1);
		for(int i = 0; i < this->rows; i++){
			for(int j = 0; j < this->cols; j++){
				outputImage.at<uchar>(i, j) = *this->getPixel(i, j);
			}
		}
	}
	else if(this->bytesPerPixel == 2){
		outputImage.create(this->rows, this->cols, CV_8UC3);
		for(int i = 0; i < this->rows; i++){
			for(int j = 0; j < this->cols; j++){
				outputImage.at<cv::Vec3b>(i,j)[0] = this->getPixel(i, j)[0];
				outputImage.at<cv::Vec3b>(i,j)[1] = this->getPixel(i, j)[1];
				outputImage.at<cv::Vec3b>(i,j)[2] = 0;
			}
		}
	}
	else if(this->bytesPerPixel == 3){
		outputImage.create(this->rows, this->cols, CV_8UC3);
		for(int i = 0; i < this->rows; i++){
			for(int j = 0; j < this->cols; j++){
				outputImage.at<cv::Vec3b>(i,j)[0] = this->getPixel(i, j)[0];
				outputImage.at<cv::Vec3b>(i,j)[1] = this->getPixel(i, j)[1];
				outputImage.at<cv::Vec3b>(i,j)[2] = this->getPixel(i, j)[2];
			}
		}
	}
	else if(this->bytesPerPixel > 3){
		outputImage.create(this->rows, this->cols, CV_8UC1);
		for(int i = 0; i < this->rows; i++){
			for(int j = 0; j < this->cols; j++){
				outputImage.at<uchar>(i, j) = this->getPixel(i, j)[0];
			}
		}
	}

	return true;
}
