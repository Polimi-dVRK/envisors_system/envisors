/*
 * filter_point_cloud.cpp
 *
 *  Created on: Dec 12, 2016
 *      Author: veronica
 */
#include<envisors/filter_point_cloud.hpp>


FilterPointCloud::FilterPointCloud()
{
	this->pc_empty = true;
	if(this->pc_in == NULL)
	{
		pc_in = new pcl::PointCloud<pcl::PointXYZRGB>;
	}
	pc_in = new pcl::PointCloud<pcl::PointXYZRGB>;
}

void FilterPointCloud::setPointCloud(sensor_msgs::PointCloud2& pc)
{

	if(pc.width == 0 || pc.height == 0)
	{
		std::cout << "[ERROR] The point Cloud is empty" << std::endl;
		this->pc_empty = true;
		return;
	}
	else
	{
		pcl::PCLPointCloud2 pcl_pc;
		pcl_conversions::toPCL(pc, pcl_pc);
		pcl::fromPCLPointCloud2(pcl_pc, *this->pc_in);
		this->pc_empty = false;
	}
}

void FilterPointCloud::filter()
{

	if(!this->pc_empty)
	{
		pcl::RadiusOutlierRemoval<pcl::PointXYZRGB> rorfilter (true); // Initializing with true will allow us to extract the removed indices

		pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr cloudPtr(this->pc_in);
		rorfilter.setInputCloud(cloudPtr);
		rorfilter.setRadiusSearch (10);
		rorfilter.setMinNeighborsInRadius (1);
		//		rorfilter.setNegative (true);
		rorfilter.filter(this->pc_out);
		// The resulting cloud_out contains all points of cloud_in that have 4 or less neighbors within the 0.1 search radius
		//		indices_rem = rorfilter.getRemovedIndices ();
		// The indices_rem array indexes all points of cloud_in that have 5 or more neighbors within the 0.1 search radius

		//		pc_out = *pc_in;
	}

}

void FilterPointCloud::get_filtered_pointCloud(sensor_msgs::PointCloud2& pc)
{
	if(!this->pc_empty)
	{
		pcl::PCLPointCloud2 pcl_pc;
		pcl::toPCLPointCloud2(this->pc_out, pcl_pc);
		pcl_conversions::fromPCL(pcl_pc, pc);


		pc.header.frame_id = "/world";
		pc.header.stamp = ros::Time::now();

	}

}
