
#include "envisors/hamming_distance.hpp"

unsigned char HammingDistance::LUTHammingValues[256];

HammingDistance::HammingDistance(){

	this->HammingDistanceImage = NULL;
	this->imageSize.height = 0;
	this->imageSize.width = 0;

	this->initilizeLUTHamming();
}

HammingDistance::~HammingDistance(){
	if(this->HammingDistanceImage != NULL){
		delete [] this->HammingDistanceImage;
	}

}

void HammingDistance::initilizeLUTHamming(){
	for(int i = 0; i < 256; i++){
		HammingDistance::LUTHammingValues[i] = getHammingDistancefromLUT((unsigned char)i);
	}
	std::cout << "Hamming LookUp table Initialized" << std::endl;

}

/*
 * The function apply Sum of Hamming Distance to compare the ref and right images and establish the right correspondence
 * */
unsigned char HammingDistance::getHammingDistancefromLUT(unsigned char hamm){

	unsigned char dist = 0;
	while(hamm)
	{
		++dist;
		hamm &= hamm - 1;
	}
	return dist;

}

unsigned short* HammingDistance::computeHammingDistanceImage(
		CensusImage& imgLeft,
		CensusImage& imgRight,
		cv::Mat& mask_SA,
		int minDisparity,
		int maxDisparity)
{
	// Check if the image exists
	if (this->HammingDistanceImage != NULL)
	{
		// The image exists, but it has different dimensions
		if (this->imageSize.height != imgLeft.rowsN() ||
				this->imageSize.width != imgLeft.colsN() ||
				(maxDisparity - minDisparity) != this->deltaDisparity)
		{
			delete [] this->HammingDistanceImage;
			this->HammingDistanceImage = new unsigned short[imgLeft.rowsN() * imgLeft.colsN() * (maxDisparity - minDisparity)];
			this->imageSize.height = imgLeft.rowsN();
			this->imageSize.width = imgLeft.colsN();
			this->deltaDisparity = maxDisparity - minDisparity;
			this->minDisparity = minDisparity;
		}
	}
	// The image doesn't exist yet
	else
	{
		this->HammingDistanceImage = new unsigned short[imgLeft.rowsN() * imgLeft.colsN() * (maxDisparity - minDisparity)];
		this->imageSize.height = imgLeft.rowsN();
		this->imageSize.width = imgLeft.colsN();
		this->deltaDisparity = maxDisparity - minDisparity;
		this->minDisparity = minDisparity;
	}
#ifdef MULTITHREAD
	int nThread = N_THREADS;

	ThreadHammingData *data = new ThreadHammingData[nThread];
	pthread_t *threadHamming = new pthread_t[nThread];

	int dispStep = (maxDisparity - minDisparity) / nThread;
	int dispMin = minDisparity;
	int dispMax = dispMin + dispStep;

	for (int i = 0; i < nThread; i++)
	{
		if (i == nThread - 1)
			dispMax = maxDisparity;

		data[i].HammingD = this;
		data[i].imgLeft = &imgLeft;
		data[i].imgRight = &imgRight;
		data[i].mask_SA = &mask_SA;
		data[i].minDisparity = dispMin;
		data[i].maxDisparity = dispMax;
		data[i].minDisparityTotal = minDisparity;

		dispMin = dispMax;
		dispMax += dispStep;

		pthread_create(
				&threadHamming[i],
				NULL,
				&HammingDistance::run_Hamming,
				(void*)&data[i]);
	}

	for (int i = 0; i < nThread; i++)
		pthread_join(threadHamming[i], NULL);

	delete [] data;
	delete [] threadHamming;
#else
	ThreadHammingData data = {(void*)this,
			&imgLeft,
			&imgRight,
			&mask_SA,
			minDisparity,
			maxDisparity,
			minDisparity};
	HammingDistance::run_Hamming(&data);
#endif

	return HammingDistanceImage;
}

unsigned short HammingDistance::getHammingDistanceImageValue(int row, int col, int disparity){

	unsigned short hummDistance;
	return hummDistance = this->HammingDistanceImage[col + row * imageSize.width +
													 (disparity - this->minDisparity) * imageSize.width * imageSize.height];
}

unsigned char HammingDistance::getHammingDistance(unsigned char x, unsigned char y){

	unsigned char val = x^y;
	return HammingDistance::LUTHammingValues[val];
}

void *HammingDistance::run_Hamming(void *data)
{
	ThreadHammingData *HData = (ThreadHammingData*)data;
	HammingDistance *HammingD = (HammingDistance*)HData->HammingD;
	
	// FIXME: Why are we iterating over the disparity ?
	for(int j = HData->minDisparity; j < HData->maxDisparity; j++){

		for(int row = 0; row < HammingD->imageSize.height; row++)
		{
			for(int col = j; col < HammingD->imageSize.width; col++)
			{
                const auto px_idx = col + row * HammingD->imageSize.width
                    + (j - HData->minDisparityTotal) * HammingD->imageSize.width * HammingD->imageSize.height;
                
                // If the pixel is not part of the safety area we should do nothing
                // FIXME: the expensive part of the hamming distance is the iteration over the image (large amount of
                // data causes cache misses). Teh masking, by introducing branching probably makes things worse because
                // we must also contend with pipeline stalls due to branch misprediction.
				if(HData->mask_SA->at<uchar>(row, col) == 0) {
                    HammingD->HammingDistanceImage[px_idx] = 0;
                    continue;
                }
                
                
                unsigned char* valueLeft = HData->imgLeft->getPixel(row, col);
                unsigned char* valueRight = HData->imgRight->getPixel(row, col -j); // prev or post
                unsigned short humm_dist = 0;
                
                for (int i = 0; i < HData->imgLeft->getBytesPerPixel(); i++)
                    humm_dist += (unsigned short) HammingD->getHammingDistance(valueLeft[i], valueRight[i]);
                
                HammingD->HammingDistanceImage[px_idx] = humm_dist;
			}
		}
	}

	return NULL;
}
