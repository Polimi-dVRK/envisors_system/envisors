/**
 * @file     cam_v4l2.cpp
 * @author   Jesús Ortiz
 * @version  1.0
 * @date     02-Dec-2013
 * @brief    Prosilica camera class
 */

/*
 * INCLUDE
 */

#include "cam_lib/cam_v4l2.h"

#include <ros/ros.h>

/*
 * CLASS: Cam_v4l2
 */

// Constructor
Cam_v4l2::Cam_v4l2(int param_width, int param_height) : Cam_interface()
{
//  ros::NodeHandle nodeHandle("~");

  // 720x480 works with IIT Misumi cameras
//  int param_width = 720;
//  int param_height = 480;

//  if (nodeHandle.hasParam(CAMERA_WIDTH_PARAM))
//    nodeHandle.getParam(CAMERA_WIDTH_PARAM, param_width);
//  if (nodeHandle.hasParam(CAMERA_HEIGHT_PARAM))
//    nodeHandle.getParam(CAMERA_HEIGHT_PARAM, param_height);

  this->width = param_width;
  this->height = param_height;
  return;
}

// Destructor
Cam_v4l2::~Cam_v4l2()
{
  return;
}

// Init camera
bool Cam_v4l2::initCamera(const std::string &config)
{
    //if (!videocap.init(config.c_str(), 640, 480, V4L2_PIX_FMT_YUYV))
//    if (!videocap.init(config.c_str(), 720, 576, V4L2_PIX_FMT_YUYV)) // 720x576 works with FEMTO Misumi cameras
  if (!videocap.init(config.c_str(), width, height, V4L2_PIX_FMT_YUYV))  // 720x480 works with IIT Misumi cameras
    return false;

  return true;
}

// Finish camera
bool Cam_v4l2::finishCamera()
{
  if (!videocap.finish())
    return false;

  return true;
}

// Grabs an image from the camera
bool Cam_v4l2::grabImage()
{
  char *frame = videocap.getFrame();
  int frameSize = videocap.getSize();

  this->image.resize(width, height);
  unsigned char *imgBuffer = this->image.getBuffer();

  // Convert frame
  Cam_v4l2::convertyuv422torgb24(
    (unsigned char*)frame, frameSize,
    imgBuffer, width * height * 3);

  // Add a little delay
  usleep(20E3);

  // Rectify
  //this->rectify();
//  this->lockImage();
  this->imageRect.copy(this->image);
//  this->unlockImage();

  return true;
}

unsigned char Cam_v4l2::clip(int v)
{
  if (v <= 0)
    return 0;
  else if (v >= 255)
    return 255;
  else
    return v;
}

void Cam_v4l2::yuv444torgb888(
  unsigned char y,
  unsigned char u,
  unsigned char v,
  unsigned char *rgb)
{
    int c = y - 16;
    int d = u - 128;
    int e = v - 128;

    rgb[2] = Cam_v4l2::clip((298 * c + 409 * e + 128) >> 8);
    rgb[1] = Cam_v4l2::clip((298 * c - 100 * d - 208 * e + 128) >> 8);
    rgb[0] = Cam_v4l2::clip((298 * c + 516 * d + 128) >> 8);
}

void Cam_v4l2::yuv422torgb24(
  unsigned char *yuv422,
  unsigned char *rgb)
{
//  unsigned char u  = yuv422[0];
//  unsigned char y1 = yuv422[1];
//  unsigned char v  = yuv422[2];
//  unsigned char y2 = yuv422[3];

  //misumi
  unsigned char u  = yuv422[3];
   unsigned char y1 = yuv422[2];
   unsigned char v  = yuv422[1];
   unsigned char y2 = yuv422[0];

  Cam_v4l2::yuv444torgb888(y2, u, v, &rgb[0]);
  Cam_v4l2::yuv444torgb888(y1, u, v, &rgb[3]);
}

void Cam_v4l2::convertyuv422torgb24(
  unsigned char *bufferYUV,
  int lenBufferYUV,
  unsigned char *bufferRGB,
  int lenBufferRGB)
{
  int posRGB = 0;

  for (int i = 0; i < lenBufferYUV; i += 4)
  {
    if (posRGB > (lenBufferRGB - 6))
      return;

    Cam_v4l2::yuv422torgb24(&bufferYUV[i], &bufferRGB[posRGB]);
    posRGB += 6;
  }
}


bool Cam_v4l2::controls(const char *name, int *value, bool set)
{
    int id;

    if(strcmp("Brightness", name) == 0)
        id = BRIGHTNESS;
    else if(strcmp("Contrast", name) == 0)
        id = CONTRAST;
    else if(strcmp("Saturation", name) == 0)
        id = SATURATION;
    else if(strcmp("Hue", name) == 0)
        id = HUE;
    else if(strcmp("Gamma", name) == 0)
        id = GAMMA;
    else if(strcmp("Exposure", name) == 0)
        id = EXPOSURE;
    else if(strcmp("Gain", name) == 0)
        id = GAIN;

    if(videocap.Controls(id, value, set) == 0)
        return false;

    return true;
}
