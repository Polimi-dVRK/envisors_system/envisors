#include "cam_lib/videocap.h"

using namespace std;

Videocap::Videocap()
{
	this->buffers = NULL;
	this->n_buffers = 0;
	this->state = VIDEOCAP_STATE_STOP;
	this->frame = NULL;
	this->size = 0;
}

Videocap::~Videocap()
{
	this->finish();
}

int Videocap::init(const char *dev, int width, int height, int format)
{

	// Opening WebCam
	fdcam = open(dev, O_RDWR | O_NONBLOCK, 0);
	if (fdcam < 0)
	{
		std::cout << "[error]" << std::endl;
		return 0;
	}


	struct v4l2_capability cap;
	struct v4l2_format fmt;
	struct v4l2_requestbuffers req;
	enum v4l2_buf_type type;

	if (xioctl(fdcam, VIDIOC_QUERYCAP, &cap) == -1)
	{
		std::cout << "[error]" << std::endl;
		return 0;
	}

	if (!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE))
	{
		std::cout << "[error]" << std::endl;
		return 0;
	}

	if (!(cap.capabilities & V4L2_CAP_STREAMING))
	{
		std::cout << "[error]" << std::endl;
		return 0;
	}

	memset(&fmt, 0, sizeof(fmt));

	fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

	if (xioctl(fdcam, VIDIOC_G_FMT, &fmt) == -1)
	{
		std::cout << "[error line:]" << __LINE__ << std::endl;
		return 0;
	}

	std::cout << "Format Get:" << std::endl;
	std::cout << fmt.fmt.pix.width << "x" << fmt.fmt.pix.height;
	std::cout << ":" << fmt.fmt.pix.pixelformat;
	std::cout << ":" << fmt.fmt.pix.field << std::endl;

	fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	fmt.fmt.pix.width = width;
	fmt.fmt.pix.height = height;
	//fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV;
	fmt.fmt.pix.pixelformat = format;
	//fmt.fmt.pix.field = V4L2_FIELD_NONE;
	fmt.fmt.pix.field = V4L2_FIELD_INTERLACED;

	std::cout << "Format Set:" << std::endl;
	std::cout << fmt.fmt.pix.width << "x" << fmt.fmt.pix.height;
	std::cout << ":" << fmt.fmt.pix.pixelformat;
	std::cout << ":" << fmt.fmt.pix.field << std::endl;

	if (xioctl(fdcam, VIDIOC_S_FMT, &fmt) == -1)
	{
		std::cout << "[error line:]" << __LINE__ << std::endl;
		return 0;
	}

	memset(&req, 0, sizeof(req));

	req.count = 2;
	req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	req.memory = V4L2_MEMORY_MMAP;

	if (xioctl(fdcam, VIDIOC_REQBUFS, &req) == -1)
	{
		std::cout << "[error line:]" << __LINE__ << std::endl;
		return 0;
	}

	if (req.count < 2)
	{
		std::cout << "[error line:]" << __LINE__ << std::endl;
		return 0;
	}

	buffers = (struct buffer*)calloc(req.count, sizeof(*buffers));

	if (!buffers)
	{
		std::cout << "[error line:]" << __LINE__ << std::endl;
		return 0;
	}

	for (n_buffers = 0; n_buffers < req.count; ++n_buffers)
	{
		struct v4l2_buffer buf;

		memset(&buf, 0, sizeof(buf));

		buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		buf.memory = V4L2_MEMORY_MMAP;
		buf.index = n_buffers;

		if (xioctl(fdcam, VIDIOC_QUERYBUF, &buf) == -1)
		{
			std::cout << "[error line:]" << __LINE__ << std::endl;
			return 0;
		}

		buffers[n_buffers].length = buf.length;
		buffers[n_buffers].start =
				mmap (NULL /* start anywhere */,
						buf.length,
						PROT_READ | PROT_WRITE /* required */,
						MAP_SHARED /* recommended */,
						fdcam,
						buf.m.offset);

		if (buffers[n_buffers].start == MAP_FAILED)
		{
			std::cout << "[error line:]" << __LINE__ << std::endl;
			return 0;
		}
	}

	for (unsigned int i = 0; i < n_buffers; ++i)
	{
		struct v4l2_buffer buf;

		memset(&buf, 0, sizeof(buf));

		buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		buf.memory = V4L2_MEMORY_MMAP;
		buf.index = i;

		if (xioctl(fdcam, VIDIOC_QBUF, &buf) == -1)
		{
			std::cout << "[error line:]" << __LINE__ << std::endl;
			return 0;
		}
	}

	type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

	if (xioctl(fdcam, VIDIOC_STREAMON, &type) == -1)
	{
		std::cout << "[error line:]" << __LINE__ << std::endl;
		return 0;
	}

	this->frame = this->captureFrame();

	this->state = VIDEOCAP_STATE_RUN;
	pthread_mutex_init(&this->mutex, NULL);
	pthread_create(&this->thread, NULL, this->thread_capture, (void*)this);

	return 1;
}

char *Videocap::getFrame()
{
	return frame;
}

int Videocap::getSize()
{
	return this->size;
}

int Videocap::finish()
{
	this->state = VIDEOCAP_STATE_STOP;
	usleep(100000);
	pthread_mutex_destroy(&this->mutex);

	enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

	if (xioctl(fdcam, VIDIOC_STREAMOFF, &type) == -1)
		return 0;

	for (unsigned int i = 0; i < n_buffers; ++i)
	{
		if (munmap(buffers[i].start, buffers[i].length) == -1)
			return 0;
	}

	free(buffers);

	if (close(fdcam) == -1)
		return 0;

	fdcam = -1;

	return 1;
}

char *Videocap::captureFrame()
{
	struct v4l2_buffer buf;

	fd_set fds;
	struct timeval tv;
	int r;

	FD_ZERO(&fds);
	FD_SET(fdcam, &fds);

	tv.tv_sec = 2;
	tv.tv_usec = 0;

	r = select(fdcam + 1, &fds, NULL, NULL, &tv);

	if (r == -1)
		return NULL;

	if (r == 0)
		return NULL;

	memset(&buf, 0, sizeof(buf));

	buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	buf.memory = V4L2_MEMORY_MMAP;

	if (xioctl(fdcam, VIDIOC_DQBUF, &buf) == -1)
		return NULL;

	if (buf.index >= n_buffers)
		return NULL;

	void *aux = buffers[buf.index].start;
	this->size = buffers[buf.index].length;

	if (xioctl(fdcam, VIDIOC_QBUF, &buf) == -1)
		return NULL;

	return (char*)aux;
}

void *Videocap::thread_capture(void *arg)
{
	Videocap *videocap = (Videocap*)arg;

	while (videocap->state == VIDEOCAP_STATE_RUN)
	{
		videocap->frame = videocap->captureFrame();
	}

	return NULL;
}

int Videocap::xioctl(int fd, int request, void *arg)
{
	int r;

	do
	{
		r = ioctl (fd, request, arg);
	}
	while (r == -1 && EINTR == errno);

	return r;
}

int Videocap::Controls(int id, int *value, bool set)
{
	struct v4l2_control ctrl;
	memset(&ctrl, 0, sizeof (ctrl));
	switch(id)
	{
	case BRIGHTNESS:
		ctrl.id = V4L2_CID_BRIGHTNESS;
		break;
	case CONTRAST:
		ctrl.id = V4L2_CID_CONTRAST;
		break;
	case SATURATION:
		ctrl.id = V4L2_CID_SATURATION;
		break;
	case HUE:
		ctrl.id = V4L2_CID_HUE;
		break;
	case GAMMA:
		//            ctrl.id = V4L2_CID_GAMMA;
		std::cout << "Gamma-control not implemented!" << std::endl;
		return 1;
	case EXPOSURE:
		//            ctrl.id = V4L2_CID_EXPOSURE;
		std::cout << "Exposure-control not implemented!" << std::endl;
		return 1;
	case GAIN:
		//            ctrl.id = V4L2_CID_GAIN;
		std::cout << "Gain-control not implemented!" << std::endl;
		return 1;
	default:
		ctrl.id = V4L2_CID_BRIGHTNESS;
		break;
	}

	if (-1 == ioctl(fdcam, VIDIOC_G_CTRL, &ctrl) && errno != ERANGE)
		return 0;

	if(!set)
	{
		*value = ctrl.value;
		return 1;
	}

	ctrl.value = *value;
	if (-1 == ioctl(fdcam, VIDIOC_S_CTRL, &ctrl) && errno != ERANGE)
		return 0;

	return 1;
}
