//
// Created by tibo on 06/07/17.
//


#pragma once

#include <opencv2/core/core.hpp>

struct Recon
{
    //	Ptr<SuperpixelSEEDS> seeds;
    cv::Mat imgL_rgb_act;
    cv::Mat imgL_gray;
    cv::Mat imgR_gray;
    cv::Mat mask_specularity;

    SuperPixel super_pixel;
    bool isGaussian;

    int stereo_corr_algorithm;
    bool subSampling;

    //--census
    bool isModifiedCensus;
    bool isAggregationCost;
    int n_census;
    int m_census;

    //--common
    int threshold_LRC;
    int minDisparities;
    int numberOfDisparities;
    float img_scalefactor;
    int spurie_Th;
    int m_aggCost;
    int n_aggCost;

    //--post processing
    bool ispostProcessing;
    bool isSpeckleRemoval;
    bool isFillHole;
    bool isAVERAGEFiltering;
    bool isMEDIANFiltering;
    bool isPLANE_FITFiltering;
    bool isQUAD_FITFiltering;
    bool isRANSAC_FITFiltering;
    bool isPoissonSmoothing;
    bool isRemove_outliers;
    bool isReplace_all_points;
    bool isRemove_inclined_plane;
    bool isBackgroundRemoval;
    bool isBilateralFilter;
    double newVal_speckle;
    int maxSize_speckle;
    double maxDiff_speckle;
    int diamPixelWind;
    double sigmaParam;
    int n_rand;
    int iteration;
    int min_num_point;
    double RANSAC_threshold;

    cv::Mat homography_prev;
    cv::Mat homography_rel;
    cv::Mat Q;
    cv::Mat disparity;
    cv::Mat pointCloud;
    int img_scale;
    cv::Mat R_e, t_e;
    cv::Mat R_e_rel, t_e_rel;
    cv::Mat R_e_prev, t_e_prev;

};
