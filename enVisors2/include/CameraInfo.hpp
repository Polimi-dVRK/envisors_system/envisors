//
// Created by tibo on 06/07/17.
//

#pragma once

#include <string>
#include <cam_lib/camimage.h>

struct CameraInfo
{
    int camera_model;
    std::string camera_name;

    CamImage camImg_left;
    CamImage camImg_right;

    std::string dev_left;
    std::string dev_right;

    int width;
    int height;
};
