/*
 * enVisors2.hpp
 *
 *  Created on: Sep 21, 2016
 *      Author: veronica
 */

#ifndef ENVISORS2_INCLUDE_ENVISORS2_HPP_
#define ENVISORS2_INCLUDE_ENVISORS2_HPP_

#define STAGE_TRACK     0
#define STAGE_REINIT    1
#define STAGE_WAIT      2

#include <string>

//ROS
#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <dynamic_reconfigure/server.h>

#include <std_msgs/Float32.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/Joy.h>

#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>

#include <eigen_conversions/eigen_msg.h>

//tf
#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>

//pcl
#include <pcl/point_cloud.h>

//enVisors
#include <envisors/pcl2ros.hpp>
#include <envisors/ros2pcl.hpp>
#include <envisors_msgs/input_device.h>
#include <envisors/define.hpp>
#include <envisors/paths.h>
#include <hmatching/HLearningModule.hpp>
#include <hmatching/HMatchingModule.hpp>
#include <envisors/bayesian_network/bayesian_network.hpp>
#include <envisors/segmentation/seg_model.h>
#include <envisors/filter_point_cloud.hpp>
#include <envisors/rectification.hpp>
#include <envisors/common.hpp>
#include <envisors/super_pixels.hpp>
#include <envisors/stereo_correspondence.hpp>
#include <envisors/3dreconstruction.hpp>
#include <envisors/mytracking.hpp>
#include <envisors/safety_augmentation.hpp>

//#include <cam_lib/cam_interface.h>
//#include <cam_lib/cam_v4l2.h>

#include "envisors_2/bb_coords.h"
#include "envisors_2/envisors2Config.h"

#include "CameraInfo.hpp"
#include "EvaluationTracking.hpp"
#include "SafetyArea.hpp"
#include "SafetyAugmentationStruct.hpp"
#include "SuperPixel.hpp"
#include "Recon.hpp"

enum
{
    INIT_PARAMETERS = 0,
    READ_IMAGES = 1,
    DEFINE_SAFETY_AREA = 2,
    KLT_TRACKER = 3,
    HOUGH_MATCHING = 4,
    UPDATE_SAFETY_AREA = 5,
    KLT_REINIT = 6,
    STEREO_RECONSTRUCTION = 7,
    SAFETY_AUGMENTATION = 8,
    EVALUATION = 9,
    UPDATE_PREV = 10
};



enum
{
    VIDEO = 0,
    STREAM = 1,
    ROS = 2,
    VIDEO_MONO = 3,
    IMAGES = 4
};

typedef message_filters::sync_policies::ApproximateTime <sensor_msgs::Image, sensor_msgs::Image> MySyncPolicy;

class enVisorsSys2
{

public:

    //class
    enVisorsSys2();
    ~enVisorsSys2();

    Rectification Rect;
    StereoCorrespondence StereoCorr;
    StereoReconstruction StereoRecon;
    MyTracking TrackL;
    HLearningModule learning;
    HMatchingModule recognition;
    BNetwork BNet;
    FilterPointCloud fpc;

    SafetyArea SA;
    Recon recon;
    DetectorParameters detectorsParam;
    CameraInfo camera_info;
    SafetyAugmentationStruct SAug;
    EvaluationTracking EvalTrack;

    //ROS variables
    ros::Subscriber datasetName_sub;
    ros::NodeHandle n;
    image_transport::ImageTransport it;

    sensor_msgs::Image imgL_rect_;
    sensor_msgs::Image imgR_rect_;
    sensor_msgs::Image imgL_grey_;
    sensor_msgs::Image imgR_grey_;
    sensor_msgs::Image imgL_census_;
    sensor_msgs::Image imgR_census_;
    sensor_msgs::Image imgL_colored_;
    sensor_msgs::Image disparity_;
    sensor_msgs::PointCloud2 pointCloud_;
    sensor_msgs::PointCloud2 pointCloud_filtered_;
    sensor_msgs::Image img_depth_;
    sensor_msgs::Image imgL_features_;
    sensor_msgs::Image img_matches_;
    sensor_msgs::Image img_opt_flow_;
    sensor_msgs::Image mask_reselection_;
    sensor_msgs::Image template_object_;
    sensor_msgs::Image img_hough_;
    sensor_msgs::Image img_seg_prob_;
    sensor_msgs::Image img_augmented_;
    std_msgs::Float32 bb_distance_;

    ros::Subscriber robotPose1_sub;
    ros::Subscriber robotPose2_sub;
    ros::Subscriber CameraPoseStamped_sub;
    tf::TransformListener robot_listener;

    image_transport::Publisher imgL_pub;
    image_transport::Publisher imgR_pub;
    image_transport::Publisher imgL_rect_pub;
    image_transport::Publisher imgR_rect_pub;
    image_transport::Publisher imgL_grey_pub;
    image_transport::Publisher imgR_grey_pub;
    image_transport::Publisher imgL_census_pub;
    image_transport::Publisher imgR_census_pub;
    image_transport::Publisher imgL_contourn_pub;
    image_transport::Publisher disparity_pub;
    image_transport::Publisher depth_pub;
    ros::Publisher pointCloud_pub;
    ros::Publisher pointCloud_filtered_pub;
    image_transport::Publisher imgL_features_pub;
    image_transport::Publisher matches_pub;
    image_transport::Publisher optical_flow_pub;
    image_transport::Publisher mask_reselection_pub;
    image_transport::Publisher template_object_pub;
    image_transport::Publisher hough_space_pub;
    image_transport::Publisher segmentation_probability_pub;
    ros::Publisher augmented_image_pub;
    ros::Publisher pointCloud_transformed_pub;
    ros::Publisher bb_coords_pub;
    ros::Publisher bb_distance_pub;
    ros::Publisher tablet_pub;

    message_filters::Synchronizer <MySyncPolicy> *sync;
    message_filters::Subscriber <sensor_msgs::Image> *imgL_sub;
    message_filters::Subscriber <sensor_msgs::Image> *imgR_sub;
    ros::Subscriber tablet_sub;

    tf::Transform T_PSM2_ft;
    tf::TransformBroadcaster broadcasterT_PSM2_ft;

    //variables
    int status;

    cv::Size imgSize;
    cv::Mat imgL;
    cv::Mat imgR;
    cv::Mat imgL_rect;
    cv::Mat imgR_rect;
    cv::Mat imgL_rect_original;
    cv::Mat imgL_grey;
    cv::Mat imgL_grey_prev;
    cv::Mat imgR_grey;
    cv::Mat imgL_census;
    cv::Mat imgR_census;
    cv::Mat imgL_hsv;
    cv::Mat histL_hsv;
    cv::Mat histL_hsv_keyframe;
    cv::Mat R1;
    cv::Mat R2;
    cv::Mat SA_template[10];

    cv::Mat img_visualization;
    cv::Mat img_seg_prob;
    cv::Mat mask_specularity;
    cv::Mat img_hough_space;
    cv::Mat img_matches;
    cv::Mat img_keyframe;

    std::string SA_file_name;
    std::string datasetName = "stereo_dataset";
    int frame_number;
    int hbin_HSV;
    int sbin_HSV;

    bool start_video;
    bool clicked;
    bool bottonUP;
    bool performRectification;
    bool KLTTracker;
    bool HMatchingTracker;
    bool safety_area_defined;
    bool is_update_model;
    bool addedNewModel;
    bool modelReplaced;
    bool wait_for_new_match;
    bool img_subSampling;
    bool disparityCompleted;
    
    /**
     * Should the 3D reconstruction be run on the full image or should the masks be respected
     */
    bool is3Drecon_FullImage;
    bool isTrackingEvaluation;
    
    /**
     * This flag indicates whther the run is only evaluating the 3D reconstruction in which case the tracking should be
     * disabled or if it is a normal run with tracking.
     */
    bool is3DReconEvaluation;
    int SADefinition_status;
    int def_saf_area;
    int n_klt_total_feat;
    int n_hm_sa_feat;
    int n_klt_sa_feat;

    int event;
    cv::Point2f temp;
    cv::Point2f temp_prev;
    cv::Point center;
    std::vector <cv::Point> temp_vec;
    int radius;
    unsigned char button_prev;

    double read_images_time;
    double define_sa_time;
    double klt_tracker_time;
    double hough_matching_time;
    double stereo_recon_up_time;
    double stereo_recon_do_time;
    double safety_aug_time;

    //cv::FileStorage time_fs;
    std::ofstream time_file;

    // Sequence number of the current image frame (grabbed from the left image)
    int frame_seq = 0;

    //param
    //	-- 3d reconstruction
    double saturation;
    double value;

    int stage;

    std::vector <KeyPoint> sceneKeyPoints;
    std::vector<MatchResult *> matchResults;

    //methods
    void images_callback(
        const sensor_msgs::ImageConstPtr &l,
        const sensor_msgs::ImageConstPtr &r);

    void callback_dynamic_reconfigure(
        envisors_2::envisors2Config &config, uint32_t level);

    static void SA_manual_selection_callback(
        int event, int x, int y,
        int flags, void *param);

    void tablet_callback(const envisors_msgs::input_deviceConstPtr &t);

    void read_images();
    void scale_image(cv::Mat &imgL, cv::Mat &imgR);

    // init parameters
    void init_params();
    void init_LTSAT();
    void init_stereo_recon();
    void init_SafetyAugmentation();

    //Define Safety area and models
    int define_safety_area(SAInputMethod input_method);
    int init_SAmodel();
    void init_bayesian_network();
    void compute_segmentation(bool init);
    int detect_KLT_features();
    void rectification(
        bool perform_rect,
        cv::Mat &R1,
        cv::Mat &R2);
    void find_specularities();

    //track features
    bool KLT_tracking();
    void detect_failure();
    bool redetect_SA();
    void reinitilize_KLT();
    void redetect_KLT();
    void draw_tracking();

    void update_keyframe_data();
    void clear_variables();
    void publish_images();
    void publish_rectifiedImages();

    // 3D reconstruction
    void prepare_3D_reconstruction();
    void compute_super_pixel();
    void compute_3D_reconstruction();
    void filter_point_cloud();
    void update_point_cloud();

    void create_fake_pointCloud(cv::Mat &img_pc_fake);

    // Safety Augmentation methods
    /*methods*/
    void robotPose_callback(const geometry_msgs::PosePtr &r);
    void robotPose1Stamped_callback(const geometry_msgs::PoseStampedPtr &r1);
    void robotPose2Stamped_callback(const geometry_msgs::PoseStampedPtr &r2);
    void CameraPoseStamped_callback(const geometry_msgs::PoseStampedPtr &t_s);
    void point_cloud_to_robot(
        sensor_msgs::PointCloud2 &out,
        pcl::PointCloud <pcl::PointXYZRGB> &pc_out,
        const Eigen::Affine3d &transformation);
    void do_augmentation();
    void save_computational_times();
    void save_images();

    // multithread function
    pthread_t threadSuperPixel;

    static void *run_disparity(void *data);
    static void *run_super_pixel(void *data);

    // evaluation
    void evaluate_tracking();
    void evaluate_3Dreconstruction();

};

#endif /* ENVISORS2_INCLUDE_ENVISORS2_HPP_ */
