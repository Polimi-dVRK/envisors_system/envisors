//
// Created by tibo on 06/07/17.
//


#pragma once

#include <vector>
#include <opencv2/core/core.hpp>

struct SuperPixel
{
    bool isGaussian_SP;
    int nSuperPixels;
    int m;
    int max_iteration;
    cv::Mat imgL_colored;
    cv::Mat imgL_superPixelsIndex;
    bool init_super_pixel;
    cv::Mat labels;
    cv::Mat imgL_rect_GF;
    cv::Rect SA_rect;
    cv::Rect SA_rect_original;
    std::vector <cv::Point2i> contours;
};
