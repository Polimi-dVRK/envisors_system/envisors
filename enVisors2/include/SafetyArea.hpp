//
// Created by tibo on 06/07/17.
//


#pragma once

/**
 * How will the safety area be defined ?
 * - Online: The SA will be drawn in an OpenCV window
 * - Tablet: The SA will be drawn using an external tablet
 */
enum class SAInputMethod
{
    Online = 1,
    Tablet = 2
};

struct SafetyArea
{
    ModelInfo info;

    /// How will the safety area be defined. See #SAInputMethod for more information
    SAInputMethod definition_modality;

    /// Array of 2D points defining the bounds of the safety area in image space
    std::vector <cv::Point2f> contour;

    std::vector <std::vector<cv::Point>> contour_PO;
    std::vector <cv::Point2f> contour_keyframe;
    std::vector <cv::Point2f> points_detected;
    std::vector <cv::Point2f> points_tracked;
    std::vector <cv::Point2f> points_tracked_keyframe;
    std::vector <cv::Point2f> pt1_KLT;
    std::vector <cv::Point2f> pt2_KLT;
    std::vector <cv::Point2f> pt1_HC;
    std::vector <cv::Point2f> pt2_HC;
    std::vector <uchar> status_points_tracked;
    std::vector <cv::Point2f> points_tracked_prev;
    std::vector <uchar> status_points_tracked_prev;
    std::vector <uchar> status_SA_points;
    std::vector <uchar> status_SA_points_keyframe;
    int n_feat_detected;
    int n_feat_in_SA;
    int n_feat_keyframe;
    //	cv::Rect SA_rect;

    cv::Point3f tablet_point;
    double tablet_pressure;
    unsigned char tablet_buttons;

    //hough clustering
    ObjectProperties obj_prop;
    std::vector <cv::KeyPoint> SURFkp_first_SA_model;
    std::vector <cv::KeyPoint> SURFkp_SA_model;
    cv::Mat SA_mask;
    cv::Mat SARect_mask_left;
    cv::Mat SARect_mask_right;
    cv::Mat SARect_mask_right_to_left;
    std::deque <ObjectProperties> objectList;
    std::deque <ModelInfo> modelInfo;

    cv::Mat homographyT;

    // check failure
    double mean_OF;
    double std_OF;
    bool hom_validity;
    int n_feat_act;
    double percLostFeat;
    double bh_distance;

    double failure_jont_probability;

};