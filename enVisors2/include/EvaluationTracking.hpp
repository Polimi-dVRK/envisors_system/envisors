//
// Created by tibo on 06/07/17.
//


#pragma once


struct EvaluationTracking
{
    std::string file_string_TK;
    std::string file_string_TK_partialOcclusion;
    std::string file_string_nContour;
    std::string path_img;

    cv::FileStorage fsTK;
    cv::FileStorage fsTK_partialOcclusion;
    cv::FileStorage fsTK_NContour;

    int totalFramesNumber;
    std::vector<int> NContours;
};
