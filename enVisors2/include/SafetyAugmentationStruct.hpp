//
// Created by tibo on 06/07/17.
//


#pragma once

#include <string>

#include <opencv2/core/core.hpp>
#include <Eigen/Geometry>
#include <geometry_msgs/Pose.h>
#include <tf/transform_datatypes.h>

#include <envisors/safety_augmentation.hpp>

struct SafetyAugmentationStruct
{
    geometry_msgs::Pose robotPose1;
    geometry_msgs::Pose robotPose2;
    geometry_msgs::Pose cameraPose;

    cv::Mat image_augmented;
    Eigen::Affine3d camera_transform;
    tf::StampedTransform tool_transform;
    //	pcl::PointCloud<pcl::PointXYZRGB> pointCloud_t;
    cv::Mat pointCloud_t;
    sensor_msgs::PointCloud2 pointCloud_t_msgs;
    float alpha;
    cv::Mat gauge_img, gauge_mask;
    SafetyAugmentation ellips;
    float distance_to_pc;
    float distance1;
    float distance2;
    float d_min;
    float d_max;
};
