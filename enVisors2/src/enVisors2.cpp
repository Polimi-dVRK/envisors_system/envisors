/*
 * enVisors2.cpp
 *
 *  Created on: Sep 21, 2016
 *      Author: veronica
 */


// Known required

#include <cstdio>
#include <iostream>
#include <list>
#include <vector>

#ifdef MULTITHREAD
#include <pthread.h>
#endif

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include <pcl/point_types.h>
#include <kdl_conversions/kdl_msg.h>

#include <cam_lib/camimage.h>
#include <hmatching/Box2D.h>
#include <hmatching/Homography.h>
#include <hmatching/KeyPoint.h>
#include <hmatching/KeyPointMatch.h>
#include <hmatching/MatchResult.h>

#include "enVisors2.hpp"

enVisorsSys2::enVisorsSys2()
    :
    it(n),
    TrackL(true, false)
{
}

enVisorsSys2::~enVisorsSys2()
{
    time_file.close();
}

void enVisorsSys2::images_callback(
    const sensor_msgs::ImageConstPtr &l, 
    const sensor_msgs::ImageConstPtr &r
)
{
    imgSize = cv::Size(l->height, l->width);
    frame_seq = l->header.seq;
    
    fromImagetoMat(*l, &imgL, "bgr8");
    fromImagetoMat(*r, &imgR, "bgr8");

    // Check for invalid input
    if (imgL.empty()) {
        ROS_WARN("Received empty frame for left image");
    }
    if (imgR.empty()) {
        ROS_WARN("Received empty frame for right image");
    }
}

void enVisorsSys2::callback_dynamic_reconfigure(
    envisors_2::envisors2Config &config, uint32_t level)
{
    //PARAMS
    ROS_INFO("Reconfigure Request:");

    recon.isGaussian = config.isGaussian_;
    performRectification = config.isPerformRectification_;

    //--super pixels
    recon.super_pixel.isGaussian_SP = config.isGaussian_SP_;
    if (config.nSuperPixels_ != recon.super_pixel.nSuperPixels) {
        recon.super_pixel.nSuperPixels = config.nSuperPixels_;
        recon.super_pixel.init_super_pixel = true;
    }

    recon.super_pixel.m = config.m_;
    if (config.maxIteration_ != recon.super_pixel.max_iteration) {
        recon.super_pixel.max_iteration = config.maxIteration_;
        recon.super_pixel.init_super_pixel = true;
    }

    //--stereo correspondence
    img_subSampling = config.subSampling_;

    //--CENSUS
    is3Drecon_FullImage = config.is3Drecon_FullImage_;
    recon.isModifiedCensus = config.isModifiedCensus_;
    recon.isAggregationCost = config.isAggregationCost_;
    if (isOdd(config.n_census_)) recon.n_census = config.n_census_;
    if (isOdd(config.m_census_)) recon.m_census = config.m_census_;

    //--common
    recon.threshold_LRC = config.threshold_LRC_;
    recon.minDisparities = config.minDisparities_ * recon.img_scalefactor;
    recon.numberOfDisparities = config.numberOfDisparities_ * recon.img_scalefactor;
    n.setParam("numberOfDisparities", recon.numberOfDisparities);
    recon.spurie_Th = config.spurie_Th_;
    if (isOdd(config.m_aggCost_)) recon.m_aggCost = config.m_aggCost_;
    if (isOdd(config.n_aggCost_)) recon.n_aggCost = config.n_aggCost_;
    saturation = config.saturation_;
    value = config.value_;

    //--post processing
    recon.ispostProcessing = config.ispostProcessing_;
    recon.isSpeckleRemoval = config.isSpeckleRemoval_;
    recon.isFillHole = config.isFillHole_;
    recon.isAVERAGEFiltering = config.isAVERAGEFiltering_;
    recon.isMEDIANFiltering = config.isMEDIANFiltering_;
    recon.isPLANE_FITFiltering = config.isPLANE_FITFiltering_;
    recon.isQUAD_FITFiltering = config.isQUAD_FITFiltering_;
    recon.isRANSAC_FITFiltering = config.isRANSAC_FITFiltering_;
    recon.isPoissonSmoothing = config.isPoissonSmoothing_;
    recon.isBackgroundRemoval = config.isBackgroundRemoval_;
    recon.isRemove_outliers = config.isRemove_outliers_;
    recon.isReplace_all_points = config.isReplace_all_points_;
    recon.isRemove_inclined_plane = config.isRemove_inclined_plane_;
    recon.isBilateralFilter = config.isBilateralFilter_;
    recon.newVal_speckle = config.newVal_speckle_;
    recon.maxSize_speckle = config.maxSize_speckle_;
    recon.maxDiff_speckle = config.maxDiff_speckle_;
    recon.diamPixelWind = config.diamPixelWind_;
    recon.sigmaParam = config.sigmaParam_;
    recon.n_rand = config.n_rand_;
    recon.iteration = config.iteration_;
    recon.min_num_point = config.min_num_point_;
    recon.RANSAC_threshold = config.RANSAC_threshold_;
    is3DReconEvaluation = config.is3DreconEvaluation_;
    isTrackingEvaluation = config.isTrackingEvaluation_;

    recon.img_scale = config.scale_;

    StereoCorr.setParamCENSUS(
        img_subSampling,
        true, // is specularity
        saturation,
        value,
        true, // is sub pixel refinement
        recon.isModifiedCensus,
        recon.isAggregationCost,
        recon.minDisparities,
        recon.numberOfDisparities,
        recon.spurie_Th,
        recon.n_census,
        recon.m_census,
        recon.threshold_LRC,
        recon.m_aggCost,
        recon.n_aggCost);

    StereoCorr.setParamPostProcessing(
        recon.ispostProcessing,
        recon.isSpeckleRemoval,
        recon.isFillHole,
        recon.isAVERAGEFiltering,
        recon.isMEDIANFiltering,
        recon.isPLANE_FITFiltering,
        recon.isQUAD_FITFiltering,
        recon.isRANSAC_FITFiltering,
        recon.isPoissonSmoothing,
        recon.isBackgroundRemoval,
        recon.isRemove_outliers,
        recon.isReplace_all_points,
        recon.isRemove_inclined_plane,
        recon.isBilateralFilter,
        recon.newVal_speckle,
        recon.maxSize_speckle,
        recon.maxDiff_speckle,
        recon.diamPixelWind,
        recon.sigmaParam,
        recon.n_rand,
        recon.iteration,
        recon.min_num_point,
        recon.RANSAC_threshold);
}

void enVisorsSys2::SA_manual_selection_callback(
    int event, int x, int y, int flags, void *param
)
{
    cv::Point2f previous_point(-1, 1);
    enVisorsSys2 *enV2 = (enVisorsSys2*) param;
    cv::Mat &img = enV2->img_visualization; // 1st cast it back, then deref

    switch (event) {
        
        case CV_EVENT_LBUTTONDOWN:
            // If we press the left mouse button we want to define a new safety area
            enV2->SA.contour.clear();
            enV2->SA.contour.push_back({x, y});
            enV2->clicked = true;
            enV2->SADefinition_status = 2;
            break;

        case CV_EVENT_MOUSEMOVE:
            // If the left mouse is pressed we are drawing the safety area manually
            if (enV2->clicked) {
                cv::Point2f new_point(x, y);
                
                // check if the safety area is defined in the image range
                if (new_point.x < 0.0)
                    new_point.x = 1;
                if (new_point.y < 0.0)
                    new_point.y = 1;
                if (new_point.x >= enV2->imgL_rect.cols)
                    new_point.x = enV2->imgL_rect.cols - 1;
                if (new_point.y >= enV2->imgL_rect.rows)
                    new_point.y = enV2->imgL_rect.rows - 1;

                enV2->SA.contour.push_back(new_point);

                if (previous_point.x > 0 && previous_point.y > 0) {
                    // Draw a line connecting the previous point and this new point
                    cv::line(enV2->img_visualization, previous_point, new_point, cv::Scalar(255, 0, 0), 3, 8);
                }

                previous_point = {new_point.x, new_point.y};
                enV2->SADefinition_status = 2; // FIXME: What constant is this ? 
            }
            break;

        case CV_EVENT_LBUTTONUP: {
            // When the left button is released collect all the points and actually create the safety area
            cv::Point2f new_point(x, y);

            // check if the safety area is defined in the image range
            if (new_point.x < 0.0)
                new_point.x = 0.0 + 1;
            if (new_point.y < 0.0)
                new_point.y = 0.0 + 1;
            if (new_point.x > enV2->imgL_rect.cols)
                new_point.x = enV2->imgL_rect.cols - 1;
            if (new_point.y > enV2->imgL_rect.rows)
                new_point.y = enV2->imgL_rect.rows - 1;

            enV2->SA.contour.push_back(new_point);
            enV2->clicked = false;

            std::vector <cv::Point> temp_vec(enV2->SA.contour.size() + 1);
            for (int i = 0; i < enV2->SA.contour.size(); i++) {
                temp_vec[i].x = enV2->SA.contour[i].x;
                temp_vec[i].y = enV2->SA.contour[i].y;
            }
            temp_vec[enV2->SA.contour.size()].x = enV2->SA.contour[0].x;
            temp_vec[enV2->SA.contour.size()].y = enV2->SA.contour[0].y;

            for (int j = 0; j < enV2->SA.contour.size(); j++) {
                cv::line(enV2->img_visualization, temp_vec[j],
                         temp_vec[(j + 1) % temp_vec.size()],
                         cv::Scalar(255, 0, 0), 3, 8);
            }

            cv::imshow("SA_Definition", enV2->img_visualization);

            if (enV2->SA.contour.size() > 10) {
                enV2->SADefinition_status = 1;
            }
            else {
                enV2->SA.contour.clear();
                enV2->SADefinition_status = 0;
            }
            break;
        }
        default:
            break;
    }
}

void enVisorsSys2::tablet_callback(const envisors_msgs::input_device::ConstPtr &t)
{
    SA.tablet_point.x = t->pose.position.x * imgL_rect.cols;
    SA.tablet_point.y = t->pose.position.y * imgL_rect.rows;
    SA.tablet_point.z = t->delta.z;
    SA.tablet_pressure = t->pressure;
    SA.tablet_buttons = t->buttons;
    
    sensor_msgs::Joy tablet_info;

    tablet_info.header.seq = 0;
    tablet_info.header.stamp = ros::Time::now();
    tablet_info.header.frame_id = "/world";

    tablet_info.axes.resize(3);
    tablet_info.axes[0] = t->pose.position.x;
    tablet_info.axes[1] = t->pose.position.y;
    tablet_info.axes[2] = t->delta.z;

    tablet_info.buttons.resize(8);
    tablet_info.buttons[0] = (SA.tablet_buttons & 0x01) ? 1 : 0;
    tablet_info.buttons[1] = (SA.tablet_buttons & 0x02) ? 1 : 0;
    tablet_info.buttons[2] = (SA.tablet_buttons & 0x04) ? 1 : 0;
    tablet_info.buttons[3] = (SA.tablet_buttons & 0x08) ? 1 : 0;
    tablet_info.buttons[4] = (SA.tablet_buttons & 0x10) ? 1 : 0;
    tablet_info.buttons[5] = (SA.tablet_buttons & 0x20) ? 1 : 0;
    tablet_info.buttons[6] = (SA.tablet_buttons & 0x40) ? 1 : 0;
    tablet_info.buttons[7] = (SA.tablet_buttons & 0x80) ? 1 : 0;

    tablet_pub.publish(tablet_info);
}


/**
 * Wait until an image has been received for both cameras before continuing
 *
 * @return
 */
void enVisorsSys2::read_images()
{
    ros::Rate loop_rate(100);

    while (ros::ok()) {
        if (!imgL.empty() && !imgR.empty()) {
//            start_video = true;
            return;
        }

        ros::spinOnce();
        loop_rate.sleep();
    }
}

void enVisorsSys2::scale_image(cv::Mat &imgL, cv::Mat &imgR)
{
    cv::Size subSize;
    subSize.height = 288;
    subSize.width = (imgL.cols * 288) / imgL.rows;

    resize(imgL, imgL, subSize, 0, 0, cv::INTER_LINEAR);
    resize(imgR, imgR, subSize, 0, 0, cv::INTER_LINEAR);
}


void enVisorsSys2::init_params()
{
    // update dynamic_reconfigure parameters
    ros::spinOnce();

    clicked = false;
    bottonUP = false;
    SADefinition_status = 0; // 0 - NOT DEFINED - 1 DEFINED - 2 DEFINING
    def_saf_area = 0;
    n_klt_total_feat = 0;
    n_hm_sa_feat = 0;
    n_klt_sa_feat = 0;
    event = -1;

    // initialize computational time of each state
    read_images_time = -1.0;
    define_sa_time = -1.0;
    klt_tracker_time = -1.0;
    hough_matching_time = -1.0;
    stereo_recon_up_time = -1.0;
    stereo_recon_do_time = -1.0;
    safety_aug_time = -1.0;

    if (is3DReconEvaluation) {
        is3Drecon_FullImage = true;
    }
    EvalTrack.totalFramesNumber = 0;
    
    recon.img_scalefactor = 1.0f;
    safety_area_defined = false;
    hbin_HSV = 10;
    sbin_HSV = 10;
    frame_number = 0;
    addedNewModel = false;
    modelReplaced = false;
    wait_for_new_match = false;
    start_video = false;
    stage = STAGE_TRACK;
    is_update_model = true;

    imgL_rect_pub = it.advertise("/image/left/rectified", 1);
    imgR_rect_pub = it.advertise("/image/right/rectified", 1);
    imgL_grey_pub = it.advertise("/image/left/gray", 1);
    imgR_grey_pub = it.advertise("/image/right/gray", 1);
    imgL_census_pub = it.advertise("/image/left/census", 1);
    imgR_census_pub = it.advertise("/image/right/census", 1);
    imgL_contourn_pub = it.advertise("/image/left/superpixels", 1);
    disparity_pub = it.advertise("/image/disparity", 1);
    depth_pub = it.advertise("/image/depth", 1);
    pointCloud_pub = n.advertise<sensor_msgs::PointCloud2>("/pointCloud", 1);
    pointCloud_filtered_pub = n.advertise<sensor_msgs::PointCloud2>("/pointCloud/filtered", 1);
    imgL_features_pub = it.advertise("/image/left/features", 1);
    matches_pub = it.advertise("/tracking/matches", 1);
    optical_flow_pub = it.advertise("/tracking/optical_flow", 1);
    mask_reselection_pub = it.advertise("/tracking/mask_reselection", 1);
    template_object_pub = it.advertise("tracking/template_object", 1);
    hough_space_pub = it.advertise("tracking/hough_space", 1);
    segmentation_probability_pub = it.advertise("tracking/seg_probability", 1);
    bb_coords_pub = n.advertise<envisors_2::bb_coords>("/bb/coords", 1);
    bb_distance_pub = n.advertise<std_msgs::Float32>("/bb/distance", 1);
    tablet_pub = n.advertise<sensor_msgs::Joy>("/tablet/info", 1);

    performRectification = true;
    imgL_sub = new message_filters::Subscriber<sensor_msgs::Image>(n, "/endoscope/left/image_raw", 1);
    imgR_sub = new message_filters::Subscriber<sensor_msgs::Image>(n, "/endoscope/right/image_raw", 1);

    sync = new message_filters::Synchronizer<MySyncPolicy>(
        MySyncPolicy(10), *imgL_sub, *imgR_sub);
    sync->registerCallback(boost::bind(&enVisorsSys2::images_callback, this, _1, _2));

    // According to the previous codebase rectification should always be performed whe using ROS images
    // if (performRectification) {
    camera_info.camera_name = "davinci";
    std::string calib_path = DAVINCI_STEREOCALIBRATION_FILE + std::string("StereoCameraParameters.xml");
    Rect.setParam(calib_path);

    // set tablet subscriber if you are using the tablet to draw the SA
    if (SA.definition_modality == SAInputMethod::Tablet) {
        tablet_sub = n.subscribe("/uralp/input_device", 1,
                                       &enVisorsSys2::tablet_callback, this);
        //		 = new uralp_msgs::input_device(n, "/uralp/input_device", 1);
    }

    //if tracker calibration
    CameraPoseStamped_sub = n.subscribe(
        "/camera/pose", 1, &enVisorsSys2::CameraPoseStamped_callback, this);
    robotPose1_sub = n.subscribe(
        "/dvrk/PSM1/position_cartesian_current", 1, &enVisorsSys2::robotPose1Stamped_callback, this);
    robotPose2_sub = n.subscribe(
        "/dvrk/PSM2/position_cartesian_current", 1, &enVisorsSys2::robotPose2Stamped_callback, this);

    augmented_image_pub = n.advertise<sensor_msgs::Image>("/image/left/augmented", 1);
    pointCloud_transformed_pub = n.advertise<sensor_msgs::PointCloud2>("/pointCloud/transformed", 1);


    if (isTrackingEvaluation) {
        // setting file names
        EvalTrack.file_string_TK = DATASET_FOLDER + datasetName + std::string("TK.xml");
        EvalTrack.file_string_TK_partialOcclusion = DATASET_FOLDER + datasetName + std::string("TK_PO.xml");
        EvalTrack.file_string_nContour = DATASET_FOLDER + datasetName + std::string("NContour.xml");
        EvalTrack.path_img = DATASET_FOLDER + datasetName + "/images/_";

        // opening files
        ROS_INFO_STREAM("Saving data in " << EvalTrack.file_string_TK);
        while (datasetName.empty()) {
            ros::spinOnce();
        }
        EvalTrack.fsTK.open(EvalTrack.file_string_TK, cv::FileStorage::WRITE);
        EvalTrack.fsTK_partialOcclusion
            .open(EvalTrack.file_string_TK_partialOcclusion, cv::FileStorage::WRITE);
        EvalTrack.fsTK_NContour.open(EvalTrack.file_string_nContour, cv::FileStorage::WRITE);
    }

    // open file for saving computational time

    //time_fs.open(std::string(CONFIG_FILE_FOLDER)+ "ComputationalTime.xml", cv::FileStorage::WRITE);
    time_file.open(
        (std::string(CONFIG_FILE_FOLDER) + "ComputationalTime.csv").c_str());

    time_file << "read_images_time,";
    time_file << "define_sa_time,";
    time_file << "klt_tracker_time,";
    time_file << "hough_matching_time,";
    time_file << "stereo_recon_do_time,";
    time_file << "stereo_recon_up_time,";
    time_file << "safety_aug_time,";
    time_file << "SA_width,";
    time_file << "SA_height\n";
    time_file.flush();
}


void enVisorsSys2::init_LTSAT()
{
    //init KLT detector
    TrackL.setKLTParamDetector("KLT", &SA.points_detected, imgSize);

    //init KLT tracker
    TrackL.setKLTParamTracking(TrackL.TrackingParam.winsize_opt_flow);

}
void enVisorsSys2::init_stereo_recon()
{
    recon.super_pixel.isGaussian_SP = false;
    recon.super_pixel.nSuperPixels = 70;
    recon.super_pixel.m = 14;
    recon.super_pixel.max_iteration = 20;
    recon.super_pixel.init_super_pixel = true;

    recon.isGaussian = false;

    StereoCorr.setParamCENSUS(
        img_subSampling,
        true, // is specularity
        saturation,
        value,
        true, // is sub pixel refinement
        recon.isModifiedCensus,
        recon.isAggregationCost,
        recon.minDisparities,
        recon.numberOfDisparities,
        recon.spurie_Th,
        recon.n_census,
        recon.m_census,
        recon.threshold_LRC,
        recon.m_aggCost,
        recon.n_aggCost);

    StereoCorr.setParamPostProcessing(
        recon.ispostProcessing,
        recon.isSpeckleRemoval,
        recon.isFillHole,
        recon.isAVERAGEFiltering,
        recon.isMEDIANFiltering,
        recon.isPLANE_FITFiltering,
        recon.isQUAD_FITFiltering,
        recon.isRANSAC_FITFiltering,
        recon.isPoissonSmoothing,
        recon.isRemove_outliers,
        recon.isReplace_all_points,
        recon.isRemove_inclined_plane,
        recon.isBackgroundRemoval,
        recon.isBilateralFilter,
        recon.newVal_speckle,
        recon.maxSize_speckle,
        recon.maxDiff_speckle,
        recon.diamPixelWind,
        recon.sigmaParam,
        recon.n_rand,
        recon.iteration,
        recon.min_num_point,
        recon.RANSAC_threshold);


    if (performRectification) {
        StereoRecon.setParam(*Rect.getQ());
    }
    else {
        StereoRecon.setParam(recon.Q);
    }
}

void enVisorsSys2::init_SafetyAugmentation()
{
    //load gauge image and gauge mask

    SAug.alpha = 0.0;
    SAug.gauge_img = cv::imread(CONFIG_FILE_FOLDER + std::string("gauge/gauge2_half.png"), cv::IMREAD_COLOR);
    SAug.gauge_mask =
        cv::imread(CONFIG_FILE_FOLDER + std::string("gauge/gauge2_half_mask.png"), cv::IMREAD_GRAYSCALE);
}
// initialization methods

int enVisorsSys2::define_safety_area(SAInputMethod input_method)
{
    if (input_method == SAInputMethod::Online) {
        cv::imshow("SA_Definition", img_visualization);

        if (SADefinition_status == 0) {
            std::cout << "Safety Area NOT Defined" << std::endl;
        }
        else if (SADefinition_status == 1) {
            std::cout << "Safety Area Defined!" << std::endl;
        }
        else if (SADefinition_status == 2) {
            std::cout << "Defining ... " << std::endl;
        }
        return SADefinition_status;

    }
    else if (input_method == SAInputMethod::Tablet) {

        enum
        {
            VIS = 0, BUTTONDOWN = 1, MOUSEMOVE = 2, LBUTTONUP = 3, NONE = 4
        };

        cv::imshow("SA_Definition", img_visualization);

        if (!(button_prev & 0x01) && (SA.tablet_buttons & 0x01)) {

            if ((SA.tablet_buttons & 0x40)) {
                event = BUTTONDOWN;
            }
            else {
                event = VIS;
            }
        }
        else if ((button_prev & 0x01) && !(SA.tablet_buttons & 0x01)) {
            if ((SA.tablet_buttons & 0x40)) {
                event = LBUTTONUP;
            }
            else {
                event = NONE;
            }
        }
        else if (SA.tablet_buttons & 0x01) {
            event = VIS;

            if (!(button_prev & 0x40) && (SA.tablet_buttons & 0x40)) {
                event = BUTTONDOWN;
            }
            else if ((button_prev & 0x40) && !(SA.tablet_buttons & 0x40)) {
                event = LBUTTONUP;
            }
            else if ((SA.tablet_buttons & 0x40)) {
                event = MOUSEMOVE;
            }

        }
        else {
            event = NONE;
        }

        center.x = 0;
        center.y = 0;
        switch (event) {

            case VIS:
                center.x = SA.tablet_point.x;
                center.y = SA.tablet_point.y;
                radius = 3;
                imgL_rect.copyTo(img_visualization);
                cv::circle(img_visualization, center, radius,
                           cv::Scalar(0, 255, 0), 2);
                std::cout << center << std::endl;
                cv::imshow("SA_Definition", img_visualization);
                SADefinition_status = 0;

                //			std::cout << "VIS" << std::endl;

                break;

            case BUTTONDOWN:

                SA.contour.clear();
                //		enV2->imgL.copyTo(enV2->img_visualization);
                std::cout << "down" << std::endl;
                temp.x = SA.tablet_point.x;
                temp.y = SA.tablet_point.y;

                // check if the safety area is defined in the image range
                if (temp.x < 1)
                    temp.x = 1;
                if (temp.y < 1)
                    temp.y = 1;
                if (temp.x >= imgL_rect.cols)
                    temp.x = imgL_rect.cols - 1;
                if (temp.y >= imgL_rect.rows)
                    temp.y = imgL_rect.rows - 1;
                SA.contour.push_back(temp);

                clicked = true;
                temp_prev.x = temp.x;
                temp_prev.y = temp.y;

                SADefinition_status = 2;

                //			std::cout << "BUTTONDOWN" << std::endl;

                break;

            case MOUSEMOVE:

                if (clicked) {
                    temp.x = SA.tablet_point.x;
                    temp.y = SA.tablet_point.y;
                    // check if the safety area is defined in the image range
                    if (temp.x < 1)
                        temp.x = 1;
                    if (temp.y < 1)
                        temp.y = 1;
                    if (temp.x >= imgL_rect.cols)
                        temp.x = imgL_rect.cols - 1;
                    if (temp.y >= imgL_rect.rows)
                        temp.y = imgL_rect.rows - 1;

                    SA.contour.push_back(temp);


                    cv::line(img_visualization, temp_prev, temp,
                             cv::Scalar(255, 0, 0), 3, 8);

                    temp_prev.x = temp.x;
                    temp_prev.y = temp.y;

                    cv::imshow("SA_Definition", img_visualization);

                    SADefinition_status = 2;
                }



                //			std::cout << "MOUSEMOVE" << std::endl;

                break;

            case LBUTTONUP:
                //			std::cout << "LBUTTONUP" << std::endl;
                temp.x = SA.tablet_point.x;
                temp.y = SA.tablet_point.y;

                // check if the safety area is defined in the image range
                if (temp.x < 1)
                    temp.x = 1;
                if (temp.y < 1)
                    temp.y = 1;
                if (temp.x >= imgL_rect.cols)
                    temp.x = imgL_rect.cols - 1;
                if (temp.y >= imgL_rect.rows)
                    temp.y = imgL_rect.rows - 1;

                SA.contour.push_back(temp);

                temp_vec.resize(SA.contour.size() + 1);
                for (int i = 0; i < SA.contour.size(); i++) {
                    temp_vec[i].x = SA.contour[i].x;
                    temp_vec[i].y = SA.contour[i].y;
                }
                temp_vec[SA.contour.size()].x = SA.contour[0].x;
                temp_vec[SA.contour.size()].y = SA.contour[0].y;

                for (int j = 0; j < SA.contour.size(); j++) {
                    cv::line(img_visualization, temp_vec[j],
                             temp_vec[(j + 1) % temp_vec.size()],
                             cv::Scalar(255, 0, 0), 3, 8);
                }

                cv::imshow("SA_Definition", img_visualization);

                if (SA.contour.size() > 10) {
                    std::cout << __LINE__ << "SA.contour.size " << SA.contour.size() << std::endl;
                    SADefinition_status = 1;
//				bottonUP = true;
                }
                else {
                    std::cout << __LINE__ << "SA.contour.size " << SA.contour.size() << std::endl;
                    SA.contour.clear();
                    SADefinition_status = 0;
                }
                clicked = false;
                //		enV2->bottonUP = true;
                //		cv::drawContours(enV2->img_visualization, *temp_vec,
                //						0, cv::Scalar(255,0,0), 2);

                //		cv::fillConvexPoly(enV2->img_visualization, temp_vec,
                //				cv::Scalar(255,0,0));


                break;
            case NONE:
                SADefinition_status = 0;
                break;

            default:

                break;
        }

        button_prev = SA.tablet_buttons;

        return SADefinition_status;

    }
    else {
        return 0;
    }

}

int enVisorsSys2::init_SAmodel()
{
    std::string modelName = std::string("000000");
    SA_template[0].create(imgL_grey.rows, imgL_grey.cols, CV_8UC1);

    cv::Mat temp_mask;
    temp_mask.create(imgL_grey.rows, imgL_grey.cols, CV_8UC1);

    // learn the object properties
    TrackL.computeSAModel(
        learning,
        imgL_grey,
        SA.contour,
        SA.SA_mask,
        SA.SURFkp_first_SA_model,
        modelName,
        SA.obj_prop);

    // draw keypoints
    cv::drawKeypoints(
        imgL_grey,
        SA.SURFkp_first_SA_model,
        temp_mask, 0.5);

    ROS_INFO_STREAM("n kp hough" << SA.SURFkp_first_SA_model.size());

    // crop the image around the safety area
    cv::Rect templateROI = boundingRect(SA.contour);

    SA_template[0] = temp_mask(templateROI);

    SA.info.SA = SA.contour;
    SA.info.nTimesUsed = 0;
    SA.info.nTimesNotUsed = 0;
    SA.info.BAdistance = 0.0;

    TrackL.clear_models(
        SA.objectList,
        SA.modelInfo);

    TrackL.add_model(
        SA.objectList,
        SA.modelInfo,
        SA.obj_prop, SA.info, 10);

    return SA.SURFkp_first_SA_model.size();
}

void enVisorsSys2::init_bayesian_network()
{
    // initilize bayesian network
    BNet.init_network(
        TrackL.TrackingParam.bnet_params,
        SA.n_feat_detected * 2);
}

void enVisorsSys2::compute_segmentation(bool init)
{
    TrackL.computeSegmentationModel(
        imgL_rect,
        img_visualization,
        imgL_hsv,
        img_seg_prob,
        SA.points_tracked,
        SA.status_points_tracked,
        SA.contour,
        KLTTracker,
        HMatchingTracker,
        init);
}

int enVisorsSys2::detect_KLT_features()
{
    std::vector <cv::Point2f> temp_pt_detected;
    std::vector <cv::KeyPoint> pt;

    TrackL.detectKLTFeatures(imgL_grey, 255 - SA.SA_mask, temp_pt_detected);

    SA.n_feat_detected = temp_pt_detected.size();
    SA.points_tracked = SA.points_detected;
    SA.status_points_tracked.resize(SA.points_detected.size(), 1);

    return SA.n_feat_detected;
}

void enVisorsSys2::rectification(bool perform_rect, cv::Mat &R1, cv::Mat &R2)
{
    if (perform_rect) {

        Rect.process(imgL, imgR, imgL_rect, imgR_rect, R1, R2);

        if (is3DReconEvaluation) {
            // save rectification maps
            Rmap rmaps;
            Rect.getRmap(rmaps);
            //				rmaps.xL.create( imgL_rect.size(), CV_32FC1 );
            //				rmaps.yL.create( imgL_rect.size(), CV_32FC1 );
            cv::FileStorage rect_maps_f;
            std::string path_base = DATASET_FOLDER;
            std::string path_maps = path_base + datasetName + "RectParam.xml";
            rect_maps_f.open(path_maps, cv::FileStorage::WRITE);
            rect_maps_f << "rmap0_left" << rmaps.xL;
            rect_maps_f << "rmap1_left" << rmaps.yL;
            rect_maps_f << "rmap0_right" << rmaps.xR;
            rect_maps_f << "rmap1_right" << rmaps.yR;
            rect_maps_f << "R1" << R1;
            rect_maps_f.release();
        }
    }
    else {
        imgL.copyTo(imgL_rect);
        imgR.copyTo(imgR_rect);
    }
}

void enVisorsSys2::find_specularities()
{
    mask_specularity.setTo(0);
    StereoCorrespondence::removeSpecularity(
        imgL_rect, mask_specularity, saturation, value);
}

// tracking methods

bool enVisorsSys2::KLT_tracking()
{
    ros::Time timeBegin;
    ros::Time timeEnd;
    ros::Duration deltaTime;

    timeBegin = ros::Time::now();
    TrackL.trackingKLTFeatures(
        imgL_grey_prev,
        imgL_grey,
        255 - mask_specularity,
        SA.points_tracked_prev,
        SA.points_tracked,
        SA.status_points_tracked);
    timeEnd = ros::Time::now();
    deltaTime = timeEnd - timeBegin;

#ifdef COMPUTATIONAL_TIME
    std::cout << "[TIME] tracking feat :" << deltaTime.toSec() << std::endl;
#endif

    timeBegin = ros::Time::now();
    SA.pt1_KLT.clear();
    SA.pt2_KLT.clear();
    SA.n_feat_act = 0;

    for (int i = 0; i < SA.points_tracked.size(); i++) {
        if (SA.status_points_tracked[i] == 1) {
            SA.pt1_KLT.push_back(SA.points_tracked_keyframe[i]);
            SA.pt2_KLT.push_back(SA.points_tracked[i]);
            SA.n_feat_act++;
        }
    }
    timeEnd = ros::Time::now();
    deltaTime = timeEnd - timeBegin;
#ifdef COMPUTATIONAL_TIME
    std::cout << "[TIME] push back :" << deltaTime.toSec() << std::endl;
#endif

    if (SA.pt2_KLT.size() > 4) {
        //find the homography between the prevoius (or keyframe) and current frame
        timeBegin = ros::Time::now();
        SA.homographyT =
            cv::findHomography(SA.pt1_KLT, SA.pt2_KLT, CV_RANSAC);

        // apply the homography to safety area (points)
        cv::perspectiveTransform(SA.contour_keyframe, SA.contour, SA.homographyT);

        timeEnd = ros::Time::now();
        deltaTime = timeEnd - timeBegin;

#ifdef COMPUTATIONAL_TIME
        std::cout << "[TIME] homography :" << deltaTime.toSec() << std::endl;
#endif

        timeBegin = ros::Time::now();
        // create new template with previous lost safety area
        TrackL.create_safety_area_mask(
            imgL_grey,
            SA.contour,
            mask_specularity,
            SA.SA_mask);

        timeEnd = ros::Time::now();
        deltaTime = timeEnd - timeBegin;
#ifdef COMPUTATIONAL_TIME
        std::cout << "[TIME] create mask :" << deltaTime.toSec() << std::endl;
#endif

        timeBegin = ros::Time::now();
        // update probability map segmentation
        compute_segmentation(false);
        timeEnd = ros::Time::now();
        deltaTime = timeEnd - timeBegin;

#ifdef COMPUTATIONAL_TIME
        std::cout << "[TIME] seg map :" << deltaTime.toSec() << std::endl;
#endif

        timeBegin = ros::Time::now();
        // discard feature in the backgrund
        TrackL.find_points_in_safety_area_high_seg_prob(
            imgL_rect,
            imgL_hsv,
            SA.points_tracked,
            SA.contour,
            SA.status_points_tracked,
            SA.status_SA_points,
            SA.n_feat_detected,
            TrackL.TrackingParam.th_segmentation);

        timeEnd = ros::Time::now();
        deltaTime = timeEnd - timeBegin;

#ifdef COMPUTATIONAL_TIME
        std::cout << "[TIME] find points :" << deltaTime.toSec() << std::endl;
#endif

        timeBegin = ros::Time::now();
        // compute hsv image histogram
        hsv_image_histogram(
            imgL_hsv,
            histL_hsv,
            SA.contour,
            SA.SA_mask,
            hbin_HSV,
            sbin_HSV,
            0.5,
            &TrackL.SegModel);

        timeEnd = ros::Time::now();
        deltaTime = timeEnd - timeBegin;

#ifdef COMPUTATIONAL_TIME
        std::cout << "[TIME] hsv :" << deltaTime.toSec() << std::endl;
#endif

        timeBegin = ros::Time::now();
        TrackL.getOpticalFlowDistribution(
            SA.pt1_KLT,
            SA.pt2_KLT,
            SA.status_SA_points,
            SA.mean_OF,
            SA.std_OF);
        timeEnd = ros::Time::now();
        deltaTime = timeEnd - timeBegin;

#ifdef COMPUTATIONAL_TIME
        std::cout << "[TIME] OF DIST :" << deltaTime.toSec() << std::endl;
#endif
        timeBegin = ros::Time::now();
        // check failure events
        TrackL.check_failure_parameters(
            imgSize,
            SA.homographyT,
            SA.contour_keyframe,
            SA.contour,
            SA.hom_validity,
            SA.n_feat_keyframe,
            SA.n_feat_act,
            SA.percLostFeat,
            imgL_rect,
            histL_hsv_keyframe,
            histL_hsv,
            hbin_HSV, sbin_HSV, 0.5,
            SA.bh_distance);
        timeEnd = ros::Time::now();
        deltaTime = timeEnd - timeBegin;

#ifdef COMPUTATIONAL_TIME
        std::cout << "[TIME] check failure :" << deltaTime.toSec() << std::endl;
#endif

        timeBegin = ros::Time::now();
        //check failure
        SA.failure_jont_probability = BNet.compute_joint_probability(
            SA.n_feat_act,
            SA.percLostFeat,
            SA.hom_validity,
            SA.std_OF,
            SA.bh_distance);
        timeEnd = ros::Time::now();
        deltaTime = timeEnd - timeBegin;
#ifdef COMPUTATIONAL_TIME
        std::cout << "[TIME] joint prob:" << deltaTime.toSec() << std::endl;
#endif

        if (SA.failure_jont_probability > TrackL.TrackingParam.th_failure
            || SA.failure_jont_probability != SA.failure_jont_probability) {
#ifdef VERBOSE
            std::cout << "Failure" << "P(F) = " << SA.failure_jont_probability <<  std::endl;
#endif

            return true;
        }
        else { // No FAILURE
            timeBegin = ros::Time::now();
            TrackL.TrackingParam.th_redetection = 0.1 * SA.n_feat_keyframe;

            if (SA.n_feat_act < TrackL.TrackingParam.th_redetection) {
                redetect_KLT();
                update_keyframe_data();
            }
            timeEnd = ros::Time::now();
            deltaTime = timeEnd - timeBegin;
#ifdef COMPUTATIONAL_TIME
            std::cout << "[TIME] redetection:" << deltaTime.toSec() << std::endl;
#endif
            timeBegin = ros::Time::now();
            if (is_update_model) {
                SA.SURFkp_SA_model.clear();
                char modelName[8];
                sprintf(modelName, "%06d", frame_number);
                std::string modelNameStr = std::string(modelName);

                addedNewModel = TrackL.updateModel(
                    learning,
                    imgL_rect,
                    imgL_grey,
                    SA.contour,
                    SA.SA_mask,
                    SA.SURFkp_SA_model,
                    modelNameStr,
                    SA.bh_distance,
                    SA.objectList,
                    SA.modelInfo,
                    modelReplaced);
            }

            // draw partial occlusion
            TrackL.find_SAContourn_partialOcclusion(
                imgL_rect,
                img_visualization,
                imgL_hsv,
                SA.contour,
                SA.contour_PO);

            timeEnd = ros::Time::now();
            deltaTime = timeEnd - timeBegin;
#ifdef COMPUTATIONAL_TIME
            std::cout << "[TIME] Update model:" << deltaTime.toSec() << std::endl;
#endif
            //continue with KLT tracker
            return false;
        }
    } // if point > 4
    else {
        return true;
    }
}

void enVisorsSys2::draw_tracking()
{
    bool SA_contour_PO = true;
    if (stage == STAGE_TRACK || stage == STAGE_REINIT) {
        // Draw features inside the polygon
        for (int i = 0; i < SA.points_tracked.size(); i++) {
            if (SA.status_SA_points[i] == 1)
                cv::circle(img_visualization, SA.points_tracked[i], 3, cv::Scalar(0, 255, 0), -1, 8);
        }

        if (SA_contour_PO) {
            if (!SA.contour_PO.empty()) {
                if (addedNewModel & !modelReplaced)
                    cv::drawContours(img_visualization, SA.contour_PO, -1, cv::Scalar(0, 0, 255), 2);
                else if (addedNewModel & modelReplaced)
                    cv::drawContours(img_visualization, SA.contour_PO, -1, cv::Scalar(0, 255, 0), 2);
                else
                    cv::drawContours(img_visualization, SA.contour_PO, -1, cv::Scalar(255, 0, 0), 2);
                addedNewModel = false;
                modelReplaced = false;
            }
        }
        else {
            if (!SA.contour.empty()) {
                std::vector <std::vector<cv::Point>> contour(1);

                contour[0].resize(SA.contour.size());

                for (int i = 0; i < SA.contour.size(); i++) {
                    contour[0][i].x = SA.contour[i].x;
                    contour[0][i].y = SA.contour[i].y;
                }

                if (addedNewModel & !modelReplaced)
                    cv::drawContours(img_visualization, contour, -1, cv::Scalar(0, 0, 255), 4);
                else if (addedNewModel & modelReplaced)
                    cv::drawContours(img_visualization, contour, -1, cv::Scalar(0, 255, 0), 4);
                else
                    cv::drawContours(img_visualization, contour, -1, cv::Scalar(255, 0, 0), 4);
                addedNewModel = false;
                modelReplaced = false;
            }
        }
    }

    bool put_text = false;
    if (put_text) {
        std::string message;
        if (stage == STAGE_TRACK) {
            message = "Track";
        }
        else if (stage == STAGE_REINIT) {
            message = "Reinit";
        }
        else if (stage == STAGE_WAIT) {
            message = "Wait";
        }

        cv::putText(img_visualization, message, cv::Point(60, 60), cv::FONT_HERSHEY_PLAIN, 4.0, cv::Scalar(255, 0, 0), 3);

        if (stage == STAGE_WAIT) {
            create_color_bar(img_visualization, TrackL.TrackingParam.th_failure, 0.0);
        }
        else {
            create_color_bar(img_visualization, TrackL.TrackingParam.th_failure, SA.failure_jont_probability);
        }
    }
}

void enVisorsSys2::update_keyframe_data()
{
    SA.n_feat_keyframe = 0;
    for (int i = 0; i < SA.points_tracked.size(); i++) {
        if (SA.status_SA_points[i] == 1) {
            SA.n_feat_keyframe++;
        }
    }
    TrackL.TrackingParam.th_redetection = 0.1 * SA.n_feat_keyframe;
    SA.points_tracked_keyframe = SA.points_tracked;
    SA.contour_keyframe = SA.contour;
    SA.status_SA_points_keyframe = SA.status_SA_points;
    histL_hsv.copyTo(histL_hsv_keyframe);
    imgL_rect.copyTo(img_keyframe);
    SA.homographyT.deallocate();
    recon.R_e_prev.deallocate();
}

bool enVisorsSys2::redetect_SA()
{
    std::vector <Box2D<double>> boundingBoxes;
    boundingBoxes.clear();
    matchResults.clear();

    // find templates in the actual image with Hough Clustering
    recognition.processImageMessage(
        imgL_grey,
        boundingBoxes,
        SA.objectList,
        matchResults,
        sceneKeyPoints,
        img_hough_space);

    if (!matchResults.empty()) {
        // copy homography
        SA.homographyT.at<double>(0, 0) = matchResults[0]->homography.m_HomMat[0];
        SA.homographyT.at<double>(0, 1) = matchResults[0]->homography.m_HomMat[1];
        SA.homographyT.at<double>(0, 2) = matchResults[0]->homography.m_HomMat[2];
        SA.homographyT.at<double>(1, 0) = matchResults[0]->homography.m_HomMat[3];
        SA.homographyT.at<double>(1, 1) = matchResults[0]->homography.m_HomMat[4];
        SA.homographyT.at<double>(1, 2) = matchResults[0]->homography.m_HomMat[5];
        SA.homographyT.at<double>(2, 0) = matchResults[0]->homography.m_HomMat[6];
        SA.homographyT.at<double>(2, 1) = matchResults[0]->homography.m_HomMat[7];
        SA.homographyT.at<double>(2, 2) = matchResults[0]->homography.m_HomMat[8];

        TrackL.get_model(
            SA.objectList,
            SA.modelInfo,
            SA.info,
            matchResults[0]->objectName);
        cv::perspectiveTransform(
            SA.info.SA,
            SA.contour,
            SA.homographyT);

        // draw matches

        SA.pt1_HC.resize(matchResults[0]->stage3Matches.size());
        SA.pt2_HC.resize(matchResults[0]->stage3Matches.size());

        std::list<KeyPointMatch>::iterator keyPointIt;

        int i = 0;
        for (keyPointIt = matchResults[0]->stage3Matches.begin();
             keyPointIt != matchResults[0]->stage3Matches.end();
             keyPointIt++) {

            SA.pt1_HC[i].x = matchResults[0]->objectKeyPoints[(*keyPointIt).index2].x;
            SA.pt1_HC[i].y = matchResults[0]->objectKeyPoints[(*keyPointIt).index2].y;

            SA.pt2_HC[i].x = sceneKeyPoints[(*keyPointIt).index1].x;
            SA.pt2_HC[i].y = sceneKeyPoints[(*keyPointIt).index1].y;

            i++;
        }


        reinitilize_KLT();
        return true;
    }
    else {
        SA.homographyT.at<double>(0, 0) = 1.0;
        SA.homographyT.at<double>(0, 1) = 0.0;
        SA.homographyT.at<double>(0, 2) = 0.0;
        SA.homographyT.at<double>(1, 0) = 0.0;
        SA.homographyT.at<double>(1, 1) = 1.0;
        SA.homographyT.at<double>(1, 2) = 0.0;
        SA.homographyT.at<double>(2, 0) = 0.0;
        SA.homographyT.at<double>(2, 1) = 0.0;
        SA.homographyT.at<double>(2, 2) = 1.0;
        return false;
    }
}
void enVisorsSys2::redetect_KLT()
{
    cv::Mat temp_mask = 255 - SA.SA_mask;

    TrackL.redetectKLT(
        imgL_rect,
        imgL_grey,
        imgL_hsv,
        img_visualization,
        img_seg_prob,
        temp_mask,
        SA.points_detected,
        SA.pt2_HC,
        SA.points_tracked,
        SA.contour,
        SA.status_SA_points,
        SA.status_points_tracked,
        false,
        KLTTracker,
        HMatchingTracker,
        SA.n_feat_in_SA);

}

void enVisorsSys2::reinitilize_KLT()
{
    // create new template with previous lost safety area
    TrackL.create_safety_area_mask(
        imgL_grey,
        SA.contour,
        mask_specularity,
        SA.SA_mask);

    redetect_KLT();

    if (is_update_model) {
        SA.SURFkp_SA_model.clear();
        char modelName[8];
        sprintf(modelName, "%06d", frame_number);
        std::string modelNameStr = std::string(modelName);

        addedNewModel = TrackL.updateModel(
            learning,
            imgL_rect,
            imgL_grey,
            SA.contour,
            SA.SA_mask,
            SA.SURFkp_SA_model,
            modelNameStr,
            SA.bh_distance,
            SA.objectList,
            SA.modelInfo,
            modelReplaced);
    }

    // draw partial occlusion
    TrackL.find_SAContourn_partialOcclusion(
        imgL_rect,
        img_visualization,
        imgL_hsv,
        SA.contour,
        SA.contour_PO);
}

void enVisorsSys2::clear_variables()
{
    SA.SA_mask.setTo(0);
    SA.contour.clear();
    SA.contour_PO.clear();
    img_seg_prob.setTo(0);
}

void enVisorsSys2::publish_images()
{
    // image for tracking visualization
    fromMattoImage(img_visualization, &imgL_features_, sensor_msgs::image_encodings::RGB8);
    imgL_features_pub.publish(imgL_features_);

    // segmentation probability map
    fromMattoImage(img_seg_prob, &img_seg_prob_, sensor_msgs::image_encodings::RGB8);
    segmentation_probability_pub.publish(img_seg_prob_);

    // hough space
    fromMattoImage(img_hough_space, &img_hough_, sensor_msgs::image_encodings::RGB8);
    hough_space_pub.publish(img_hough_);

    fromMattoImage(img_matches, &img_matches_, sensor_msgs::image_encodings::RGB8);
    matches_pub.publish(img_matches_);

   fromMattoPointCloud2(recon.imgL_rgb_act, SAug.pointCloud_t, SAug.pointCloud_t_msgs);

    // 3d recon
    // super pixel
    fromMattoImage(recon.super_pixel.imgL_colored, &imgL_colored_, sensor_msgs::image_encodings::BGR8);
    imgL_contourn_pub.publish(imgL_colored_);

    imgL_census.setTo(0);
    imgR_census.setTo(0);
    StereoCorr.getImgLCENSUS(imgL_census);
    StereoCorr.getImgRCENSUS(imgR_census);
    imgL_census.convertTo(imgL_census, CV_8UC1, recon.img_scale);
    fromMattoImage(imgL_census, &imgL_census_, sensor_msgs::image_encodings::MONO8);
    imgL_census_pub.publish(imgL_census_);
    imgR_census.convertTo(imgR_census, CV_8UC1, recon.img_scale);
    fromMattoImage(imgR_census, &imgR_census_, sensor_msgs::image_encodings::MONO8);
    imgR_census_pub.publish(imgR_census_);

    cv::Mat disp8(recon.disparity.rows, recon.disparity.cols, CV_8UC1);
    recon.disparity.convertTo(disp8, CV_8UC1, recon.img_scale);
    fromMattoImage(disp8, &disparity_,
                   sensor_msgs::image_encodings::MONO8);
    disparity_pub.publish(disparity_);

    fromMattoImage(SAug.image_augmented, &img_augmented_, sensor_msgs::image_encodings::RGB8);
    augmented_image_pub.publish(img_augmented_);

    //Publish point cloud transformed
    pointCloud_transformed_pub.publish(SAug.pointCloud_t_msgs);

    //publish point cloud
    fromMattoImage(recon.pointCloud, &img_depth_, sensor_msgs::image_encodings::TYPE_32FC3);

    pointCloud_pub.publish(pointCloud_);
    pointCloud_filtered_pub.publish(pointCloud_filtered_);
    depth_pub.publish(img_depth_);
}

void enVisorsSys2::publish_rectifiedImages()
{
    cv::Mat tempL, tempR;
    tempL.create(imgL_rect_original.rows, imgL_rect_original.cols, CV_32FC1);
    tempL.setTo(0);
    tempR.create(imgL_rect_original.rows, imgL_rect_original.cols, CV_32FC1);
    tempR.setTo(0);

    // opencv scaling
    resize(imgL_rect, tempL, tempL.size(), 0, 0, cv::INTER_NEAREST);
    imgL_rect.create(imgL_rect_original.rows, imgL_rect_original.cols, CV_32FC1);
    tempL.copyTo(imgL_rect);

    resize(imgR_rect, tempR, tempR.size(), 0, 0, cv::INTER_NEAREST);
    imgR_rect.create(imgL_rect_original.rows, imgL_rect_original.cols, CV_32FC1);
    tempR.copyTo(imgR_rect);

    // republish rectified images
    // FIXME: If we use rectified images coming from ros::imgproc then we can skip this bit entirely
    fromMattoImage(imgL_rect, &imgL_rect_, sensor_msgs::image_encodings::RGB8);
    imgL_rect_pub.publish(imgL_rect_);

    fromMattoImage(imgR_rect, &imgR_rect_, sensor_msgs::image_encodings::RGB8);
    imgR_rect_pub.publish(imgR_rect_);
}

void enVisorsSys2::prepare_3D_reconstruction()
{

    imgL_rect_original.copyTo(recon.imgL_rgb_act);
    // copy super pixel input data
    imgL_rect_original.copyTo(recon.super_pixel.imgL_rect_GF);
    
    recon.super_pixel.SA_rect = boundingRect(SA.contour);
    
    // Ensure that rect is within bounds of `original` size image ?
    if (recon.super_pixel.SA_rect.x < 0) {
        recon.super_pixel.SA_rect.width = recon.super_pixel.SA_rect.width + recon.super_pixel.SA_rect.x;
        recon.super_pixel.SA_rect.x = 0;
    }

    if (recon.super_pixel.SA_rect.y < 0) {
        recon.super_pixel.SA_rect.height = recon.super_pixel.SA_rect.height + recon.super_pixel.SA_rect.y;
        recon.super_pixel.SA_rect.y = 0;
    }

    if (recon.super_pixel.SA_rect.y > imgL_rect_original.rows) {
        recon.super_pixel.SA_rect.height = 0;
        recon.super_pixel.SA_rect.y = imgL_rect_original.rows;
    }

    if (recon.super_pixel.SA_rect.x > imgL_rect_original.cols) {
        recon.super_pixel.SA_rect.width = 0;
        recon.super_pixel.SA_rect.x = imgL_rect_original.cols;
    }

    if (recon.super_pixel.SA_rect.y + recon.super_pixel.SA_rect.height > imgL_rect_original.rows) {
        // FIXME: This line looks like a bug, should be:
        // recon.super_pixel.SA_rect.height = (recon.super_pixel.SA_rect.y + recon.super_pixel.SA_rect+height) - imgL_rect_original.rows;
        // ?
        recon.super_pixel.SA_rect.height = imgL_rect_original.rows - recon.super_pixel.SA_rect.y;
        
        // FIXME: This does nothing, copy&paste leftover ?
        recon.super_pixel.SA_rect.y;
    }

    if (recon.super_pixel.SA_rect.x + recon.super_pixel.SA_rect.width > imgL_rect_original.cols) {
        recon.super_pixel.SA_rect.width = imgL_rect_original.cols - recon.super_pixel.SA_rect.x;
        recon.super_pixel.SA_rect.x;
    }

    // copy 3d recon input data
    imgL_grey.copyTo(recon.imgL_gray);
    imgR_grey.copyTo(recon.imgR_gray);

    mask_specularity.copyTo(recon.mask_specularity);
    
    // FIXME: This overwrites the previous adjustment, is this intentional ?
    if (is3Drecon_FullImage) {
        // FIXME: Why get the bounding box of the full image if previously we were getting the BB of the SA contour
        recon.super_pixel.SA_rect = boundingRect(recon.imgL_gray);
    }
}

void enVisorsSys2::compute_super_pixel()
{
    std::cerr << "[computer_superPixel] => State: Started\n";
    
    // scale SA to original image dimension
    recon.super_pixel.SA_rect_original = recon.super_pixel.SA_rect;

    if (img_subSampling) {
        float scale = (float) imgL_rect_original.cols / (float) imgL_rect.cols;
        recon.super_pixel.SA_rect_original.x *= scale;
        recon.super_pixel.SA_rect_original.y *= scale;
        recon.super_pixel.SA_rect_original.width *= scale;
        recon.super_pixel.SA_rect_original.height *= scale;
    }

    // Apply the new ROI to the image
    recon.super_pixel.imgL_rect_GF = recon.super_pixel.imgL_rect_GF(recon.super_pixel.SA_rect_original);

    // Blur the image if required
    if (recon.super_pixel.isGaussian_SP) {
        cv::GaussianBlur(
            recon.super_pixel.imgL_rect_GF, recon.super_pixel.imgL_rect_GF, cv::Size(7, 7), 3, 3);
    }

    // Compute super pixels
    SLICSuperpixel slic(recon.super_pixel.imgL_rect_GF,
                        recon.super_pixel.nSuperPixels,
                        recon.super_pixel.m,
                        recon.super_pixel.max_iteration
        /*400, 10, 10 SLIC.nr_superpixels_prop*/ );
    slic.generateSuperPixels();

    recon.super_pixel.contours = slic.getContours();

    recon.SP.contours = slic.getContours();

#ifdef VISUALIZATION
    // Create a pretty image with each super pixel coloured differently                                                                                                           /* Recolor based on the average cluster color */
	recon.super_pixel.imgL_colored = slic.recolor();
	/* Draw the contours bordering the clusters */
	for( int i = 0; i< recon.super_pixel.contours.size(); i++ ){
		recon.super_pixel.imgL_colored.at<cv::Vec3b>( recon.super_pixel.contours[i].y, recon.super_pixel.contours[i].x ) =
				cv::Vec3b(255, 0, 0);
	}
#endif

    cv::Mat temp_super_pixel;

    temp_super_pixel = slic.getClustersIndex();

    assert(!imgL_rect_original.empty());
    recon.super_pixel.imgL_superPixelsIndex.create(imgL_rect_original.rows, imgL_rect_original.cols, CV_16UC1);
    recon.super_pixel.imgL_superPixelsIndex.setTo(LABEL_NULL); // LABEL_NULL = #define 1000

    // For pixels in the ROI of the SuperPixel do ?
    for (int i = recon.super_pixel.SA_rect_original.y;
         i < recon.super_pixel.SA_rect_original.y + recon.super_pixel.SA_rect_original.height;
         i++
        )
    {
        for (int j = recon.super_pixel.SA_rect_original.x;
             j < recon.super_pixel.SA_rect_original.x + recon.super_pixel.SA_rect_original.width;
             j++
            )
        {
            recon.super_pixel.imgL_superPixelsIndex.at<ushort>(i, j) =
                temp_super_pixel.at<ushort>(i - recon.super_pixel.SA_rect_original.y, j - recon.super_pixel.SA_rect_original.x);
        }
    }


    for (int i = 0; i < recon.super_pixel.contours.size(); i++) {
        recon.super_pixel.contours[i].x = recon.super_pixel.contours[i].x + recon.super_pixel.SA_rect_original.x;
        recon.super_pixel.contours[i].y = recon.super_pixel.contours[i].y + recon.super_pixel.SA_rect_original.y;
    }
    
    std::cerr << "[computer_superPixel] => State: Done\n";
}

void enVisorsSys2::compute_3D_reconstruction()
{
    // FIXME: What are these masks for ?
    SA.SARect_mask_left.create(recon.imgL_gray.rows, recon.imgL_gray.cols, CV_8UC1);
    SA.SARect_mask_left.setTo(0);
    SA.SARect_mask_right.create(recon.imgL_gray.rows, recon.imgL_gray.cols, CV_8UC1);
    SA.SARect_mask_right.setTo(0);
    SA.SARect_mask_right_to_left.create(recon.imgL_gray.rows, recon.imgL_gray.cols, CV_8UC1);
    SA.SARect_mask_right_to_left.setTo(0);

    // FIXME: What are j_min_r, j_max_r ?
    // Extend right image to account for disparity
    int j_min_r = recon.super_pixel.SA_rect.x - recon.numberOfDisparities;
    int j_max_r = recon.super_pixel.SA_rect.x + recon.super_pixel.SA_rect.width - recon.minDisparities;
    
    // compute disparity both ways
    int j_min_rtl = j_min_r + recon.minDisparities;
    int j_max_rtl = j_max_r + recon.numberOfDisparities;

    if (j_min_r < 0)
        j_min_r = 0;

    if (j_max_r > recon.imgL_gray.cols)
        j_max_r = recon.imgL_gray.cols;

    if (j_min_rtl < 0)
        j_min_rtl = 0;

    if (j_max_rtl > recon.imgL_gray.cols)
        j_max_rtl = recon.imgL_gray.cols;

    // Probably not a bug
    // Considering the entire image
    j_min_rtl = 0;
    j_max_rtl = recon.imgL_gray.cols;

    
    // Loop over pixels includes in the safety area bounds to create SA Masks
    for (int i = recon.super_pixel.SA_rect.y; i < recon.super_pixel.SA_rect.y + recon.super_pixel.SA_rect.height; i++) {
        for (int j = recon.super_pixel.SA_rect.x; j < recon.super_pixel.SA_rect.x + recon.super_pixel.SA_rect.width; j++) {
            SA.SARect_mask_left.at<uchar>(i, j) = 255;
        }
        
        // FIXME: What are we doing ?
        for (int j = j_min_r; j < j_max_r; j++) {
            SA.SARect_mask_right.at<uchar>(i, j) = 255;
        }
        
        for (int j = j_min_rtl; j < j_max_rtl; j++) {
            SA.SARect_mask_right_to_left.at<uchar>(i, j) = 255;
        }
    }

    StereoCorr.processCENSUS(
        recon.imgL_gray,
        recon.imgR_gray,
        /*recon.super_pixel.imgL_superPixelsIndex,*/
        SA.SARect_mask_left,
        SA.SARect_mask_right,
        SA.SARect_mask_right_to_left,
        recon.super_pixel.SA_rect,
        /*recon.disparity,*/
        recon.mask_specularity);
#ifdef MULTITHREAD
    pthread_join(threadSuperPixel, NULL);
#endif
    
    assert(!recon.super_pixel.imgL_superPixelsIndex.empty());
    StereoCorr.postProcess(
        recon.super_pixel.imgL_superPixelsIndex,
        recon.disparity,
        &TrackL.SegModel,
        imgL_hsv,
        recon.super_pixel.contours,
        recon.super_pixel.SA_rect_original);


    StereoRecon.process(recon.disparity, recon.pointCloud);
    fromMattoPointCloud2(recon.imgL_rgb_act, recon.pointCloud, pointCloud_);
}

void enVisorsSys2::filter_point_cloud()
{
    fpc.setPointCloud(pointCloud_);
    fpc.filter();
    fpc.get_filtered_pointCloud(pointCloud_filtered_);
}

void enVisorsSys2::update_point_cloud()
{
    if (recon.pointCloud.cols == 0 ||
        recon.pointCloud.rows == 0) {
        std::cout << "Empty point cloud" << std::endl;
        return;
    }

    // apply homography to previous point cloud using directly on the Mat
    // Calculate the approximate transformation from the homography
    // considering that the translation in z and the rotation in x, y is zero

    recon.R_e.create(3, 3, CV_64FC1);
    recon.t_e.create(3, 1, CV_64FC1);

    double angle = 0.0;
    double scale = 1.0;
    double dx;
    double dy;
    double averagez = 0.0;
    int nPoints = 0;
    double fx = Rect.cameraMatrixL.at<double>(0, 0);
    double fy = Rect.cameraMatrixL.at<double>(1, 1);
    double cx = Rect.cameraMatrixL.at<double>(0, 2);
    double cy = Rect.cameraMatrixL.at<double>(1, 2);

    // Calculate average z
    for (int x = 0; x < recon.pointCloud.cols; x++) {
        for (int y = 0; y < recon.pointCloud.rows; y++) {
            if (recon.pointCloud.at<cv::Point3f>(y, x).z != -1.0 &&
                recon.pointCloud.at<cv::Point3f>(y, x).z == recon.pointCloud.at<cv::Point3f>(y, x).z) {
                averagez += recon.pointCloud.at<cv::Point3f>(y, x).z;
                nPoints++;
            }
        }
    }

    if (nPoints == 0)
        return;

    averagez /= (double) nPoints;

    // Calculate scale
    double scalex = sqrt(
        SA.homographyT.at<double>(0, 0) * SA.homographyT.at<double>(0, 0) +
            SA.homographyT.at<double>(1, 0) * SA.homographyT.at<double>(1, 0));
    double scaley = sqrt(
        SA.homographyT.at<double>(0, 1) * SA.homographyT.at<double>(0, 1) +
            SA.homographyT.at<double>(1, 1) * SA.homographyT.at<double>(1, 1));
    scale = 0.5 / (scalex + scaley);

    // Calculate angle
    double anglex = atan2(SA.homographyT.at<double>(1, 0), SA.homographyT.at<double>(0, 0));
    double angley = -atan2(SA.homographyT.at<double>(0, 1), SA.homographyT.at<double>(1, 1));
    angle = (anglex + angley) * 0.5;

    // Calculate displacement
    dx = (SA.homographyT.at<double>(0, 2) - cx) * averagez / fx * scale;
    dy = (SA.homographyT.at<double>(1, 2) - cy) * averagez / fy * scale;

    // Calculate matrices
    recon.R_e.at<double>(0, 0) = cos(angle);//SA.homographyT.at<double>(0, 0) / scalex;//
    recon.R_e.at<double>(0, 1) = -sin(angle);//SA.homographyT.at<double>(0, 1) / scalex;//
    recon.R_e.at<double>(0, 2) = 0.0;
    recon.R_e.at<double>(1, 0) = sin(angle);//SA.homographyT.at<double>(1, 0) / scaley;//
    recon.R_e.at<double>(1, 1) = cos(angle);//SA.homographyT.at<double>(1, 1) / scaley;//
    recon.R_e.at<double>(1, 2) = 0.0;
    recon.R_e.at<double>(2, 0) = 0.0;
    recon.R_e.at<double>(2, 1) = 0.0;
    recon.R_e.at<double>(2, 2) = 1.0;

    recon.t_e.at<double>(0, 0) = dx;
    recon.t_e.at<double>(1, 0) = dy;
    recon.t_e.at<double>(2, 0) = 0.0;

    if (recon.R_e_prev.empty()) {
        recon.R_e.copyTo(recon.R_e_rel);
        recon.t_e.copyTo(recon.t_e_rel);
    }
    else {
        recon.R_e_rel = recon.R_e_prev.inv() * recon.R_e;
        recon.t_e_rel = recon.R_e_prev.inv() * (recon.t_e - recon.t_e_prev);
    }

    cv::Point3f point1, point2;
    for (int x = 0; x < recon.pointCloud.cols; x++) {
        for (int y = 0; y < recon.pointCloud.rows; y++) {
            point1 = recon.pointCloud.at<cv::Point3f>(y, x);

            point2.x =
                recon.t_e_rel.at<double>(0, 0) + //point1.x;
                    recon.R_e_rel.at<double>(0, 0) * point1.x +
                    recon.R_e_rel.at<double>(0, 1) * point1.y +
                    recon.R_e_rel.at<double>(0, 2) * point1.z;
            point2.y =
                recon.t_e_rel.at<double>(0, 1) + //point1.y;
                    recon.R_e_rel.at<double>(1, 0) * point1.x +
                    recon.R_e_rel.at<double>(1, 1) * point1.y +
                    recon.R_e_rel.at<double>(1, 2) * point1.z;
            point2.z =
                recon.t_e_rel.at<double>(0, 2) + //point1.z;
                    recon.R_e_rel.at<double>(2, 0) * point1.x +
                    recon.R_e_rel.at<double>(2, 1) * point1.y +
                    recon.R_e_rel.at<double>(2, 2) * point1.z;

            recon.pointCloud.at<cv::Point3f>(y, x) = point2;
        }
    }

    fromMattoPointCloud2(recon.imgL_rgb_act, recon.pointCloud, pointCloud_);
}

void enVisorsSys2::create_fake_pointCloud(cv::Mat &img_pc_fake)
{
    cv::Mat img_pc_fake_out;

    //image size
    int w = imgL.cols;
    int h = imgL.rows;
    float t;

    cv::Vec3f temp{0.0f, 0.0f, 0.0f};

    img_pc_fake.create(h, w, CV_32FC3);
    img_pc_fake.setTo(temp);
    img_pc_fake_out.create(h, w, CV_8UC3);

    // plane equation
    // a*x + b*y + c*z +d = 0;
    // versor orthogonal to the plane
    float v_x = 0;
    float v_y = cos(MYPI / 6);
    float v_z = sin(MYPI / 6);
    // point Xp
    float x_p = 20;
    float y_p = 35;
    float z_p = 100;

    //compute plane coefficient
    float d = -(v_x * x_p + v_y * y_p + v_z * z_p);

    float alpha_x = 1 / 860.0;
    float alpha_y = 1 / 860.0;

    for (int j = 0; j < imgL.cols; j++) {
        for (int i = 0; i < imgL.rows; i++) {
            t = -d / (v_x * alpha_x * (i - imgL.cols / 2) + v_y * alpha_y * (j - imgL.cols / 2) + v_z);
            img_pc_fake.at<cv::Vec3f>(i, j)[0] = alpha_x * (i - imgL.rows / 2) * t;
            img_pc_fake.at<cv::Vec3f>(i, j)[1] = alpha_y * (j - imgL.cols / 2) * t;
            img_pc_fake.at<cv::Vec3f>(i, j)[2] = t;

        }
    }

    cv::namedWindow("fake_PC", CV_NORMAL);
    img_pc_fake.convertTo(img_pc_fake_out, 8);
    cv::imshow("fake_PC", img_pc_fake_out);
}

// multithread function
void *enVisorsSys2::run_disparity(void *data)
{
    enVisorsSys2 *enV2 = (enVisorsSys2 *) data;
#ifdef MULTITHREAD
    pthread_create(&enV2->threadSuperPixel, NULL, enVisorsSys2::run_super_pixel, (void*)enV2);
#else
    enV2->compute_super_pixel();
#endif

    enV2->compute_3D_reconstruction();
    enV2->disparityCompleted = true;
    return NULL;
}

void *enVisorsSys2::run_super_pixel(void *data)
{
    enVisorsSys2 *enV2 = (enVisorsSys2 *) data;
    enV2->compute_super_pixel();

    return NULL;
}

//robot tip pose callback
void enVisorsSys2::robotPose_callback(const geometry_msgs::PosePtr &r)
{
    SAug.robotPose1.orientation = r->orientation;
    SAug.robotPose1.position = r->position;
}

//robot tip pose callback
void enVisorsSys2::robotPose1Stamped_callback(const geometry_msgs::PoseStampedPtr &r1)
{
    SAug.robotPose1.orientation = r1->pose.orientation;
    SAug.robotPose1.position = r1->pose.position;
}

void enVisorsSys2::robotPose2Stamped_callback(const geometry_msgs::PoseStampedPtr &r2)
{
    SAug.robotPose2.orientation = r2->pose.orientation;
    SAug.robotPose2.position = r2->pose.position;

    KDL::Vector pc_points_pre;
    KDL::Vector pc_points_post;
    pc_points_pre[0] = SAug.robotPose2.position.x;
    pc_points_pre[1] = SAug.robotPose2.position.y;
    pc_points_pre[2] = SAug.robotPose2.position.z;

    // read robot2 robot2 calibration
    KDL::Frame T_1_2;
    cv::Mat R, t;
    T_1_2 = KDL::Frame::Identity();

    readExtrisicParamsCamera(
        std::string(CONFIG_FILE_FOLDER) + "camera/" + "/davinci" + "/Robot_Tracker_Transform.xml",
        R,
        t);
    // convert calibration to KDL
    fromMattoKDL(
        R,
        t,
        T_1_2);
    // transform points to PSM1 reference frame
    pc_points_post = T_1_2.Inverse() * pc_points_pre;

    SAug.robotPose2.position.x = pc_points_post[0];
    SAug.robotPose2.position.y = pc_points_post[1];
    SAug.robotPose2.position.z = pc_points_post[2];

    T_PSM2_ft.setOrigin(tf::Vector3(SAug.robotPose2.position.x, SAug.robotPose2.position.y,
                                          SAug.robotPose2.position.z));
    tf::Quaternion quat;
    tf::quaternionMsgToTF(SAug.robotPose2.orientation, quat);

    T_PSM2_ft.setRotation(quat);

    //	// publish tf tranform
    broadcasterT_PSM2_ft
        .sendTransform(tf::StampedTransform(T_PSM2_ft, ros::Time::now(), "/PSM1remote_center", "/PSM2_ee"));
}

void enVisorsSys2::CameraPoseStamped_callback(const geometry_msgs::PoseStampedPtr &t_s)
{
    // conver to Pose
    SAug.cameraPose.orientation = t_s->pose.orientation;
    SAug.cameraPose.position = t_s->pose.position;
}

// Convert point cloud points in robot reference frame
void enVisorsSys2::point_cloud_to_robot(
    sensor_msgs::PointCloud2 &out,
    pcl::PointCloud <pcl::PointXYZRGB> &pc_out,
    const Eigen::Affine3d &transformation)
{
    if (pointCloud_.width == 0 || pointCloud_.height == 0)
        return;

    pcl::PointCloud <pcl::PointXYZRGB> pc_in;

    // pc_in from sensor_msgs to pcl
    int size = pointCloud_.height * pointCloud_.width;
    int offset = pointCloud_.point_step * size / 3;
    float *points = (float *) &pointCloud_.data[offset];

    ros2pcl::fromROSMsg(pointCloud_, pc_in);
    // Executing the transformation
    // You can either apply transform_1 or transform_2; they are the same
    pcl::transformPointCloud(pc_in, pc_out, transformation);

    pcl::PointCloud<pcl::PointXYZRGB>::iterator pc_in_it;
    pcl::PointCloud<pcl::PointXYZRGB>::iterator pc_out_it;

    pc_in_it = pc_in.begin();
    pc_out_it = pc_out.begin();

    for (pc_in_it = pc_in.begin(); pc_in_it != pc_in.end(); pc_in_it++) {
        if (pc_in_it->x == -1 || pc_in_it->y == -1 || pc_in_it->z == -1) {
            pc_out_it->x = -1;
            pc_out_it->y = -1;
            pc_out_it->z = -1;
        }

        pc_out_it++;
    }

    pcl2ros::toROSMsg(pc_out, out);

    points = (float *) &out.data[offset];

}

void enVisorsSys2::do_augmentation()
{
    cv::Mat img_augmented_rescaled;
    KDL::Frame cam_pose;
    KDL::Frame R1_kdl;
    KDL::Frame T_rectified;

    KDL::Vector pc_points_t_pre;
    KDL::Vector pc_points_t_post;
    KDL::Vector pc_points;
    cam_pose.Identity();

    bool simple_BB = false;

    // convert Camera pose to Eigen
    tf::poseMsgToEigen(SAug.cameraPose, SAug.camera_transform);
    tf::poseMsgToKDL(SAug.cameraPose, cam_pose);
    std::cout << SAug.cameraPose << std::endl;

    R1_kdl.M = KDL::Rotation(
        R1.at<double>(0, 0), R1.at<double>(0, 1), R1.at<double>(0, 2),
        R1.at<double>(1, 0), R1.at<double>(1, 1), R1.at<double>(1, 2),
        R1.at<double>(2, 0), R1.at<double>(2, 1), R1.at<double>(2, 2));
    R1_kdl.p[0] = 0.0;
    R1_kdl.p[1] = 0.0;
    R1_kdl.p[2] = 0.0;


    // Transform the point cloud and keep only the 3D points inside the SA
    cv::Point3f point1;
    cv::Point3f point2;
    envisors_2::bb_coords bb_coordinate;
    float coord[6];

    SAug.pointCloud_t.create(recon.pointCloud.rows, recon.pointCloud.cols, CV_32FC3);

    for (int x = 0; x < recon.pointCloud.cols; x++) {
        for (int y = 0; y < recon.pointCloud.rows; y++) {
            point1 = recon.pointCloud.at<cv::Point3f>(y, x);
            pc_points[0] = recon.pointCloud.at<cv::Point3f>(y, x).x;
            pc_points[1] = recon.pointCloud.at<cv::Point3f>(y, x).y;
            pc_points[2] = recon.pointCloud.at<cv::Point3f>(y, x).z;

            if (point1.x != -1 && point1.y != -1 && point1.z != -1) {
                T_rectified = cam_pose; //* R1_kdl;
                pc_points_t_post = T_rectified * pc_points;

                SAug.pointCloud_t.at<cv::Point3f>(y, x).x = pc_points_t_post[0];
                SAug.pointCloud_t.at<cv::Point3f>(y, x).y = pc_points_t_post[1];
                SAug.pointCloud_t.at<cv::Point3f>(y, x).z = pc_points_t_post[2];
            }
            else {
                SAug.pointCloud_t.at<cv::Point3f>(y, x) = cv::Point3f(-1, -1, -1);
            }
        }
    }

    // convert cv::Mat to PointCloud2
    if (!SAug.pointCloud_t.empty() && !imgL_rect.empty()) {
        SAug.d_min = 0.002;
        SAug.d_max = 0.05;

        // compute bounding box
        SAug.ellips.set_pointCloud(SAug.pointCloud_t);
        SAug.ellips.set_bounding_box(coord);

        if (simple_BB) {
            SAug.distance1 = SAug.ellips.is_inside(
                SAug.robotPose1.position.x,
                SAug.robotPose1.position.y,
                SAug.robotPose1.position.z);

            SAug.distance2 = SAug.ellips.is_inside(
                SAug.robotPose2.position.x,
                SAug.robotPose2.position.y,
                SAug.robotPose2.position.z);
        }
        else {

            std::vector<int> k_indices;
            std::vector<float> k_sqr_distance;
            float resolution = 0.005f;
            double radius = SAug.d_max;
            int th_Npoints = 10;
            pcl::PointXYZRGB query_pt1;
            pcl::PointXYZRGB query_pt2;
            int n_close_pts;

            pcl::PointCloud <pcl::PointXYZRGB> *pc_in = new pcl::PointCloud<pcl::PointXYZRGB>;
            pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud(pc_in);
            pcl::octree::OctreePointCloudSearch <pcl::PointXYZRGB> octree_search(resolution);

            // convert the point cloud from Mat to to pcl
            fromMattoPointCloud2(recon.imgL_rgb_act, SAug.pointCloud_t, SAug.pointCloud_t_msgs);
            ros2pcl::fromROSMsg(SAug.pointCloud_t_msgs, *pc_in);

            // initialize octree
            // initialize octree search
            octree_search.setInputCloud(cloud);
            octree_search.addPointsFromInputCloud();

            // iterate to find the instruments close to the point cloud
            query_pt1.x = SAug.robotPose1.position.x;
            query_pt1.y = SAug.robotPose1.position.y;
            query_pt1.z = SAug.robotPose1.position.z;

            query_pt2.x = SAug.robotPose2.position.x;
            query_pt2.y = SAug.robotPose2.position.y;
            query_pt2.z = SAug.robotPose2.position.z;

            // distance from intruments1
            n_close_pts = octree_search.radiusSearch(query_pt1, radius, k_indices, k_sqr_distance);

            if (n_close_pts == 0) {
                SAug.distance1 = radius;
            }
            else {
                // Find minimum distance
                float minDist = 1E6;

                for (unsigned int i = 0; i < k_sqr_distance.size(); i++) {
                    if (k_sqr_distance[i] < minDist)
                        minDist = k_sqr_distance[i];
                }

                SAug.distance1 = sqrtf(minDist);
            }

            // distance from intruments2
            n_close_pts = octree_search.radiusSearch(query_pt2, radius, k_indices, k_sqr_distance);

            if (n_close_pts == 0) {
                SAug.distance2 = radius;
            }
            else {
                // Find minimum distance
                float minDist = 1E6;

                for (unsigned int i = 0; i < k_sqr_distance.size(); i++) {
                    if (k_sqr_distance[i] < minDist)
                        minDist = k_sqr_distance[i];
                }

                SAug.distance2 = sqrtf(minDist);
            }

            pcl2ros::toROSMsg(*pc_in, SAug.pointCloud_t_msgs);
        }

        if (SAug.distance1 == 0.0 || SAug.distance2 == 0.0) {
            SAug.distance_to_pc = 0.0;
        }
        else if (SAug.distance1 <= -0.99 || SAug.distance2 <= -0.99) {
            SAug.distance_to_pc = -1.0;
        }
        else {
            if (SAug.distance1 < SAug.distance2)
                SAug.distance_to_pc = SAug.distance1;
            else
                SAug.distance_to_pc = SAug.distance2;
        }

        if (SAug.distance_to_pc == -1.0) {
            SAug.alpha = 0.0;
        }
        else if (SAug.distance_to_pc < SAug.d_min) {
            SAug.alpha = 1.0 * M_PI;
        }
        else if (SAug.distance_to_pc > SAug.d_max) {
            SAug.alpha = 0.0;
        }
        else {
            SAug.alpha =
                (SAug.distance_to_pc - SAug.d_max) / (SAug.d_min - SAug.d_max) * M_PI;
        }

        // draw gauge
        imgL_rect.copyTo(SAug.image_augmented);
        // rescale the image
        img_augmented_rescaled.create(imgL_rect.rows, imgL_rect.cols, CV_32FC1);
        img_augmented_rescaled.setTo(0);
        // opencv scaling
        resize(SAug.image_augmented,
               img_augmented_rescaled,
               img_augmented_rescaled.size(),
               0,
               0,
               cv::INTER_NEAREST);
        SAug.image_augmented.create(imgL_rect_original.rows, imgL_rect_original.cols, CV_32FC1);
        img_augmented_rescaled.copyTo(SAug.image_augmented);

        draw_gauge(SAug.image_augmented,
                   cv::Point(SAug.image_augmented.cols - 110, 10),
                   SAug.gauge_img, //510,10
                   SAug.gauge_mask,
                   SAug.alpha);
    }

    // define bb
    // publish the bounding box coordinates
    bb_coordinate.bb_coords[0] = coord[0];
    bb_coordinate.bb_coords[1] = coord[1];
    bb_coordinate.bb_coords[2] = coord[2];
    bb_coordinate.bb_coords[3] = coord[3];
    bb_coordinate.bb_coords[4] = coord[4];
    bb_coordinate.bb_coords[5] = coord[5];
    bb_coords_pub.publish(bb_coordinate);
    SAug.ellips.publish_bounding_box();

    // publish intruments distance from pc
    bb_distance_.data = SAug.distance_to_pc;
    bb_distance_pub.publish(bb_distance_);
}

void enVisorsSys2::save_computational_times()
{
    char varName[32];

    if (!time_file.is_open()) {
        std::cout << "Failed to open " << std::endl;
        return;
    }
    else {
        time_file << (float) read_images_time << ",";
        time_file << (float) define_sa_time << ",";
        time_file << (float) klt_tracker_time << ",";
        time_file << (float) hough_matching_time << ",";
        time_file << (float) stereo_recon_do_time << ",";
        time_file << (float) stereo_recon_up_time << ",";
        time_file << (float) safety_aug_time << ",";
        time_file << recon.super_pixel.SA_rect.width << ",";
        time_file << recon.super_pixel.SA_rect.height << "\n";
        time_file.flush();
    }
}

void enVisorsSys2::save_images()
{
    char frame_number_char[32];
    sprintf(frame_number_char, "%06d", frame_seq);
    cv::imwrite(std::string(CONFIG_FILE_FOLDER) + "images/" + "_Lrect_" + frame_number_char + ".jpg", imgL_rect);
    cv::imwrite(std::string(CONFIG_FILE_FOLDER) + "images/" + "_Rrect_" + frame_number_char + ".jpg", imgR_rect);
    cv::imwrite(std::string(CONFIG_FILE_FOLDER) + "images/" + "_tracking_" + frame_number_char + ".jpg",
                img_visualization);
    cv::imwrite(std::string(CONFIG_FILE_FOLDER) + "images/" + "_segmentation_" + frame_number_char + ".jpg",
                img_seg_prob);
    cv::imwrite(std::string(CONFIG_FILE_FOLDER) + "images/" + "_template_" + frame_number_char + ".jpg",
                SA_template[0]);
    cv::imwrite(std::string(CONFIG_FILE_FOLDER) + "images/" + "_hough_" + frame_number_char + ".jpg",
                img_hough_space);

}

void enVisorsSys2::evaluate_tracking()
{
    char varName[32];

    if (!EvalTrack.fsTK.isOpened()) {
        std::cout << "Failed to open " << std::endl;
        return;
    }
    else {
        // convert in cv::Mat
        cv::Mat SAmat;
        SAmat.create(SA.contour.size(), 2, CV_32FC1);
        SAmat.setTo(0);
        if (!SA.contour.empty() && !wait_for_new_match) {
            for (int i = 0; i < SAmat.rows; i++) {
                SAmat.at<float>(i, 0) = SA.contour[i].x;
                SAmat.at<float>(i, 1) = SA.contour[i].y;
            }
        }
        else {
            SA.contour.resize(1);
            SA.contour[0].x = -1;
            SA.contour[0].y = -1;
            SAmat.at<float>(0, 0) = -1;
            SAmat.at<float>(0, 1) = -1;
        }


        sprintf(varName, "TK%06d", EvalTrack.totalFramesNumber);

        EvalTrack.fsTK << varName << SAmat;

        char frame_number_char[32];
        sprintf(frame_number_char, "%06d", EvalTrack.totalFramesNumber++);
        std::cout << EvalTrack.path_img + std::string(frame_number_char) + "_tracking.jpg" << std::endl;
        cv::imwrite(EvalTrack.path_img + std::string(frame_number_char) + "_tracking.jpg",
                    img_visualization);
        cv::imwrite(EvalTrack.path_img + std::string(frame_number_char) + "_matches.jpg", img_matches);
        //		cv::imwrite(EvalTrack.path_img + std::string(frame_number_char) +"_optical_flow.jpg", imgL_opt_flow);
        cv::imwrite(EvalTrack.path_img + std::string(frame_number_char) + "_segmentation.jpg",
                    img_seg_prob);
        cv::imwrite(EvalTrack.path_img + std::string(frame_number_char) + "_template.jpg", SA_template[0]);
        cv::imwrite(EvalTrack.path_img + std::string(frame_number_char) + "_hough.jpg", img_hough_space);

    }

    // write TK_partialOcclusion for frame n
    if (!EvalTrack.fsTK_partialOcclusion.isOpened()) {
        std::cout << "Failed to open " << std::endl;
        return;
    }
    else {
        int size_contour = 0;
        int n_cont = 0;
        // convert in cv::Mat
        if (!SA.contour_PO.empty()) {
            size_contour = SA.contour_PO.size();
            for (int j = 0; j < size_contour; j++) {
                double area_contour = 0;
                // compute area inside the contourn
                area_contour = contourArea(SA.contour_PO[j]);
                if (area_contour > 100) {
                    n_cont++;
                }
            }
        }


        EvalTrack.NContours.push_back(n_cont);
        EvalTrack.fsTK_NContour.open(EvalTrack.file_string_nContour, cv::FileStorage::WRITE);
        EvalTrack.fsTK_NContour << "NContour" << cv::Mat(EvalTrack.NContours);
        EvalTrack.fsTK_NContour.release();

        if (wait_for_new_match || n_cont == 0) {
            cv::Mat TKPOmat;
            TKPOmat.create(1, 2, CV_32FC1);
            TKPOmat.setTo(0);
            TKPOmat.at<float>(0, 0) = -1;
            TKPOmat.at<float>(0, 1) = -1;
            sprintf(varName, "TK_PO%06d", EvalTrack.totalFramesNumber);
            EvalTrack.fsTK_partialOcclusion << varName << TKPOmat;
        }
        else {
            std::cout << "n_cont: " << n_cont << std::endl;

            cv::Mat TKPOmat[n_cont];
            int charact_count = 0;
            for (int j = 0; j < size_contour; j++) {
                double area_contour = 0;

                if (!SA.contour_PO[j].empty())
                    area_contour = contourArea(SA.contour_PO[j]);


                if (area_contour > 100) {

                    TKPOmat[charact_count].create(SA.contour_PO[j].size(), 2, CV_32FC1);
                    TKPOmat[charact_count].setTo(0);


                    for (int i = 0; i < TKPOmat[charact_count].rows; i++) {
                        TKPOmat[charact_count].at<float>(i, 0) = SA.contour_PO[j][i].x;
                        TKPOmat[charact_count].at<float>(i, 1) = SA.contour_PO[j][i].y;
                        //									std::cout << "[" << enV.new_contour[j][i].x << " , "
                        //											<< enV.new_contour[j][i].y << "]" << std::endl;

                    }

                    if (n_cont > 1) {
                        sprintf(varName, "TK_PO%06d%c", EvalTrack.totalFramesNumber, 'a' + charact_count);
                        EvalTrack.fsTK_partialOcclusion << varName << TKPOmat[charact_count];
                        charact_count++;
                    }
                    else {
                        sprintf(varName, "TK_PO%06d", EvalTrack.totalFramesNumber);
                        EvalTrack.fsTK_partialOcclusion << varName << TKPOmat[charact_count];
                    }

                }
            }
        }

    }

    EvalTrack.totalFramesNumber++;
}

void enVisorsSys2::evaluate_3Dreconstruction()
{

}
