/*
 * main.cpp
 *
 *  Created on: Sep 21, 2016
 *      Author: veronica
 */

#include "enVisors2.hpp"


int main(int argc, char **argv)
{
    
    // ROS Initiliazation
    ros::init(argc, argv, "enVisors2");
    
    // local variable declaration:
    
    enVisorsSys2 enV2;
    enV2.status = INIT_PARAMETERS;
    enV2.recon.img_scalefactor = 1.0f;
    bool publish = true;
    
    // dynamic reconfigure
    dynamic_reconfigure::Server<envisors_2::envisors2Config> server;
    dynamic_reconfigure::Server<envisors_2::envisors2Config>::CallbackType f;
    
    f = boost::bind(&enVisorsSys2::callback_dynamic_reconfigure, &enV2, _1, _2);
    server.setCallback(f);

//        cv::startWindowThread();
    // Create a window in which to show the camera image that we can use to define the safety
    // area with the mouse. This is quite useful if, for some reason, we can't use the tablet or
    // master to draw the safety area.
    //
    // Simply focus the window and draw the safety area whilst keeping the left mouse button pressed
    cv::namedWindow("SA_Definition", CV_WINDOW_NORMAL);
    
    ros::Time timeBegin;
    ros::Time timeEnd;
    ros::Time time3DreconBegin;
    ros::Duration deltaTime;
    
    // Thread variables for the disparity map calculation
    enV2.disparityCompleted = true;
    bool first_frame = false;
    pthread_t threadDisparity;
    
    cv::setMouseCallback(
        "SA_Definition",
        enVisorsSys2::SA_manual_selection_callback, &enV2);
    
    cv::FileStorage r1_f;
    std::string path_r1;
    cv::FileStorage r2_f;
    std::string path_r2;
    
    ros::Rate loop_rate(100);
    while (ros::ok()) {
        cv::waitKey(1);
        switch (enV2.status) {
            
            case INIT_PARAMETERS : {
                ROS_INFO("Initialising parameters ... ");
                
                enV2.n.getParam("/camera_info/model", enV2.camera_info.camera_model);
                enV2.n.getParam("/camera_info/left/dev", enV2.camera_info.dev_left);
                enV2.n.getParam("/camera_info/right/dev", enV2.camera_info.dev_right);
                
                int sa_definition_modality = 0;
                // enV2.n.getParam("/SA/definition/manual", sa_definition_modality);
                sa_definition_modality = 1; // 1 is online AKA using the mouse on the opencv window
                enV2.SA.definition_modality = static_cast<SAInputMethod>(sa_definition_modality);
                
                enV2.init_params();
                enV2.read_images();
                
                enV2.rectification(enV2.performRectification, enV2.R1, enV2.R2);
                // save R1 and R2 on xml file
                path_r1 = std::string(CONFIG_FILE_FOLDER) + "camera/" + enV2.camera_info.camera_name + "/R1.xml";
                r1_f.open(path_r1, cv::FileStorage::WRITE);
                r1_f << "R" << enV2.R1;
                r1_f.release();
                
                path_r2 = std::string(CONFIG_FILE_FOLDER) + "camera/" + enV2.camera_info.camera_name + "/R2.xml";
                r2_f.open(path_r2, cv::FileStorage::WRITE);
                r2_f << "R" << enV2.R1;
                r2_f.release();
                
                //save image with original dimension
                enV2.imgL_rect.copyTo(enV2.imgL_rect_original);
                
                
                //scale image
                if (enV2.img_subSampling) {
                    enV2.scale_image(enV2.imgL_rect, enV2.imgR_rect);
                    enV2.imgSize.height = enV2.imgL_rect.rows;
                    enV2.imgSize.width = enV2.imgL_rect.cols;
                    enV2.recon.img_scalefactor = (float) enV2.imgL_rect.cols / (float) enV2.imgL_rect_original.cols;
                    enV2.recon.minDisparities = enV2.recon.minDisparities * enV2.recon.img_scalefactor;
                    enV2.recon.numberOfDisparities = enV2.recon.numberOfDisparities * enV2.recon.img_scalefactor;
                }
                else {
                    enV2.recon.img_scalefactor = 1.0f;
                }
                
                if (!enV2.is3DReconEvaluation)
                    enV2.init_LTSAT();
                
                enV2.init_stereo_recon();
                enV2.init_SafetyAugmentation();
                enV2.status = READ_IMAGES;
                
                break;
            }
            case READ_IMAGES :
                ROS_DEBUG("current state: READ_IMAGES");
                
                // Wait for images ...
                enV2.read_images();
                timeBegin = ros::Time::now();
                
                // image rectification
                enV2.rectification(enV2.performRectification, enV2.R1, enV2.R2);
                
                //save image with original dimension
                enV2.imgL_rect.copyTo(enV2.imgL_rect_original);
                
                enV2.publish_rectifiedImages();
                
                //scale image
                if (enV2.img_subSampling) {
                    enV2.scale_image(enV2.imgL_rect, enV2.imgR_rect);
                    enV2.imgSize.height = enV2.imgL_rect.rows;
                    enV2.imgSize.width = enV2.imgL_rect.cols;
                }
                
                enV2.imgL_rect.copyTo(enV2.img_visualization);
                
                // convert images from RGB to grey
                fromRGBtoGray(enV2.imgL_rect, enV2.imgL_grey);
                fromRGBtoGray(enV2.imgR_rect, enV2.imgR_grey);
                
                // convert from RGB to HSV
                cv::cvtColor(enV2.imgL_rect, enV2.imgL_hsv, cv::COLOR_BGR2HSV);
                
                if (enV2.recon.isGaussian) {
                    cv::GaussianBlur(enV2.imgL_rect, enV2.imgL_rect, cv::Size(7, 7), 3, 3);
                }
                
                // create mask with specular reflections
                enV2.find_specularities();
                
                enV2.frame_number++;
                
                if (enV2.SA.definition_modality == SAInputMethod::Tablet && (enV2.SA.tablet_buttons & 0x08)) {
                    enV2.SA.contour.clear();
                    enV2.safety_area_defined = false;

#ifdef MULTITHREAD
                    if (!enV2.disparityCompleted)
                        pthread_join(threadDisparity, NULL);
#endif
                }
                
                // change status
                if (!enV2.is3DReconEvaluation) {
                    if (!enV2.safety_area_defined || enV2.start_video) {
                        enV2.status = DEFINE_SAFETY_AREA;
                    }
                    else {
                        if (!enV2.wait_for_new_match) {
                            enV2.status = KLT_TRACKER;
                        }
                        else {
                            enV2.status = HOUGH_MATCHING;
                        }
                    }
                }
                else {
                    enV2.status = STEREO_RECONSTRUCTION;
                }
                
                enV2.clear_variables();
                
                // save computational time
                timeEnd = ros::Time::now();
                deltaTime = timeEnd - timeBegin;
                enV2.read_images_time = deltaTime.toSec();
                
                break;
            
            case DEFINE_SAFETY_AREA :
                ROS_DEBUG("current state: DEFINE_SAFETY_AREA");
                
                enV2.def_saf_area = enV2.define_safety_area(enV2.SA.definition_modality);
                if (enV2.def_saf_area == 1) {
                    // save time stamp
                    timeBegin = ros::Time::now();
                    
                    // create new template with previous lost safety area
                    enV2.TrackL.create_safety_area_mask(
                        enV2.imgL_grey,
                        enV2.SA.contour,
                        enV2.mask_specularity,
                        enV2.SA.SA_mask);
                    
                    // detect features inside the SA and background
                    enV2.n_klt_total_feat = enV2.detect_KLT_features();
                    
                    // compute probability map segmentation initialization
                    enV2.compute_segmentation(true);
                    
                    //compute first SA model
                    enV2.n_hm_sa_feat = enV2.init_SAmodel();
                    
                    if (enV2.n_hm_sa_feat > 10) {
                        // compute hsv image histogram
                        hsv_image_histogram(
                            enV2.imgL_hsv,
                            enV2.histL_hsv,
                            enV2.SA.contour,
                            enV2.SA.SA_mask,
                            enV2.hbin_HSV,
                            enV2.sbin_HSV,
                            0.5,
                            &enV2.TrackL.SegModel);
                        
                        //initialize bayesian network
                        enV2.init_bayesian_network();
                        
                        // discard feature in the backgorund
                        enV2.TrackL.find_points_in_safety_area_high_seg_prob(
                            enV2.imgL_rect,
                            enV2.imgL_hsv,
                            enV2.SA.points_tracked,
                            enV2.SA.contour,
                            enV2.SA.status_points_tracked,
                            enV2.SA.status_SA_points,
                            enV2.SA.n_feat_detected,
                            enV2.TrackL.TrackingParam.th_segmentation);
                        
                        enV2.update_keyframe_data();
                        
                        enV2.TrackL.find_SAContourn_partialOcclusion(
                            enV2.imgL_rect,
                            enV2.img_visualization,
                            enV2.imgL_hsv,
                            enV2.SA.contour,
                            enV2.SA.contour_PO);
                        
                        enV2.safety_area_defined = true;
                        first_frame = true;
                        enV2.status = STEREO_RECONSTRUCTION;
                        
                        // save computational time
                        timeEnd = ros::Time::now();
                        deltaTime = timeEnd - timeBegin;
                        enV2.define_sa_time = deltaTime.toSec();
                        
                    }
                    else {
                        enV2.SA.contour.clear();
                        enV2.SADefinition_status = 0;
                        enV2.safety_area_defined = false;
                        ROS_WARN("Please, select the Safety Area on the image");
                        
                        enV2.status = READ_IMAGES;
                    }
                }
                else if (enV2.def_saf_area == 0) {
                    enV2.SA.contour.clear();
                    enV2.SADefinition_status = 0;
                    enV2.safety_area_defined = false;
                    ROS_WARN("Please, select the Safety Area on the image");
                    
                    enV2.status = READ_IMAGES;
                }
                else if (enV2.def_saf_area == 2) {
                    enV2.safety_area_defined = false;
                    ROS_WARN("Please, select the Safety Area on the image");
                    enV2.status = DEFINE_SAFETY_AREA;
                }
                
                break;
            
            case KLT_TRACKER :
                ROS_DEBUG("current state: KLT_TRACKER");
                
                enV2.KLTTracker = true;
                enV2.HMatchingTracker = false;
                
                timeBegin = ros::Time::now();
                
                if (!enV2.KLT_tracking()) {
                    enV2.stage = STAGE_TRACK;
                    enV2.status = STEREO_RECONSTRUCTION;
                }
                else {
                    enV2.status = HOUGH_MATCHING;
                }
                
                // save computational time
                timeEnd = ros::Time::now();
                deltaTime = timeEnd - timeBegin;
                enV2.klt_tracker_time = deltaTime.toSec();
                
                ROS_DEBUG_STREAM("[TIME] KLT time :" << deltaTime.toSec());
                break;
            
            case HOUGH_MATCHING :
                ROS_DEBUG("current state: HOUGH_MATCHING");
                
                enV2.KLTTracker = false;
                enV2.HMatchingTracker = true;
                
                timeBegin = ros::Time::now();
                if (enV2.redetect_SA()) {
                    enV2.stage = STAGE_REINIT;
                    enV2.update_keyframe_data();
                    enV2.wait_for_new_match = false;
                    
                    enV2.status = STEREO_RECONSTRUCTION;
                }
                else {
                    enV2.stage = STAGE_WAIT;
                    enV2.wait_for_new_match = true;
                    enV2.SAug.distance_to_pc = -1.0;
                    enV2.status = SAFETY_AUGMENTATION;
                }
                
                // save computational time
                timeEnd = ros::Time::now();
                deltaTime = timeEnd - timeBegin;
                enV2.hough_matching_time = deltaTime.toSec();
                
                ROS_DEBUG_STREAM("[TIME]  Hough Matching time :" << deltaTime.toSec());
                
                break;
            
            case STEREO_RECONSTRUCTION :
                ROS_DEBUG("current state: STEREO_RECONSTRUCTION");
                
                
                if (enV2.disparityCompleted) {
                    ROS_INFO("Disparity computation completed");
                    if (!first_frame) {
                        // save computational time
                        timeEnd = ros::Time::now();
                        deltaTime = timeEnd - time3DreconBegin;
                        enV2.stereo_recon_do_time = deltaTime.toSec();
                    }
                    
                    time3DreconBegin = ros::Time::now();
                    
                    enV2.prepare_3D_reconstruction();

#ifdef MULTITHREAD
                    enV2.disparityCompleted = false;
                    pthread_create(&threadDisparity, NULL, enV2.run_disparity, (void *) &enV2);
                    
                    if (enV2.is3DReconEvaluation) {
                        pthread_join(threadDisparity, NULL);
                    }
                    
                    ROS_DEBUG_STREAM("[TIME] Stereo :" << deltaTime.toSec());
#else // NOT MULTITHREAD
                    timeBegin = ros::Time::now();
                    enV2.compute_super_pixel();
                    timeEnd = ros::Time::now();
                    deltaTime = timeEnd - timeBegin;
                    ROS_DEBUG_STREAM("[TIME] compute SP :" << deltaTime.toSec());

                    timeBegin = ros::Time::now();
                    enV2.compute_3D_reconstruction();

                    //				enV2.filter_point_cloud();
                    timeEnd = ros::Time::now();
                    deltaTime = timeEnd - timeBegin;
                    ROS_DEBUG_STREAM("[TIME] compute 3D :" << deltaTime.toSec());
#endif
                    
                    if (!enV2.is3DReconEvaluation && !first_frame) {
                        enV2.status = SAFETY_AUGMENTATION;
                    }
                    else {
                        enV2.status = UPDATE_PREV;
                    }
                }
                else {
                    timeBegin = ros::Time::now();
                    if (!enV2.is3DReconEvaluation) {
                        enV2.update_point_cloud();
                        enV2.status = SAFETY_AUGMENTATION;
                    }
                    else {
                        enV2.status = UPDATE_PREV;
                    }
                    // save computational time
                    timeEnd = ros::Time::now();
                    deltaTime = timeEnd - timeBegin;
                    enV2.stereo_recon_up_time = deltaTime.toSec();
                }
                
                break;
            
            case SAFETY_AUGMENTATION:
                ROS_DEBUG("current state: SAFETY AUGMENTATION");
                
                timeBegin = ros::Time::now();
                
                enV2.do_augmentation();
                
                // save computational time
                timeEnd = ros::Time::now();
                deltaTime = timeEnd - timeBegin;
                enV2.safety_aug_time = deltaTime.toSec();
                
                ros::spinOnce();
                enV2.status = UPDATE_PREV;
                break;
            
            case EVALUATION :
                ROS_DEBUG("current state: EVALUATION");
                
                if (enV2.isTrackingEvaluation) {
                    enV2.evaluate_tracking();
                }
                
                enV2.status = UPDATE_PREV;
                break;
            
            case UPDATE_PREV :
                ROS_DEBUG("current state: UPDATE_PREV");
                enV2.draw_tracking();
                
                enV2.imgL_grey.copyTo(enV2.imgL_grey_prev);
                enV2.SA.points_tracked_prev.resize(enV2.SA.points_tracked.size());
                enV2.SA.points_tracked_prev = enV2.SA.points_tracked;
                enV2.recon.R_e.copyTo(enV2.recon.R_e_prev);
                enV2.recon.t_e.copyTo(enV2.recon.t_e_prev);
                
                enV2.camera_info.camImg_left.~CamImage();
                enV2.camera_info.camImg_left.~CamImage();
                
                if (publish) {
                    enV2.publish_images();
                }
                
                cv::imshow("SA_Definition", enV2.img_visualization);
                cv::waitKey(1);
                
                // save computational time on file
                enV2.save_computational_times();
                enV2.save_images();
                
                // initialize computational time of each state
                enV2.read_images_time = -1.0;
                enV2.define_sa_time = -1.0;
                enV2.klt_tracker_time = -1.0;
                enV2.hough_matching_time = -1.0;
                enV2.stereo_recon_up_time = -1.0;
                enV2.stereo_recon_do_time = -1.0;
                enV2.safety_aug_time = -1.0;
                
                enV2.imgL.release();
                enV2.imgR.release();
                
                // change status
                first_frame = false;
                enV2.status = READ_IMAGES;
                
                break;
            
            default :
                ROS_DEBUG("current state: DEFAULT (error ?)");
                break;
        }
        
        ros::spinOnce();
        loop_rate.sleep();
    }// while
    
    if (!enV2.disparityCompleted)
        pthread_join(threadDisparity, NULL);
    
    return 0;
}

