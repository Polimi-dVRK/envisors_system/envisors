/*
 * main.cpp
 *
 *  Created on: Jan 20, 2017
 *      Author: veronica
 */



#include <stdio.h>
#include <sstream>

//ROS
#include <ros/ros.h>
#include "ros/rate.h"
#include <geometry_msgs/PoseStamped.h>
#include <tf/transform_datatypes.h>
#include <tf/transform_broadcaster.h>
#include <kdl/frames.hpp>
#include <kdl_conversions/kdl_msg.h>

// My Library
#include"enVisorsLib/common.hpp"
#include"enVisorsLib/paths.h"
#include"enVisorsLib/define.hpp"



geometry_msgs::Pose ECM_pose;


//
//// since the pose of the camera is published on a topic, I subscribe to it and convert it to tf
void ECM_pose_callback(const geometry_msgs::PoseStamped::ConstPtr& msg)
{

	ECM_pose = msg->pose;
}


int main(int argc, char **argv)
{

	ros::init(argc, argv, "ECM_tf_streamer");
	ros::NodeHandle n;

	int camera_model;
	std::string camera_name;
	std::string path = CONFIG_FILE_FOLDER;

	cv::Mat R_device_robot;
	cv::Mat t_device_robot;
	geometry_msgs::Pose T_device_robot_;
	KDL::Frame T_device_robot_kdl;
	geometry_msgs::Pose T_PSM1remoteCenter_ecm_;
	tf::Transform T_PSM1remoteCenter_ecm;
	tf::TransformBroadcaster broadcaster_PSM1_ECM;

	KDL::Frame T_PSM1rc_ECMee;
	KDL::Frame T_ECMrc_ECMee;
	KDL::Frame T_PSM1rc_camera;

	cv::Mat R_device_camera;
	cv::Mat t_device_camera;
	KDL::Frame T_device_camera_kdl;
	geometry_msgs::Pose T_PSM1rc_camera_;
	tf::TransformBroadcaster broadcaster_PSM1_camera;
	tf::Transform T_PSM1remoteCenter_camera;

	ros::Subscriber  ecm_ee_pose_sub =
			n.subscribe("/dvrk/ECM/position_cartesian_current", 1, &ECM_pose_callback);
	ros::Rate loop_rate(100);

	n.getParam("/camera_info/model", camera_model);

	if(camera_model == DAVINCI)
	{
		camera_name = "davinci";
	}
	else
	{
		std::cout << "[ERROR] wrong camera model" << std::endl;
	}

	readExtrisicParamsCamera(
			path + "camera/" + camera_name +
			"/Device_Robot_Transform.xml",
			R_device_robot,
			t_device_robot);
	readExtrisicParamsCamera(
			path + "camera/" + camera_name +
			"/Endoscope_Camera_Transform.xml",
			R_device_camera,
			t_device_camera);


	while(ros::ok()){

		// publish ECM tf transform with respect to its /PSM1remote_center


		//convert to KDL
		T_device_robot_kdl.M = KDL::Rotation(
				R_device_robot.at<double>(0,0), R_device_robot.at<double>(0,1), R_device_robot.at<double>(0,2),
				R_device_robot.at<double>(1,0), R_device_robot.at<double>(1,1), R_device_robot.at<double>(1,2),
				R_device_robot.at<double>(2,0), R_device_robot.at<double>(2,1), R_device_robot.at<double>(2,2));
		T_device_robot_kdl.p[0] = t_device_robot.at<double>(0,0);
		T_device_robot_kdl.p[1] = t_device_robot.at<double>(1,0);
		T_device_robot_kdl.p[2] = t_device_robot.at<double>(2,0);


		T_device_camera_kdl.M = KDL::Rotation(
				R_device_camera.at<double>(0,0), R_device_camera.at<double>(0,1), R_device_camera.at<double>(0,2),
				R_device_camera.at<double>(1,0), R_device_camera.at<double>(1,1), R_device_camera.at<double>(1,2),
				R_device_camera.at<double>(2,0), R_device_camera.at<double>(2,1), R_device_camera.at<double>(2,2));
		T_device_camera_kdl.p[0] = t_device_camera.at<double>(0,0);
		T_device_camera_kdl.p[1] = t_device_camera.at<double>(1,0);
		T_device_camera_kdl.p[2] = t_device_camera.at<double>(2,0);


		tf::poseMsgToKDL(ECM_pose, T_ECMrc_ECMee);


		// multiply
		T_PSM1rc_ECMee = T_device_robot_kdl.Inverse() * T_ECMrc_ECMee;
		T_PSM1rc_camera = T_device_robot_kdl.Inverse() * T_ECMrc_ECMee * T_device_camera_kdl;

		//convert to tf
		tf::poseKDLToMsg(T_PSM1rc_ECMee, T_PSM1remoteCenter_ecm_);
		T_PSM1remoteCenter_ecm.setOrigin(tf::Vector3(
				T_PSM1remoteCenter_ecm_.position.x,
				T_PSM1remoteCenter_ecm_.position.y,
				T_PSM1remoteCenter_ecm_.position.z) );
		tf::Quaternion quat_PSM1_ECM;
		tf::quaternionMsgToTF(T_PSM1remoteCenter_ecm_.orientation, quat_PSM1_ECM);
		T_PSM1remoteCenter_ecm.setRotation(quat_PSM1_ECM);

		tf::poseKDLToMsg(T_PSM1rc_camera, T_PSM1rc_camera_);
		T_PSM1remoteCenter_camera.setOrigin(tf::Vector3(
				T_PSM1rc_camera_.position.x,
				T_PSM1rc_camera_.position.y,
				T_PSM1rc_camera_.position.z) );
		tf::Quaternion quat_PSM1_camera;
		tf::quaternionMsgToTF(T_PSM1rc_camera_.orientation, quat_PSM1_camera);
		T_PSM1remoteCenter_camera.setRotation(quat_PSM1_camera);


		broadcaster_PSM1_ECM.sendTransform(tf::StampedTransform(T_PSM1remoteCenter_ecm, ros::Time::now(), "/PSM1remote_center", "/ECM_ee"));
		broadcaster_PSM1_camera.sendTransform(tf::StampedTransform(T_PSM1remoteCenter_camera, ros::Time::now(), "/PSM1remote_center", "/camera"));


		ros::spinOnce();
		loop_rate.sleep();
	}

}
