/*
 * main.cpp
 *
 *  Created on: Jan 20, 2017
 *      Author: veronica
 */



#include <stdio.h>
#include <sstream>

//ROS
#include <ros/ros.h>
#include "ros/rate.h"
#include <geometry_msgs/PoseStamped.h>
#include <tf/transform_datatypes.h>
#include <tf/transform_broadcaster.h>



geometry_msgs::Pose ECM_pose;


//
//// since the pose of the camera is published on a topic, I subscribe to it and convert it to tf
void ECM_pose_callback(const geometry_msgs::PoseStamped::ConstPtr& msg)
{

	ECM_pose = msg->pose;
}


int main(int argc, char **argv)
{

	ros::init(argc, argv, "ECM_tf_streamer");
	ros::NodeHandle n;

	tf::Transform T_remoteCenter_ecm;
	tf::TransformBroadcaster broadcaster;

	ros::Subscriber  ecm_ee_pose_sub =
			n.subscribe("/dvrk/ECM/position_cartesian_current", 1, &ECM_pose_callback);
	ros::Rate loop_rate(100);


	while(ros::ok()){

		T_remoteCenter_ecm.setOrigin(tf::Vector3(ECM_pose.position.x, ECM_pose.position.y,
				ECM_pose.position.z) );
		tf::Quaternion quat;
		tf::quaternionMsgToTF(ECM_pose.orientation, quat);
		std::cout << quat[0] << " \n";
		std::cout << quat[1] << " \n";
		std::cout << quat[2] << " \n";
		std::cout << quat[3] << std::endl;
		T_remoteCenter_ecm.setRotation(quat);

		// publish tf transform with respect to its /ECMremote_center
		broadcaster.sendTransform(tf::StampedTransform(T_remoteCenter_ecm, ros::Time::now(), "/ECMremote_center", "/ECM_ee"));

		ros::spinOnce();
		loop_rate.sleep();
	}

}
