cmake_minimum_required(VERSION 2.8.3)
project(davinci_tf_streamer)

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED COMPONENTS
             roscpp
             geometry_msgs
             tf
             enVisorsLib
             kdl_conversions)

###################################
## catkin specific configuration ##
###################################
## The catkin_package macro generates cmake config files for your package
## Declare things to be passed to dependent projects
## INCLUDE_DIRS: uncomment this if you package contains header files
## LIBRARIES: libraries you create in this project that dependent projects also need
## CATKIN_DEPENDS: catkin_packages dependent projects also need
## DEPENDS: system dependencies of this project that dependent projects also need
catkin_package(
#  INCLUDE_DIRS include
#  LIBRARIES davinci_tf_streamer
  CATKIN_DEPENDS roscpp geometry_msgs tf 
             enVisorsLib
             kdl_conversions
#  DEPENDS system_lib
)

###########
## Build ##
###########

include_directories(include)
include_directories(
  ${catkin_INCLUDE_DIRS}
)

## Declare a C++ executable
add_executable(ECM_streamer 
	ECM_streamer.cpp )

add_executable(PSM1_ECM_streamer 
	PSM1_ECM_streamer.cpp )
	
add_executable(PSM1_streamer 
	PSM1_streamer.cpp )
	
## Add cmake target dependencies of the executable
## same as for the library above
add_dependencies(ECM_streamer 
 			 roscpp
             geometry_msgs
             tf)

add_dependencies(PSM1_ECM_streamer 
 			 roscpp
             geometry_msgs
             tf)
             
add_dependencies(PSM1_streamer 
			 roscpp
             geometry_msgs
             tf)

target_link_libraries(ECM_streamer ${catkin_LIBRARIES})
target_link_libraries(PSM1_ECM_streamer ${catkin_LIBRARIES})
target_link_libraries(PSM1_streamer ${catkin_LIBRARIES})
