/*  ros_to_slicer_pose.hpp
 *
 *  Created on: Novembre 8, 2015
 *  Author: Veronica Penza
 */

#include <stdio.h>
#include <sstream>
#include <vector>

//ROS
#include <ros/ros.h>
#include "ros/rate.h"
#include "std_msgs/String.h"
#include <sensor_msgs/fill_image.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/CameraInfo.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Bool.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/Image.h>
#include <cv_bridge/cv_bridge.h>
#include <ros/package.h>
#include <image_transport/image_transport.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseStamped.h>


#include <ros_to_slicer_pose/udp.h>

//tf
#include <tf/LinearMath/Matrix3x3.h>
#include <kdl/frames.hpp>
#include <kdl_conversions/kdl_msg.h>


// My Library
#include"enVisorsLib/common.hpp"
#include"enVisorsLib/paths.h"


typedef struct {
	double position[3];
	double quaternion[4];
}TransformType;

typedef struct {
	TransformType transform1;
	TransformType transform2;
}Data_to_send;


class RosToSlicerPose
{
public:

	//configuration file path
	std::string path_root;
	std::string path_configFile;
	std::string path;

	geometry_msgs::Pose virtualCameraPose;
	geometry_msgs::Pose robotPose;
	Udp udp_manager;

    int port;
    std::string remote_ip;
    int buffer_size;

	RosToSlicerPose();
	~RosToSlicerPose();

	bool init();
	void changeRemoteIP(const std::string& in_ip);
	void fromMsgtoUDPData(geometry_msgs::Pose& pose, TransformType& data_in);

	ros::NodeHandle n;
	ros::Subscriber cameraPose_sub;
	ros::Subscriber robotPose_sub;

	/*method*/
	void virtual_camera_pose_callback(const geometry_msgs::PoseStampedPtr& vr);
	void robot_pose_callback(const geometry_msgs::PosePtr& rp);
	void robot_poseStamped_callback(const geometry_msgs::PoseStampedPtr& rp);
};

