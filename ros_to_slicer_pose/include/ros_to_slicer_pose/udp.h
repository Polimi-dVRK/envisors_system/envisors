#ifndef _UDP_H_
#define _UDP_H_

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>

#ifdef linux
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#else
#include <winsock2.h>
#endif

// States
#define UDP_STATE_NOINIT		0
#define UDP_STATE_DISCONNECTED	1
#define UDP_STATE_CONNECTED		2

// Errors
#define UDP_ERROR_NOERROR		0
#define UDP_ERROR_GENERIC		1
#define UDP_ERROR_SEND			2
#define UDP_ERROR_RECEIVE		3
#define UDP_ERROR_OVERFLOW		4

class Udp
{
private:
	// State and error
	int state;
	int error;

	// UDP conection
#ifdef linux
	int udp_s;
	struct sockaddr_in udp_addr;
#else
	SOCKET udp_s;
	SOCKADDR_IN udp_addr;
#endif

	// Pthreads
	pthread_mutex_t mutex;
	pthread_t thread;
	
	// Read buffer
	char *buffer;
	int lBuffer;
	
	// New data
	int newData;

public:
	Udp();
	~Udp();

	int connect(const char *address, int port);
	int disconnect();
	
	int setBufferSize(int size);

	int send(char *data);
	int send(char *data, int size);
	int receive(char *data);

	int getState();
	int getError();

private:
	static void *udp_thread(void *arg);
};

#endif
