/*  ros_to_slicer_pose.cpp
 *
 *  Created on: Novembre 8, 2015
 *  Author: Veronica Penza
 */


#include"ros_to_slicer_pose/ros_to_slicer_pose.hpp"


RosToSlicerPose::RosToSlicerPose()
{

	//init path
//	path_root = "/home/veronica/PhD/ros_workspace";
//	path_configFile = "/config_file/";
//	path = path_root + path_configFile;
	path = DATASET_FOLDER;

	// initiate ros subscriber
	this->cameraPose_sub = n.subscribe("/camera/pose/", 1,
			&RosToSlicerPose::virtual_camera_pose_callback, this);
//	this->robotPose_sub = n.subscribe("/lwrCartCurrent", 1,
//				&RosToSlicerPose::robot_pose_callback, this);
	this->robotPose_sub = n.subscribe("/omni1/omni1_pose", 1,
					&RosToSlicerPose::robot_poseStamped_callback, this);

	//set UPD data
	this->remote_ip = "192.168.0.5";
	//uralp
//    this->remote_ip = "169.254.200.13";
	//Nearlab!
	//this->remote_ip = "192.168.10.25";
	this->port = 22048;
	this->buffer_size = sizeof(Data_to_send);

	// Set buffer size
	udp_manager.setBufferSize(this->buffer_size);
}

RosToSlicerPose::~RosToSlicerPose()
{
	int disc;

	//disconnect UDP communication
	disc =  udp_manager.disconnect();
	if (disc == 1){
		std::cout << "UDP Disconnected!" << std::endl;
	}
	else{
		std::cout << "[ERROR]: UDP not Disconnected!" << std::endl;
	}
}

void RosToSlicerPose::virtual_camera_pose_callback(const geometry_msgs::PoseStampedPtr& vr)
{
	this->virtualCameraPose.position = vr->pose.position;
	this->virtualCameraPose.orientation = vr->pose.orientation;
}

void RosToSlicerPose::robot_pose_callback(const geometry_msgs::PosePtr& rp)
{
	robotPose.orientation = rp->orientation;
	robotPose.position = rp->position;

	KDL::Frame robotPose_;
	KDL::Frame robot_end_tip;
	KDL::Frame robotTipPose;
	cv::Mat R;
	cv::Mat t;


	bool pivoting = false;
	if(pivoting)
	{

		readExtrisicParamsCamera(
				this->path + "CalibrationTipRobot.xml",
				R,
				t);

		//convert to KLD
		robot_end_tip.M = KDL::Rotation(R.at<float>(0,0), R.at<float>(0,1), R.at<float>(0,2),
				R.at<float>(1,0), R.at<float>(1,1), R.at<float>(1,2),
				R.at<float>(2,0), R.at<float>(2,1), R.at<float>(2,2));
		robot_end_tip.p[0] = t.at<float>(0,0);
		robot_end_tip.p[1] = t.at<float>(1,0);
		robot_end_tip.p[2] = t.at<float>(2,0);

		tf::poseMsgToKDL(robotPose, robotPose_);
		robotTipPose = robotPose_ * robot_end_tip;
		tf::poseKDLToMsg(robotTipPose, robotPose);
	}
}

void RosToSlicerPose::robot_poseStamped_callback(const geometry_msgs::PoseStampedPtr& rp){
	robotPose.orientation = rp->pose.orientation;
	robotPose.position = rp->pose.position;
}


bool RosToSlicerPose::init()
{

	int connection;
	int error;

	// init UDP connection
	connection = udp_manager.connect(this->remote_ip.c_str(), this->port);
	if (connection == 1){
		std::cout << "UDP Connected!" << std::endl;
		return true;
	}
	else{
		std::cout << "buffer size " << this->buffer_size << std::endl;
		error = udp_manager.getError();

		switch( error )
		{
		case 0:
			std::cout << "UDP_ERROR_NOERROR" << std::endl;
			break;
		case 1:
			std::cout << "UDP_ERROR_GENERIC" << std::endl;
			break;
		case 2:
			std::cout << "UDP_ERROR_SEND" << std::endl;
			break;
		case 3:
			std::cout << "UDP_ERROR_RECEIVE" << std::endl;
			break;
		case 4:
			std::cout << "UDP_ERROR_OVERFLOW" << std::endl;
			break;
		}

		std::cout << "[ERROR]: UDP not Connected!" << std::endl;
		return false;
	}

}

void RosToSlicerPose::fromMsgtoUDPData(geometry_msgs::Pose& pose, TransformType& data_in)
{
	data_in.position[0] = pose.position.x;
	data_in.position[1] = pose.position.y;
	data_in.position[2] = pose.position.z;
	data_in.quaternion[0] = pose.orientation.w;
	data_in.quaternion[1] = pose.orientation.x;
	data_in.quaternion[2] = pose.orientation.y;
	data_in.quaternion[3] = pose.orientation.z;
}

