/*  main.cpp
 *
 *  Created on: Novembre 8, 2015
 *  Author: Veronica Penza
 */

#include "ros_to_slicer_pose/ros_to_slicer_pose.hpp"


int main(int argc, char **argv)
{

	ros::init(argc, argv, "camera_device_calibration");
	
	// Definition of Class
	RosToSlicerPose ROSSlicer;
	ros::Rate loop_rate(10);

	if(ROSSlicer.init())
	{

		while(ros::ok()){

			Data_to_send data;

			// Save geometry_msgs::Pose Transform1 in data_to_send struct
			ROSSlicer.fromMsgtoUDPData(ROSSlicer.virtualCameraPose, data.transform1);
			ROSSlicer.fromMsgtoUDPData(ROSSlicer.robotPose, data.transform2);

			// Send data
			ROSSlicer.udp_manager.send((char*)&data);

			ros::spinOnce();
			loop_rate.sleep();
		}
	}

}
