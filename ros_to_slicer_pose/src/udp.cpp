

#ifdef linux
#include "../include/ros_to_slicer_pose/udp.h"
#else
#include "../../include/libr3d/udp.h"
#endif

Udp::Udp()
{
	this->state = UDP_STATE_NOINIT;
	this->error = UDP_ERROR_NOERROR;
	this->newData = 0;
	this->lBuffer = 0;
}

Udp::~Udp()
{
	this->disconnect();
	
	if (this->lBuffer > 0)
		delete [] this->buffer;
}

int Udp::connect(const char *address, int port)
{
	this->disconnect();

	this->state = UDP_STATE_DISCONNECTED;
	this->error = UDP_ERROR_NOERROR;

	// Checking the port
	if (port < 0 || port > 32767)
		return 0;

	// Create the UDP socket
	this->udp_s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

	if (this->udp_s <= 0)
		return 0;

	// Configurando remoto
	udp_addr.sin_family = AF_INET;
	udp_addr.sin_port = htons(port);
	udp_addr.sin_addr.s_addr = inet_addr(address);

	// Resuelto del nombre de la direccin remota
	struct hostent *host;

	if (udp_addr.sin_addr.s_addr == INADDR_NONE)
	{
		host = NULL;
		host = gethostbyname(address);
		if (host == NULL)
			return 0;

		memcpy(&udp_addr.sin_addr, host->h_addr_list[0], host->h_length);
	}

	// Configurando local
	struct sockaddr_in addrL;
	addrL.sin_family = AF_INET;
	addrL.sin_port = htons(port);
	addrL.sin_addr.s_addr = INADDR_ANY;

	if (bind(this->udp_s, (struct sockaddr*)&addrL, sizeof(addrL)) == -1)
		return 0;

	this->state = UDP_STATE_CONNECTED;

	// Lanzo el thread
	pthread_mutex_init(&this->mutex, NULL);
	pthread_create(&this->thread, NULL, this->udp_thread, (void*)this);

	return 1;
}

int Udp::disconnect()
{
	if (this->state == UDP_STATE_CONNECTED)
	{
		this->state = UDP_STATE_DISCONNECTED;
#ifdef linux
		int ret = close(this->udp_s);
#else
		int ret = closesocket(this->udp_s);
#endif
		
		// Espero a que se cierren los threads
#ifdef linux
		usleep(100000);
#else
		Sleep(100);
#endif

		pthread_mutex_destroy(&this->mutex);

		return (ret == 0);
	}

	return 1;
}

int Udp::send(char *data)
{
	int ret = sendto(this->udp_s, (char*)data, this->lBuffer, 0, (struct sockaddr*)&this->udp_addr, sizeof(this->udp_addr));

	if (ret <= 0)
	{
		this->error = UDP_ERROR_SEND;
		return 0;
	}
	
	return ret;
}

int Udp::send(char *data, int size)
{
	int ret = sendto(this->udp_s, (char*)data, size, 0, (struct sockaddr*)&this->udp_addr, sizeof(this->udp_addr));

	if (ret <= 0)
	{
		this->error = UDP_ERROR_SEND;
		return 0;
	}

	return ret;
}

int Udp::receive(char *data)
{
	if (!this->newData)
		return 0;
		
	memcpy(data, this->buffer, this->lBuffer);
	this->newData = 0;
	
	return 1;
}

int Udp::setBufferSize(int size)
{
	if (size <= 0)
		return 0;
	
	if (this->lBuffer != 0)
		delete [] this->buffer;
		
	this->lBuffer = size;
	this->buffer = new char[this->lBuffer];
	
	return this->lBuffer;
}

int Udp::getState()
{
	return this->state;
}

int Udp::getError()
{
	return this->error;
}

void *Udp::udp_thread(void *arg)
{
	Udp *udp = (Udp*)arg;
	
	if (udp->lBuffer <= 0)
	{
		pthread_exit(NULL);
		return NULL;
	}
		
	int ret;

	while (udp->getState() == UDP_STATE_CONNECTED)
	{
#ifdef linux
		usleep(1000);
#else
		Sleep(0);
#endif

		ret = recvfrom(udp->udp_s, udp->buffer, udp->lBuffer, 0, NULL, NULL);

		if (ret > 0)
		  {
		    printf("Receiving data: %d bytes\n", ret);
		    printf("Data: %f, %f\n",
			   ((double*)udp->buffer)[0],
			   ((double*)udp->buffer)[1]);

			udp->newData = 1;
		  }
	}

	pthread_exit(NULL);
	return NULL;
}
