/*
 * main.cpp
 *
 *  Created on: Sept 5, 2014
 *      Author: veronica
 */

#include "image_sender/image_sender.hpp"


/** ********************************************************************************************************************
 Main Fuction
 ***********************************************************************************************************************/

int main(int argc, char **argv)
{
	//Initiate ROS
	ros::init(argc, argv, "image_sender");
	std_msgs::Bool isRect_;
	IMGSender IMGSend;


	IMGSend.f = boost::bind(&IMGSender::callback_dynamic_reconfigure, &IMGSend, _1, _2);
	IMGSend.server.setCallback(IMGSend.f);
	IMGSend.init();
	IMGSend.start();
	ros::Rate loop_rate(10);
	cout << "image size: rows " << IMGSend.image_size.height << "rows" << IMGSend.image_size.width << endl;

	while (ros::ok())	{

		if( IMGSend.picture_left.data &&  IMGSend.picture_right.data)
		{
			// Convert Images to sensor_msgs::Images

			IMGSend.fromMattoImage(IMGSend.picture_left, &IMGSend.picture_left_, sensor_msgs::image_encodings::RGB8);
			IMGSend.fromMattoImage(IMGSend.picture_right, &IMGSend.picture_right_, sensor_msgs::image_encodings::RGB8);


			IMGSend.picture_left_.header.seq = IMGSend.frameID;
			IMGSend.picture_right_.header.seq = IMGSend.frameID;

			IMGSend.pub_picture_left.publish(IMGSend.picture_left_);
			IMGSend.pub_picture_right.publish(IMGSend.picture_right_);

			if (IMGSend.isRectified){
				cout << "rect" << endl;
			IMGSend.pub_rect_picture_left.publish(IMGSend.picture_left_);
			IMGSend.pub_rect_picture_right.publish(IMGSend.picture_right_);
			}


			IMGSend.directory_images.data = IMGSend.file_name;
			IMGSend.pub_directory.publish(IMGSend.directory_images);
			isRect_.data = IMGSend.isRectified;
			IMGSend.pub_isRectified.publish(isRect_);
		}
		ros::spinOnce();
		loop_rate.sleep();
	}
}
