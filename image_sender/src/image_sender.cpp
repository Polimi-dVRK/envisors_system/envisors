/*
 /*
 * image_sender.cpp
 *
 *  Created on: Sept 5, 2014
 *      Author: Veronica Penza
 */

#include "image_sender/image_sender.hpp"


void IMGSender::fromMattoImage(cv::Mat img_in, sensor_msgs::Image* img_out, std::string encoding){
	// Convert Images to sensor_msgs::Images

	cv_bridge::CvImage pics;
	pics.encoding = sensor_msgs::image_encodings::RGB8 ;
	pics.image = img_in;
	pics.toImageMsg(*img_out);
	img_out->header.stamp = ros::Time::now();
	img_out->header.frame_id = "depth_image";
	img_out->encoding = encoding ;
	img_out->width = img_in.cols;
	img_out->height = img_in.rows;

}

bool IMGSender::init(){
	image_transport::ImageTransport it(this->n);
    this->isRectified = false;

//	this->pub_picture_left = it.advertise("/image/left/rgb", 1);
//	this->pub_picture_right = it.advertise("/image/right/rgb", 1);
    this->pub_picture_left = it.advertise("/uralp/camera_left/image", 1);
	this->pub_picture_right = it.advertise("/uralp/camera_right/image", 1);


	this->pub_rect_picture_left = it.advertise("/image/left/rectified", 1);
	this->pub_rect_picture_right = it.advertise("/image/right/rectified", 1);
	this->pub_directory = this->n.advertise<std_msgs::String>("image/directory", 1000);
	this->pub_isRectified = this->n.advertise<std_msgs::Bool>("image/isRectified", 1000);
	this->frameID = 0;
	return true;
}

bool IMGSender::start(){

//	this->name_picture_left = "/home/microralp/svn/microralp-jade/dataset/stereo_pairs/";
//	this->name_picture_right = "/home/microralp/svn/microralp-jade/dataset/stereo_pairs/";
	this->name_picture_left = DATASET_FOLDER;
	this->name_picture_right = DATASET_FOLDER;
	this->name_picture_left_end = "imL.png";
	this->name_picture_right_end = "imR.png";
	this->picture_left = imread(this->name_picture_left+ this->file_name + this->name_picture_left_end, CV_LOAD_IMAGE_COLOR);
	this->picture_right = imread(this->name_picture_right+ this->file_name + this->name_picture_right_end, CV_LOAD_IMAGE_COLOR);

	cout << "file name" << this->name_picture_left+ this->file_name + this->name_picture_left_end << endl;

	// Check for invalid input
	if(! this->picture_left.data && ! this->picture_right.data)
	{
		if(!this->picture_left.data ){
			cout <<  "Could not open or find the left image" << std::endl ;
		}
		else if(! this->picture_right.data){
			cout <<  "Could not open or find the right image" << std::endl ;
		}
	}

	this->image_size.height = picture_left.rows;
	this->image_size.width = picture_left.cols;
	this->frameID++;

	std::cout << "ImGSender started !" <<std::endl;
	return true;

}

void IMGSender::callback_dynamic_reconfigure(image_sender::image_senderConfig &config, uint32_t level){

	dynamic_reconfig = &config;

	this->file_name = dynamic_reconfig->fine_name_;

	this->isRectified = dynamic_reconfig->isRectified_;
	if(this->file_name != this->file_name_old){
		this->start();
	}
	ROS_INFO("Reconfigure Request: %s %d ",

			dynamic_reconfig->fine_name_.c_str(),
			dynamic_reconfig->isRectified_);
	this->file_name_old = this->file_name;

}
