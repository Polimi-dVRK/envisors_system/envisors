/*
 * stereo_calibration.hpp
 *
 *  Created on: Sept 5, 2014
 *      Author: Veronica Penza
 */


#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include <sensor_msgs/image_encodings.h>

//OpenCV
#include <cv_bridge/cv_bridge.h>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>

//ROS
#include "ros/ros.h"
#include "std_msgs/String.h"
#include <sensor_msgs/fill_image.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/CameraInfo.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Bool.h>
#include <sensor_msgs/image_encodings.h>
#include <image_transport/image_transport.h>
#include <sensor_msgs/Image.h>

#include <dynamic_reconfigure/server.h>
#include <image_sender/image_senderConfig.h>

#include <enVisorsLib/paths.h>

using namespace cv;
using namespace std;
using namespace sensor_msgs;
using namespace sensor_msgs::image_encodings;


class IMGSender{
public:
	string name_picture_left;
	string name_picture_right;
	string name_picture_left_end;
	string name_picture_right_end;
	string file_name;
	string file_name_old;
	Mat picture_left;
	Mat picture_right;
	int frameID;

	sensor_msgs::Image picture_left_;
	sensor_msgs::Image picture_right_;
	bool isRectified;
	ros::NodeHandle n;
	image_transport::Publisher pub_picture_left;
	image_transport::Publisher pub_picture_right;
	image_transport::Publisher pub_rect_picture_left;
	image_transport::Publisher pub_rect_picture_right;

    Size image_size;
	std_msgs::String directory_images;
	ros::Publisher pub_directory;
	ros::Publisher pub_isRectified;

	image_sender::image_senderConfig* dynamic_reconfig;
	dynamic_reconfigure::Server<image_sender::image_senderConfig> server;
	dynamic_reconfigure::Server<image_sender::image_senderConfig>::CallbackType f;

	void fromMattoImage(cv::Mat img_in, sensor_msgs::Image* img_out, std::string encoding);
	bool init();
	bool start();
	void callback_dynamic_reconfigure(image_sender::image_senderConfig &config, uint32_t level);

};
