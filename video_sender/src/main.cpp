/*
 * main.cpp
 *
 *  Created on: Sept 16, 2014
 *      Author: Veronica Penza
 */

#include "video_sender/video_sender.hpp"


/** ********************************************************************************************************************
 Main Fuction
 ***********************************************************************************************************************/

int main(int argc, char **argv)
{
	//Initiate ROS
	ros::init(argc, argv, "video_sender");
	VIDEOSender VideoSend;
	ros::Rate loop_rate(100);
	VideoSend.f = boost::bind(&VIDEOSender::callback_dynamic_reconfigure, &VideoSend, _1, _2);
	VideoSend.server.setCallback(VideoSend.f);
	VideoSend.init();
	VideoSend.stereo = true;

	while (ros::ok())
	{

		if(!VideoSend.video_name_prop.compare(VideoSend.video_name_old) == 0){

			VideoSend.init_directory();
			if(!VideoSend.tracking_evaluation){
				if(VideoSend.stereo)
				{
					VideoSend.open_stereo_video();
				}
				else
				{
					VideoSend.open_mono_video();
				}

				cout << "Opening new video:" << VideoSend.video_name_prop << endl;
			}

			VideoSend.video_name_old = VideoSend.video_name_prop;
		}
		else{
			if(!VideoSend.tracking_evaluation){
				// Check for invalid input

				VideoSend.read_video(VideoSend.stereo);
				if (VideoSend.video_left.cols < 1 || VideoSend.video_left.rows < 1){
					return 0;
				}

				if(VideoSend.stereo)
				{
					VideoSend.fromMattoImage(VideoSend.video_left, &VideoSend.video_left_, sensor_msgs::image_encodings::RGB8);
					VideoSend.fromMattoImage(VideoSend.video_right, &VideoSend.video_right_, sensor_msgs::image_encodings::RGB8);

					if (VideoSend.isRectified_prop){
						cout << "rect" << endl;
						VideoSend.pub_video_left_rectified.publish(VideoSend.video_left_);
						VideoSend.pub_video_right_rectified.publish(VideoSend.video_right_);
					}

					VideoSend.pub_video_left.publish(VideoSend.video_left_);
					VideoSend.pub_video_right.publish(VideoSend.video_right_);

					VideoSend.directory_video.data = VideoSend.video_name_prop;
					VideoSend.pub_directory.publish(VideoSend.directory_video);
				}
				else
				{
					VideoSend.fromMattoImage(VideoSend.video_left, &VideoSend.video_left_, sensor_msgs::image_encodings::RGB8);

					if (VideoSend.isRectified_prop){
						cout << "rect" << endl;
						VideoSend.pub_video_left_rectified.publish(VideoSend.video_left_);
					}

					VideoSend.pub_video_left.publish(VideoSend.video_left_);
					VideoSend.directory_video.data = VideoSend.video_name_prop;
					VideoSend.pub_directory.publish(VideoSend.directory_video);
				}
				// Convert Images to sensor_msgs::Images

			}
		}

		ros::spinOnce();
		loop_rate.sleep();
	}
}
