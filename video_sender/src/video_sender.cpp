/* image_sender.cpp
 *
 *  Created on: Sept 16, 2014
 *      Author: Veronica Penza
 */

#include "video_sender/video_sender.hpp"

////srv callback
//bool VIDEOSender::add(video_sender::FramesRS::Request  &req,
//         video_sender::FramesRS::Response &res)
//{
//	this->tracking_evaluation = req.req_img;
//	std::cout << "tracking_evaluation" << this->tracking_evaluation << std::endl;
//	if(req.req_img){
//
//		std::cout << "response" << std::endl;
//		this->read_videoFrame(req.nframe);
//		// Convert Images to sensor_msgs::Images
//		this->fromMattoImage(this->img_left, &this->img_left_, sensor_msgs::image_encodings::RGB8);
//		this->fromMattoImage(this->img_right, &this->img_right_, sensor_msgs::image_encodings::RGB8);
//
//		//publishing
//		if (this->isRectified_prop){
//			this->pub_video_left_rectified.publish(this->img_left_);
//			this->pub_video_right_rectified.publish(this->img_right_);
//		}
//		else{
//			//images
//			this->pub_video_left.publish(this->img_left_);
//			this->pub_video_right.publish(this->img_right_);
//		}
//
//		this->directory_video.data = this->video_name_prop;
//		this->pub_directory.publish(this->directory_video);
//	}
//
//  return true;
//}

bool VIDEOSender::send_frame(video_sender::FramesRS::Request  &req,
		video_sender::FramesRS::Response &res)
{
	this->tracking_evaluation = req.req_img;
	this->stereo = req.stereo;
	static int videoID = 0;

	std::cout << "tracking_evaluation " << this->tracking_evaluation << std::endl;

	if(req.req_img)
	{
		if(req.first_frame)
		{
			videoID = 0;

			if(stereo)
			{
				this->open_stereo_video();
			}
			else
			{
				this->open_mono_video();
			}
		}

		this->grab_videoFrame(this->stereo);

		if(stereo)
		{
			std::cout << "stereo ---------" << std::endl;
			// Convert Images to sensor_msgs::Images
			this->fromMattoImage(this->img_left, &this->img_left_, sensor_msgs::image_encodings::RGB8);
			this->fromMattoImage(this->img_right, &this->img_right_, sensor_msgs::image_encodings::RGB8);

			res.imageL = this->img_left_;
			res.imageL.header.seq = videoID;
			res.imageL.encoding = "bgr8";
			res.imageR = this->img_right_;
			res.imageR.header.seq = videoID;
			res.imageR.encoding = "bgr8";

			//			//publishing
			//			if (this->isRectified_prop){
			//				this->pub_video_left_rectified.publish(this->img_left_);
			//				this->pub_video_right_rectified.publish(this->img_right_);
			//			}
			//			else{
			//				//images
			//				this->pub_video_left.publish(this->img_left_);
			//				this->pub_video_right.publish(this->img_right_);
			//		}
			this->directory_video.data = this->video_name_prop;
			this->pub_directory.publish(this->directory_video);

		}
		else
		{

			std::cout << "mono ---------" << std::endl;
			//			// Convert Images to sensor_msgs::Images
			this->fromMattoImage(this->img_left, &this->img_left_, sensor_msgs::image_encodings::RGB8);

			res.imageL = this->img_left_;
			res.imageL.header.seq = videoID;
			res.imageL.encoding = "bgr8";

			//			//publishing
			//			if (this->isRectified_prop){
			//				this->pub_video_left_rectified.publish(this->img_left_);
			//			}
			//			else{
			//				//images
			//				this->pub_video_left.publish(this->img_left_);
			//			}
			this->directory_video.data = this->video_name_prop;
			this->pub_directory.publish(this->directory_video);

		}
	}

	videoID++;

	return true;
}




//video_name_
void VIDEOSender::callback_dynamic_reconfigure(video_sender::VideoConfig &config, uint32_t level){

	dynamic_reconfig = &config;

	this->video_name_prop = dynamic_reconfig->video_name_;
	this->isRectified_prop = dynamic_reconfig->isRectified_;

	cout << this->video_name_prop << endl;
	cout << this->isRectified_prop << endl;

}


void VIDEOSender::init_directory(){

	this->directory_video_left = DATASET_FOLDER;
	this->directory_video_right = DATASET_FOLDER;
	this->name_video_left = this->video_name_prop;
	this->name_video_right = this->video_name_prop;
	this->name_video_left_end = "/videoLeft.avi";
	this->name_video_right_end = "/videoRight.avi";
	this->name_oneVideo_end = "/oneVideo.avi";

}

void VIDEOSender::fromMattoImage(cv::Mat img_in, sensor_msgs::Image* img_out, std::string encoding){
	// Convert Images to sensor_msgs::Images

	cv_bridge::CvImage video;
	video.encoding = sensor_msgs::image_encodings::RGB8 ;
	video.image = img_in;
	video.toImageMsg(*img_out);
	img_out->header.stamp = ros::Time::now();
	img_out->header.frame_id = "depth_image";
	img_out->encoding = encoding ;
	img_out->width = img_in.cols;
	img_out->height = img_in.rows;

}


bool VIDEOSender::init(){

	image_transport::ImageTransport it(this->n);
	this->init_directory();
	//	this->pub_video_left = it.advertise("/image/left/rgb", 1);
	//	this->pub_video_right = it.advertise("/image/right/rgb", 1);
	//	this->pub_video_left = it.advertise("/uralp/camera_left/image", 1);
	//	this->pub_video_right = it.advertise("/uralp/camera_right/image", 1);
	this->pub_video_left = it.advertise("/iit/ecm/left", 1);
	this->pub_video_right = it.advertise("/iit/ecm/right", 1);
	this->pub_video_left_rectified = it.advertise("/image/left/rectified", 1);
	this->pub_video_right_rectified = it.advertise("/image/right/rectified", 1);
	this->pub_directory = this->n.advertise<std_msgs::String>("image/directory", 1000);
	this->isRectified_prop = false;

	this->service = n.advertiseService("/frame_request", &VIDEOSender::send_frame, this);
	return true;
}

bool VIDEOSender::open_mono_video(){

	cout<< "Opening new video Left:" << this->directory_video_left + this->name_video_left + this->name_video_left_end << endl;

	this->capture_left.open(this->directory_video_left + this->name_video_left + this->name_video_left_end);

	if(this->capture_left.isOpened()){

		this->video_width = this->capture_left.get(CV_CAP_PROP_FRAME_WIDTH);
		this->video_height = this->capture_left.get(CV_CAP_PROP_FRAME_HEIGHT);

		this->video_left.create(this->video_height,this->video_width, CV_8UC3);
		return true;
	}
	else {
		std::cout << "[ERROR] Problem in opening the video" << std::endl;
		return false;
	}
}

bool VIDEOSender::open_stereo_video(){

	cout<< "Opening new video Left:" << this->directory_video_left + this->name_video_left + this->name_video_left_end << endl;
	cout<< "Opening new video Right:" << this->directory_video_right + this->name_video_right + this->name_video_right_end << endl;

	this->capture_left.open(this->directory_video_left + this->name_video_left + this->name_video_left_end);
	this->capture_right.open(this->directory_video_right + this->name_video_right + this->name_video_right_end);

	if(this->capture_left.isOpened() && this->capture_right.isOpened()){

		this->video_width = this->capture_left.get(CV_CAP_PROP_FRAME_WIDTH);
		this->video_height = this->capture_left.get(CV_CAP_PROP_FRAME_HEIGHT);

		this->video_left.create(this->video_height,this->video_width, CV_8UC3);
		this->video_right.create(this->video_height,this->video_width, CV_8UC3);
		return true;
	}
	else {
		std::cout << "[ERROR] Problem in opening the video" << std::endl;
		return false;
	}
}

bool VIDEOSender::read_video(bool stereo){

	if(stereo){
		if(this->capture_left.isOpened() && this->capture_right.isOpened()){
			this->capture_left.read(this->video_left);
			this->capture_right.read(this->video_right);
			return true;
		}
		else{
			this->open_stereo_video();
			return true;
		}
	}
	else
	{
		if(this->capture_left.isOpened() ){
			this->capture_left.read(this->video_left);
			return true;
		}
		else{
			this->open_mono_video();
			return true;
		}
	}
}

void VIDEOSender::read_videoFrame(int i){


	this->name_video_left_end = ".png";
	this->name_video_right_end = ".png";

	char inttmp[32];
	sprintf(inttmp, "%04d", i);

	cout << "Reading Frame " << inttmp << endl;

	this->img_left = imread(this->directory_video_left + this->name_video_left + "/imL" + inttmp + this->name_video_left_end, CV_LOAD_IMAGE_COLOR);
	this->img_right = imread(this->directory_video_right + this->name_video_right + "/imR" + inttmp + this->name_video_right_end, CV_LOAD_IMAGE_COLOR);

	cout << "Reading: " << this->directory_video_left + this->name_video_left + "/imL" + inttmp + this->name_video_left_end << endl;

	// Check for invalid input
	if(! this->img_left.data && ! this->img_right.data)
	{
		if(!this->img_left.data ){
			cout <<  "Could not open or find the left frame" << std::endl ;
		}
		else if(! this->img_right.data){
			cout <<  "Could not open or find the right frame" << std::endl ;
		}
	}

}

void VIDEOSender::grab_videoFrame(bool stereo){

	if(!stereo)
	{
		if(this->capture_left.isOpened()){
			this->capture_left.read(this->img_left);
		}
		else{
			this->open_mono_video();
			this->capture_left.read(this->img_left);
		}
	}
	else
	{
		if(this->capture_left.isOpened() && this->capture_right.isOpened()){
			this->capture_left.read(this->img_left);
			this->capture_right.read(this->img_right);
		}
		else{
			this->open_stereo_video();
			this->capture_left.read(this->img_left);
			this->capture_right.read(this->img_right);
		}

	}

}
