/*
 * stereo_calibration.hpp
 *
 *  Created on: Sept 16, 2014
 *      Author: Veronica Penza
 */


#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include <string>
#include <sensor_msgs/image_encodings.h>

//OpenCV
#include <cv_bridge/cv_bridge.h>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>

//ROS
#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/Bool.h"
#include "std_msgs/Int32.h"
#include <sensor_msgs/fill_image.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/CameraInfo.h>
#include <std_msgs/Int32.h>
#include <sensor_msgs/image_encodings.h>
#include <image_transport/image_transport.h>
#include <sensor_msgs/Image.h>

#include <dynamic_reconfigure/server.h>
#include <video_sender/VideoConfig.h>

#include "video_sender/FramesRS.h"

#include <enVisorsLib/paths.h>


using namespace cv;
using namespace std;
using namespace sensor_msgs;
using namespace sensor_msgs::image_encodings;


class VIDEOSender{
public:

	String directory_video_left;
	String directory_video_right;
	string name_video_left;
	string name_video_right;
	string name_video_left_end;
	string name_video_right_end;
	string name_oneVideo_end;

	Mat video_left;
	Mat video_right;
	Mat img_left;
	Mat img_right;

	double video_width;
	double video_height;

	VideoCapture capture_left;
	VideoCapture capture_right;
	sensor_msgs::Image video_left_;
	sensor_msgs::Image video_right_;

	sensor_msgs::Image img_left_;
	sensor_msgs::Image img_right_;

	string video_name_prop;
	string video_name_old;
	int f_number_prop;
	bool tracking_evaluation;

	bool stereo;
	VideoCapture capture_oneVideo;
	cv::Mat video;

	ros::NodeHandle n;
	ros::ServiceServer service;
	image_transport::Publisher pub_video_left;
	image_transport::Publisher pub_video_right;
	image_transport::Publisher pub_video_left_rectified;
	image_transport::Publisher pub_video_right_rectified;

	video_sender::VideoConfig* dynamic_reconfig;
	dynamic_reconfigure::Server<video_sender::VideoConfig> server;
	dynamic_reconfigure::Server<video_sender::VideoConfig>::CallbackType f;

	ros::Publisher pub_directory;
	std_msgs::String directory_video;


	bool isRectified_prop;
	bool send_frame(video_sender::FramesRS::Request  &req,
         video_sender::FramesRS::Response &res);
	void callback_dynamic_reconfigure(video_sender::VideoConfig &config, uint32_t level);
	void fromMattoImage(cv::Mat img_in, sensor_msgs::Image* img_out, std::string encoding);
	bool init();
	void init_directory();
	bool open_stereo_video();
	bool open_mono_video();
	bool read_video(bool stereo);
	void read_videoFrame(int i);
	void grab_videoFrame(bool stereo);

};
