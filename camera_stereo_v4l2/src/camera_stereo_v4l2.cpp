/*
 * camera_stereo_v4l2.cpp
 *
 *  Created on: Dec 2, 2016
 *      Author: veronica
 */

#include<camera_stereo_v4l2.hpp>


CameraStereov4l2::CameraStereov4l2() :
it(n),
loop_rate(100)
{
	//this->imgL_pub = this->it.advertise("/uralp/camera_left/image", 1);
	//this->imgR_pub = this->it.advertise("/uralp/camera_right/image", 1);
	this->imgL_pub = this->it.advertise("/iit/ecm/left", 1);
	this->imgR_pub = this->it.advertise("/iit/ecm/right", 1);
}

CameraStereov4l2::~CameraStereov4l2()
{

}

void CameraStereov4l2::grab_images()
{
	this->cam_node_left->grabImage();
	this->cam_node_right->grabImage();

	while(!this->cam_node_left->getImage(this->camera_info.camImg_left) || !this->cam_node_right->getImage(this->camera_info.camImg_right))
	{
		this->camera_info.camImg_left.resize(this->imgL.rows, this->imgL.cols);
		std::cout << "Waiting for images" << std::endl;
		this->cam_node_left->grabImage();
		this->cam_node_right->grabImage();
		usleep(1E5);
	}

}

void CameraStereov4l2::grab_start()
{
	this->grab_images();
	this->cam_node_left->grabStart();
	this->cam_node_right->grabStart();
}

void CameraStereov4l2::grab_stop()
{
	this->cam_node_left->grabStop();
	this->cam_node_right->grabStop();
}

void CameraStereov4l2::init()
{
	this->n.getParam("/camera_info/width", this->camera_info.width);
	this->n.getParam("/camera_info/height", this->camera_info.height);
	this->n.getParam("/camera_info/left/dev", this->camera_info.dev_left);
	this->n.getParam("/camera_info/right/dev", this->camera_info.dev_right);

	std::cout << "width " << this->camera_info.width << " , " << "height " << this->camera_info.height << std::endl;

	this->cam_node_left = new Cam_v4l2(this->camera_info.width,this->camera_info.height);
	if(!this->cam_node_left->initCamera(this->camera_info.dev_left))
	{
		std::cout << "[ERROR] Problem in left camera initialization" << std::endl;
	}

	this->cam_node_right = new Cam_v4l2(this->camera_info.width,this->camera_info.height);
	if(!this->cam_node_right->initCamera(this->camera_info.dev_right))
	{
		std::cout << "[ERROR] Problem in right camera initialization" << std::endl;
	}
}

void CameraStereov4l2::publish_images()
{
	this->cam_node_left->getImage(this->camera_info.camImg_left);
	this->cam_node_right->getImage(this->camera_info.camImg_right);

	this->cam_node_left->fromCamImagetoMat(this->camera_info.camImg_left, this->imgL);
	this->cam_node_right->fromCamImagetoMat(this->camera_info.camImg_right, this->imgR);

	fromMattoImage(this->imgL, &this->imgL_, sensor_msgs::image_encodings::RGB8);
	fromMattoImage(this->imgR, &this->imgR_, sensor_msgs::image_encodings::RGB8);

	this->imgL_pub.publish(this->imgL_);
	this->imgR_pub.publish(this->imgR_);
}
