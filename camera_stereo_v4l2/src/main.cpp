/*
 * main.cpp
 *
 *  Created on: Dec 2, 2016
 *      Author: veronica
 */




#include<camera_stereo_v4l2.hpp>



int main(int argc, char **argv){

	// ROS Initiliazation
	ros::init(argc, argv, "CameraStereov4l2");

	// local variable declaration:

	CameraStereov4l2 stereo;

	stereo.init();
	stereo.grab_start();

	while(ros::ok())
	{
		//stereo.grab_images();
		stereo.publish_images();

		ros::spinOnce();
		stereo.loop_rate.sleep();
	}

	stereo.grab_stop();
}
