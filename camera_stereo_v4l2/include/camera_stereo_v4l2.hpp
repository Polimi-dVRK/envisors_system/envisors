/*
 * camera_stereo_v4l2.hpp
 *
 *  Created on: Dec 2, 2016
 *      Author: veronica
 */

#ifndef SRC_CAMERA_STEREO_V4L2_INCLUDE_CAMERA_STEREO_V4L2_HPP_
#define SRC_CAMERA_STEREO_V4L2_INCLUDE_CAMERA_STEREO_V4L2_HPP_


#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <pthread.h>
#include <unistd.h>

// opencv
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>



//ros
#include "ros/ros.h"
#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>
#include <image_transport/image_transport.h>

#include <enVisorsLib/paths.h>
#include <enVisorsLib/define.hpp>
#include <enVisorsLib/common.hpp>
#include <cam_lib/cam_interface.h>
#include <cam_lib/cam_v4l2.h>

typedef message_filters::sync_policies::ApproximateTime<sensor_msgs::Image, sensor_msgs::Image> MySyncPolicy;


struct CameraInfo
{

	int camera_model;

	CamImage camImg_left;
	CamImage camImg_right;
	std::string dev_left;
	std::string dev_right;
	int width;
	int height;
};

class CameraStereov4l2
{
public:
	CameraStereov4l2();
	~CameraStereov4l2();

	Cam_interface* cam_node_left;
	Cam_interface* cam_node_right;
	CameraInfo camera_info;

	cv::Mat imgL;
	cv::Mat imgR;

	void init();
	void grab_images();

	void grab_start();
	void grab_stop();

	void publish_images();

	ros::Subscriber datasetName_sub;
	ros::NodeHandle n;
	image_transport::ImageTransport it;
	ros::Rate loop_rate;
	sensor_msgs::Image imgL_;
	sensor_msgs::Image imgR_;

	image_transport::Publisher imgL_pub;
	image_transport::Publisher imgR_pub;

};



#endif /* SRC_CAMERA_STEREO_V4L2_INCLUDE_CAMERA_STEREO_V4L2_HPP_ */
