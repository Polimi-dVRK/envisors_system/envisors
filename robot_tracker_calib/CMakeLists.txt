cmake_minimum_required(VERSION 2.8.3)
project(robot_tracker_calib)
set(CMAKE_CXX_STANDARD 14)

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED
        enVisorsLib
        roscpp
        image_transport
        cv_bridge
        tf
        kdl_conversions
        )

###################################
## catkin specific configuration ##
###################################
catkin_package()

###########
## Build ##
###########

## Specify additional locations of header files
## Your package locations should be listed before other locations
include_directories(include ${catkin_INCLUDE_DIRS})

add_executable(robot_tracker_calib src/robot_tracker_calib.cpp src/main.cpp)
target_link_libraries(robot_tracker_calib ${catkin_LIBRARIES})
