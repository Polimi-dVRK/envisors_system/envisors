/*
 * robot_tracker_calib.hpp
 *
 *  Created on: Oct 10, 2016
 *      Author: veronica
 */

#ifndef ROBOT_TRACKER_CALIB_INCLUDE_ROBOT_TRACKER_CALIB_ROBOT_TRACKER_CALIB_HPP_
#define ROBOT_TRACKER_CALIB_INCLUDE_ROBOT_TRACKER_CALIB_ROBOT_TRACKER_CALIB_HPP_


#include <stdio.h>
#include <sstream>
#include <vector>

//ROS
#include <ros/ros.h>
#include "ros/rate.h"
#include "std_msgs/String.h"
#include <sensor_msgs/fill_image.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/CameraInfo.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Bool.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/Image.h>
#include <cv_bridge/cv_bridge.h>
#include <ros/package.h>
#include <image_transport/image_transport.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseStamped.h>
#include <visualization_msgs/Marker.h>

//OpenCV
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <cv_bridge/cv_bridge.h>
#include "opencv/cxcore.h"
#include <opencv2/core/core.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

//tf
#include <tf/LinearMath/Matrix3x3.h>
#include <kdl/frames.hpp>
#include <kdl_conversions/kdl_msg.h>

// My Library
#include"enVisorsLib/common.hpp"
#include"enVisorsLib/paths.h"
#include"enVisorsLib/define.hpp"

#include <eigen3/Eigen/SVD>
#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Sparse>
#include <eigen3/Eigen/Geometry>

#include <tf/transform_listener.h>


typedef std::pair <Eigen::Matrix3d, Eigen::Vector3d> TransformType;

typedef std::vector <Eigen::Vector3d> PointsType;

struct Pose
{
    cv::Mat quat;
    cv::Mat t;

};

class RobotTrackerCalib
{
public:

    /*variables */
    std::string path;
    geometry_msgs::Pose robotPose;
    geometry_msgs::Pose trackerTipPose;
    KDL::Frame trackerPointerTipPose;
    KDL::Frame robotTipPose;

    cv::Mat t_tip_;

    TransformType T_robot_tracker;
    int robot_type;
    int tracker_type;
    int npoints;
    std::string robot_name;
    std::string tracker_name;

    /*Param*/

    Pose robot_pose;
    Pose tracker_tip_pose;
    std::vector <Eigen::Vector3d> robot_points;
    std::vector <Eigen::Vector3d> tracker_points;
    enum
    {
        ROBOT, TRACKER
    };

    RobotTrackerCalib();

    /*methods */
    void get_points(std::vector <Eigen::Vector3d> &points, int device);
    bool compute_extrinsics();
    void compute_error();

    /*callback */
    void robotPose_callback(const geometry_msgs::PosePtr &r);
    void robotPoseStamped_callback(const geometry_msgs::PoseStampedPtr &r_s);
    void robotPose2Stamped_callback(const geometry_msgs::PoseStampedPtr &r_s);
    void trackerTipPose_callback(const geometry_msgs::PosePtr &tp);
    void trackerTipPoseStamped_callback(const geometry_msgs::PoseStampedPtr &tp_s);
    void trackerTipMarker_callback(const visualization_msgs::MarkerPtr &tp_s);
    TransformType computeRigidTransform(const PointsType &src, const PointsType &dst);

    /*ROS parameters */
    ros::NodeHandle n;
    ros::Subscriber robotPose_sub;
    ros::Subscriber robotPose2_sub;
    ros::Subscriber robotPose2Stamped_sub;
    ros::Subscriber trackerTipPose_sub;
    ros::Subscriber trackerTipPoseStamped_sub;
    tf::TransformListener robot_listener;
    tf::StampedTransform tool_transform;

};


#endif /* ROBOT_TRACKER_CALIB_INCLUDE_ROBOT_TRACKER_CALIB_ROBOT_TRACKER_CALIB_HPP_ */
