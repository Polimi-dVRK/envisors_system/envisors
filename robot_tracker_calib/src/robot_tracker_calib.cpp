/*
 * robot_tracker_calib.cpp
 *
 *  Created on: Oct 10, 2016
 *      Author: veronica
 */
/*
 * cam_dev_calib.cpp
 *
 *  Created on: Oct 10, 2016
 *      Author: veronica
 */



#include "robot_tracker_calib/robot_tracker_calib.hpp"


RobotTrackerCalib::RobotTrackerCalib()
{
    //init path
    this->path = CONFIG_FILE_FOLDER;

    this->n.getParam("/robot_info/model", this->robot_type);
    this->n.getParam("/tracker_info/model", this->tracker_type);

    if (this->tracker_type == OPTITRACK) {
        this->tracker_name = "optitrack";
        //if tracker calibration
        //		this->trackerTipPose_sub = n.subscribe("/optitrack/pointer/pose", 1,
        //				&ExtrinsicCalibration::trackerTipPose_callback, this);
        this->trackerTipPoseStamped_sub = n.subscribe("/optitrack/pointer/pose", 1,
                                                      &RobotTrackerCalib::trackerTipPoseStamped_callback, this);

        readTranslation(
            this->path + "tracker/" + this->tracker_name + "/Tracker_tip.xml",
            this->t_tip_);
    }
    else if (this->tracker_type == VICRA) {
        this->tracker_name = "vicra";
        this->trackerTipPoseStamped_sub = n.subscribe("/vicra/tool_1", 1,
                                                      &RobotTrackerCalib::trackerTipMarker_callback, this);
    }
    else {
        std::cout << "[ERROR] Please insert robot type " << std::endl;
    }


    if (this->robot_type == UR5) {
        this->robot_name = "ur5";

    }
    else if (this->robot_type == LWR4) {
        this->robot_name = "lwr4";
        //KUKA Nearlab
        this->robotPose_sub = n.subscribe("/lwrCartCurrent", 1,
                                          &RobotTrackerCalib::robotPose_callback, this);
    }
    else if (this->robot_type == PHANTOM) {
        this->robot_name = "phantom";
        //Phantom - IIT
        this->robotPose_sub = n.subscribe("/PhantomOmni1_current_transform", 1,
                                          &RobotTrackerCalib::robotPose_callback, this);
    }
    else if (this->robot_type == DAVINCI_ARMS) {
        this->robot_name = "davinci";
        //Phantom - IIT
        this->robotPose_sub = n.subscribe("/dvrk/PSM1/position_cartesian_current", 1,
                                          &RobotTrackerCalib::robotPoseStamped_callback, this);
        this->trackerTipPoseStamped_sub = n.subscribe("/dvrk/PSM2/position_cartesian_current", 1,
                                                      &RobotTrackerCalib::robotPose2Stamped_callback, this);
    }
    else {
        std::cout << "[ERROR] Please insert robot type " << std::endl;
    }
}

void RobotTrackerCalib::robotPose_callback(const geometry_msgs::PosePtr &pose)
{
    robotPose = *pose;
    tf::poseMsgToKDL(robotPose, this->robotTipPose);
}

void RobotTrackerCalib::robotPoseStamped_callback(const geometry_msgs::PoseStampedPtr &pose)
{
    // convert to Pose
    robotPose = pose->pose;
    tf::poseMsgToKDL(this->robotPose, this->robotTipPose);
}

/*
 * Really big gotcha here ... in cases in which the robot has two arms (e.g. daVinci) the second arm is treated as a
 * tracker to compute the transform from tracker to arm. In reality this is the arm to arm
 */
void RobotTrackerCalib::robotPose2Stamped_callback(const geometry_msgs::PoseStampedPtr &pose)
{
    trackerTipPose = pose->pose;
    tf::poseMsgToKDL(this->trackerTipPose, this->trackerPointerTipPose);
}

void RobotTrackerCalib::trackerTipPoseStamped_callback(const geometry_msgs::PoseStampedPtr &pose)
{
    // conver to Pose
    trackerTipPose = pose->pose;

    //convert to KLD
    KDL::Vector t_tip_kdl;
    t_tip_kdl[0] = this->t_tip_.at<float>(0, 0);
    t_tip_kdl[1] = this->t_tip_.at<float>(1, 0);
    t_tip_kdl[2] = this->t_tip_.at<float>(2, 0);

    tf::poseMsgToKDL(trackerTipPose, trackerPointerTipPose);
    KDL::Vector trackeTipPosition = trackerPointerTipPose * t_tip_kdl;

    this->trackerTipPose.position.x = trackeTipPosition[0];
    this->trackerTipPose.position.y = trackeTipPosition[1];
    this->trackerTipPose.position.z = trackeTipPosition[2];

}

void RobotTrackerCalib::trackerTipMarker_callback(const visualization_msgs::MarkerPtr &pose)
{
    // conver to Pose
    trackerTipPose = pose->pose;
    tf::poseMsgToKDL(trackerTipPose, trackerPointerTipPose);
}

void RobotTrackerCalib::get_points(std::vector <Eigen::Vector3d> &points, int device)
{
    /*Save 3D points with robot tip*/
    ROS_INFO("Acquire points pressing l ; ");

    Eigen::Vector3d position;
    int counter = 0;

    while (1) {

        ros::spinOnce();

        int c = cv::waitKey(15);
        if (c == 'l') {

            switch (device) {
                case TRACKER:
                    position[0] = this->trackerPointerTipPose.p[0];
                    position[1] = this->trackerPointerTipPose.p[1];
                    position[2] = this->trackerPointerTipPose.p[2];
                    break;

                case ROBOT:
                    if (robot_name == "ur5") {
                        this->robot_listener.lookupTransform("/base_link", "/tool0_controller",
                                                             ros::Time(0), this->tool_transform);

                        position[0] = this->tool_transform.getOrigin().x();
                        position[1] = this->tool_transform.getOrigin().y();
                        position[2] = this->tool_transform.getOrigin().z();
                    }
                    else {
                        position[0] = this->robotTipPose.p[0];
                        position[1] = this->robotTipPose.p[1];
                        position[2] = this->robotTipPose.p[2];
                    }

                    break;

                default:
                    break;
            }

            ROS_INFO_STREAM(
                "Acquired point (x, y, z) = (" << position[0] << ", " << position[1] << ", " << position[2] << ")");
            points.push_back(position);

            counter += 1;
            if (counter == npoints) {
                break;
            }
        }
    }
}

TransformType RobotTrackerCalib::computeRigidTransform(const PointsType &src, const PointsType &dst)
{
    assert(src.size() == dst.size());
    int pairSize = src.size();
    Eigen::Vector3d center_src(0, 0, 0), center_dst(0, 0, 0);
    for (int i = 0; i < pairSize; ++i) {
        center_src += src[i];
        center_dst += dst[i];
    }
    center_src /= (double) pairSize;
    center_dst /= (double) pairSize;

    Eigen::MatrixXd S(pairSize, 3), D(pairSize, 3);
    for (int i = 0; i < pairSize; ++i) {
        for (int j = 0; j < 3; ++j)
            S(i, j) = src[i][j] - center_src[j];
        for (int j = 0; j < 3; ++j)
            D(i, j) = dst[i][j] - center_dst[j];
    }
    Eigen::MatrixXd Dt = D.transpose();
    Eigen::Matrix3d H = Dt * S;
    Eigen::Matrix3d W, U, V;

    Eigen::JacobiSVD <Eigen::MatrixXd> svd;
    Eigen::MatrixXd H_(3, 3);
    for (int i = 0; i < 3; ++i) for (int j = 0; j < 3; ++j) H_(i, j) = H(i, j);
    svd.compute(H_, Eigen::ComputeThinU | Eigen::ComputeThinV);
    if (!svd.computeU() || !svd.computeV()) {
        ROS_ERROR("Decomposition error");
        return std::make_pair(Eigen::Matrix3d::Identity(), Eigen::Vector3d::Zero());
    }
    Eigen::Matrix3d Vt = svd.matrixV().transpose();
    Eigen::Matrix3d R = svd.matrixU() * Vt;
    Eigen::Vector3d t = center_dst - R * center_src;

    return std::make_pair(R, t);
}

bool RobotTrackerCalib::compute_extrinsics()
{

    cvNamedWindow("Pose", 1);

    //initialization
    this->npoints = 4;

    ROS_INFO("Get robot points ...");
    this->get_points(this->robot_points, ROBOT);
    ROS_INFO("Robot points saved!");

    ROS_INFO("Get tracker points ...");
    this->get_points(this->tracker_points, TRACKER);
    ROS_INFO("Tracker points saved!");

    /*Compute 2D-3D calibration*/
    ROS_INFO("Computing extrinsic calibration ...");

    this->T_robot_tracker = computeRigidTransform(this->robot_points, this->tracker_points);

    ROS_INFO_STREAM("Rotation " << this->T_robot_tracker.first);
    ROS_INFO_STREAM("Translation " << this->T_robot_tracker.second);

    ROS_INFO("Done computing extrinsics!");
    // conversion form eigen to Mat TODO

    cv::Mat R_robot_tracker, t_robot_tracker;
    R_robot_tracker.create(3, 3, CV_64FC1);
    t_robot_tracker.create(3, 1, CV_64FC1);

    R_robot_tracker.at<double>(0, 0) = this->T_robot_tracker.first.data()[0];
    R_robot_tracker.at<double>(1, 0) = this->T_robot_tracker.first.data()[1];
    R_robot_tracker.at<double>(2, 0) = this->T_robot_tracker.first.data()[2];
    R_robot_tracker.at<double>(0, 1) = this->T_robot_tracker.first.data()[3];
    R_robot_tracker.at<double>(1, 1) = this->T_robot_tracker.first.data()[4];
    R_robot_tracker.at<double>(2, 1) = this->T_robot_tracker.first.data()[5];
    R_robot_tracker.at<double>(0, 2) = this->T_robot_tracker.first.data()[6];
    R_robot_tracker.at<double>(1, 2) = this->T_robot_tracker.first.data()[7];
    R_robot_tracker.at<double>(2, 2) = this->T_robot_tracker.first.data()[8];

    ROS_INFO_STREAM("R_robot_tracker " << R_robot_tracker);

    t_robot_tracker.at<double>(0, 0) = this->T_robot_tracker.second.x();
    t_robot_tracker.at<double>(1, 0) = this->T_robot_tracker.second.y();
    t_robot_tracker.at<double>(2, 0) = this->T_robot_tracker.second.z();

    /*Save Extrinsic calibration*/
    saveExtrisicParamsCamera(
        this->path + "robot/" + this->robot_name + "/Robot_Tracker_Transform.xml", R_robot_tracker, t_robot_tracker);

    ROS_INFO_STREAM(
        "Transformation saved in: " << (this->path + "robot/" + this->robot_name + "/Robot_Tracker_Transform.xml"));

    robot_points.clear();
    tracker_points.clear();

    ////////// ERROR EVALUATION /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    bool compute_error = true;
    if (compute_error)
        this->compute_error();


    return true;
}

void RobotTrackerCalib::compute_error()
{
    ROS_INFO(" --------------ERROR EVALUATION--------- ");

    //initialization
    this->npoints = 4;

    ROS_INFO("Get robot points ...");
    this->get_points(robot_points, ROBOT);
    ROS_INFO("Robot points saved!");

    ROS_INFO("Get tracker points ...");
    this->get_points(tracker_points, TRACKER);
    ROS_INFO("Tracker points saved!");


    std::vector <Eigen::Vector3d> robot_points_t;
    robot_points_t.resize(robot_points.size());

    for (int i = 0; i < robot_points.size(); i++) {
        robot_points_t[i] = T_robot_tracker.first * robot_points[i];
        robot_points_t[i] += T_robot_tracker.second;
    }


    ////// Computing the reprojection error ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    float temp = 0;
    float temp1 = 0;
    float errorpos = 0;
    float temp2 = 0;
    float totalErr = 0;
    float sum = 0;
    float RMS = 0;

    std::vector<float> err;

    err.resize(npoints);

    CvScalar average = cvScalarAll(0);
    CvScalar std = cvScalarAll(0);

    for (int j = 0; j < npoints; j++) {

        ROS_INFO_STREAM("tracker points" << tracker_points[j]);
        ROS_INFO_STREAM("robot points projected" << robot_points_t[j]);

        temp = (tracker_points[j][0] - robot_points_t[j][0]) * (tracker_points[j][0] - robot_points_t[j][0]) +
            (tracker_points[j][1] - robot_points_t[j][1]) * (tracker_points[j][1] - robot_points_t[j][1]) +
            (tracker_points[j][2] - robot_points_t[j][2]) * (tracker_points[j][2] - robot_points_t[j][2]);

        err[j] = temp;

    }
    for (int j = 0; j < npoints; j++) {
        sum += err[j];
    }

    sum = sum / npoints;

    RMS = sqrt(sum);
    ROS_INFO_STREAM("Root Mean Square Error:" << RMS);

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    err.clear();
}







