/*
 * main.cpp
 *
 *  Created on: Oct 10, 2016
 *      Author: veronica
 */



#include "robot_tracker_calib/robot_tracker_calib.hpp"


int main(int argc, char **argv)
{

    ros::init(argc, argv, "robot_tracker_calibration");
    cv::startWindowThread();

    // Definition of Class
    RobotTrackerCalib RTCalib;
    RTCalib.compute_extrinsics();
}




