/*  camera_device_calibration.hpp
 *
 *  Created on: Novembre 8, 2015
 *  Author: Veronica Penza
 */

#include <opencv2/core/core.hpp>
#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <geometry_msgs/PoseStamped.h>

// My Library
#include"enVisorsLib/common.hpp"
#include"enVisorsLib/paths.h"
#include"enVisorsLib/define.hpp"

class CamDevCalibration
{

private: /* ROS Paramaters */

	/// If set to true then once the extrinsics have been computed a new set of points will be
	/// acquired and used to compute the projection error.
	bool _compute_error = true;

	ros::NodeHandle _node;
    ros::NodeHandle _pnode;

    ros::Subscriber _robot_pose_sub;
    ros::Subscriber _camera_arm_pose_sub;

    image_transport::ImageTransport _it;
    image_transport::Subscriber _image_rgb_sub;
    image_transport::Subscriber _image_grey_sub;

    geometry_msgs::Pose _robot_pose;
    geometry_msgs::Pose _camera_arm_pose;

private: /* Member Variables */
    cv::Mat _img_rgb;
    cv::Mat _img_grey;

public:
	/*variables */
	std::string path;

	cv::Size img_size;

	cv::Mat R, T, R_r;
	cv::Mat R_endoscope_camera;
	cv::Mat t_endoscope_camera;
	KDL::Frame T_holder_robot;
	cv::Mat R_holder_robot;
	cv::Mat t_holder_robot;

public:
	CamDevCalibration();

    void acquire_robot_points(
        std::vector<cv::Point2f> & image_points,
        std::vector<cv::Point3f> & object_points,
        unsigned int number_of_points
    );

    void compute_robot_holder_calib();
    bool compute_camera_robot_extrinsics();

public:	/* ROS Callbacks */

    void image_rgb_cb(const sensor_msgs::ImageConstPtr& rgb_l);

    void image_grey_cb(const sensor_msgs::ImageConstPtr& rgb_l);

    void robot_pose_cb(const geometry_msgs::PoseStampedPtr& pose);

	void camera_arm_pose_cb(const geometry_msgs::PoseStampedPtr& r_s);

};
