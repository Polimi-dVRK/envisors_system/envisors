/*  camera_device_calibration.cpp
 *
 *  Created on: Novembre 8, 2015
 *  Author: Veronica Penza
 */


#include <tf/LinearMath/Matrix3x3.h>
#include <kdl_conversions/kdl_msg.h>

#include "camera_device_calibration/camera_device_calibration.hpp"


CamDevCalibration::CamDevCalibration()
    : _it(_node)
{

	// Grab the path used to store the configuration files
	// FIXME: The configurations should be stored in the .ros/ folder
	path = CONFIG_FILE_FOLDER;


	_image_rgb_sub = _it.subscribe(
		"/endoscope/left/image_raw", 1, &CamDevCalibration::image_rgb_cb, this);

    _image_grey_sub = _it.subscribe(
        "/endoscope/left/image_gray", 1, &CamDevCalibration::image_grey_cb, this);


    _robot_pose_sub = _node.subscribe(
		"/dvrk/PSM1/position_cartesian_current", 1, &CamDevCalibration::robot_pose_cb, this);

    _camera_arm_pose_sub = _node.subscribe(
        "/dvrk/ECM/position_cartesian_current", 1, &CamDevCalibration::camera_arm_pose_cb, this); }


void CamDevCalibration::image_rgb_cb(const sensor_msgs::ImageConstPtr& rgb_l){
	fromImagetoMat(*rgb_l, &_img_rgb, rgb_l->encoding);
	img_size = cv::Size(_img_rgb.cols, _img_rgb.rows);
}

void CamDevCalibration::image_grey_cb(const sensor_msgs::ImageConstPtr& img){
    fromImagetoMat(*img, &_img_grey, img->encoding);
}


void CamDevCalibration::robot_pose_cb(const geometry_msgs::PoseStampedPtr& pose)
{
	_robot_pose = pose->pose;
}


void CamDevCalibration::camera_arm_pose_cb(const geometry_msgs::PoseStampedPtr& pose)
{
	_camera_arm_pose = pose->pose;
}

void CamDevCalibration::acquire_robot_points(
    std::vector<cv::Point2f> & image_points,
    std::vector<cv::Point3f> & object_points,
    unsigned int number_of_points
)
{
    // Find the chessboard and compute the image positions of the targets
    while (1) {
        ros::spinOnce();

        if(_img_rgb.empty()) {
            ROS_WARN("No RGB image available yet ...");
            usleep(1E5);
            continue;
        }

        // FIXME: configure board size from parameters
        cv::Size pattern_size(9, 6);
        int board_corners = pattern_size.height * pattern_size.width;
        std::vector<cv::Point2f> corners(board_corners);


        int pattern_was_found = cv::findChessboardCorners(
            _img_grey, pattern_size, corners, CV_CALIB_CB_ADAPTIVE_THRESH|CV_CALIB_CB_FILTER_QUADS);

        if (pattern_was_found && corners.size() == board_corners) {
            cornerSubPix(_img_grey, corners, cvSize(3,3), cvSize(-1,-1),
                         cvTermCriteria(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS,10,1E-4));

            // FIXME: Uniform initialisation of image_points here
            image_points.push_back(corners[pattern_size.width + 1]);
            image_points.push_back(corners[(pattern_size.height - 2) * pattern_size.width + 1]);
            image_points.push_back(corners[pattern_size.width + 3]);
            image_points.push_back(corners[(pattern_size.height - 2) * pattern_size.width + 3]);
            image_points.push_back(corners[pattern_size.width + 5]);
            image_points.push_back(corners[(pattern_size.height - 2) * pattern_size.width + 5]);
            image_points.push_back(corners[pattern_size.width + 7]);
            image_points.push_back(corners[(pattern_size.height - 2) * pattern_size.width + 7]);

            break;
        }

        cv::imshow("Pose", _img_rgb);

        cvWaitKey(250);
    }

    /*Save 3D points with robot tip*/
    ROS_INFO("Acquire points pressing 'l'");

    cv::Point3f position;
    int counter = 0;

    while( 1 ) {
        ros::spinOnce();

        // draw points the target points. The next point to be targeted is colored red.
        for (int i=0; i < number_of_points; i++) {
            auto target_color = (i == counter ? CV_RGB(255, 0, 0) : CV_RGB(0 , 255, 0));
            circle(_img_rgb, image_points[i], 10, target_color, 2);
        }

        //display image
        cv::imshow("Pose",_img_rgb);

        int c = cv::waitKey( 15 );

        if (c == 'l') {
            position.x = _robot_pose.position.x;
            position.y = _robot_pose.position.y;
            position.z = _robot_pose.position.z;
            // FIXME: Uniform initialisation here ?
            object_points.push_back(position);

            ROS_INFO_STREAM(
                "" << "Acquired Robot Point (x, y, z) = (" << position.x << ", "
                   << position.y << ", " << position.z << ")"
            );

            counter += 1;
            if (counter == number_of_points) {
                break;
            }
        }

        cvWaitKey( 5 );
    }
}

bool CamDevCalibration::compute_camera_robot_extrinsics()
{
	cvNamedWindow( "Pose", 1 );

    // The number of points on the image that will be read
    const unsigned int number_of_points = 8;

	std::vector<cv::Point2f> image_points;  // Points to target with the robot
	std::vector<cv::Point3f> object_points; // Robot position when l is pressed
    acquire_robot_points(image_points, object_points, number_of_points);

	/*Compute 2D-3D calibration*/
    ROS_INFO("Computing extrinsic calibration ...");

	cv::Mat cameraMatrix;
	cv::Mat distCoeff;

	readParamsCamera(
			this->path + "/camera/davinci/LeftCameraParameters.xml",
			cameraMatrix,
			distCoeff
    );

	std::vector<cv::Point2f> image_projected;
	std::vector<cv::Point2f> image_viewed;
	solvePnP(object_points, image_points, cameraMatrix, distCoeff, R, T);
	projectPoints(object_points, R, T, cameraMatrix, distCoeff, image_projected);

	//Show points on image
	for (int i=0; i<number_of_points; i++) {
		circle(_img_rgb, image_projected[i], 10, CV_RGB(0,0,255), 2);
	}

	cv::imshow("Pose",_img_rgb);
	cvWaitKey(1);

	Rodrigues(R, R_r);
	projectPoints(object_points, R_r, T, cameraMatrix, distCoeff, image_projected);


	/*Save Extrinsic calibration*/
	saveExtrisicParamsCamera(
			this->path + "camera/davinci/CameraRobotCalibration.xml", R_r, T);

	ROS_INFO("Done computing extrinsics!");

	if(_compute_error)
	{
        image_points.clear();  // Points to target with the robot
        object_points.clear(); // Robot position when l is pressed
        acquire_robot_points(image_points, object_points, number_of_points);

		ROS_INFO("Computing extrinsics for error estimation...");

        solvePnP(object_points, image_points, cameraMatrix, distCoeff, R, T, CV_P3P);
		projectPoints(object_points, R, T, cameraMatrix, distCoeff, image_projected);

		// Show the reprojected points on the image
		for (int i=0; i<number_of_points; i++) {
			circle(_img_rgb, image_projected[i], 10, CV_RGB(0,0,255), 2);
		}

		cv::imshow("Pose",_img_rgb);
		cv::waitKey(1);


        // Compute RMSE of the re-projection error
        unsigned int square_error = 0;
		for (int j=0; j< number_of_points;j++){
			auto err_x = image_points[j].x - image_projected[j].x;
			auto err_y = image_points[j].y - image_projected[j].y;
            square_error += err_x*err_x + err_y*err_y;
		}

		auto RMS = sqrt(square_error / number_of_points);
		ROS_INFO_STREAM("Root Mean Square Error of re-projection:" << RMS);
	}

	return true;
}

void CamDevCalibration::compute_robot_holder_calib()
{

	KDL::Frame T_holder_EE;
	KDL::Frame T_EE_camera;
	KDL::Frame T_camera_robot;

	//convert T_holder_EE  to KLD
	tf::poseMsgToKDL(this->_camera_arm_pose, T_holder_EE);

	// read T_EE_camera
	readExtrisicParamsCamera(this->path +  "camera/davinci/Endoscope_Camera_Transform.xml",
			this->R_endoscope_camera,
			this->t_endoscope_camera);


	// convert T_EE_camera pose to KLD
	T_EE_camera.M = KDL::Rotation(
			this->R_endoscope_camera.at<double>(0,0), this->R_endoscope_camera.at<double>(0,1), this->R_endoscope_camera.at<double>(0,2),
			this->R_endoscope_camera.at<double>(1,0), this->R_endoscope_camera.at<double>(1,1), this->R_endoscope_camera.at<double>(1,2),
			this->R_endoscope_camera.at<double>(2,0), this->R_endoscope_camera.at<double>(2,1), this->R_endoscope_camera.at<double>(2,2));
	T_EE_camera.p[0] = this->t_endoscope_camera.at<double>(0,0);
	T_EE_camera.p[1] = this->t_endoscope_camera.at<double>(1,0);
	T_EE_camera.p[2] = this->t_endoscope_camera.at<double>(2,0);

	//convert T_camera_robot  to KLD
	T_camera_robot.M = KDL::Rotation(
			this->R_r.at<double>(0,0), this->R_r.at<double>(0,1), this->R_r.at<double>(0,2),
			this->R_r.at<double>(1,0), this->R_r.at<double>(1,1), this->R_r.at<double>(1,2),
			this->R_r.at<double>(2,0), this->R_r.at<double>(2,1), this->R_r.at<double>(2,2));
	T_camera_robot.p[0] = this->T.at<double>(0,0);
	T_camera_robot.p[1] = this->T.at<double>(1,0);
	T_camera_robot.p[2] = this->T.at<double>(2,0);

	this->T_holder_robot = T_holder_EE * T_EE_camera * T_camera_robot;

	// convert from T_holder_robot KDL to c::Mat

	this->R_holder_robot.create(3,3, CV_64FC1);
	this->R_holder_robot.at<double>(0,0) = this->T_holder_robot.M.data[0];
	this->R_holder_robot.at<double>(0,1) = this->T_holder_robot.M.data[1];
	this->R_holder_robot.at<double>(0,2) = this->T_holder_robot.M.data[2];
	this->R_holder_robot.at<double>(1,0) = this->T_holder_robot.M.data[3];
	this->R_holder_robot.at<double>(1,1) = this->T_holder_robot.M.data[4];
	this->R_holder_robot.at<double>(1,2) = this->T_holder_robot.M.data[5];
	this->R_holder_robot.at<double>(2,0) = this->T_holder_robot.M.data[6];
	this->R_holder_robot.at<double>(2,1) = this->T_holder_robot.M.data[7];
	this->R_holder_robot.at<double>(2,2) = this->T_holder_robot.M.data[8];

	this->t_holder_robot.create(3,1, CV_64FC1);
	this->t_holder_robot.at<double>(0,0) = this->T_holder_robot.p.data[0];
	this->t_holder_robot.at<double>(1,0) = this->T_holder_robot.p.data[1];
	this->t_holder_robot.at<double>(2,0) = this->T_holder_robot.p.data[2];


	std::cout << "Saving Device_Robot_Transform.xml in " << this->path + "robot/davinci" << std::endl;
	saveExtrisicParamsCamera(this->path + "robot/davinci/Device_Robot_Transform.xml",
			this->R_holder_robot,
			this->t_holder_robot);
}

