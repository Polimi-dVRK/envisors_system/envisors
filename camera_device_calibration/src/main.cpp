/*  main.cpp
 *
 *  Created on: Novembre 8, 2015
 *  Author: Veronica Penza
 */

#include "camera_device_calibration/camera_device_calibration.hpp"


int main(int argc, char **argv)
{

	ros::init(argc, argv, "camera_device_calibration");

	// Definition of Class
	CamDevCalibration CamDevCalib;

	cv::startWindowThread();

	std::cout << "Acquiring. Press 'l' to acquire location." << std::endl;
	CamDevCalib.compute_camera_robot_extrinsics();
	CamDevCalib.compute_robot_holder_calib();
}
