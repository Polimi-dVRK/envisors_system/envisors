/*
 * camera_pose_streamer.hpp
 *
 *  Created on: Oct 23, 2016
 *      Author: veronica
 */

#ifndef CAMERA_POSE_STREAMER_INCLUDE_CAMERA_POSE_STREAMER_CAMERA_POSE_STREAMER_HPP_
#define CAMERA_POSE_STREAMER_INCLUDE_CAMERA_POSE_STREAMER_CAMERA_POSE_STREAMER_HPP_


#include <stdio.h>
#include <sstream>
#include <vector>

//ROS
#include <ros/ros.h>
#include "ros/rate.h"
#include "std_msgs/String.h"
#include <sensor_msgs/fill_image.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/CameraInfo.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Bool.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/Image.h>
#include <cv_bridge/cv_bridge.h>
#include <ros/package.h>
#include <image_transport/image_transport.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseStamped.h>
#include <visualization_msgs/Marker.h>

//tf
#include <tf/LinearMath/Matrix3x3.h>
#include <kdl/frames.hpp>
#include <kdl_conversions/kdl_msg.h>

#include <tf/transform_broadcaster.h>


// My Library
#include"enVisorsLib/common.hpp"
#include"enVisorsLib/paths.h"
#include"enVisorsLib/define.hpp"

class ComputeCameraPose
{
public:


	/*variables */
	std::string path;
	int robot_model;
	int camera_model;
	std::string robot_name;
	std::string camera_name;
	int tracker_model;
	std::string tracker_name;

	geometry_msgs::Pose EndoscopePose;
	geometry_msgs::PoseStamped CameraPose;
	geometry_msgs::PoseStamped CameraPose_inv;

	cv::Mat R_robot_device;
	cv::Mat t_robot_device;
	cv::Mat R_endoscope_camera;
	cv::Mat t_endoscope_camera;
	cv::Mat R_camera_device;
	cv::Mat t_camera_device;

	//KDL
	KDL::Frame T_robot_device;
	KDL::Frame T_camera_device;

	KDL::Frame T_device_endoscope;
	KDL::Frame T_endoscope_camera;
	KDL::Frame T_camera;
	KDL::Frame T_camera_inv;
	KDL::Frame RLeft;

	/*ROS parameters */
	ros::NodeHandle n;
	ros::Subscriber trackerCameraPose_sub;
	ros::Subscriber trackerCameraPoseStamped_sub;
	ros::Subscriber virtuoseCameraPoseStamped_sub;
	ros::Subscriber  ecm_ee_pose_sub;

	ros::Publisher cameraPose_pub;


	KDL::Frame T_virtuose_in_robot;
	ros::Publisher virtuose_inRobot_Pose_pub;
	geometry_msgs::PoseStamped virtuose_inRobotPose;

	tf::TransformBroadcaster broadcaster;
	tf::TransformBroadcaster broadcaster_inv;
	tf::Transform T_remoteCenter_camera;
	tf::Transform T_remoteCenter_camera_inv;


	ComputeCameraPose();
	void trackerCameraPose_callback(const geometry_msgs::PosePtr& t);
	void trackerCameraPoseStamped_callback(const geometry_msgs::PoseStampedPtr& t_s);
	void virtuosePoseStamped_callback(const geometry_msgs::PoseStampedPtr& t_s);
	void trackerCameraMarker_callback(const visualization_msgs::MarkerPtr& t_s);
	void ECM_pose_callback(const geometry_msgs::PoseStamped::ConstPtr& msg);

};




#endif /* CAMERA_POSE_STREAMER_INCLUDE_CAMERA_POSE_STREAMER_CAMERA_POSE_STREAMER_HPP_ */
