/*
 * main.cpp
 *
 *  Created on: Oct 23, 2016
 *      Author: veronica
 */


#include "camera_pose_streamer/camera_pose_streamer.hpp"


int main(int argc, char **argv)
{

	ros::init(argc, argv, "camera_pose_streamer");

	// Definition of Class
	ComputeCameraPose CamPos;

	ros::Rate loop_rate(100);
	cv::startWindowThread();
	bool tracker = false;
	bool virtuose = true;
	bool hand_eye = false;

	if(CamPos.tracker_model == OPTITRACK)
	{

		// read robot-tracker transform
		readExtrisicParamsCamera(
				CamPos.path + "robot/" + CamPos.robot_name +
				"/Robot_Tracker_Transform.xml",
				CamPos.R_robot_device,
				CamPos.R_robot_device);
	}
	else if(CamPos.tracker_model == VIRTUOSE)
	{
		// read robot-tracker transform
		readExtrisicParamsCamera(
				CamPos.path + "robot/" + CamPos.robot_name +
				"/Device_Robot_Transform.xml",
				CamPos.R_robot_device,
				CamPos.t_robot_device);
	}
	else if(CamPos.tracker_model == DAVINCI_ECM)
	{
		if(hand_eye)
		{
			//		read robot-tracker transform
			readExtrisicParamsCamera(
					CamPos.path +  "camera/" + CamPos.camera_name  +
					"/Device_Robot_Transform.xml",
					CamPos.R_robot_device,
					CamPos.t_robot_device);
			//convert to KLD
			CamPos.T_robot_device.Identity();
			CamPos.T_robot_device.M = KDL::Rotation(
					CamPos.R_robot_device.at<double>(0,0), CamPos.R_robot_device.at<double>(0,1), CamPos.R_robot_device.at<double>(0,2),
					CamPos.R_robot_device.at<double>(1,0), CamPos.R_robot_device.at<double>(1,1), CamPos.R_robot_device.at<double>(1,2),
					CamPos.R_robot_device.at<double>(2,0), CamPos.R_robot_device.at<double>(2,1), CamPos.R_robot_device.at<double>(2,2));
			CamPos.T_robot_device.p[0] = CamPos.t_robot_device.at<double>(0,0);
			CamPos.T_robot_device.p[1] = CamPos.t_robot_device.at<double>(1,0);
			CamPos.T_robot_device.p[2] = CamPos.t_robot_device.at<double>(2,0);

			readExtrisicParamsCamera(
					CamPos.path + "camera/" + CamPos.camera_name + "/Endoscope_Camera_Transform.xml",
					CamPos.R_endoscope_camera,CamPos.t_endoscope_camera);

			//convert to KLD
			CamPos.T_endoscope_camera.M = KDL::Rotation(
					CamPos.R_endoscope_camera.at<double>(0,0), CamPos.R_endoscope_camera.at<double>(0,1), CamPos.R_endoscope_camera.at<double>(0,2),
					CamPos.R_endoscope_camera.at<double>(1,0), CamPos.R_endoscope_camera.at<double>(1,1), CamPos.R_endoscope_camera.at<double>(1,2),
					CamPos.R_endoscope_camera.at<double>(2,0), CamPos.R_endoscope_camera.at<double>(2,1), CamPos.R_endoscope_camera.at<double>(2,2));
			CamPos.T_endoscope_camera.p[0] = CamPos.t_endoscope_camera.at<double>(0,0);
			CamPos.T_endoscope_camera.p[1] = CamPos.t_endoscope_camera.at<double>(1,0);
			CamPos.T_endoscope_camera.p[2] = CamPos.t_endoscope_camera.at<double>(2,0);
		}
		else
		{
			readExtrisicParamsCamera(
					CamPos.path +  "camera/" + CamPos.camera_name  +
					"/CameraRobotCalibration.xml",
					CamPos.R_camera_device,
					CamPos.t_camera_device);

			std::cout << __LINE__ << std::endl;
			// convert to KDL
			CamPos.T_camera_device.Identity();
			CamPos.T_camera_device.M = KDL::Rotation(
					CamPos.R_camera_device.at<double>(0,0), CamPos.R_camera_device.at<double>(0,1), CamPos.R_camera_device.at<double>(0,2),
					CamPos.R_camera_device.at<double>(1,0), CamPos.R_camera_device.at<double>(1,1), CamPos.R_camera_device.at<double>(1,2),
					CamPos.R_camera_device.at<double>(2,0), CamPos.R_camera_device.at<double>(2,1), CamPos.R_camera_device.at<double>(2,2));
			CamPos.T_camera_device.p[0] = CamPos.t_camera_device.at<double>(0,0);
			CamPos.T_camera_device.p[1] = CamPos.t_camera_device.at<double>(1,0);
			CamPos.T_camera_device.p[2] = CamPos.t_camera_device.at<double>(2,0);
			std::cout << __LINE__ << std::endl;
		}

	}
	else
	{
		std::cout << "[ERROR] wrong tracker name" << std::endl;
	}


	// read rectification parameters
	cv::Mat R1;
	cv::FileStorage fs;
	fs.open(CamPos.path + "camera/" + CamPos.camera_name + "/R1.xml", cv::FileStorage::READ);

	if(!fs.isOpened())
	{
		std::cout << "[ERROR] Failed to open :" << CamPos.path + "camera/" + CamPos.camera_name + "/R1.xml" << std::endl;
		return 0;
	}

	fs["R"] >> R1;
	fs.release();

	//convert to KLD
	CamPos.RLeft.M = KDL::Rotation(
			R1.at<double>(0,0), R1.at<double>(0,1), R1.at<double>(0,2),
			R1.at<double>(1,0), R1.at<double>(1,1), R1.at<double>(1,2),
			R1.at<double>(2,0), R1.at<double>(2,1), R1.at<double>(2,2));
	CamPos.RLeft.p[0] = 0.0;
	CamPos.RLeft.p[1] = 0.0;
	CamPos.RLeft.p[2] = 0.0;


	//	std::cout << "Endoscope_Camera_Transform" << std::endl;
	//	std::cout <<  CamPos.R_endoscope_camera << std::endl;
	//	std::cout <<  CamPos.t_endoscope_camera << std::endl;
	//
	//	std::cout << "Device_Robot_Transform" << std::endl;
	//		std::cout <<  CamPos.R_robot_device << std::endl;
	//		std::cout <<  CamPos.t_robot_device << std::endl;

	while(ros::ok()){
		//compute run time transformation
		if(CamPos.tracker_model == OPTITRACK)
		{
			CamPos.T_camera = CamPos.T_robot_device * CamPos.T_device_endoscope * CamPos.T_endoscope_camera; // * CamPos.RLeft;
		}
		else if(CamPos.tracker_model == VIRTUOSE)
		{
			CamPos.T_camera = CamPos.T_robot_device * CamPos.T_device_endoscope * CamPos.T_endoscope_camera; // * CamPos.RLeft;
			CamPos.T_virtuose_in_robot = CamPos.T_robot_device * CamPos.T_device_endoscope; // * CamPos.RLeft;
			tf::poseKDLToMsg(CamPos.T_virtuose_in_robot, CamPos.virtuose_inRobotPose.pose);
			CamPos.virtuose_inRobotPose.header.frame_id = "/world";
			CamPos.virtuose_inRobotPose.header.stamp = ros::Time::now();
			CamPos.virtuose_inRobot_Pose_pub.publish(CamPos.virtuose_inRobotPose);
		}
		else if(CamPos.tracker_model == DAVINCI_ECM)
		{
			if(hand_eye)
			{
				//tf::poseMsgToKDL(CamPos.EndoscopePose, CamPos.T_device_endoscope);
				CamPos.T_camera = CamPos.T_robot_device.Inverse() * CamPos.T_device_endoscope * CamPos.T_endoscope_camera;
			}
			else
			{
				CamPos.T_camera = CamPos.T_camera_device.Inverse();// * CamPos.RLeft;
			}

		}
		//convert to Msg
		tf::poseKDLToMsg(CamPos.T_camera, CamPos.CameraPose.pose);
//		tf::poseKDLToMsg(CamPos.T_camera_inv, CamPos.CameraPose_inv.pose);

		// publish topic
		CamPos.CameraPose.header.frame_id = "/PSM1remote_center";
		CamPos.CameraPose.header.stamp = ros::Time::now();
		CamPos.cameraPose_pub.publish(CamPos.CameraPose);

		//publish tf camera pose
		CamPos.T_remoteCenter_camera.setOrigin(tf::Vector3(CamPos.CameraPose.pose.position.x,
				CamPos.CameraPose.pose.position.y,
				CamPos.CameraPose.pose.position.z) );
		tf::Quaternion quat;
		tf::quaternionMsgToTF(CamPos.CameraPose.pose.orientation, quat);
		CamPos.T_remoteCenter_camera.setRotation(quat);

		CamPos.broadcaster.sendTransform(
				tf::StampedTransform(CamPos.T_remoteCenter_camera, ros::Time::now(), "/PSM1remote_center", "/camera"));

		//publish tf camera pose inv
//		CamPos.T_remoteCenter_camera_inv.setOrigin(tf::Vector3(CamPos.CameraPose_inv.pose.position.x,
//				CamPos.CameraPose_inv.pose.position.y,
//				CamPos.CameraPose_inv.pose.position.z) );
//		tf::Quaternion quat_inv;
//		tf::quaternionMsgToTF(CamPos.CameraPose_inv.pose.orientation, quat_inv);
//		CamPos.T_remoteCenter_camera_inv.setRotation(quat_inv);
//
//		CamPos.broadcaster_inv.sendTransform(
//				tf::StampedTransform(CamPos.T_remoteCenter_camera_inv, ros::Time::now(), "/PSM1remote_center", "/camera_2"));





		ros::spinOnce();
		loop_rate.sleep();
	}
}




