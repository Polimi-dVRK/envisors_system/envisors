/*
 * camera_pose_stramer.cpp
 *
 *  Created on: Oct 23, 2016
 *      Author: veronica
 */


#include"camera_pose_streamer/camera_pose_streamer.hpp"


ComputeCameraPose::ComputeCameraPose()
{

	//init path
	path = CONFIG_FILE_FOLDER;

	this->n.getParam("/robot_info/model", this->robot_model);
	this->n.getParam("/camera_info/model", this->camera_model);
	this->n.getParam("/tracker_info/model", this->tracker_model);

	if(this->robot_model == UR5)
	{
		this->robot_name = "ur5";
	}
	else if(this->robot_model == LWR4)
	{
		this->robot_name = "lwr4";
	}
	else if(this->robot_model == PHANTOM)
	{
		this->robot_name = "phantom";
	}
	else if(this->robot_model == DAVINCI_ARMS)
	{
		this->robot_name = "davinci_arms";
	}
	else
	{
		std::cout << "[ERROR] No robot found!" << std::endl;
		this->robot_name = "/";
	}



	if(this->camera_model == MISUMI)
	{
		this->camera_name = "misumi";
	}
	else if(this->camera_model == DAVINCI)
	{
		this->camera_name = "davinci";
	}
	else if(this->camera_model == PROSILICA)
	{
		this->camera_name = "prosilica";
	}
	else if(this->camera_model == UEYE)
	{
		this->camera_name = "ueye";
	}
	else
	{
		std::cout << "[ERROR] No camera found!" << std::endl;
		camera_name = "/";
	}

	if(this->tracker_model == OPTITRACK)
	{
		this->tracker_name = "optitrack";
		//if tracker calibration
		this->trackerCameraPoseStamped_sub = n.subscribe("/optitrack/endoscope/pose", 1,
				&ComputeCameraPose::trackerCameraPoseStamped_callback, this);
	}
	else if(this->tracker_model == VICRA)
	{
		this->tracker_name = "vicra";
		this->trackerCameraPoseStamped_sub = n.subscribe("/vicra/tool_2", 1,
				&ComputeCameraPose::trackerCameraMarker_callback, this);
	}
	else if(this->tracker_model == VIRTUOSE)
	{
		this->tracker_name = "virtuose";
		this->virtuoseCameraPoseStamped_sub = n.subscribe("/virtuose/pose", 1,
				&ComputeCameraPose::virtuosePoseStamped_callback, this);
		this->virtuose_inRobot_Pose_pub = n.advertise<geometry_msgs::PoseStamped>("/virtuose/pose_in_robot/", 1);
	}
	else if(this->tracker_model == DAVINCI_ECM)
	{
		this->tracker_name = "davinci_ecm";
		this->ecm_ee_pose_sub =
				n.subscribe("/dvrk/ECM/position_cartesian_current", 1, &ComputeCameraPose::ECM_pose_callback, this);

	}
	else
	{
		std::cout << "[ERROR] No tracker found!" << std::endl;
		this->tracker_name = "/";
	}

	this->cameraPose_pub = n.advertise<geometry_msgs::PoseStamped>("/camera/pose/", 1);

}


void ComputeCameraPose::trackerCameraPose_callback(const geometry_msgs::PosePtr& t)
{
	this->EndoscopePose.orientation = t->orientation;
	this->EndoscopePose.position = t->position;

	tf::poseMsgToKDL(this->EndoscopePose, this->T_device_endoscope);
}

void ComputeCameraPose::trackerCameraPoseStamped_callback(const geometry_msgs::PoseStampedPtr& t_s)
{
	// conver to Pose
	this->EndoscopePose.orientation = t_s->pose.orientation;
	this->EndoscopePose.position = t_s->pose.position;
	tf::poseMsgToKDL(this->EndoscopePose, this->T_device_endoscope);
}

void ComputeCameraPose::trackerCameraMarker_callback(const visualization_msgs::MarkerPtr& t_s)
{
	// conver to Pose
	this->EndoscopePose.orientation = t_s->pose.orientation;
	this->EndoscopePose.position = t_s->pose.position;
	tf::poseMsgToKDL(this->EndoscopePose, this->T_device_endoscope);
}

void ComputeCameraPose::virtuosePoseStamped_callback(const geometry_msgs::PoseStampedPtr& t_s)
{
	// conver to Pose
	this->EndoscopePose.orientation = t_s->pose.orientation;
	this->EndoscopePose.position = t_s->pose.position;
	tf::poseMsgToKDL(this->EndoscopePose, this->T_device_endoscope);
}

// since the pose of the camera is published on a topic, I subscribe to it and convert it to tf
void ComputeCameraPose::ECM_pose_callback(const geometry_msgs::PoseStamped::ConstPtr& msg)
{
	// conver to Pose
	this->EndoscopePose.orientation = msg->pose.orientation;
	this->EndoscopePose.position = msg->pose.position;
	tf::poseMsgToKDL(this->EndoscopePose, this->T_device_endoscope);

}
