/*******************************************************************************
 *  FLANNMatcher.cpp
 *
 *  (C) 2007 AG Aktives Sehen <agas@uni-koblenz.de>
 *           Universitaet Koblenz-Landau
 *
 *  Additional information:
 *  $Id: $
 *******************************************************************************/

// FIXME: cassert, cmath ?
#include <map>
#include <list>
#include <math.h>

#include "hmatching/vec2.h"
#include "hmatching/FLANNMatcher.h"
#include "hmatching/Architecture/Singleton/Clock.h"

using namespace std;

#define THIS FLANNMatcher

THIS::THIS()
{
    m_hasIndex = false;
    m_descriptorLength = 64; // May differ, set when building an index

    m_flannParams = DEFAULT_FLANN_PARAMETERS;

    m_flannParams.target_precision = 0.95;
    m_flannParams.log_level = FLANN_LOG_INFO;
    m_flannParams.algorithm = FLANN_INDEX_KDTREE;
    m_flannParams.checks = 32;
    m_flannParams.trees = 8;
    m_flannParams.branching = 32;
    m_flannParams.iterations = 7;
//    m_flannParams.target_precision = -1;

    /* search time parameters */
//    m_flannParams.eps;     /* eps parameter for eps-knn search */
//    m_flannParams.sorted = 0;     /* indicates if results returned by radius search should be sorted or not */
//    m_flannParams.max_neighbors = 100;  /* limits the maximum number of neighbors should be returned by radius search */
//    m_flannParams.cores = 1;      /* number of parallel cores to use for searching */
//
//	/*  kdtree index parameters */
//    m_flannParams.leaf_max_size = 10;
//
//	/* kmeans index parameters */
//    m_flannParams.centers_init = FLANN_CENTERS_RANDOM;  /* algorithm used for picking the initial cluster centers for kmeans tree */
//    m_flannParams.cb_index;            /* cluster boundary index. Used when searching the kmeans tree */
//
//	/* autotuned index parameters */
//    //m_flannParams.build_weight;        /* build tree time weighting factor */
//    //m_flannParams.memory_weight;       /* index memory weigthing factor */
//    //m_flannParams.sample_fraction;     /* what fraction of the dataset to use for autotuning */
//
//	/* LSH parameters */
//    //m_flannParams.table_number_; /** The number of hash tables to use */
//    //m_flannParams.key_size_;     /** The length of the key in the hash tables */
//    //m_flannParams.multi_probe_level_; /** Number of levels to use in multi-probe LSH, 0 for standard LSH */
//
//	/* other parameters */
//    m_flannParams.random_seed = 0;            /* random seed to use */

    m_FlannModelData = 0;

    m_Log << "FLANNMatcher created\n";
}

THIS::THIS(const FLANNMatcher &other)
{
    operator=(other);
}

THIS &THIS::operator=(const FLANNMatcher &other)
{
    if (this == &other) {
        return *this;
    }

    m_Matches = other.m_Matches;

    //m_Log = other.m_Log;

    m_flannIndex = other.m_flannIndex;
    m_flannParams = other.m_flannParams;
    m_hasIndex = other.m_hasIndex;
    m_descriptorLength = other.m_descriptorLength;

    return *this;
}

THIS::~THIS()
{
    clearFLANNMembers();
    delete[] m_FlannModelData;
}

void THIS::createIndex(std::vector<KeyPoint> *keyPoints)
{
    if (keyPoints->size() == 0) {
        ROS_ERROR_STREAM("Cannot create index, because there are no keypoints.");
        return;
    }
    clearFLANNMembers();
    if (keyPoints->size() != 0) {
        m_descriptorLength = keyPoints->at(0).featureVector.size();
    }

    int numFeatures = keyPoints->size();
    if (m_FlannModelData != 0) {
        delete[] m_FlannModelData;
    }
    m_FlannModelData = new float[numFeatures * m_descriptorLength];
    fillFlannDataWithDescriptors(keyPoints, m_FlannModelData);
    float speedup = 0.0;

//  std::cout << "createIndex" << std::endl;
//  std::cout << "  numFeatures: " << numFeatures << std::endl;
//  std::cout << "  m_descriptorLength: " << m_descriptorLength << std::endl;
//  std::cout << "  m_FlannModelData:" << std::endl;
//  for (int i = 0; i < numFeatures*m_descriptorLength; i++)
//	  std::cout << m_FlannModelData[i] << ", ";
//  std::cout << std::endl;

    m_flannIndex = flann_build_index(m_FlannModelData, numFeatures, m_descriptorLength, &speedup, &m_flannParams);
    m_hasIndex = true;
}

void THIS::match(std::vector<KeyPoint> *keyPoints, float maxDistRatio)
{
//	std::cout << "maxDistRatio: " << maxDistRatio << std::endl;

    m_Matches.clear();
    if ((keyPoints->size() == 0) || (m_Matches.size() != 0) || (!m_hasIndex)) {
        ROS_ERROR_STREAM("Cannot match features.");

        if (keyPoints->size() == 0)
            ROS_ERROR_STREAM("Key Points Size is 0.");
        if (m_Matches.size() != 0)
            ROS_ERROR_STREAM("Matches not 0.");
        if (!m_hasIndex)
            ROS_ERROR_STREAM("No Index.");

        return;
    }

    int startTime = Clock::getInstance()->getTimestamp();

    unsigned int numKeypoints = keyPoints->size();
    int numberOfNeighbours = 2; // Number of approximate nearest neigbours to be determined
    int *indices = new int[numberOfNeighbours * numKeypoints]; // Holds the indices of the nearest neighbours
    float *distances =
        new float[numberOfNeighbours * numKeypoints]; // Holds the distance to the 1st and 2nd nearest neighbour
    float *testset = new float[numKeypoints * m_descriptorLength]; // Contains the data of the descriptors to be matched
    m_flannParams.cores = 2; // 2 is the fastest option // this fixes crash in flann_find_nearest_neighbors_index *-*
    fillFlannDataWithDescriptors(keyPoints, testset);

//	std::cout << "numberOfNeighbours: " << numberOfNeighbours << std::endl;
//	std::cout << "numKeypoints      : " << numKeypoints << std::endl;
//	std::cout << "m_descriptorLength: " << m_descriptorLength << std::endl;

//    std::cout << "testset:" << std::endl;
//    for (int i = 0; i < numKeypoints*m_descriptorLength; i++)
//    	std::cout << "  " << testset[i] << ", ";
//    std::cout << std::endl;


    int ret = flann_find_nearest_neighbors_index(m_flannIndex, testset, numKeypoints, indices,
                                                 distances, numberOfNeighbours, &m_flannParams);

//    std::cout << "flann_find_nearest_neighbors_index return: " << ret << std::endl;

//    std::cout << "indices:" << std::endl;
//	for (int i = 0; i < numberOfNeighbours*numKeypoints; i++)
//		std::cout << "  " << indices[i] << ", ";
//	std::cout << std::endl;
//
//	std::cout << "distances:" << std::endl;
//	for (int i = 0; i < numberOfNeighbours*numKeypoints; i++)
//		std::cout << "  " << distances[i] << ", ";
//	std::cout << std::endl;


    // check for distance ratios
    double distanceRatio = 0.0;
    for (unsigned i = 0; i < numKeypoints; i++) {
        distanceRatio = distances[i * 2 + 0] / distances[i * 2 + 1];
        if (distanceRatio < maxDistRatio) {
            KeyPointMatch match = {indices[i * 2 + 0], i, distanceRatio, 0, 0};
            m_Matches.push_back(match);
        }
    }
    delete[] indices;
    delete[] distances;
    delete[] testset;
    m_Log << "\n--- " << m_Matches.size() << " keypoints matched in first phase in "
          << (Clock::getInstance()->getTimestamp() - startTime) << "ms\n";

//    eliminateMultipleMatches();
}

void THIS::eliminateMultipleMatches()
{
    //It is possible that more than one ipoint in First has been matched to
    //the same ipoint in Second, in this case eliminate all but the closest one

    int startTime = Clock::getInstance()->getTimestamp();

    //maps keypoints in Second to their closest match result
    //first: index in m_KeyPointsB
    //second: iterator in m_Matches
    map<unsigned, MatchElem> bestMatch;

    m_Log << "deleting ";

    MatchElem currentMatch = m_Matches.begin();
    while (currentMatch != m_Matches.end()) {
        unsigned index2 = currentMatch->index2;

        //check if a match with this keypoint in Second was found before
        map<unsigned, MatchElem>::iterator previous = bestMatch.find(index2);

        //this is the first match found which maps to current second index
        if (previous == bestMatch.end()) {
            bestMatch[index2] = currentMatch;
            currentMatch++;
            continue;
        }

        MatchElem previousMatch = previous->second;
        //a match mapping to this index in second has been found previously, and had a higher distance
        //so delete the previously found match
        if (currentMatch->distance < previousMatch->distance) {
            m_Log << previousMatch->index1 << "->" << previousMatch->index2;
            m_Log << " (better:" << currentMatch->index1 << "->" << currentMatch->index2 << ")  ";
            m_Matches.erase(previousMatch);
            bestMatch[index2] = currentMatch;
            currentMatch++;
            continue;
        }
        //otherwise, the previously found best match is better than current,
        //so delete current
        m_Log << currentMatch->index1 << "->" << currentMatch->index2;
        m_Log << " (better:" << previousMatch->index1 << "->" << previousMatch->index2 << ")  ";
        currentMatch = m_Matches.erase(currentMatch);
    }
    m_Log << "\n--- " << m_Matches.size() << " remaining after multiple match elimination in "
          << (Clock::getInstance()->getTimestamp() - startTime) << "ms\n";

}

string THIS::getLog()
{
    string log = m_Log.str();
    m_Log.str("");
    return log;
}

void THIS::fillFlannDataWithDescriptors(const std::vector<KeyPoint> *features, float *flannDataPtr)
{
    for (unsigned int i = 0; i < features->size(); i++) {
        for (unsigned int j = 0; j < m_descriptorLength; j++) {
            flannDataPtr[m_descriptorLength * i + j] = features->at(i).featureVector[j];
        }
    }
}

void THIS::clearFLANNMembers()
{
    if (m_hasIndex) {
        flann_free_index(m_flannIndex, &m_flannParams);
        m_hasIndex = false;
    }

}


#undef THIS
