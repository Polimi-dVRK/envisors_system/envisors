/*******************************************************************************
 *  DefaultExtractor.cpp
 *
 *  (C) 2008 AG Aktives Sehen <agas@uni-koblenz.de>
 *           Universitaet Koblenz-Landau
 *
 *  $Id: $
 *
 *******************************************************************************/

#include "hmatching/DefaultExtractor.h"
#include "hmatching/ParallelSurfExtractor.h"

// TODO: Why ?
// #include "enVisorsLib/define.hpp"

#define THIS DefaultExtractor

// FIXME: N_THREADS should be computed automatically
#define N_THREADS 8

using namespace std;

KeyPointExtractor *THIS::createInstance()
{
    ExtractorType type = ExtractorType(0); // TODO ExtractorType( Config::getInt( "KeyPointExtraction.iAlgorithm" ) );

    switch (type) {
        case ExtParallelSurf:
            return new ParallelSurfExtractor(N_THREADS);

/*    case ExtOrigSurf:
      return new OrigSurfExtractor();*/

        default:
            //TRACE_ERROR( "Unknown extractor type!" ); // TODO use ros
            return new ParallelSurfExtractor(N_THREADS);
    }
}

#undef THIS

