/*******************************************************************************
 *  ImagePropertiesCV.cpp
 *
 *  (C) 2007 AG Aktives Sehen <agas@uni-koblenz.de>
 *           Universitaet Koblenz-Landau
 *
 *  Additional information:
 *  $Id: $
 *******************************************************************************/

#include "hmatching/ImagePropertiesCV.h"
#include "hmatching/DefaultExtractor.h"
#include "hmatching/KeyPointExtractor.h"
#include "hmatching/KeyPointHelper.h"
#include "hmatching/Architecture/Config/Config.h"

#define THIS ImagePropertiesCV

ImagePropertiesCV::ImagePropertiesCV()
{
    clear();
}

ImagePropertiesCV::ImagePropertiesCV(std::string name, cv::Mat *image_template, ImageMaskCV *imageMask)
{
    clear();

    if (!image_template) {
        ROS_ERROR_STREAM("Received 0-pointer as source image.");
        return;
    }

    m_Name = name;
    m_Image_template = image_template;
    m_ImageMask = imageMask;

    applyMask();
}

void ImagePropertiesCV::clear()
{
    m_Image_template = 0;
    m_KeyPoints = 0;
    m_ImageMask = 0;
    m_MaskedImageY = 0;
}

ImagePropertiesCV::~ImagePropertiesCV()
{
//	deleteAll();
}

ImagePropertiesCV &ImagePropertiesCV::operator=(const ImagePropertiesCV &other)
{


    this->m_Name = other.m_Name;
    this->m_Image_template = other.m_Image_template;
    this->m_ImageUV = other.m_ImageUV;
    this->m_ImageMask = other.m_ImageMask;
    this->m_ImageMaskWithBorder = other.m_ImageMaskWithBorder;

    /// @brief extracted data
    this->m_MaskedImageY = other.m_MaskedImageY;
    this->m_MaskedImageUV = other.m_MaskedImageUV;
    this->m_KeyPoints = other.m_KeyPoints;
    this->m_Outline = other.m_Outline;
    this->m_Center = other.m_Center;

    this->m_Border = other.m_Border;

    return *this;
}

void ImagePropertiesCV::deleteAll()
{
    if (m_Image_template != 0)
        delete m_Image_template;
    if (m_ImageMask != 0)
        delete m_ImageMask;
    if (m_KeyPoints != 0)
        delete m_KeyPoints;
    clear();

}

void ImagePropertiesCV::calculateProperties()
{
    applyMask();
//	m_MaskedImageY = new cv::Mat( *m_Image_template );
//	m_MaskedImageUV = new cv::Mat( *m_ImageUV );

//	m_Center = Point2D( m_Image_template->cols/2, m_Image_template->rows/2 );
    //  traceOutline();
    extractKeyPoints();
}

void ImagePropertiesCV::extractKeyPoints()
{
    KeyPointExtractor *extractor = DefaultExtractor::createInstance();

    m_KeyPoints = new std::vector<KeyPoint>();

    extractor->setImage(*m_Image_template);
    extractor->getKeyPoints(*m_KeyPoints);

    if (m_ImageMask) {
        KeyPointHelper::maskFilter(*m_KeyPoints, *m_KeyPoints, *m_ImageMask);
    }

    delete extractor;
}

std::vector<Point2D> ImagePropertiesCV::getBoundingBox() const
{
//	std::cout << "Image mask: " << m_ImageMask->getWidth() << ", " << m_ImageMask->getHeight() << std::endl;

    std::vector<Point2D> bBox;
    bBox.reserve(5);
    bBox.push_back(Point2D(0, 0));
    bBox.push_back(Point2D(m_ImageMask->getWidth() - 1 + 2 * m_Border, 0));
    bBox.push_back(Point2D(m_ImageMask->getWidth() - 1 + 2 * m_Border, m_ImageMask->getHeight() - 1 + 2 * m_Border));
    bBox.push_back(Point2D(0, m_ImageMask->getHeight() - 1 + 2 * m_Border));
    bBox.push_back(Point2D(0, 0));
    return bBox;
}

void ImagePropertiesCV::applyMask()
{
    if (m_MaskedImageY) {
        return;
    }

    if (m_ImageMask) {
        m_Border = 0;

        int width = m_Image_template->cols;
        int height = m_Image_template->rows;
        int newWidth = width + 2 * m_Border;
        int newHeight = height + 2 * m_Border;

        if (m_MaskedImageY)
            delete m_MaskedImageY;

        m_MaskedImageY = new cv::Mat(newHeight, newWidth, CV_8UC1);
        m_MaskedImageY->setTo(0);

//    if(m_MaskedImageUV)
//        delete m_MaskedImageUV;
//
//    m_MaskedImageUV = new cv::Mat( newHeight, newWidth, CV_8UC3);
//    m_MaskedImageUV->setTo(cv::Vec3b(0,0,0));


        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                //   if(m_ImageMask->getData()[x+m_ImageMask->getWidth()*y] != 0)
                //   {
                m_MaskedImageY->at < unsigned
                char > (m_Border + y, m_Border + x)
                    = m_Image_template->at < unsigned
                char > (y, x);
//                m_MaskedImageUV->at<cv::Vec3b>(m_Border+y, m_Border+x)
//                		= m_ImageUV->at<cv::Vec3b>(y,x);
                //   }
            }
        }

//    for ( int y=0; y<height; y++ )
//    {
//      for ( int x=0; x<width; x++ )
//      {
//     //     if(m_ImageMask->at<unsigned char>(y,x) != 0)
//     //     {
//                m_MaskedImageUV->at<cv::Vec3b>(m_Border+y, m_Border+x)
//                		= m_ImageUV->at<cv::Vec3b>(y,x);
//     //     }
//      }
//    }

        m_ImageMaskWithBorder = new ImageMaskCV(newWidth, newHeight);
        m_ImageMaskWithBorder->fill(ImageMaskCV::MASKED);
        unsigned char *maskData = m_ImageMask->getData();
        unsigned char *maskDataNew = m_ImageMaskWithBorder->getData();

        int i = 0;
        for (int y = m_Border; y < m_Border + height; y++) {
            for (int x = m_Border; x < m_Border + width; x++) {
                maskDataNew[x + newWidth * y] = maskData[i];
                i++;
            }
        }

        m_Center = m_ImageMaskWithBorder->getGravCenter();
//    std::cout << "Grav Center: " << m_Center.x() << " , " << m_Center.y() << std::endl;
    }
    else {
        ROS_INFO_STREAM("in imagePropertiesCV -- image mask is NOT valid");
        m_MaskedImageY = new cv::Mat(*m_Image_template);
        m_MaskedImageUV = new cv::Mat(*m_ImageUV);

        m_Center = Point2D(m_Image_template->cols / 2, m_Image_template->rows / 2);
//    std::cout << "Center: " << m_Center.x() << " , " << m_Center.y() << std::endl;
    }
}

#undef THIS

