/*
 * h_recognition.cpp
 *
 *  Created on: Feb 6, 2016
 *      Author: veronica
 */

#include <sstream>

#include "hmatching/HLearningModule.hpp"
#include "hmatching/ObjectProperties.h"
#include "hmatching/Architecture/Config/Config.h"

HLearningModule::HLearningModule()
{
    m_ObjectProperties = new ObjectProperties();

    // FIXME: get a sane path here
    // This path used t be defined with `CONFIG_FILE_FOLDER`
    std::string path("/home/tibo/Projects/Nearlab/daVinci/RosNodes/envisors/config_file/");
    std::vector<std::string> fileNames;
    std::vector<std::string> profilesToLoad;

    fileNames.push_back(path + "/merged.xml");
    profilesToLoad.push_back("drinks");
    Config::loadConfig(fileNames, profilesToLoad, path);

}

HLearningModule::~HLearningModule()
{
//	std::cout << __FILE__ << " "<< __LINE__ << std::endl;
    delete m_ObjectProperties;
//	std::cout << __FILE__ << " "<< __LINE__ << std::endl;
}

void HLearningModule::computeTemplateProperties(
    std::string name,
    cv::Mat template_image,
    ImageMaskCV mask,
    std::vector<cv::KeyPoint> &kp_cv,
    ObjectProperties &obj_properties)
{
    /*
    std::cout << "computeTemplateProperties(). Object type: " << this->m_ObjectType << std::endl;
    this->m_ObjectProperties->setType(this->m_ObjectType);
    std::cout << "computeTemplateProperties(). Object name: " << name << std::endl;
    this->m_ObjectProperties->setName(name);
//	obj_properites.setType(this->m_ObjectType);
//	obj_properites.setName(name);

    std::cout << "computeTemplateProperties(). Create ImagePropertiesCV" << std::endl;
    ImagePropertiesCV ImageProperties(name, &template_image, &mask);
    std::cout << "computeTemplateProperties(). Calculate properties" << std::endl;
    ImageProperties.calculateProperties();
    std::cout << "computeTemplateProperties(). Add image properties" << std::endl;
    this->m_ObjectProperties->addImageProperties(&ImageProperties);
//	obj_properites.addImageProperties(&ImageProperties);

    // save keypoints in opencv format
    std::vector <KeyPoint>* kp;

    kp = ImageProperties.getKeyPoints();
    kp_cv.resize(kp->size());

    for(int i = 0; i < kp->size(); i++)
    {
        kp_cv[i].pt.x = kp->at(i).x;
        kp_cv[i].pt.y = kp->at(i).y;
//		std::cout << "x[" << i << "]" << kp->at(i).x << std::endl;
//		std::cout << "y[" << i << "]" << kp->at(i).y << std::endl;
    }

    std::cout << "computeTemplateProperties(). Copy object properties" << std::endl;
    obj_properites = *(this->m_ObjectProperties);
    std::cout << "computeTemplateProperties(). end" << std::endl;*/

    obj_properties.deleteAll();

    obj_properties.setType(this->m_ObjectType);
    obj_properties.setName(name);

    ImagePropertiesCV *ImageProperties = new ImagePropertiesCV(name, &template_image, &mask);
    ImageProperties->calculateProperties();
    obj_properties.addImageProperties(ImageProperties);
    std::vector<ImagePropertiesCV *> aux = obj_properties.getImageProperties();

    std::vector<KeyPoint> *kp;

    kp = ImageProperties->getKeyPoints();
    kp_cv.resize(kp->size());

    for (int i = 0; i < kp->size(); i++) {
        kp_cv[i].pt.x = kp->at(i).x;
        kp_cv[i].pt.y = kp->at(i).y;
    }
}


#undef HLearningModule

