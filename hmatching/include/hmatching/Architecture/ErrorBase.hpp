//
// Created by tibo on 26/07/17.
//

#pragma once

#include <boost/exception/all.hpp>

/**
 * Base class for all exception thrown by the library and client applications.
 */
class HMatchingError: public virtual boost::exception,
                      public virtual std::exception
{
};

typedef boost::error_info<struct tag_error, const std::string> error_description;
