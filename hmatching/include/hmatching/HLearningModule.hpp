/*******************************************************************************
 *  ORLearningModule.h
 *
 *  (C) 2005, 2013 AG Aktives Sehen <agas@uni-koblenz.de>
 *                 Universitaet Koblenz-Landau
 *
 ******************************************************************************/

#ifndef HLearningModule_H
#define HLearningModule_H

#include <string>
#include <map>

#include "hmatching/ImageMaskCV.h"


class ObjectProperties;
class ImagePropertiesCV;

class HLearningModule
{

public:
    std::string name;
    std::string type;

    HLearningModule();

    virtual ~HLearningModule();

    void computeTemplateProperties(
        std::string name,
        cv::Mat template_image,
        ImageMaskCV mask,
        std::vector<cv::KeyPoint> &kp_cv,
        ObjectProperties &obj_properties);
    void saveImage(std::string name);
    ObjectProperties *m_ObjectProperties;

    std::string m_ObjectType;

};

#endif //HLearningModule_H
