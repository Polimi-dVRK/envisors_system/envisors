/*
 * HMatchingModule.hpp
 *
 *  Created on: Feb 17, 2016
 *      Author: veronica
 */

#ifndef SOURCE_DIRECTORY__ENVISORSLIB_INCLUDE_ENVISORSLIB_HMATCHING_HMATCHINGMODULE_HPP_
#define SOURCE_DIRECTORY__ENVISORSLIB_INCLUDE_ENVISORSLIB_HMATCHING_HMATCHINGMODULE_HPP_

/*******************************************************************************
 *  ORMatchingModule.h
 *
 *  (C) 2006 AG Aktives Sehen <agas@uni-koblenz.de>
 *           Universitaet Koblenz-Landau
 *
 *******************************************************************************/

#ifndef ORMatchingModule_H
#define ORMatchingModule_H

#include <deque>

#include <ros/ros.h>

#include "hmatching/KeyPoint.h"
#include "hmatching/ObjectProperties.h"
#include "hmatching/MatchResult.h"
#include "hmatching/KeyPointMatch.h"
#include "hmatching/ImagePropertiesCV.h"
#include "hmatching/Box2D.h"

class FLANNMatcher;

class KeyPointExtractor;

/**
 * @class  ORMatchingModule
 * @brief  Default module template
 * @author David Gossow (RX), Viktor Seib (R20)
 */
class HMatchingModule
{
public:

    /** @brief The constructor. */
    HMatchingModule();

    /** @brief The destructor. */
    virtual ~HMatchingModule();

    void processImageMessage(
        cv::Mat &image,
        std::vector<Box2D<double> > &m_BoundingBoxes,
        std::deque<ObjectProperties> &m_ObjectList,
        std::vector<MatchResult *> &matchResults,
        std::vector<KeyPoint> &sceneKeyPoints_out,
        cv::Mat &img_hough);

private:

    enum Stage1MatcherT
    {
        NearestNeighbor = 1,
        Flann = 2
    };

    enum Stage2MatcherT
    {
        SimpleHoughClustering = 1,
        HoughClustering = 2
    };

    /// @brief extract all keypoints, then do object recognition on all bounding boxes separately
    /// @param boundingBoxes bounding boxes to use. if empty, the full image is used.
    void processImages(
        cv::Mat &image,
        std::vector<Box2D<int> > &boundingBoxes,
        std::deque<ObjectProperties> &m_ObjectList,
        std::vector<MatchResult *> &matchResults,
        std::vector<KeyPoint> &sceneKeyPoints_out,
        cv::Mat &img_hough);

    /// @brief search for objects on all given keypoints
    void processKeyPoints(
        cv::Mat &image,
        std::vector<KeyPoint> *rawKeyPoints,
        std::vector<Box2D<int> > &boundingBoxes,
        std::deque<ObjectProperties> &m_ObjectList,
        std::vector<MatchResult *> &matchResults,
        cv::Mat &img_hough);

    /// @brief try to match keypoints to one specific object
    bool matchObject(
        std::vector<KeyPoint> *sceneKeyPoints,
        ObjectProperties &objectProperties,
        MatchResult &matchResult,
        cv::Mat &img_hough);

    /* Match keypoints by nearest neighbor ratio */
    std::list<KeyPointMatch>
    matchStage1(std::vector<KeyPoint> *sceneKeyPoints, ImagePropertiesCV *objectImageProperties);

    /* Match keypoints with flann */
    std::list<KeyPointMatch> matchStage1Flan(ImagePropertiesCV *objectImageProperties);

    /* Hough-Transform-Clustering */
    std::vector<std::list<KeyPointMatch> > matchStage2(
        std::vector<KeyPoint> *sceneKeyPoints,
        ImagePropertiesCV *objectImageProperties,
        std::list<KeyPointMatch> &stage1Matches,
        cv::Mat &img_hough);

    /* Homography */
    std::list<KeyPointMatch> matchStage3(std::vector<KeyPoint> *sceneKeyPoints,
                                         ImagePropertiesCV *objImageProperties,
                                         std::vector<std::list<KeyPointMatch> > &stage2Matches,
                                         Homography &homography);

    /* helper function */
    std::vector<KeyPoint> getSceneKeyPointsWithinOutline(std::vector<KeyPoint> *sceneKeyPoints,
                                                         Homography &homography,
                                                         std::list<KeyPointMatch> &stage3Matches);

    int m_ImagesRequested;

    Stage1MatcherT m_Stage1Matcher;
    Stage2MatcherT m_Stage2Matcher;

    int m_ImageWidth;
    int m_ImageHeight;

    KeyPointExtractor *m_Extractor;

    std::list<double> m_ProcessingTimes;

    FLANNMatcher *m_FlannMatcher;

};


#endif


#endif /* SOURCE_DIRECTORY__ENVISORSLIB_INCLUDE_ENVISORSLIB_HMATCHING_HMATCHINGMODULE_HPP_ */
