#!/bin/bash

# set CAMERA model (MISUMI=0, DAVINCI=1, PROSILICA=2, UEYE=3)
misumi=0
davinci=1
prosilica=2
ueye=3
#########################
cam=$davinci
#########################

# set ROBOT model (UR5=0, LWR4=1, PHANTOM=2,VIRTUOSE)
ur5=0
lwr4=1
phantom=2
davinci_arms=3
#########################
robot=$davinci_arms
#########################

# set TRACKER model (OPTITRACK=0, VICRA=1, VIRTUOSE=2)
optitrack=0
vicra=1
virtuose=2
davinci_ecm=3
#########################
tracker=$davinci_ecm
#########################

# set IMAGE options
video=0
stream=1
ros=2
video_mono=3
images=4
#########################
image_modality=$ros
#########################

#set Safety Area selection
manual=0 # read from file
online=1 # mouse
tablet=2
#########################
safety_area_selection=$online
#########################


# set ROSPARAM

rosparam set /camera_info/stream $image_modality # 0 for VIDEO # 1 for stream
rosparam set /camera_info/model $cam

# set camera device
rosparam set /camera_info/left/dev /dev/video2
rosparam set /camera_info/right/dev /dev/video3

if [ $cam == $misumi ] ; then
    rosparam set /camera_info/width 720
	rosparam set /camera_info/height 480
elif [ $cam == $davinci ] ; then
    rosparam set /camera_info/width 720
	rosparam set /camera_info/height 576
elif [ $cam == $prosilica ] ; then
    rosparam set /camera_info/width 720
	rosparam set /camera_info/height 480
elif [ $cam == $ueye ] ; then
    rosparam set /camera_info/width 640
	rosparam set /camera_info/height 480
fi

# 0 for from file - # 0 for MANUAL - #1 for ONLINE - # 2 for TABLET
echo  $safety_area_selection
rosparam set /SA/definition/manual $safety_area_selection 

# set robot model (UR5=0, LWR4=1, PHANTOM=2)
rosparam set /robot_info/model $robot

# set tracker model (OPTITRACK=0, VICRA=1)
rosparam set /tracker_info/model $tracker 

#ui_bino params
#rosparam set mode stereo
#rosparam set video fullscreen
#rosparam set screen 0
#rosparam set offsetX 0
#rosparam set offsetY 0
#rosparam set offsetAngle 0
rosparam set /uralp/camera_left/device kk
rosparam set /uralp/camera_right/device kk


#rosrun ui_bino remote _video:=fullscreen _mode:=stereo

# Tablet rotation
rosparam set /uralp/input/tablet/rotate 180

