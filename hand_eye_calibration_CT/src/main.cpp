/*
 * main.cpp
 *
 *  Created on: Oct 10, 2016
 *      Author: veronica
 */


#include "hand_eye_calibration_CT/hand_eye_calibration_CT.hpp"


int main(int argc, char **argv)
{

    // init ROS
    ros::init(argc, argv, "hand_eye_calibration_CT");
    // Declare class for hand-eye calibration

    HandEyeCalibrationCT hand_eye_calib;

    cv::startWindowThread();

    //    ros::Rate loop_rate(100);
    //    while(ros::ok())
    //      {
    //          loop_rate.sleep();
    //          ros::spinOnce();
    //      }
    // start thread for visualizing images


    // Compute hand-eye calibration between trcker and camera
    hand_eye_calib.compute_extrinsics();



}

