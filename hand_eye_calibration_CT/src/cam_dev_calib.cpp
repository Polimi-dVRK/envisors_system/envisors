/*
 * cam_dev_calib.cpp
 *
 *  Created on: Oct 10, 2016
 *      Author: veronica
 */

/*  camera_device_calibration.cpp
 *
 *  Created on: Novembre 8, 2015
 *  Author: Veronica Penza
 */


#include"cam_dev_calib/cam_dev_calib.hpp"



ExtrinsicCalibration::ExtrinsicCalibration(CalibrationType calib_type)
{

	//init path

	path = CONFIG_FILE_FOLDER;
	image_transport::ImageTransport it(this->n);
	// define image subscriber
	this->left_image_rgb_sub = it.subscribe("/image/left/rgb", 1,
			&ExtrinsicCalibration::left_image_rgb_callback, this);

	this->n.getParam("/robot_info/model", this->robot_model);
	this->n.getParam("/camera_info/model", this->camera_model);


	this->calib_type = calib_type;

	if(this->calib_type == CAMERA_TRACKER || this->calib_type == CAMERA_ENDOSCOPE)
	{
		//if tracker calibration
		this->trackerCameraPose_sub = n.subscribe("/tracker/camera/pose", 1,
				&ExtrinsicCalibration::trackerCameraPose_callback, this);
		this->trackerCameraPoseStamped_sub = n.subscribe("/tracker/camera/poseStamped", 1,
				&ExtrinsicCalibration::trackerCameraPoseStamped_callback, this);
		this->trackerTipPose_sub = n.subscribe("/tracker/tip/pose", 1,
				&ExtrinsicCalibration::trackerTipPose_callback, this);
		this->trackerTipPoseStamped_sub = n.subscribe("/tracker/tip/poseStamped", 1,
				&ExtrinsicCalibration::trackerTipPoseStamped_callback, this);
	}
	else if(this->calib_type == CAMERA_ROBOT)
	{
		if(this->robot_model == UR5)
		{
			// if hand_eye calibration
			//this->robotPose_sub = n.subscribe("/robot/pose", 1,
			//		&ExtrinsicCalibration::robotPose_callback, this);
		}
		else if(this->robot_model == LWR4)
		{
			//KUKA Nearlab
			this->robotPose_sub = n.subscribe("/FRI_CartesianPositionOut", 1,
					&ExtrinsicCalibration::robotPose_callback, this);

			this->robotPoseStamped_sub = n.subscribe("/robot/poseStamped", 1,
					&ExtrinsicCalibration::robotPoseStamped_callback, this);
		}
		else if(this->robot_model == PHANTOM)
		{
			//Phantom - IIT
			this->robotPose_sub = n.subscribe("/PhantomOmni1_current_transform", 1,
					&ExtrinsicCalibration::robotPose_callback, this);
		}
		else
		{
			std::cout << "[ERROR] Please insert robot type "<< std::endl;
		}

	}
	else if(this->calib_type == INIT)
	{
		std::cout << "[ERROR] Please insert calibratyon type "<< std::endl;
	}

}


void ExtrinsicCalibration::left_image_rgb_callback(const sensor_msgs::ImageConstPtr& rgb_l){


	fromImagetoMat(*rgb_l, &this->imgL, rgb_l->encoding);

	img_size = cv::Size(this->imgL.cols, this->imgL.rows);

	// display image
	cv::namedWindow("Left Image RGB", CV_WINDOW_AUTOSIZE);
	cv::imshow("Left Image RGB", this->imgL);

}

void ExtrinsicCalibration::robotPose_callback(const geometry_msgs::PosePtr& r)
{
	robotPose.orientation = r->orientation;
	robotPose.position = r->position;

	KDL::Frame robotPose_;
	KDL::Frame robot_end_tip;
	KDL::Frame robotTipPose;
	cv::Mat R;
	cv::Mat t;

	bool pivoting = true;
	if(pivoting)
	{

		if(this->robot_model == UR5)
		{
			readExtrisicParamsCamera(
					this->path + "UR5/CalibrationTipRobot.xml",
					R,
					t);
		}
		else if(this->robot_model == LWR4)
		{
			readExtrisicParamsCamera(
					this->path + "LWR4/CalibrationTipRobot.xml",
					R,
					t);
		}
		else if(this->robot_model == PHANTOM)
		{
			readExtrisicParamsCamera(
					this->path +  "PHANTOM/CalibrationTipRobot.xml",
					R,
					t);
		}


		//convert to KLD
		robot_end_tip.M = KDL::Rotation(R.at<float>(0,0), R.at<float>(0,1), R.at<float>(0,2),
				R.at<float>(1,0), R.at<float>(1,1), R.at<float>(1,2),
				R.at<float>(2,0), R.at<float>(2,1), R.at<float>(2,2));
		robot_end_tip.p[0] = t.at<float>(0,0);
		robot_end_tip.p[1] = t.at<float>(1,0);
		robot_end_tip.p[2] = t.at<float>(2,0);

		tf::poseMsgToKDL(robotPose, robotPose_);
		robotTipPose = robotPose_ * robot_end_tip;
		tf::poseKDLToMsg(robotTipPose, robotPose);
	}
}

void ExtrinsicCalibration::robotPoseStamped_callback(const geometry_msgs::PoseStampedPtr& r_s)
{
	// conver to Pose
	robotPose.orientation = r_s->pose.orientation;
	robotPose.position = r_s->pose.position;
}

void ExtrinsicCalibration::trackerCameraPose_callback(const geometry_msgs::PosePtr& t)
{
	trackerCameraPose.orientation = t->orientation;
	trackerCameraPose.position = t->position;
}

void ExtrinsicCalibration::trackerCameraPoseStamped_callback(const geometry_msgs::PoseStampedPtr& t_s)
{
	// conver to Pose
	trackerCameraPose.orientation = t_s->pose.orientation;
	trackerCameraPose.position = t_s->pose.position;
}

void ExtrinsicCalibration::trackerTipPose_callback(const geometry_msgs::PosePtr& tp)
{
	trackerTipPose.orientation = tp->orientation;
	trackerTipPose.position = tp->position;
}

void ExtrinsicCalibration::trackerTipPoseStamped_callback(const geometry_msgs::PoseStampedPtr& tp_s)
{
	// conver to Pose
	trackerTipPose.orientation = tp_s->pose.orientation;
	trackerTipPose.position = tp_s->pose.position;
}


void ExtrinsicCalibration::get_image_corners()
{
	this->corners.resize(ncorners);

	int step;
	int view_counter = 0;

	/*Find 2D points on chessboard*/
	while (1/*view_counter < nviews*/) {

		ros::spinOnce();

		if(imgL.empty()){
			printf("Out\n");
			usleep(1E5);
			ros::spinOnce();
			continue;
		}

		int pattern_was_found = cv::findChessboardCorners(imgL, pattern_size,
				corners, CV_CALIB_CB_ADAPTIVE_THRESH|CV_CALIB_CB_FILTER_QUADS);

		if (pattern_was_found && corners.size() == ncorners) {
			printf("In %d %d\n", pattern_was_found, view_counter);

			step = view_counter * ncorners;
			cv::Mat gray_img(imgL.rows, imgL.cols, CV_8UC1);

			cvtColor(imgL, gray_img, CV_BGR2GRAY);
			cornerSubPix(gray_img, corners, cvSize(3,3), cvSize(-1,-1),
					cvTermCriteria(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS,10,1E-4));

			//			this->image_points.push_back(corners[0]);
			//			this->image_points.push_back(corners[ pattern_size.width - 1]);
			//			this->image_points.push_back(corners[(pattern_size.height - 1) *
			//												 pattern_size.width]);
			//			this->image_points.push_back(corners[(pattern_size.height - 1) *
			//												 pattern_size.width + (pattern_size.width - 1)]);

			this->image_points.push_back(corners[1 * pattern_size.width + 1]);
			this->image_points.push_back(corners[(pattern_size.height - 2) * pattern_size.width + 1]);
			this->image_points.push_back(corners[1 * pattern_size.width + 3]);
			this->image_points.push_back(corners[(pattern_size.height - 2) * pattern_size.width + 3]);
			this->image_points.push_back(corners[1 * pattern_size.width + 5]);
			this->image_points.push_back(corners[(pattern_size.height - 2) * pattern_size.width + 5]);
			this->image_points.push_back(corners[1 * pattern_size.width + 7]);
			this->image_points.push_back(corners[(pattern_size.height - 2) * pattern_size.width + 7]);

			cv::imshow("Pose",imgL);
			break;
		}

		cv::imshow("Pose",imgL);

		cvWaitKey(250);
	}

	std::cout << "Image Points saved!" << std::endl;
}

void ExtrinsicCalibration::save_current_device_pose()
{

	switch(calib_type)
	{
	case CAMERA_ENDOSCOPE:

		fromPosetoMat(this->trackerCameraPose,
				this->camera_pose.quat, this->camera_pose.t);

		saveExtrisicParamsCamera(
				this->path +  "calibration/Tracker_CameraPose.xml",
				this->camera_pose.quat, this->camera_pose.t);
		break;

	case HAND_EYE:

		fromPosetoMat(this->robotPose,
				this->robot_pose.quat, this->robot_pose.t);

		saveExtrisicParamsCamera(
				this->path + "calibration/RobotPose.xml",
				this->robot_pose.quat, this->robot_pose.t);
		break;
	default:
		break;
	}
	std::cout << "Robot pose Tip saved!" << std::endl;
}

void ExtrinsicCalibration::get_device_points()
{
	/*Save 3D points with robot tip*/
	std::cout << "Acquire points pressing l ; " << std::endl;

	cv::Point3f position;
	int counter = 0;

	while( 1 ) {

		ros::spinOnce();
		int radius = 10;
		cv::Point2f center;

		// draw points to be stored
		for (int i=0; i< npoints; i++) {
			center = image_points[i];
			if (i == counter) {
				circle(imgL, center, radius, CV_RGB(255,0,0),2);
			}
			else {
				circle(imgL, center, radius, CV_RGB(0,255,0),2);
			}
		}

		//dispaly image
		cv::imshow("Pose",imgL);

		int c = cvWaitKey( 15 ) & 0xFF;

		//printf("%f %f %f\n",pose_mm[0],pose_mm[1],pose_mm[2]);
		if ( c == 'l' ) {

			switch(calib_type)
			{
			case CAMERA_TRACKER:
				position.x = trackerTipPose.position.x;
				position.y = trackerTipPose.position.y;
				position.z = trackerTipPose.position.z;
				break;

			case CAMERA_ENDOSCOPE:
				position.x = trackerTipPose.position.x;
				position.y = trackerTipPose.position.y;
				position.z = trackerTipPose.position.z;
				break;

			case CAMERA_ROBOT:
				position.x = robotPose.position.x;
				position.y = robotPose.position.y;
				position.z = robotPose.position.z;
				break;

			case HAND_EYE:

				position.x = robotPose.position.x;
				position.y = robotPose.position.y;
				position.z = robotPose.position.z;

				break;
			default:
				break;
			}

			//printf("TCP1: %f %f %f\n", shared_tcp1_pose[0], shared_tcp1_pose[1], shared_tcp1_pose[2]);
			std::cout << "Acquired:" << "x" << position.x << "y" << position.y << "z" << position.z << std::endl;


			object_points.push_back(position);

			counter += 1;

			if (counter == npoints) {
				break;
			}
		}

		cvWaitKey( 5 );
	}
}



bool ExtrinsicCalibration::compute_extrinsics()
{

	cvNamedWindow( "Pose", 1 );

	//initialization
	this->npoints = 8;
	this->pattern_size = cv::Size(9,6);
	//	pattern_size = cv::Size(7,5);
	this->ncorners = this->pattern_size.height * this->pattern_size.width;

	this->get_image_corners();

	this->save_current_device_pose();

	this->get_device_points();

	/*Compute 2D-3D calibration*/
	std::cout << "Computing extrinsic calibration ..." << std::endl;


	readParamsCamera(
			this->path + "LeftCameraParameters.xml",
			this->cameraMatrix,
			this->distCoeff);



	solvePnP(this->object_points, this->image_points,
			this->cameraMatrix, this->distCoeff, R_camera_device, t_camera_device);

	/*Project object points using the computed extrinsic
	 *  calibartion for visual evaluation*/

	projectPoints(this->object_points, R_camera_device, t_camera_device, this->cameraMatrix,
			this->distCoeff, this->image_projected);

	//Show points on image
	for (int i=0; i<npoints; i++) {
		CvPoint center = image_projected[i];
		int radius = 10;
		circle(imgL, center, radius, CV_RGB(0,0,255), 2);
	}


	//dispaly image
	cv::imshow("Pose",imgL);
	cvWaitKey(0);


	std::cout << "Rotation " << R_camera_device << std::endl;
	std::cout << "Translation " << t_camera_device << std::endl;
	Rodrigues(R_camera_device, R_r);

	//	projectPoints(object_points, R_r, T, cameraMatrix, distCoeff, image_projected);

	if(calib_type == CAMERA_TRACKER)
	{
		/*Save Extrinsic calibration*/
		saveExtrisicParamsCamera(
				this->path + "calibration/Camera_Tracker_ext_transform.xml",R_r , t_camera_device);
	}
	else if(calib_type == CAMERA_ENDOSCOPE)
	{
		cv::Mat cam_end_R;
		cv::Mat cam_end_T;
		compute_camera_endoscope_pose();
		/*Save Extrinsic calibration*/
		saveExtrisicParamsCamera(
				this->path + "calibration/Camera_Tracker_ext_transform.xml",R_r , t_camera_device);
		/*Save Extrinsic calibration*/
		saveExtrisicParamsCamera(
				this->path + "calibration/Camera_Endoscope_ext_transform.xml",
				R_camera_endoscope_mat , t_camera_endoscope_mat);
	}
	else if(calib_type == CAMERA_ROBOT)
	{
		/*Save Extrinsic calibration*/
		saveExtrisicParamsCamera(
				this->path + "calibration/Camera_Robot_ext_transform.xml",R_r , t_camera_device);
	}
	else if(calib_type == HAND_EYE)
	{
		/*Save Extrinsic calibration*/
		saveExtrisicParamsCamera(
				this->path + "calibration/hand_eye_ext_transform.xml",R_r , t_camera_device);
	}

	std::cout << "Done computing extrinsics!" << std::endl;

	corners.clear();
	object_points.clear();
	image_points.clear();
	image_projected.clear();

	////////// ERROR EVALUATION /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	bool compute_error = true;
	if(compute_error)
		this->compute_error();


	return true;
}

void ExtrinsicCalibration::compute_error()
{
	std::cout << " --------------ERROR EVALUATION--------- " << std::endl;

	this->get_image_corners();

	this->get_device_points();

	printf("Computing extrinsics\n");

	projectPoints(object_points, R_camera_device, t_camera_device, cameraMatrix, distCoeff, image_projected);

	//Show points on image
	for (int i=0; i<npoints; i++) {
		CvPoint center = image_projected[i];
		int radius = 10;
		circle(imgL, center, radius, CV_RGB(0,0,255), 2);
	}


	//dispaly image
	cv::imshow("Pose",imgL);
	cvWaitKey(0);

	////// Computing the reprojection error ////////////////////////////////////////////////////////////////////////////////////////////////////////////
	float temp = 0;
	float temp1 = 0;
	float errorpos = 0;
	float temp2 = 0;
	float totalErr = 0;
	float sum = 0;
	float RMS = 0;

	std::vector<float> err;

	err.resize(npoints);

	CvScalar average = cvScalarAll(0);
	CvScalar std = cvScalarAll(0);

	for (int j=0; j< npoints;j++){

		std::cout << "image_points" << image_points[j] << std::endl;
		std::cout << "image_projected" << image_projected[j] << std::endl;


		temp = image_points[j].x - image_projected[j].x;
		temp = temp * temp;
		temp1 = image_points[j].y - image_projected[j].y;
		temp1 = temp1 * temp1;
		temp2 = temp1 + temp;
		errorpos = sqrt(temp2);
		err[j] = errorpos;

	}
	for (int j = 0; j < npoints;j++){
		sum += err[j];
	}

	sum = sum / npoints;

	RMS = sqrt(sum);
	std::cout << "Root Mean Square Error:" << RMS << std::endl;


	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	image_viewed.clear();
	err.clear();
	corners.clear();
	object_points.clear();
	image_points.clear();
	image_projected.clear();
}


void ExtrinsicCalibration::compute_camera_endoscope_pose()
{
	KDL::Frame T_camera_tracker;
	KDL::Frame T_tracker_endoscope;
	KDL::Frame T_camera_endoscope;
	//convert to KLD
	T_camera_tracker.M = KDL::Rotation(R_camera_device.at<float>(0,0), R_camera_device.at<float>(0,1), R_camera_device.at<float>(0,2),
			R_camera_device.at<float>(1,0), R_camera_device.at<float>(1,1), R_camera_device.at<float>(1,2),
			R_camera_device.at<float>(2,0), R_camera_device.at<float>(2,1), R_camera_device.at<float>(2,2));
	T_camera_tracker.p[0] = t_camera_device.at<float>(0,0);
	T_camera_tracker.p[1] = t_camera_device.at<float>(1,0);
	T_camera_tracker.p[2] = t_camera_device.at<float>(2,0);


	T_tracker_endoscope.M = KDL::Rotation(camera_pose.quat.at<float>(0,0), camera_pose.quat.at<float>(0,1), camera_pose.quat.at<float>(0,2),
			camera_pose.quat.at<float>(1,0), camera_pose.quat.at<float>(1,1), camera_pose.quat.at<float>(1,2),
			camera_pose.quat.at<float>(2,0), camera_pose.quat.at<float>(2,1), camera_pose.quat.at<float>(2,2));;
	T_tracker_endoscope.p[0] = camera_pose.t.at<float>(0,0);
	T_tracker_endoscope.p[1] = camera_pose.t.at<float>(1,0);
	T_tracker_endoscope.p[2] = camera_pose.t.at<float>(2,0);


	// compute T between image and camera as:
	T_camera_endoscope = T_camera_tracker * T_tracker_endoscope;

	R_camera_endoscope_mat.create(9,9, CV_32FC1);
	this->R_camera_endoscope_mat.at<float>(0,0) = T_tracker_endoscope.M.data[0];
	this->R_camera_endoscope_mat.at<float>(0,1) = T_tracker_endoscope.M.data[1];
	this->R_camera_endoscope_mat.at<float>(0,2) = T_tracker_endoscope.M.data[2];
	this->R_camera_endoscope_mat.at<float>(1,0) = T_tracker_endoscope.M.data[3];
	this->R_camera_endoscope_mat.at<float>(1,1) = T_tracker_endoscope.M.data[4];
	this->R_camera_endoscope_mat.at<float>(1,2) = T_tracker_endoscope.M.data[5];
	this->R_camera_endoscope_mat.at<float>(2,0) = T_tracker_endoscope.M.data[6];
	this->R_camera_endoscope_mat.at<float>(2,1) = T_tracker_endoscope.M.data[7];
	this->R_camera_endoscope_mat.at<float>(2,2) = T_tracker_endoscope.M.data[8];

	t_camera_endoscope_mat.create(3,1, CV_32FC1);
	this->t_camera_endoscope_mat.at<float>(0,0) = T_tracker_endoscope.p.data[0];
	this->t_camera_endoscope_mat.at<float>(0,0) = T_tracker_endoscope.p.data[1];
	this->t_camera_endoscope_mat.at<float>(0,0) = T_tracker_endoscope.p.data[2];



}


