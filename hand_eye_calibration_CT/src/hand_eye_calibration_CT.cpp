/*
 * hand_eye_calibration_CT.cpp
 *
 *  Created on: Oct 10, 2016
 *      Author: veronica
 */

/*  camera_device_calibration.cpp
 *
 *  Created on: Novembre 8, 2015
 *  Author: Veronica Penza
 */


#include "hand_eye_calibration_CT/hand_eye_calibration_CT.hpp"



HandEyeCalibrationCT::HandEyeCalibrationCT()
{

	//init path
	path = CONFIG_FILE_FOLDER;
	image_transport::ImageTransport it(this->n);

	// get ROS parameters
	this->n.getParam("/camera_info/model", this->camera_model);
	this->n.getParam("/tracker_info/model", this->tracker_model);

	// define subscriber
	this->left_image_rgb_sub = it.subscribe("/uralp/camera_left/image", 1,
			&HandEyeCalibrationCT::left_image_rgb_callback, this);

	if(this->tracker_model == OPTITRACK){

		this->tracker_name = "optitrack";
		this->trackerEndoscopePoseStamped_sub = n.subscribe("/optitrack/endoscope/pose", 1,
				&HandEyeCalibrationCT::trackerEndoscopePoseStamped_callback, this);

		this->trackerTipPoseStamped_sub = n.subscribe("/optitrack/pointer/pose", 1,
				&HandEyeCalibrationCT::trackerTipPoseStamped_callback, this);
	}
	else if(this->tracker_model == VICRA)
	{
		this->tracker_name = "vicra";
		std::cout << "TODO"<< std::endl;
		this->trackerEndoscopePoseStamped_sub = n.subscribe("/vicra/tool_2", 1,
				&HandEyeCalibrationCT::trackerEndoscopeMarker_callback, this);

		this->trackerTipPoseStamped_sub = n.subscribe("/vicra/tool_1", 1,
				&HandEyeCalibrationCT::trackerTipMarker_callback, this);
	}
	else
	{
		std::cout << "[ERROR] Please, insert the tracker model"<< std::endl;
	}

	readTranslation(
			this->path + "tracker/" + this->tracker_name + "/Tracker_tip.xml",
			t_tip_);
}


void HandEyeCalibrationCT::left_image_rgb_callback(const sensor_msgs::ImageConstPtr& rgb_l){


	fromImagetoMat(*rgb_l, &this->imgL, rgb_l->encoding);

	img_size = cv::Size(this->imgL.cols, this->imgL.rows);

	// display image
	cv::namedWindow("Left Image RGB", CV_WINDOW_AUTOSIZE);
	cv::imshow("Left Image RGB", this->imgL);

}

// callback for endoscope pose
void HandEyeCalibrationCT::trackerEndoscopePose_callback(const geometry_msgs::PosePtr& t)
{

	this->trackerEndoscopePose.orientation = t->orientation;
	this->trackerEndoscopePose.position = t->position;
}

void HandEyeCalibrationCT::trackerEndoscopePoseStamped_callback(const geometry_msgs::PoseStampedPtr& t_s)
{

	// conver to Pose
	this->trackerEndoscopePose.orientation = t_s->pose.orientation;
	this->trackerEndoscopePose.position = t_s->pose.position;
}

void HandEyeCalibrationCT::trackerEndoscopeMarker_callback(const visualization_msgs::MarkerPtr& t_s)
{

	// conver to Pose
	this->trackerEndoscopePose.orientation = t_s->pose.orientation;
	this->trackerEndoscopePose.position = t_s->pose.position;
}

// callback for tracker pointer
void HandEyeCalibrationCT::trackerTipPose_callback(const geometry_msgs::PosePtr& tp)
{

	trackerTipPose.orientation = tp->orientation;
	trackerTipPose.position = tp->position;

	//	std::vector<double> t_tip;
	//	n.getParam("/transform/ee/to/tip",t_tip);

	trackerTipPose.position.x = trackerTipPose.position.x + this->t_tip_.at<float>(0,0);
	trackerTipPose.position.y = trackerTipPose.position.y + this->t_tip_.at<float>(1,0);
	trackerTipPose.position.z = trackerTipPose.position.z + this->t_tip_.at<float>(2,0);
}

void HandEyeCalibrationCT::trackerTipPoseStamped_callback(const geometry_msgs::PoseStampedPtr& tp_s)
{
	// conver to Pose
	this->trackerTipPose.orientation = tp_s->pose.orientation;
	this->trackerTipPose.position = tp_s->pose.position;
	//    std::cout << __LINE__ << std::endl;


	//convert to KLD
	KDL::Vector robot_end_tip;
	robot_end_tip[0] = this->t_tip_.at<float>(0,0);
	robot_end_tip[1] = this->t_tip_.at<float>(1,0);
	robot_end_tip[2] = this->t_tip_.at<float>(2,0);

	tf::poseMsgToKDL(trackerTipPose, trackerTipPose_);
	KDL::Vector robotTipPose = trackerTipPose_ * robot_end_tip;

	//    std::cout <<  robotTipPose[0] << std::endl;
	//    std::cout <<  robotTipPose[1] << std::endl;
	//    std::cout <<  robotTipPose[2] << std::endl;

	this->trackerTipPose.position.x = robotTipPose[0];
	this->trackerTipPose.position.y = robotTipPose[1];
	this->trackerTipPose.position.z = robotTipPose[2];
}

void HandEyeCalibrationCT::trackerTipMarker_callback(const visualization_msgs::MarkerPtr& tp_s)
{
	// convert to Pose
	this->trackerTipPose.orientation = tp_s->pose.orientation;
	this->trackerTipPose.position = tp_s->pose.position;
}

void HandEyeCalibrationCT::get_image_corners()
{
	this->corners.resize(ncorners);

	int step;
	int view_counter = 0;

	/*Find 2D points on chessboard*/
	while (1/*view_counter < nviews*/) {

		ros::spinOnce();

		if(imgL.empty()){
			printf("Out\n");
			usleep(1E5);
			ros::spinOnce();
			continue;
		}

		int pattern_was_found = cv::findChessboardCorners(imgL, pattern_size,
				corners, CV_CALIB_CB_ADAPTIVE_THRESH|CV_CALIB_CB_FILTER_QUADS);

		if (pattern_was_found && corners.size() == ncorners) {
			printf("In %d %d\n", pattern_was_found, view_counter);

			step = view_counter * ncorners;
			cv::Mat gray_img(imgL.rows, imgL.cols, CV_8UC1);

			cvtColor(imgL, gray_img, CV_BGR2GRAY);
			cornerSubPix(gray_img, corners, cvSize(3,3), cvSize(-1,-1),
					cvTermCriteria(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS,10,1E-4));

			//			this->image_points.push_back(corners[0]);
			//			this->image_points.push_back(corners[ pattern_size.width - 1]);
			//			this->image_points.push_back(corners[(pattern_size.height - 1) *
			//												 pattern_size.width]);
			//			this->image_points.push_back(corners[(pattern_size.height - 1) *
			//												 pattern_size.width + (pattern_size.width - 1)]);

			this->image_points.push_back(corners[1 * pattern_size.width + 1]);
			this->image_points.push_back(corners[(pattern_size.height - 2) * pattern_size.width + 1]);
			this->image_points.push_back(corners[1 * pattern_size.width + 3]);
			this->image_points.push_back(corners[(pattern_size.height - 2) * pattern_size.width + 3]);
			this->image_points.push_back(corners[1 * pattern_size.width + 5]);
			this->image_points.push_back(corners[(pattern_size.height - 2) * pattern_size.width + 5]);
			this->image_points.push_back(corners[1 * pattern_size.width + 7]);
			this->image_points.push_back(corners[(pattern_size.height - 2) * pattern_size.width + 7]);

			cv::imshow("Pose",imgL);
			break;
		}

		cv::imshow("Pose",imgL);

		cvWaitKey(250);
	}

	std::cout << "Image Points saved!" << std::endl;
}

void HandEyeCalibrationCT::save_current_device_pose()
{

	//	fromPosetoMat(this->trackerCameraPose,
	//			this->camera_pose.quat, this->camera_pose.t);
	//
	//	fromPosetoMat(this->robotPose,
	//			this->robot_pose.quat, this->robot_pose.t);
	//
	//	saveExtrisicParamsCamera(
	//			this->path +  "calibration/Tracker_Camera_Transformation.xml",
	//			this->camera_pose.quat, this->camera_pose.t);
	//
	//	saveExtrisicParamsCamera(
	//			this->path + "calibration/Tracker_Endoscope_Transformation.xml",
	//			this->robot_pose.quat, this->robot_pose.t);

}

void HandEyeCalibrationCT::get_device_points()
{
	/*Save 3D points with robot tip*/
	ros::spinOnce();
	std::cout << "Acquire points pressing l ; " << std::endl;

	cv::Point3f position;
	int counter = 0;

	while( 1 ) {

		ros::spinOnce();
		int radius = 10;
		cv::Point2f center;

		// draw points to be stored
		for (int i=0; i< npoints; i++) {
			center = image_points[i];
			if (i == counter) {
				circle(imgL, center, radius, CV_RGB(255,0,0),2);
			}
			else {
				circle(imgL, center, radius, CV_RGB(0,255,0),2);
			}
		}

		//dispaly image
		cv::imshow("Pose",imgL);

		int c = cvWaitKey( 15 ) & 0xFF;

		//printf("%f %f %f\n",pose_mm[0],pose_mm[1],pose_mm[2]);
		if ( c == 'l' ) {

			position.x = trackerTipPose.position.x;
			position.y = trackerTipPose.position.y;
			position.z = trackerTipPose.position.z;
			//printf("TCP1: %f %f %f\n", shared_tcp1_pose[0], shared_tcp1_pose[1], shared_tcp1_pose[2]);
			std::cout << "Acquired:" << "x" << position.x << "y" << position.y << "z" << position.z << std::endl;

			object_points.push_back(position);

			counter += 1;

			if (counter == npoints) {
				break;
			}

		}
	}
}



bool HandEyeCalibrationCT::compute_extrinsics()
{

	cvNamedWindow( "Pose", 1 );

	//initialization
	this->npoints = 8;
	this->pattern_size = cv::Size(9,6);
	//	pattern_size = cv::Size(7,5);
	this->ncorners = this->pattern_size.height * this->pattern_size.width;


	//    compute corner in camera {C} reference system
	this->get_image_corners();

	//compute corner in tracker {T} reference system
	this->get_device_points();

	/*Compute 2D-3D calibration between tracker {T} and camera {C} */
	std::cout << "Computing Tracker-Camera Transformation calibration ..." << std::endl;

	std::string camera_name;
	if(this->camera_model == MISUMI)
	{
		camera_name = "/misumi/";
	}
	else if(this->camera_model == DAVINCI)
	{
		camera_name = "/davinci/";
	}
	else if(this->camera_model == PROSILICA)
	{
		camera_name = "/prosilica/";
	}
	else if(this->camera_model == UEYE)
	{
		camera_name = "/ueye/";
	}
	else
	{
		std::cout << "[ERROR] No camera found!" << std::endl;
		camera_name = "/";
	}


	readParamsCamera(
			this->path + "camera" + camera_name + "LeftCameraParameters.xml",
			this->cameraMatrix,
			this->distCoeff);

	solvePnP(this->object_points, this->image_points,
			this->cameraMatrix, this->distCoeff, R_tracker_camera, t_tracker_camera);

	/*Project object points using the computed extrinsic
	 *  calibration for visual evaluation*/

	projectPoints(this->object_points, R_tracker_camera, t_tracker_camera,
			this->cameraMatrix, this->distCoeff, this->image_projected);

	//Show points on image
	for (int i=0; i<npoints; i++) {
		CvPoint center = image_projected[i];
		int radius = 10;
		circle(imgL, center, radius, CV_RGB(0,0,255), 2);
	}

	//dispaly image
	cv::imshow("Pose",imgL);
	std::cout << "Press any key to continue .." << std::endl;
	cvWaitKey(0);

	std::cout << "Tracker Camera Transform: " << std::endl;
	std::cout << "Rotation " << R_tracker_camera << std::endl;
	std::cout << "Translation " << t_tracker_camera << std::endl;
	Rodrigues(R_tracker_camera, R_tracker_camera_r);

	//	projectPoints(object_points, R_r, T, cameraMatrix, distCoeff, image_projected);

	/*Save Extrinsic calibration*/
	saveExtrisicParamsCamera(
			this->path + "camera" +camera_name + "/Tracker_Camera_Transform.xml",
			R_tracker_camera_r , t_tracker_camera);

	compute_camera_endoscope_pose();

	saveExtrisicParamsCamera(
			this->path + "camera" + camera_name + "/Endoscope_Camera_Transform.xml",
			this->R_endoscope_camera , this->t_endoscope_camera);

	std::cout << "Done computing hand_eye calibration!" << std::endl;

	corners.clear();
	object_points.clear();
	image_points.clear();
	image_projected.clear();

	////////// ERROR EVALUATION /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	bool compute_error = true;
	if(compute_error)
		this->compute_error();

	return true;
}

void HandEyeCalibrationCT::compute_error()
{
	std::cout << " --------------ERROR EVALUATION--------- " << std::endl;

	this->get_image_corners();

	this->get_device_points();

	printf("Computing extrinsics\n");

	projectPoints(object_points, this->R_tracker_camera, this->t_tracker_camera,
			this->cameraMatrix, this->distCoeff, this->image_projected);

	//Show points on image
	for (int i=0; i<npoints; i++) {
		CvPoint center = image_projected[i];
		int radius = 10;
		circle(imgL, center, radius, CV_RGB(0,0,255), 2);
	}


	//dispaly image
	cv::imshow("Pose",imgL);
	cvWaitKey(0);

	////// Computing the reprojection error ////////////////////////////////////////////////////////////////////////////////////////////////////////////
	float temp = 0;
	float temp1 = 0;
	float errorpos = 0;
	float temp2 = 0;
	float totalErr = 0;
	float sum = 0;
	float RMS = 0;

	std::vector<float> err;

	err.resize(npoints);

	CvScalar average = cvScalarAll(0);
	CvScalar std = cvScalarAll(0);

	for (int j=0; j< npoints;j++){

		std::cout << "image_points" << image_points[j] << std::endl;
		std::cout << "image_projected" << image_projected[j] << std::endl;


		temp = image_points[j].x - image_projected[j].x;
		temp = temp * temp;
		temp1 = image_points[j].y - image_projected[j].y;
		temp1 = temp1 * temp1;
		temp2 = temp1 + temp;
		errorpos = sqrt(temp2);
		err[j] = errorpos;

	}
	for (int j = 0; j < npoints;j++){
		sum += err[j];
	}

	sum = sum / npoints;

	RMS = sqrt(sum);
	std::cout << "Root Mean Square Error:" << RMS << std::endl;


	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	err.clear();
	corners.clear();
	object_points.clear();
	image_points.clear();
	image_projected.clear();
}


void HandEyeCalibrationCT::compute_camera_endoscope_pose()
{
	KDL::Frame T_tracker_camera;
	KDL::Frame T_tracker_endoscope;
	KDL::Frame T_endoscope_camera;

	std::cout << "Rtc mat: " << R_tracker_camera_r << std::endl;
	std::cout << "ttc mat: " << t_tracker_camera << std::endl;

	// convert tracker_camera pose to KLD
	T_tracker_camera.M = KDL::Rotation(
			R_tracker_camera_r.at<double>(0,0), R_tracker_camera_r.at<double>(0,1), R_tracker_camera_r.at<double>(0,2),
			R_tracker_camera_r.at<double>(1,0), R_tracker_camera_r.at<double>(1,1), R_tracker_camera_r.at<double>(1,2),
			R_tracker_camera_r.at<double>(2,0), R_tracker_camera_r.at<double>(2,1), R_tracker_camera_r.at<double>(2,2));
	T_tracker_camera.p[0] = t_tracker_camera.at<double>(0,0);
	T_tracker_camera.p[1] = t_tracker_camera.at<double>(1,0);
	T_tracker_camera.p[2] = t_tracker_camera.at<double>(2,0);

	std::cout << "Ttc kdl: " << std::endl;
	printKDLFrame(T_tracker_camera);

	//save Endoscope pose
	tf::poseMsgToKDL(this->trackerEndoscopePose, T_tracker_endoscope);

	std::cout << "Tte kdl: "<< std::endl;
	printKDLFrame(T_tracker_endoscope);

	// compute T between image and camera as:
	T_endoscope_camera = T_tracker_endoscope.Inverse() * T_tracker_camera.Inverse();

	std::cout << "Tec kdl: " << std::endl;
	printKDLFrame(T_endoscope_camera);

	R_endoscope_camera.create(3,3, CV_64FC1);
	this->R_endoscope_camera.at<double>(0,0) = T_endoscope_camera.M.data[0];
	this->R_endoscope_camera.at<double>(0,1) = T_endoscope_camera.M.data[1];
	this->R_endoscope_camera.at<double>(0,2) = T_endoscope_camera.M.data[2];
	this->R_endoscope_camera.at<double>(1,0) = T_endoscope_camera.M.data[3];
	this->R_endoscope_camera.at<double>(1,1) = T_endoscope_camera.M.data[4];
	this->R_endoscope_camera.at<double>(1,2) = T_endoscope_camera.M.data[5];
	this->R_endoscope_camera.at<double>(2,0) = T_endoscope_camera.M.data[6];
	this->R_endoscope_camera.at<double>(2,1) = T_endoscope_camera.M.data[7];
	this->R_endoscope_camera.at<double>(2,2) = T_endoscope_camera.M.data[8];

	t_endoscope_camera.create(3,1, CV_64FC1);
	this->t_endoscope_camera.at<double>(0,0) = T_endoscope_camera.p.data[0];
	this->t_endoscope_camera.at<double>(1,0) = T_endoscope_camera.p.data[1];
	this->t_endoscope_camera.at<double>(2,0) = T_endoscope_camera.p.data[2];

	std::cout << "Rec mat: " << R_endoscope_camera << std::endl;
	std::cout << "tec mat: " << t_endoscope_camera << std::endl;

}

void HandEyeCalibrationCT::printKDLFrame(KDL::Frame in)
{
	std::cout << "[" << in.M.data[0]  << " " << in.M.data[3] << " " << in.M.data[0] << " " << std::endl;
	std::cout        << in.M.data[1]  << " " << in.M.data[4] << " " << in.M.data[7] << " " << std::endl;
	std::cout        << in.M.data[2]  << " " << in.M.data[5] << " " << in.M.data[8] << " ]" << std::endl;

	std::cout << "[" << in.p.data[0]  << " " << in.p.data[1] << " " << in.p.data[2] << " ]" << std::endl;

}

