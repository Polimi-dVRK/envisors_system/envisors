/*
 * cam_dev_calib.hpp
 *
 *  Created on: Oct 10, 2016
 *      Author: veronica
 */

#ifndef CAM_DEV_CALIB_INCLUDE_CAM_DEV_CALIB_HPP_
#define CAM_DEV_CALIB_INCLUDE_CAM_DEV_CALIB_HPP_


#include <stdio.h>
#include <sstream>
#include <vector>

//ROS
#include <ros/ros.h>
#include "ros/rate.h"
#include "std_msgs/String.h"
#include <sensor_msgs/fill_image.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/CameraInfo.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Bool.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/Image.h>
#include <cv_bridge/cv_bridge.h>
#include <ros/package.h>
#include <image_transport/image_transport.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseStamped.h>
#include <visualization_msgs/Marker.h>


//OpenCV
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <cv_bridge/cv_bridge.h>
#include "opencv/cxcore.h"
#include <opencv2/core/core.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

//tf
#include <tf/LinearMath/Matrix3x3.h>
#include <kdl/frames.hpp>
#include <kdl_conversions/kdl_msg.h>

// My Library
#include"enVisorsLib/common.hpp"
#include"enVisorsLib/paths.h"
#include"enVisorsLib/define.hpp"



struct Pose
{
    cv::Mat quat;
    cv::Mat t;

};


class HandEyeCalibrationCT
{
public:


    /*variables */
    std::string path;
    cv::Mat imgL;
    cv::Size img_size;
    int npoints;
    cv::Size pattern_size;
    int corner_count;
    int ncorners;
    int camera_model;
    int tracker_model;
    std::string tracker_name;
    cv::Mat t_tip_;


    geometry_msgs::Pose trackerEndoscopePose;
    geometry_msgs::Pose trackerTipPose;
    KDL::Frame trackerTipPose_;

    std::vector<cv::Point2f> image_points;
    std::vector<cv::Point3f> object_points;
    std::vector<cv::Point2f> image_projected;
    std::vector<cv::Point2f> corners;

    cv::Mat cameraMatrix;
    cv::Mat distCoeff;
    cv::Mat R_tracker_camera, t_tracker_camera;
    cv::Mat R_tracker_camera_r;
    cv::Mat R_endoscope_camera;
    cv::Mat t_endoscope_camera;


    HandEyeCalibrationCT();

    /*methods */
    void left_image_rgb_callback(const sensor_msgs::ImageConstPtr& rgb_l);
    /*callback */
    void robotPose_callback(const geometry_msgs::PosePtr& r);
    void robotPoseStamped_callback(const geometry_msgs::PoseStampedPtr& r_s);
    void trackerEndoscopePose_callback(const geometry_msgs::PosePtr& t);
    void trackerEndoscopePoseStamped_callback(const geometry_msgs::PoseStampedPtr& t_s);
    void trackerTipPose_callback(const geometry_msgs::PosePtr& tp);
    void trackerTipPoseStamped_callback(const geometry_msgs::PoseStampedPtr& tp_s);

    void trackerTipMarker_callback(const visualization_msgs::MarkerPtr& tp);
    void trackerEndoscopeMarker_callback(const visualization_msgs::MarkerPtr& t_s);

    void get_image_corners();
    void save_current_device_pose();
    void get_device_points();
    bool compute_extrinsics();
    void compute_error();
    void compute_camera_endoscope_pose();
    void printKDLFrame(KDL::Frame in);

    /*ROS parameters */
    ros::NodeHandle n;
    image_transport::Subscriber left_image_rgb_sub;
    ros::Subscriber robotPose_sub;
    ros::Subscriber robotPoseStamped_sub;
    ros::Subscriber trackerEndoscopePose_sub;
    ros::Subscriber trackerEndoscopePoseStamped_sub;
    ros::Subscriber trackerTipPose_sub;
    ros::Subscriber trackerTipPoseStamped_sub;

    ros::Publisher cameraPose_pub;
};




#endif /* CAM_DEV_CALIB_INCLUDE_CAM_DEV_CALIB_HPP_ */
