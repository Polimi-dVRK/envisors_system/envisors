/**
 * @file	ros/visualizer/src/main.cpp
 * @author	Veronica Penza
 * @version	1.0
 * @date	27-Agosto-2014
 * @brief	visualization of images from misumi cam with OpenCV
 * @details	This is a ROS node to visualize images with OpenCV
 */

#include "visualizer/visualizer.hpp"

/** ********************************************************************************************************************
 Main Fuction
 ***********************************************************************************************************************/


int main(int argc, char **argv)
{
	ros::init (argc, argv, "visualizer");
	Visualizer visualizer;
	ros::Rate loop_rate(10);
	Mat rect_images;

	visualizer.f = boost::bind(&Visualizer::callback_dynamic_reconfigure, &visualizer, _1, _2);
	visualizer.server.setCallback(visualizer.f);

	startWindowThread();


	while (ros::ok()){

		if(visualizer.isRECT_images_prop)
		{
			visualizer.show_rect_images();
		}
		else
		{
			cv::destroyWindow("Images Rectified");
		}

		if(visualizer.isSidebyside_prop)
		{
			visualizer.show_sidebyside_images();
		}
		else
		{
			cv::destroyWindow("sidebyside");

		}

		ros::spinOnce();
		cv::waitKey(1);
		loop_rate.sleep();

	}

}

