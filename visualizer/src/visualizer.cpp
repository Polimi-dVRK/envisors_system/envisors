/*  misca_vis.cpp
 *
 *  Created on: Agosto 27, 2014
 *  Author: Veronica
 */

#include "visualizer/visualizer.hpp"

//Constructor
Visualizer::Visualizer(){

	image_transport::ImageTransport it(this->n);

	//	namedWindow("Right Pics RGB", CV_WINDOW_AUTOSIZE);
	//	namedWindow("Left Video RGB", CV_WINDOW_AUTOSIZE);
	//	namedWindow("Right Video RGB", CV_WINDOW_AUTOSIZE);
	//	namedWindow("Left Image RGB", CV_WINDOW_AUTOSIZE);
	//	namedWindow("Right Image RGB", CV_WINDOW_AUTOSIZE);
	//	namedWindow("Left Image Gray", CV_WINDOW_AUTOSIZE);
	//	namedWindow("Right Image Gray", CV_WINDOW_AUTOSIZE);
	//	namedWindow("Left Image Rectified", CV_WINDOW_AUTOSIZE);
	//	namedWindow("Right Image Rectified", CV_WINDOW_AUTOSIZE);
	//	namedWindow("Left Image Census", CV_WINDOW_AUTOSIZE);
	//	namedWindow("Right Image Census", CV_WINDOW_AUTOSIZE);
	//	namedWindow("Left Image Equalized", CV_WINDOW_AUTOSIZE);
	//	namedWindow("Right Image Equalized", CV_WINDOW_AUTOSIZE);
	//	namedWindow("Images Rectified", CV_WINDOW_AUTOSIZE);
	//	namedWindow("Disparity", CV_WINDOW_AUTOSIZE);
	//	namedWindow("myDisparity", CV_WINDOW_AUTOSIZE);

	// Subscriber
	this->left_pics_rgb_sub = it.subscribe("/picture/left/rgb", 1, &Visualizer::left_pics_rgb_callback, this);
	this->right_pics_rgb_sub = it.subscribe("/picture/right/rgb", 1, &Visualizer::right_pics_rgb_callback, this);
	this->left_video_rgb_sub = it.subscribe("/video/left/rgb", 1, &Visualizer::left_video_rgb_callback, this);
	this->right_video_rgb_sub = it.subscribe("/video/right/rgb", 1, &Visualizer::right_video_rgb_callback, this);
//	this->left_image_rgb_sub = it.subscribe("/uralp/camera_left/image", 1, &Visualizer::left_image_rgb_callback, this);
//	this->right_image_rgb_sub = it.subscribe("/uralp/camera_right/image", 1, &Visualizer::right_image_rgb_callback, this);
	this->left_image_rgb_sub = it.subscribe("/iit/ecm/left", 1, &Visualizer::left_image_rgb_callback, this);
	this->right_image_rgb_sub = it.subscribe("/iit/ecm/right", 1, &Visualizer::right_image_rgb_callback, this);

	this->master_image_rgb_sub = it.subscribe("/master/image_raw", 1, &Visualizer::master_image_rgb_callback, this);
	this->slave_image_rgb_sub = it.subscribe("/slave1/image_raw", 1, &Visualizer::slave_image_rgb_callback, this);
	this->left_image_gray_sub = it.subscribe("/image/left/gray", 1, &Visualizer::left_image_gray_callback, this);
	this->right_image_gray_sub = it.subscribe("/image/right/gray", 1, &Visualizer::right_image_gray_callback, this);
	this->left_image_rectified_sub = it.subscribe("/image/left/rectified", 1, &Visualizer::left_image_rectified_callback, this);
	this->right_image_rectified_sub = it.subscribe("/image/right/rectified", 1, &Visualizer::right_image_rectified_callback, this);
	this->left_image_census_sub = it.subscribe("/image/left/census", 1, &Visualizer::left_image_census_callback, this);
	this->right_image_census_sub = it.subscribe("/image/right/census", 1, &Visualizer::right_image_census_callback, this);
	this->left_image_equalized_sub = it.subscribe("/image/left/equalized", 1, &Visualizer::left_image_equalized_callback, this);
	this->right_image_equalized_sub = it.subscribe("/image/right/equalized", 1, &Visualizer::right_image_equalized_callback, this);
//	this->images_rectified_sub = it.subscribe("/images/rectified", 1, &Visualizer::images_rectified_callback, this);
	this->disparity_sub = it.subscribe("/image/disparity", 1, &Visualizer::disparity_callback, this);
	this->mydisparity_sub = it.subscribe("/image/mydisparity", 1, &Visualizer::mydisparity_callback, this);
	this->isRect_sub = this->n.subscribe("/image/isRectified", 1000, &Visualizer::isRect_callback, this );
	this->left_superpixels_sub = it.subscribe("/image/left/superpixels", 1, &Visualizer::superpixels_callback, this );
	this->left_features_sub = it.subscribe("/image/left/features", 1, &Visualizer::featuresLeft_callback, this );
	this->right_features_sub = it.subscribe("/image/right/features", 1, &Visualizer::featuresRight_callback, this );
	this->augmented_img_sub = it.subscribe("/image/left/augmented", 1, &Visualizer::augmented_image_callback, this );
	this->matches_sub = it.subscribe("/tracking/matches", 1, &Visualizer::matches_callback, this );
	this->optical_flow_sub = it.subscribe("/tracking/optical_flow", 1, &Visualizer::optical_flow_callback, this );
	this->mask_reselection_sub = it.subscribe("/tracking/mask_reselection", 1, &Visualizer::mask_reselection_callback, this );
	this->template_object_sub = it.subscribe("/tracking/template_object", 1, &Visualizer::tamplate_object_callback, this );
	this->hough_space_sub = it.subscribe("/tracking/hough_space", 1, &Visualizer::hough_space_callback, this );
	this->seg_prob_sub = it.subscribe("/tracking/seg_probability", 1, &Visualizer::seg_prob_callback, this );


	this->flag_rect = false;

	//	pointCloud_sub = n.subscribe("/point_cloudCustom", 100, &Visualizer::pointCloud_callback, this);
}


void Visualizer::isRect_callback(const std_msgs::Bool::ConstPtr& msg){
	this->flag_rect = msg->data;
	//	cout << this->flag_rect << endl;
}


void Visualizer::callback_dynamic_reconfigure(visualizer::VisConfig &config, uint32_t level){

	dyn_reconfig = &config;

	this->isPICLeft_prop = dyn_reconfig->isPICLeft_;
	this->isPICRight_prop = dyn_reconfig->isPICRight_;
	this->isVIDEOLeft_prop = dyn_reconfig->isVIDEOLeft_;
	this->isVIDEORight_prop = dyn_reconfig->isVIDEORight_;
	this->isRGBLeft_prop = dyn_reconfig->isRGBLeft_;
	this->isRGBRight_prop = dyn_reconfig->isRGBRight_;
	this->isGRAYLeft_prop = dyn_reconfig->isGRAYLeft_;
	this->isGRAYRight_prop = dyn_reconfig->isGRAYRight_;
	this->isRECTLeft_prop = dyn_reconfig->isRECTLeft_;
	this->isRECTRight_prop = dyn_reconfig->isRECTRight_;
	this->isRECT_prop = dyn_reconfig->isRECT_;
	this->isRECT_images_prop = dyn_reconfig->isRECT_images_;
	this->isEQUALIZEDLeft_prop = dyn_reconfig->isEQUALIZEDLeft_;
	this->isEQUALIZEDRight_prop = dyn_reconfig->isEQUALIZEDRight_;
	this->isCENSUSLeft_prop = dyn_reconfig->isCENSUSLeft_;
	this->isCENSUSRight_prop = dyn_reconfig->isCENSUSRight_;
	this->isDisparity_prop = dyn_reconfig->isDisparity_;
	this->isMYDisparity_prop = dyn_reconfig->isMYDisparity_;
	this->isSuperPixels_prop = dyn_reconfig->isSuperPixels_;
	this->isFeaturesLeft_prop = dyn_reconfig->isFeaturesLeft_;
	this->isFeaturesRight_prop = dyn_reconfig->isFeaturesRight_;
	this->isAugmented_prop = dyn_reconfig->isAugmented_;
	this->isImg_matches_prop = dyn_reconfig->isImg_matches_;
	this->isOpticalFlow_prop = dyn_reconfig->isOpticalFlow_;
	this->isMaskReselection_prop = dyn_reconfig->isMaskReselection_;
	this->isTemplateObject_prop = dyn_reconfig->isTemplateObject_;
	this->isHough_space_prop = dyn_reconfig->isHough_space_;
	this->isSegProb_prop = dyn_reconfig->isSegProb_;
	this->isSidebyside_prop = dyn_reconfig->isSidebyside_;


	ROS_INFO("Reconfigure request : %d" , this->isRGBLeft_prop);
}

void Visualizer::left_pics_rgb_callback(const sensor_msgs::ImageConstPtr& pics_l){

	fromImagetoMat(*pics_l, &this->pics_left_rgb, pics_l->encoding);

	// Check for invalid input
	if(! this->pics_left_rgb.data ){
		cout <<  "No left RGB image data" << std::endl ;
		return;
	}

	if (this->isPICLeft_prop){
		cv::namedWindow("Left Pics RGB", CV_WINDOW_AUTOSIZE);
		cv::imshow("Left Pics RGB", this->pics_left_rgb);
	}
	else{
		//		cv::destroyWindow("Left Pics RGB");
	}

}

void Visualizer::right_pics_rgb_callback(const sensor_msgs::ImageConstPtr& pics_r){

	fromImagetoMat(*pics_r, &this->pics_right_rgb, pics_r->encoding);

	// Check for invalid input
	if(! this->pics_right_rgb.data ){
		cout <<  "No right RGB image data" << std::endl ;
		return;
	}

	if (this->isPICRight_prop){
		cv::namedWindow("Right Pics RGB", CV_WINDOW_AUTOSIZE);
		cv::imshow("Right Pics RGB", this->pics_right_rgb);
	}
	else{
		//		cv::destroyWindow("Right Pics RGB");
	}


}

void Visualizer::left_video_rgb_callback(const sensor_msgs::ImageConstPtr& video_l){

	fromImagetoMat(*video_l, &this->video_left_rgb, video_l->encoding);

	// Check for invalid input
	if(! this->video_left_rgb.data ){
		cout <<  "No left VIDEO data" << std::endl ;
		return;
	}

	if (this->isVIDEOLeft_prop){
		cv::namedWindow("Left Video RGB", CV_WINDOW_AUTOSIZE);
		cv::imshow("Left Video RGB", this->video_left_rgb);
	}
	else{
		//		cv::destroyWindow("Left Video RGB");
	}

}

void Visualizer::right_video_rgb_callback(const sensor_msgs::ImageConstPtr& video_r){

	fromImagetoMat(*video_r, &this->video_right_rgb, video_r->encoding);

	// Check for invalid input
	if(! this->video_right_rgb.data ){
		cout <<  "No right VIDEO data" << std::endl ;
		return;
	}

	if (this->isVIDEORight_prop){
		cv::namedWindow("Right Video RGB", CV_WINDOW_AUTOSIZE);
		cv::imshow("Right Video RGB", this->video_right_rgb);
	}
	else{
		//cv::destroyWindow("Right Video RGB");
	}

}

void Visualizer::left_image_rgb_callback(const sensor_msgs::ImageConstPtr& rgb_l){

	fromImagetoMat(*rgb_l, &this->img_left_rgb, rgb_l->encoding);

	// Check for invalid input
	if(! this->img_left_rgb.data ){
		cout <<  "No left RGB image data" << std::endl ;
		return;
	}

	if (this->isRGBLeft_prop){
		cv::namedWindow("Left Image RGB", CV_WINDOW_AUTOSIZE);
		cv::imshow("Left Image RGB", this->img_left_rgb);
	}
	else{
		//cv::destroyWindow("Left Image RGB");
	}

}

void Visualizer::right_image_rgb_callback(const sensor_msgs::ImageConstPtr& rgb_r){

	fromImagetoMat(*rgb_r, &this->img_right_rgb, rgb_r->encoding);

	// Check for invalid input
	if(! this->img_right_rgb.data ){
		cout <<  "No rigth RGB image data" << std::endl ;
		return;
	}

	if (this->isRGBRight_prop){
		cv::namedWindow("Right Image RGB", CV_WINDOW_AUTOSIZE);
		cv::imshow("Right Image RGB", this->img_right_rgb);
	}
	else{
		//cv::destroyWindow("Right Image RGB");
	}
}

void Visualizer::master_image_rgb_callback(const sensor_msgs::ImageConstPtr& master){

	fromImagetoMat(*master, &this->img_left_rgb, master->encoding);

	// Check for invalid input
	if(! this->img_left_rgb.data ){
		cout <<  "No left RGB image data" << std::endl ;
		return;
	}

	if (this->isRGBLeft_prop){
		cv::namedWindow("Left Image RGB", CV_WINDOW_AUTOSIZE);
		cv::imshow("Left Image RGB", this->img_left_rgb);
	}
	else{
		//cv::destroyWindow("Left Image RGB");
	}

}

void Visualizer::slave_image_rgb_callback(const sensor_msgs::ImageConstPtr& slave){

	fromImagetoMat(*slave, &this->img_right_rgb, slave->encoding);

	// Check for invalid input
	if(! this->img_right_rgb.data ){
		cout <<  "No rigth RGB image data" << std::endl ;
		return;
	}

	if (this->isRGBRight_prop){
		cv::namedWindow("Right Image RGB", CV_WINDOW_AUTOSIZE);
		cv::imshow("Right Image RGB", this->img_right_rgb);
	}
	else{
		//cv::destroyWindow("Right Image RGB");
	}
}

void Visualizer::left_image_gray_callback(const sensor_msgs::ImageConstPtr& gray_l){

	fromImagetoMat(*gray_l, &this->img_left_gray, gray_l->encoding);

	// Check for invalid input
	if(! this->img_left_gray.data ){
		cout <<  "No left GRAY image data" << std::endl ;
		return;
	}

	if (this->isGRAYLeft_prop){
		cv::namedWindow("Left Image Gray", CV_WINDOW_AUTOSIZE);
		cv::imshow("Left Image Gray", this->img_left_gray);
	}
	else{
		//cv::destroyWindow("Left Image Gray");
	}

}

void Visualizer::right_image_gray_callback(const sensor_msgs::ImageConstPtr& gray_r){

	fromImagetoMat(*gray_r, &this->img_right_gray, gray_r->encoding);

	// Check for invalid input
	if(! this->img_right_gray.data ){
		cout <<  "No right GRAY image data" << std::endl ;
		return;
	}

	if (this->isGRAYRight_prop){
		cv::namedWindow("Right Image Gray", CV_WINDOW_AUTOSIZE);
		cv::imshow("Right Image Gray", this->img_right_gray);
	}
	else{
		//cv::destroyWindow("Right Image Gray");
	}

}

void Visualizer::left_image_rectified_callback(const sensor_msgs::ImageConstPtr& rect_l){

	this->imageSize.height = rect_l->height;
	this->imageSize.width = rect_l->width;

	fromImagetoMat(*rect_l, &this->img_left_rect, rect_l->encoding);

	// Check for invalid input
	if(! this->img_left_rect.data ){
		cout <<  "No left RECT image data" << std::endl ;
		return;
	}

	if (this->isRECTLeft_prop){
		cv::namedWindow("Left Image Rectified", CV_WINDOW_AUTOSIZE);
		cv::imshow("Left Image Rectified", this->img_left_rect);
	}
	else{
		//cv::destroyWindow("Left Image Rectified");
	}

}


void Visualizer::right_image_rectified_callback(const sensor_msgs::ImageConstPtr& rect_r){
	fromImagetoMat(*rect_r, &this->img_right_rect, rect_r->encoding);

	// Check for invalid input
	if(! this->img_right_rect.data ){
		cout <<  "No right RECT image data" << std::endl ;
		return;
	}

	if (this->isRECTRight_prop){
		cv::namedWindow("Right Image Rectified", CV_WINDOW_AUTOSIZE);
		cv::imshow("Right Image Rectified", this->img_right_rect);
	}
	else{
		//cv::destroyWindow("Right Image Rectified");
	}

}

void Visualizer::left_image_census_callback(const sensor_msgs::ImageConstPtr& cns_l){
	fromImagetoMat(*cns_l, &this->img_left_census, cns_l->encoding);

	// Check for invalid input
	if(! this->img_left_census.data ){
		cout <<  "No left census image data" << std::endl ;
		return;
	}

	if (this->isCENSUSLeft_prop){
		cv::namedWindow("Left Image Census", CV_WINDOW_AUTOSIZE);
		cv::imshow("Left Image Census", this->img_left_census);
	}
	else{
		//cv::destroyWindow("Left Image Census");
	}

}

void Visualizer::right_image_census_callback(const sensor_msgs::ImageConstPtr& cns_r){
	fromImagetoMat(*cns_r, &this->img_right_census, cns_r->encoding);

	// Check for invalid input
	if(! this->img_right_census.data ){
		cout <<  "No right census image data" << std::endl ;
		return;
	}

	if (this->isCENSUSRight_prop){
		cv::namedWindow("Right Image Census", CV_WINDOW_AUTOSIZE);
		cv::imshow("Right Image Census", this->img_right_census);
	}
	else{
		//cv::destroyWindow("Right Image Census");
	}

}

void Visualizer::left_image_equalized_callback(const sensor_msgs::ImageConstPtr& eq_l){

	fromImagetoMat(*eq_l, &this->img_left_equalized, eq_l->encoding);

	// Check for invalid input
	if(! this->img_left_equalized.data ){
		cout <<  "No left equalized image data" << std::endl ;
		return;
	}

	if (this->isEQUALIZEDLeft_prop){
		cv::namedWindow("Left Image Equalized", CV_WINDOW_AUTOSIZE);
		cv::imshow("Left Image Equalized", this->img_left_equalized);
	}
	else{
		//cv::destroyWindow("Left Image Equalized");
	}


}
void Visualizer::right_image_equalized_callback(const sensor_msgs::ImageConstPtr& eq_r){

	fromImagetoMat(*eq_r, &this->img_right_equalized, eq_r->encoding);

	// Check for invalid input
	if(! this->img_right_equalized.data ){
		cout <<  "No right equalized image data" << std::endl ;
		return;
	}

	if (this->isEQUALIZEDRight_prop){
		cv::namedWindow("Right Image Equalized", CV_WINDOW_AUTOSIZE);
		cv::imshow("Right Image Equalized", this->img_right_equalized);
	}
	else{
		//cv::destroyWindow("Right Image Equalized");
	}


}

//void Visualizer::images_rectified_callback(const sensor_msgs::ImageConstPtr& imgs_rect){
//	fromImagetoMat(*imgs_rect, &this->rectified_images, imgs_rect->encoding);
//
//	// Check for invalid input
//	if(! this->rectified_images.data ){
//		cout <<  "No left and right RECT images data" << std::endl ;
//		return;
//	}
//
//	Point pt1, pt2;
//	for (int i=0; i<this->rectified_images.rows; i+=16){
//
//		pt1.x = 0;
//		pt1.y = i;
//		pt2.x = this->rectified_images.cols-1;
//		pt2.y = i;
//		//if(i%7==0)
//		line( this->rectified_images, pt1, pt2, CV_RGB(0, 255, 0));
//	}
//
//	if (this->isRECT_prop){
//		cv::namedWindow("Images Rectified", CV_WINDOW_AUTOSIZE);
//		cv::imshow("Images Rectified", this->rectified_images);
//	}
//	else{
//		//cv::destroyWindow("Images Rectified");
//	}
//	//	imwrite("Rectified_Images.png", this->rectified_images);
//}

void Visualizer::disparity_callback(const sensor_msgs::ImageConstPtr& disp){
	fromImagetoMat(*disp, &this->disparity, MONO8);

	// Check for invalid input
	if(! this->disparity.data ){
		cout <<  "No disparity image data" << std::endl ;
		return;
	}

	if (this->isDisparity_prop){
		cv::namedWindow("Disparity", CV_WINDOW_AUTOSIZE);
		cv::imshow("Disparity", this->disparity);
	}
	else{
		//cv::destroyWindow("Disparity");
	}

}

void Visualizer::mydisparity_callback(const sensor_msgs::ImageConstPtr& mydisp){

	fromImagetoMat(*mydisp, &this->mydisparity, mydisp->encoding);

	// Check for invalid input
	if(! this->mydisparity.data ){
		cout <<  "No mydisparity image data" << std::endl ;
		return;
	}

	//	mydisparity.convertTo(this->mydisparity_scaled, CV_8U, scale_prop);

	if (this->isMYDisparity_prop){
		cv::namedWindow("myDisparity", CV_WINDOW_AUTOSIZE);
		cv::imshow("myDisparity", this->mydisparity);
	}
	else{
		//cv::destroyWindow("myDisparity");
	}

}

void Visualizer::superpixels_callback(const sensor_msgs::ImageConstPtr& spx){

	fromImagetoMat(*spx, &this->superpixels_image, spx->encoding);

	// Check for invalid input
	if(! this->superpixels_image.data ){
		cout <<  "No super pixels image data" << std::endl ;
		return;
	}

	//	superpixels_image.convertTo(this->mydisparity_scaled, CV_8U, scale_prop);

	if (this->isSuperPixels_prop){
		cv::namedWindow("SuperPixels", CV_WINDOW_AUTOSIZE);
		cv::imshow("SuperPixels", this->superpixels_image);
	}
	else{
		//cv::destroyWindow("SuperPixels");
	}
}


void Visualizer::featuresLeft_callback(const sensor_msgs::ImageConstPtr& fl){
	fromImagetoMat(*fl, &this->img_features_left, fl->encoding);

	//	static int count = 0;
	//	count ++;
	//	char filename[64];
	//	sprintf(filename, "Left_Tracking_%03d.png", count);

	// Check for invalid input
	if(! this->img_features_left.data ){
		cout <<  "No features left image data" << std::endl ;
		return;
	}

	if (this->isFeaturesLeft_prop){
		cv::namedWindow("Left Tracking", CV_WINDOW_NORMAL);
		cv::imshow("Left Tracking", this->img_features_left);
		//		cv::updateWindow("Left Tracking");
		//		imwrite(filename, this->img_features_left);
	}
	else{
		//cv::destroyWindow("Left Tracking");
	}
}

void Visualizer::featuresRight_callback(const sensor_msgs::ImageConstPtr& fr){
	fromImagetoMat(*fr, &this->img_features_right, fr->encoding);

	static int count = 0;
	count ++;
	char filename[64];
	sprintf(filename, "Right_Tracking_%03d.png", count);

	// Check for invalid input
	if(! this->img_features_right.data ){
		cout <<  "No features right image data" << std::endl ;
		return;
	}

	if (this->isFeaturesRight_prop){
		cv::namedWindow("Right Tracking", WINDOW_NORMAL);
		cv::imshow("Right Tracking", this->img_features_right);
		//		imwrite(filename, this->img_features_right);
	}
	else{
		//cv::destroyWindow("Right Tracking");
	}
}

void Visualizer::matches_callback(const sensor_msgs::ImageConstPtr& mtc)
{
	fromImagetoMat(*mtc, &this->img_matches, mtc->encoding);

	//	static int count = 0;
	//	count ++;
	//	char filename[64];
	//	sprintf(filename, "Right_Tracking_%03d.png", count);

	// Check for invalid input
	if(! this->img_matches.data ){
		cout <<  "No features right image data" << std::endl ;
		return;
	}

	if (this->isImg_matches_prop){
		cv::namedWindow("Matches", WINDOW_NORMAL);
		cv::imshow("Matches", this->img_matches);
		//		imwrite(filename, this->img_features_right);
	}
	else{
		//cv::destroyWindow("Matches");
	}
}

void Visualizer::optical_flow_callback(const sensor_msgs::ImageConstPtr& mtc)
{
	fromImagetoMat(*mtc, &this->img_optical_flow, mtc->encoding);

	//	static int count = 0;
	//	count ++;
	//	char filename[64];
	//	sprintf(filename, "Right_Tracking_%03d.png", count);

	// Check for invalid input
	if(! this->img_optical_flow.data ){
		cout <<  "No features right image data" << std::endl ;
		return;
	}

	if (this->isOpticalFlow_prop){
		cv::namedWindow("KLT Optical Flow", WINDOW_NORMAL);
		cv::imshow("KLT Optical Flow", this->img_optical_flow);
		//		imwrite(filename, this->img_features_right);
	}
	else{
		//cv::destroyWindow("KLT Optical Flow");
	}
}

void Visualizer::mask_reselection_callback(const sensor_msgs::ImageConstPtr& mtc)
{
	fromImagetoMat(*mtc, &this->img_mask_relection, mtc->encoding);

	//	static int count = 0;
	//	count ++;
	//	char filename[64];
	//	sprintf(filename, "Right_Tracking_%03d.png", count);

	// Check for invalid input
	if(!this->img_mask_relection.data ){
		cout <<  "No features right image data" << std::endl ;
		return;
	}

	if (this->isMaskReselection_prop){
		cv::namedWindow("Mask Reselection", WINDOW_NORMAL);
		cv::imshow("Mask Reselection", this->img_mask_relection);
		//		imwrite(filename, this->img_features_right);
	}
	else{
		//cv::destroyWindow("Mask Reselection");
	}
}

void Visualizer::tamplate_object_callback(const sensor_msgs::ImageConstPtr& mtc)
{
	fromImagetoMat(*mtc, &this->img_template_object, mtc->encoding);

	// Check for invalid input
	if(! this->img_template_object.data ){
		cout <<  "No augmented image data" << std::endl ;
		return;
	}

	if (this->isTemplateObject_prop){
		cv::namedWindow("Object Template", CV_WINDOW_AUTOSIZE);
		cv::imshow("Object Template", this->img_template_object);
		//		imwrite(filename, this->img_features_right);
	}
	else{
		//cv::destroyWindow("Object Template");
	}
}

void Visualizer::hough_space_callback(const sensor_msgs::ImageConstPtr& mtc)
{
	fromImagetoMat(*mtc, &this->img_hough_space, mtc->encoding);

	// Check for invalid input
	if(! this->img_hough_space.data ){
		cout <<  "No augmented image data" << std::endl ;
		return;
	}

	if (this->isHough_space_prop){
		cv::namedWindow("Hough Space", CV_WINDOW_AUTOSIZE);
		cv::imshow("Hough Space", this->img_hough_space);
		//		imwrite(filename, this->img_features_right);
	}
	else{
		//cv::destroyWindow("Hough Space");
	}
}

void Visualizer::seg_prob_callback(const sensor_msgs::ImageConstPtr& mtc)
{
	fromImagetoMat(*mtc, &this->img_seg_prob, mtc->encoding);

	// Check for invalid input
	if(! this->img_seg_prob.data ){
		cout <<  "No augmented image data" << std::endl ;
		return;
	}

	if (this->isSegProb_prop){
		cv::namedWindow("Segmentation Probability", WINDOW_NORMAL);
		cv::imshow("Segmentation Probability", this->img_seg_prob);
		//		imwrite(filename, this->img_features_right);
	}
	else{
		//cv::destroyWindow("Segmentation Probability");
	}
}



void Visualizer::augmented_image_callback(const sensor_msgs::ImageConstPtr& ai){

	fromImagetoMat(*ai, &this->img_augmented, ai->encoding);

	// Check for invalid input
	if(! this->img_augmented.data ){
		cout <<  "No augmented image data" << std::endl ;
		return;
	}

	if (this->isAugmented_prop){
		cv::namedWindow("Augmented Image", CV_WINDOW_NORMAL);
		cv::imshow("Augmented Image", this->img_augmented);
		//		imwrite(filename, this->img_features_right);
	}
	else{
		//cv::destroyWindow("Augmented Image");
	}
}


void Visualizer::fromImagetoMat(const sensor_msgs::Image& img_in,cv::Mat* img_out, std::string encoding){

	std::cout << "fromImagetoMat" << std::endl;

	cv_bridge::CvImagePtr image_bridge;

	try
	{
		//std::cout << "  img_in: " << img_in.width << ", " << img_in.height << std::endl;
		image_bridge = cv_bridge::toCvCopy(img_in, encoding);
		//std::cout << "  image_bridge: " << image_bridge->image.rows << ", " << image_bridge->image.cols << std::endl;
	}
	catch (cv_bridge::Exception& e)
	{
		ROS_ERROR("cv_bridge exception: %s", e.what());
		return;
	}

	*img_out = image_bridge->image.clone();
}

void Visualizer::show_sidebyside_images()
{
	if(!img_left_rect.empty() && !img_right_rect.empty())
	{
		cv::Mat sidebyside_img;
		sidebyside_img.create(img_left_rect.rows, img_left_rect.cols*2, CV_8UC3);

		for(int i = 0; i < img_left_rect.rows; i++)
		{
			for(int j = 0; j < img_left_rect.cols; j++)
			{
				sidebyside_img.at<cv::Vec3b>(i,j) = img_left_rect.at<cv::Vec3b>(i,j);
			}
			for(int k =  img_left_rect.cols; k <  img_left_rect.cols*2; k++)
			{
				sidebyside_img.at<cv::Vec3b>(i,k) = img_right_rect.at<cv::Vec3b>(i, k -img_left_rgb.cols);
			}
		}

		cv::namedWindow("sidebyside", CV_WINDOW_NORMAL);
		cv::imshow("sidebyside", sidebyside_img);
	}

}

void Visualizer::show_rect_images()
{
	if(!img_left_rect.empty() && !img_right_rect.empty())
	{
		rectified_images.create(img_left_rect.rows, img_left_rect.cols*2, CV_8UC3);

		for(int i = 0; i < img_left_rect.rows; i++)
		{
			for(int j = 0; j < img_left_rect.cols; j++)
			{
				rectified_images.at<cv::Vec3b>(i,j) = img_left_rect.at<cv::Vec3b>(i,j);
			}
			for(int k =  img_left_rect.cols; k <  img_left_rect.cols*2; k++)
			{
				rectified_images.at<cv::Vec3b>(i,k) = img_right_rect.at<cv::Vec3b>(i, k -img_left_rgb.cols);
			}
		}

		Point pt1, pt2;
		for (int i=0; i<this->rectified_images.rows; i+=16){

			pt1.x = 0;
			pt1.y = i;
			pt2.x = this->rectified_images.cols-1;
			pt2.y = i;
			//if(i%7==0)
			line( this->rectified_images, pt1, pt2, CV_RGB(0, 255, 0));
		}

			cv::namedWindow("Images Rectified", CV_WINDOW_NORMAL);
			cv::imshow("Images Rectified", this->rectified_images);

	}

}
//void Visualizer::pointCloud_callback(const sensor_msgs::Image& pc){
//	//convert to cv_bridge image type
//	pointCloud_ = cv_bridge::toCvCopy(pc, sensor_msgs::image_encodings::BGR8);
//	//convert to cv::Mat
//	pointCloud = pointCloud_->image;
//	if (pointCloud.empty ())
//		ROS_INFO("Problem in reading left image");
//	//	imshow("LEFT image raw", pointCloud);
//
//}
