#include <stdio.h>
#include <sstream>

//ROS
#include <ros/ros.h>
#include "ros/rate.h"
#include "std_msgs/String.h"
#include <sensor_msgs/fill_image.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/CameraInfo.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Bool.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/Image.h>
#include <cv_bridge/cv_bridge.h>
#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <ros/package.h>
#include <image_transport/image_transport.h>

#include <dynamic_reconfigure/server.h>
#include <visualizer/VisConfig.h>



//OpenCV
//#include <opencv/cv.h>
//#include <opencv/highgui.h>
#include <cv_bridge/cv_bridge.h>
//#include "opencv/cxcore.h"
#include <opencv2/core/core.hpp>
//#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>



using namespace sensor_msgs::image_encodings;
using namespace cv;
using namespace std;
using namespace sensor_msgs;
using namespace message_filters;


class Visualizer
{
public:

	Visualizer();
    //Read images
    cv_bridge::CvImagePtr calib_left_;
    cv_bridge::CvImagePtr calib_right_;
    cv_bridge::CvImagePtr rgb_left_;
    cv_bridge::CvImagePtr rgb_right_;
    cv_bridge::CvImagePtr rect_left_;
    cv_bridge::CvImagePtr rect_right_;
    cv_bridge::CvImagePtr disparity_;
    cv_bridge::CvImagePtr mydisparity_;
    cv_bridge::CvImagePtr depthOpenCV_;
    cv_bridge::CvImagePtr depthCustom_;
    cv_bridge::CvImagePtr pointCloud_;
    cv_bridge::CvImagePtr census_left_;
    cv_bridge::CvImagePtr census_right_;
    cv_bridge::CvImagePtr equalized_left_;
    cv_bridge::CvImagePtr equalized_right_;
    cv_bridge::CvImagePtr rectified_images_;
    cv_bridge::CvImagePtr superpixels_image_;
    cv_bridge::CvImagePtr img_features_left_;
    cv_bridge::CvImagePtr img_features_right_;


    Mat pics_left_rgb;
    Mat pics_right_rgb;
    Mat video_left_rgb;
    Mat video_right_rgb;
    Mat img_left_rgb;
    Mat img_right_rgb;
    Mat img_left_gray;
    Mat img_right_gray;
    Mat img_left_rect;
    Mat img_right_rect;
    Mat img_left_census;
    Mat img_right_census;
    Mat img_left_equalized;
    Mat img_right_equalized;
    Mat rectified_images;
    Mat disparity;
    Mat mydisparity;
    Mat disparity_scaled;
    Mat mydisparity_scaled;
    Mat superpixels_image;
    Mat img_features_left;
    Mat img_features_right;
    Mat img_augmented;
    Mat img_matches;
    Mat img_optical_flow;
    Mat img_mask_relection;
    Mat img_template_object;
    Mat img_hough_space;
    Mat img_seg_prob;

    Size imageSize;

    int scale_prop;
    bool flag_rect;
    bool isPICLeft_prop;
    bool isPICRight_prop;
    bool isVIDEOLeft_prop;
    bool isVIDEORight_prop;
    bool isRGBLeft_prop;
    bool isRGBRight_prop;
    bool isRECTLeft_prop;
    bool isGRAYRight_prop;
    bool isGRAYLeft_prop;
    bool isRECTRight_prop;
    bool isRECT_prop;
    bool isRECT_images_prop;
    bool isCENSUSLeft_prop;
    bool isCENSUSRight_prop;
    bool isEQUALIZEDLeft_prop;
    bool isEQUALIZEDRight_prop;
    bool isDisparity_prop;
    bool isMYDisparity_prop;
    bool isDisparityScaled_prop;
    bool isMYDisparityScaled_prop;
    bool isSuperPixels_prop;
    bool isFeaturesLeft_prop;
    bool isFeaturesRight_prop;

    bool isAugmented_prop;
    bool isImg_matches_prop;
    bool isOpticalFlow_prop;
    bool isMaskReselection_prop;
    bool isTemplateObject_prop;
    bool isHough_space_prop;
    bool isSegProb_prop;
    bool isSidebyside_prop;


    visualizer::VisConfig* dyn_reconfig;
    dynamic_reconfigure::Server<visualizer::VisConfig> server;
    dynamic_reconfigure::Server<visualizer::VisConfig>::CallbackType f;

    void callback_dynamic_reconfigure(visualizer::VisConfig &config, uint32_t level);

    void left_pics_rgb_callback(const sensor_msgs::ImageConstPtr& pics_l);
	void right_pics_rgb_callback(const sensor_msgs::ImageConstPtr& pics_r);
    void left_video_rgb_callback(const sensor_msgs::ImageConstPtr& video_l);
    void right_video_rgb_callback(const sensor_msgs::ImageConstPtr& video_r);
    void left_image_rgb_callback(const sensor_msgs::ImageConstPtr& rgb_l);
	void right_image_rgb_callback(const sensor_msgs::ImageConstPtr& master);
	void master_image_rgb_callback(const sensor_msgs::ImageConstPtr& slave);
	void slave_image_rgb_callback(const sensor_msgs::ImageConstPtr& rgb_r);
	void left_image_gray_callback(const sensor_msgs::ImageConstPtr& gray_l);
	void right_image_gray_callback(const sensor_msgs::ImageConstPtr& gray_r);
	void left_image_rectified_callback(const sensor_msgs::ImageConstPtr& rect_l);
	void right_image_rectified_callback(const sensor_msgs::ImageConstPtr& rect_r);
	void left_image_census_callback(const sensor_msgs::ImageConstPtr& cns_l);
	void right_image_census_callback(const sensor_msgs::ImageConstPtr& cns_r);
	void left_image_equalized_callback(const sensor_msgs::ImageConstPtr& eq_l);
	void right_image_equalized_callback(const sensor_msgs::ImageConstPtr& eq_r);
	void images_rectified_callback(const sensor_msgs::ImageConstPtr& imgs_rect);
	void disparity_callback(const sensor_msgs::ImageConstPtr& disp);
	void mydisparity_callback(const sensor_msgs::ImageConstPtr& msg);
	void superpixels_callback(const sensor_msgs::ImageConstPtr& spx);

	void featuresLeft_callback(const sensor_msgs::ImageConstPtr& fl);
	void featuresRight_callback(const sensor_msgs::ImageConstPtr& fr);


	void augmented_image_callback(const sensor_msgs::ImageConstPtr& ai);

	void isRect_callback(const std_msgs::Bool::ConstPtr& msg);

	void matches_callback(const sensor_msgs::ImageConstPtr& mtc);
	void optical_flow_callback(const sensor_msgs::ImageConstPtr& mtc);
	void mask_reselection_callback(const sensor_msgs::ImageConstPtr& mtc);
	void tamplate_object_callback(const sensor_msgs::ImageConstPtr& mtc);
	void hough_space_callback(const sensor_msgs::ImageConstPtr& mtc);
	void seg_prob_callback(const sensor_msgs::ImageConstPtr& mtc);

	void fromImagetoMat(const sensor_msgs::Image& img_in,cv::Mat* img_out, std::string encoding);

	void show_sidebyside_images();
	void show_rect_images();

//	void depthOpenCV_callback(const sensor_msgs::Image& d_OCV);
//	void depthCustom_callback(const sensor_msgs::Image& d_Custom);
//	void pointCloud_callback(const sensor_msgs::Image& pc);


	ros::NodeHandle n;

	//Subscriber
	image_transport::Subscriber left_pics_rgb_sub;
	image_transport::Subscriber right_pics_rgb_sub;
	image_transport::Subscriber left_video_rgb_sub;
	image_transport::Subscriber right_video_rgb_sub;
	image_transport::Subscriber left_image_rgb_sub;
	image_transport::Subscriber right_image_rgb_sub;

	image_transport::Subscriber master_image_rgb_sub;
	image_transport::Subscriber slave_image_rgb_sub;

	image_transport::Subscriber left_image_gray_sub;
	image_transport::Subscriber right_image_gray_sub;
	image_transport::Subscriber left_image_rectified_sub;
	image_transport::Subscriber right_image_rectified_sub;
	image_transport::Subscriber left_image_census_sub;
	image_transport::Subscriber right_image_census_sub;
	image_transport::Subscriber left_image_equalized_sub;
	image_transport::Subscriber right_image_equalized_sub;
	image_transport::Subscriber images_rectified_sub;
	image_transport::Subscriber disparity_sub;
	image_transport::Subscriber mydisparity_sub;
	image_transport::Subscriber left_superpixels_sub;
	image_transport::Subscriber left_features_sub;
	image_transport::Subscriber right_features_sub;
	image_transport::Subscriber augmented_img_sub;
	image_transport::Subscriber matches_sub;
	image_transport::Subscriber optical_flow_sub;
	image_transport::Subscriber mask_reselection_sub;
	image_transport::Subscriber template_object_sub;
	image_transport::Subscriber hough_space_sub;
	image_transport::Subscriber seg_prob_sub;



	ros::Subscriber isRect_sub;


};
