//
// Created by tibo on 07/07/17.
//

#include "sa_tools/SASourceWindow.hpp"

int main(int argc, char** argv) {
    ros::init(argc, argv, "sa_from_cv_window");

    SASourceWindow sa;
    sa.run();
}
