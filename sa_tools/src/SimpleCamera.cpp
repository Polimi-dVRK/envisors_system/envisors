//
// Created by tibo on 07/06/17.
//

#include "sa_tools/SimpleCamera.hpp"

/**
 * Constructor
 *
 * @param node The ROS node used to initialise the \c image_transport::ImageTransport.
 */
SimpleCamera::SimpleCamera(const ros::NodeHandle &node)
    : _node(node), _it(node)
{
    // Do nothing
}


/**
 * Constructor
 *
 * @param node The ROS node used to initialise the \c image_transport::ImageTransport.
 * @param topic The topic on which to listen for images
 */
SimpleCamera::SimpleCamera(const ros::NodeHandle &node, const std::string &topic)
    : SimpleCamera(node)
{
    setCameraTopic(topic);
}


/**
 * Set the topic that the camera should listen on
 * @param topic The topic on which to listen for images
 */
void SimpleCamera::setCameraTopic(std::string topic) {
    _camera = _it.subscribeCamera(topic, 1, &SimpleCamera::OnImageReceived, this);
}


/**
 * Retrieve the latest image.
 *
 * To ensure that this function will return a real image you should call #SimpleCamera::getImageCount() first and check
 * that the image count is greater tha 0. Otherwise the `data` member of the image will be empty.
 *
 * @return A \c sensor_msgs::Image containing the latest image received.
 */
const sensor_msgs::Image & SimpleCamera::getLatestImage() const {
    return _latest_image;
}


/**
 * Get the number of images received by the camera so far.
 *
 * @return The number of images received by the camera so far.
 */
u_int SimpleCamera::getImageCount() const {
    return _image_count;
}


/**
 * Callback called when a new image is received.
 *
 * @param img The image
 * @param camera_info The intrinsic parameters of the camera
 */
void SimpleCamera::OnImageReceived(
    const sensor_msgs::ImageConstPtr &img,
    const sensor_msgs::CameraInfoConstPtr &camera_info
) {
    _latest_image = *img;
    _latest_camera_info = *camera_info;
    _image_count += 1;
}
