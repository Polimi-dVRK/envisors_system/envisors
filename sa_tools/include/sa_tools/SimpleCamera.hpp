//
// Created by tibo on 07/07/17.
//

#pragma once

#include <string>

#include <ros/ros.h>
#include <image_transport/image_transport.h>

/**
 * A simple camera class to expose a ROS \c image_transport compatible \c sensor_msgs::Image topic.
 */
class SimpleCamera
{
public:
    SimpleCamera(const ros::NodeHandle &node);
    SimpleCamera(
        const ros::NodeHandle &node,
        const std::string &topic
    );

    void setCameraTopic(std::string topic);

    const sensor_msgs::Image & getLatestImage() const;
    u_int getImageCount() const;

private /* callbacks */:

    void OnImageReceived(
        const sensor_msgs::ImageConstPtr &img,
        const sensor_msgs::CameraInfoConstPtr &camera_info
    );

protected:
    ros::NodeHandle _node;
    image_transport::ImageTransport _it;
    image_transport::CameraSubscriber _camera;

    u_int _image_count = 0;

    sensor_msgs::Image _latest_image;
    sensor_msgs::CameraInfo _latest_camera_info;
};

