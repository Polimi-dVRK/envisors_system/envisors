//
// Created by tibo on 07/07/17.
//

#pragma once

#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <geometry_msgs/Polygon.h>

#include "sa_tools/SimpleCamera.hpp"

enum class ButtonState {
    Pressed, Released
};

template <class T>
T clamp(const T& val, const T& low, const T& high) {
    return (val < low)? low : (val > high)? high : val;
}

geometry_msgs::Point32 clampPointToImageSize(const sensor_msgs::Image &img, const int x, const int y);

void cvMouseEventDispatcher(int event, int x, int y, int, void *data);

class SASourceWindow {
public:
    SASourceWindow();

    void run();

public: /* Callbacks */
    void cvMouseEvent(int event, int mouse_x, int mouse_y);

private: /* Methods */
    template <class T>
    T getPrivateParam(const std::string& name) const;


private: /* Member Variables */

    /// Name of the window used to show the safety area image.
    const std::string _window_name = "Define Safety Area";

    ros::NodeHandle _node;
    ros::NodeHandle _pnode;

    /// Publisher to the topic onto which we will push the safety area when it has been defined
    ros::Publisher _sa_publisher;

    /// Camera subscriber from which we pull the images displayed in the SA selection window
    SimpleCamera _camera;

    /// The shape of the safety area as it is currently defined.
    /// This is updated in the \c cvMouseEvent callback and contains the current set of selected points
    geometry_msgs::Polygon _safety_area;

    /// The state of the left mouse button.
    ButtonState _lbutton_state = ButtonState::Released;
};

/**
 * Get a parameter from the private namespace of a node.
 *
 * If the parameter is not set a message is printed with \c ROS_ERROR and an exception of type
 * #RosParamError is thrown.
 *
 * @tparam T The type of the value to query. This is also the type of the returned value.
 * @param The name of the parameter to query.
 * @return The value of the parameter.
 */
template <class T>
T SASourceWindow::getPrivateParam(const std::string& name) const {
    T value;
    if (!_pnode.getParam(name, value)) {
        ROS_ERROR("Unable to retrieve parameter <%s>.", _pnode.resolveName(name).c_str());
    }

    return value;
}